<?php

namespace App\WebusShop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class CustomerAdresses extends Model
{
   protected $table = 'customer_addresses';
    
   public $timestamps = false;

   static function customer_addresses(){
      return CustomerAdresses::with('country')->where('user_id', Auth::id())->get();
   }

   static function get_address($id){
      return CustomerAdresses::with('country')->find($id);
   }

   public function country(){
      return $this->hasOne('App\WebusModels\TaxonomyTerms', 'id', 'country_id')->where('taxonomy', 'countries')->select(['id', 'name']);
   }
}
