<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTexture extends Model
{
    protected $table = 'product_textures';
}
