<?php

namespace App\WebusModels;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\Http\Helpers\webus_help;

class TaxonomyTerms extends Model
{
    protected $table = 'taxonomy_terms';

    public function parent() {
        return $this->belongsTo(static::class, 'term_parent');
    }

    public function children() {
        return $this->hasMany(static::class, 'term_parent')->with('parent')->orderBy('term_order', 'asc');
    }

    public function count_children() {
        return $this->hasOne(static::class, 'term_parent')->selectRaw('term_parent, count(term_parent) as aggregate')->groupBy('term_parent');;
    }

    static function get_terms($name_taxonomy, $search, $parent=false, $childrens=false, $select=false){
        $query = TaxonomyTerms::where('taxonomy', $name_taxonomy);

        if($parent){
            $query = $query->where('term_parent', 0);
        }

        if($childrens && $childrens == 'count'){
            $query = $query->with('count_children');
        }elseif($childrens && $childrens == 'posts'){
            $query = $query->with('children');
        }


        if($select){
            $query = $query->select($select);
        }

        $query = $query->orderBy('term_order', 'asc')->get();
        return $query;

    }

    static function get_children_terms($name_taxonomy, $parent_id,$select=false){
        $query = DB::table('taxonomy_terms')->where('taxonomy', $name_taxonomy)->where('term_parent', $parent_id);
        if($select){
            $query = $query->select($select);
        }
        $query = $query->orderBy('term_order', 'asc')->get();
        return $query;
    }

    static function get_terms_for_selected($name_taxonomy){
        $query = TaxonomyTerms::where('taxonomy', $name_taxonomy)->where('term_parent',0)->with('children')->get();
        $query_array = array();
        $query_array[0] = 'None';
        if($query){
            foreach ($query as $item){
                $query_array[$item->id] = webus_help::translate_automat($item->name);
                if($item->children->count()){
                    foreach ($item->children as $child){
                        $query_array[$child->id] = ' -'. webus_help::translate_automat($child->name);

                    }
                }
            }
        }

        return $query_array;

    }

    static function get_post_terms($name_taxonomy, $post_id, $post_tpe, $for_array = false){
        $query = DB::table('taxonomy_relations')
            ->where('taxonomy', $name_taxonomy)
            ->where('post_id', $post_id)
            ->where('post_type', $post_tpe)
            ->get();


        if($for_array){
            $query_array = array();
            foreach ($query as $item){
                $query_array[$item->term_id] = $item->term_id;
            }
            $query = $query_array;
        }

        return $query;

    }

    static function get_post_terms_full($name_taxonomy, $post_id, $post_tpe){
        $query = DB::table('taxonomy_relations')
            ->join('taxonomy_terms', 'taxonomy_terms.id', '=', 'taxonomy_relations.term_id')
            ->where('taxonomy_relations.taxonomy', $name_taxonomy)
            ->where('taxonomy_relations.post_id', $post_id)
            ->where('taxonomy_relations.post_type', $post_tpe)
            ->groupBy('taxonomy_relations.post_id')
            ->get();

        return $query;
    }

    static function atach_terms_to_post($name_taxonomy, $terms, $post_id, $post_type){

        //remove all

        $remove_all = DB::table('taxonomy_relations')->where('post_id', $post_id)->where('taxonomy', $name_taxonomy)->where('post_type', $post_type)->delete();

        if($terms){
            if(is_array($terms)){
                foreach ($terms as $term){
                    $dashboard_item =  DB::table('taxonomy_relations')->insert([
                        'post_id' => $post_id,
                        'post_type' => $post_type,
                        'term_id' => $term,
                        'taxonomy' => $name_taxonomy,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }else{
                $dashboard_item =  DB::table('taxonomy_relations')->insert([
                    'post_id' => $post_id,
                    'post_type' => $post_type,
                    'term_id' => $terms,
                    'taxonomy' => $name_taxonomy,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }

        }

    }

    static function delete_terms($post_id, $post_type){
        $remove_all = DB::table('taxonomy_relations')->where('post_id', $post_id)->where('post_type', $post_type)->delete();

    }

    static function get_term_detail($term_id, $taxonomy){
        $query =  DB::table('taxonomy_terms')
            ->where('taxonomy',$taxonomy)
            ->where('id',$term_id)
            ->first();
        return $query;
    }
    static function get_term_detail_by_slug($slug, $taxonomy){
        $query =  DB::table('taxonomy_terms')
            ->where('taxonomy',$taxonomy)
            ->where('slug',$slug)
            ->first();
        return $query;
    }

    static function get_posts_by_taxonomy($terms_id, $post_type, $taxonomy, $limit = false, $full = false, $exclude = false){
        if($limit){$limit = $limit;}else{$limit = '-1';}
        $query = DB::table('taxonomy_relations')
            ->join($post_type, $post_type.'.id', '=', 'taxonomy_relations.post_id')
            ->whereIn('term_id', array_map('intval', explode(',',$terms_id)))
            ->where('taxonomy_relations.post_type', $post_type)
            ->where('taxonomy_relations.taxonomy', $taxonomy)
            ->groupBy('taxonomy_relations.post_id');

        if($exclude){
            $query = $query->whereNotIn($post_type.'.id', [$exclude]);
        }

        $query = $query->select($post_type.'.*')
            ->take($limit)
            ->get();

        $query_array = array();

        if($query){
            if($full){
                foreach ($query as $item) {
                    $query_array = $query;
                }
            }else{
                $query_array[0] = 'None';
                foreach ($query as $item) {
                    $query_array[$item->id] = $item->name;
                }
            }

        }

        return $query_array;
    }

    static function get_posts_ids_filter_by_terms($request, $table){
        //list of taxonomy, example: category, country
        //use this function: $get_its = TaxonomyTerms::get_posts_ids_filter_by_terms($request, 'news');
        $list_taxonomy = array_map('strval', explode(',', $request->list_taxonomy));
        foreach ($list_taxonomy as $tax){
            if(isset($request->$tax) && $request->$tax){
                $posts = DB::table($table);
                $posts->join('taxonomy_relations', 'taxonomy_relations.post_id', '=', $table.'.id');
                $query_tax[$tax] = array(
                    'total' => $posts->whereIn('taxonomy_relations.term_id', [$request->$tax])
                        ->whereIn('taxonomy_relations.taxonomy', [$tax])
                        ->where('taxonomy_relations.post_type', $table) //fixed
                        ->select($table.'.id')
                        ->groupBy('taxonomy_relations.post_id')
                        ->get()
                        ->toArray()
                );
            }else{
                $posts = DB::table($table);
                $query_tax[$tax] = array(
                    'total' => $posts->get()->toArray()
                );
            }
        }
        //Exclude which are not identical
        $ids = array();
        if(isset($query_tax)){
            foreach ($query_tax as $key => $item) {
                foreach ($item['total'] as $total) {
                    $ids[] = $total->id;
                }
            }
        }
        $find_dublicates = implode(',', array_unique( array_diff_assoc( $ids, array_unique( $ids ) ) ));
        $all_ids_from_taxonomy_query = $find_dublicates;
        //return result all ids from filter this terms taxonomies
        return $all_ids_from_taxonomy_query;

    }
}
