<?php

namespace App\WebusModels;

use Illuminate\Database\Eloquent\Model;

class MetaBoxes extends Model
{
    protected $table = 'meta_boxes';
    public $timestamps = false;
}
