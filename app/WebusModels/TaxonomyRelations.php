<?php

namespace App\WebusModels;

use Illuminate\Database\Eloquent\Model;

class TaxonomyRelations extends Model
{
    protected $table = 'taxonomy_relations';
}
