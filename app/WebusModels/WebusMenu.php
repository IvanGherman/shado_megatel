<?php

namespace App\WebusModels;

use Illuminate\Database\Eloquent\Model;


class WebusMenu extends Model
{
    protected $table = 'menu';
    protected $fillable = [
        'name',
        'slug',
        'create_at',
        'updated_at'
    ];

//    public function items()
//    {
//        return $this->hasMany('App\WebusMenuItems');
//    }
    
    public function parent_items()
    {
        return $this->hasMany('App\WebusModels\WebusMenuItems', 'menu_id')->whereNull('parent_id');
    }

}
