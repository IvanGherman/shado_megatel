<?php

namespace App\WebusModels;

use App\Http\Helpers\webus_help;
use Illuminate\Database\Eloquent\Model;
use DB;

class WordpressPosts extends Model
{
    protected $table = 'wp_posts';

    protected  $primaryKey = 'ID';

    public $timestamps = false;


//    protected $fillable = [
//        'ID',
//        'post_content',
//        'post_title',
//        'post_excerpt',
//        'post_name',
//        'post_type',
//    ];

    static function get_posts_parent($post_type){
        $query =  DB::table('wp_posts')->where('post_type', $post_type)->get();
        $posts = array();
        $posts[0] = 'None';
        if($query){
            foreach ($query as $post){
                $posts[$post->ID] = webus_help::translate_automat($post->post_title);
            }
        }
        return $posts;
    }

}
