<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class CustomModel extends Model
{
    public function getLocale()
    {
        return Config::get('app.locale');
    }

    public function translate($item)
    {
        return \App\Http\Helpers\webus_help::translate_manual($this->getLocale(), $item);
    }

    public function getShortcoded($item)
    {
        return \App\Http\Shortcodes\shortcodes::do_shortcode($item);
    }
}
