<?php

namespace App;

use App\Http\Helpers\webus_help;
use Illuminate\Database\Eloquent\Model;

class News extends CustomModel
{
    protected $table = 'news';

    public function getDate($locale)
    {
        $months = [
            '01' => '[:ro]Ianuarie[:ru]Января',
            '02' => '[:ro]Februarie[:ru]Февраля',
            '03' => '[:ro]Martie[:ru]Марта',
            '04' => '[:ro]Aprilie[:ru]Апреля',
            '05' => '[:ro]Mai[:ru]Мая',
            '06' => '[:ro]Iunie[:ru]Июня',
            '07' => '[:ro]Iulie[:ru]Июля',
            '08' => '[:ro]August[:ru]Августа',
            '09' => '[:ro]Septembrie[:ru]Сентября',
            '10' => '[:ro]Octombrie[:ru]Октября',
            '11' => '[:ro]Noiembrie[:ru]Ноября',
            '12' => '[:ro]Decembrie[:ru]Декабря',
        ];

        $date = new \DateTime($this->created_at);

        $d = $date->format('d');
        $m = $date->format('m');
        $y = $date->format('Y');

        return $d . ' ' . webus_help::translate_manual($locale, $months[$m]) . ' ' . $y;
    }

    public function getDescription()
    {
        return $this->getShortcoded($this->translate($this->body));
    }

    public function getTitle()
    {
        return $this->translate($this->title);
    }
}
