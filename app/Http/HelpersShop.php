<?php
namespace App\Http\HelpersShop;
use DB;
use App\Http\Helpers\webus_help;
use Cookie;
class webus_shop
{
    private static function get_currencies(){
        $default = config('app.default_currency', 'USD');
        $currencies =  array(
            $default => 1,
            'EUR' => webus_help::get_option('curs_euro'),
            'RUB' => webus_help::get_option('curs_rub')
        );
        return $currencies;
    }

    static private function get_current_currency(){
        $currencies = self::get_currencies();
        $get_currency = Cookie::get('currency');
        if($get_currency != null){
            if(array_key_exists($get_currency, $currencies)){
                $currency = $get_currency;
            }else{
                $currency = config('app.default_currency', 'USD');
            }
        }else{
            $currency = config('app.default_currency', 'USD');
        }
        return $currency;
    }

    static function calculate_price($price,$currency=false){
        $currencies = self::get_currencies();
        if($currency){
            $use_this_currency = $currency;
        }else{
            $use_this_currency = self::get_current_currency();
        }
        if(isset($currencies[$use_this_currency])){
            return number_format($currencies[$use_this_currency]*$price,2,'.','');
        }else{
            return number_format($currencies[config('app.default_currency', 'USD')]*$price,2,'.','');
        }
    }

    /***** Functions for public access *****/
    
    static function list_currencies(){
        return array_keys(self::get_currencies());
    }
    
    static function get_currency(){
        return self::get_current_currency();
    }

    static function get_payment_methods(){
        return array(
            'paypal' => 'PayPal',
            'paypal_card' => 'PayPal (Visa Masterkard)',
            'yandex' => 'Yandex Money',
        );
    }

    static function get_payment_methodsAdmin(){
        return array(
            'paypal' => 'PayPal',
            'paypal_card' => 'PayPal (Visa Masterkard)',
            'yandex' => 'Yandex Money',
        );
    }

    static function order_status($get=false){
        $all = array(
            0 => 'Cancel',
            1 => 'Completed',
            2 => 'Denied',
            3 => 'Expired',
            4 => 'Failed',
            5 => 'Pending',
            6 => 'Processed',
            7 => 'Refunded',
            8 => 'Reversed',
            9 => 'Voided',
        );
        
        if($get){
            return $all[$get];
        }else{
            return $all;
        }

    }

    static function get_countries(){
      $model = 'App\TaxonomyCountries';
      return $model::where('taxonomy', 'countries')->where('term_parent', 0)->select('id', 'name')->orderBy('term_order', 'asc')->get();
    }

    static function get_totals(){
        $cart_subtotal = Cart::subtotal();

        $totals = array();

        $totals['subtotal'] = array(
            'sort_order' => 1,
            'code' => 'subtotal',
            'text' => 'Subtotal',
            'value' => '$'.number_format(Cart::subtotal(),0)
        );

        $get_coupon_code = session('coupon');
        if($get_coupon_code){
            $get_coupon_information = Coupons::where('code', $get_coupon_code)->first();
            if($get_coupon_information){

                if($get_coupon_information->type == '$'){
                    $cart_subtotal-=$get_coupon_information->summ;
                }

                if($get_coupon_information->type == '%'){
                    $calculate_procent = $cart_subtotal * $get_coupon_information->summ/100;
                    $cart_subtotal-=$calculate_procent;
                }

                $totals['coupon'] = array(
                    'sort_order' => 2,
                    'code' => 'coupon',
                    'text' => $get_coupon_information->name,
                    'value' => '-'.number_format($get_coupon_information->summ,0).$get_coupon_information->type
                );


            }
        }

        $totals['total'] = array(
            'sort_order' => 3,
            'code' => 'total',
            'text' => 'Total',
            'value' => '$'.number_format($cart_subtotal,0)
        );

        self::array_sort_by_column($totals, 'sort_order');

        return $totals;

    }


    private static function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }
        array_multisort($sort_col, $dir, $arr);
    }
}