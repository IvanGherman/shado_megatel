<?php

namespace App\Http\Services\ImportMapper;

use App\Category;

class CategoryMapper extends ImportMapper
{
    const SHEET = 'Лист1';

    protected $imageFolder = self::PREFIX_IMAGE_FOLDER.'/categories/';

    protected $model = 'App\Category';

    public $map = [
        'A' => [
            'title' => 'id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'C' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
        'D' => [
            'title' => 'description',
            'validate' => false,
            'lang' => 'ro',
        ],
        'E' => [
            'title' => 'description',
            'validate' => false,
            'lang' => 'ru',
        ],
        'F' => [
            'title' => 'position',
            'validate' => false,
        ],
        'G' => [
            'title' => 'image',
            'validate' => false,
            'is_image' => true,
        ],
        'H' => [
            'title' => 'post_status',
            'validate' => self::REQUIRED,
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }
}