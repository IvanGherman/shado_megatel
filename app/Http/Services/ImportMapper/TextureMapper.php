<?php

namespace App\Http\Services\ImportMapper;

use App\Texture;

class TextureMapper extends ImportMapper
{
    const SHEET = 'Тип текстуры';
    protected $model = 'App\Texture';
    protected $imageFolder = self::PREFIX_IMAGE_FOLDER.'/textures/';

    public $map = [
        'A' => [
            'title' => 'id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'C' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
        'D' => [
            'title' => 'icon',
            'validate' => false,
            'is_image' => true,
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }
}