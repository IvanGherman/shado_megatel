<?php

namespace App\Http\Services\ImportMapper;

use App\Color;

class ColorMapper extends ImportMapper
{
    const SHEET = 'Цвет';
    protected $model = 'App\Color';

    public $map = [
        'A' => [
            'title' => 'id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'C' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
        'D' => [
            'title' => 'color',
            'validate' => self::REQUIRED,
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }
}