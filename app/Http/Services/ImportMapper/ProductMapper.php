<?php

namespace App\Http\Services\ImportMapper;

use App\Attribute;
use App\AttributeValue;
use App\Product;
use App\ProductAttributeValue;

class ProductMapper extends ImportMapper
{
    const SHEET = 'Лист1';
    const EAV_VALUES_SEPARATOR = '_';
    const EAV_ATTRIBUTE_VALUE_SEPARATOR = '=';

    protected $imageFolder = self::PREFIX_IMAGE_FOLDER.'/products/';

    protected $model = 'App\Product';

    protected $values = null;

    public $map = [
        'A' => [
            'title' => 'id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'model',
            'validate' => self::REQUIRED,
        ],
        'C' => [
            'title' => 'code',
            'validate' => self::REQUIRED,
        ],
        'D' => [
            'title' => 'post_status',
            'validate' => self::REQUIRED,
        ],
        'E' => [
            'title' => 'position',
            'validate' => self::REQUIRED,
        ],
        'F' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'G' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
        'H' => [
            'title' => 'images',
            'validate' => false,
            'is_image' => true,
            'multiple' => true,
        ],
        'I' => [
            'title' => 'model_image',
            'validate' => false,
            'is_image' => true,
        ],
        'J' => [
            'title' => 'description',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'K' => [
            'title' => 'description',
            'validate' => self::REQUIRED,
            'lang' => 'ru'
        ],
        'L' => [
            'title' => 'categories',
            'validate' => [
                self::REQUIRED,
                self::WRONG_SEPARATOR,
            ],
            'collection' => [
                'related' => 'App\Category',
                'separator' => ',',
                'multiple' => true,
            ],
        ],
        'M' => [
            'title' => 'colors',
            'validate' => [
                self::REQUIRED,
                self::WRONG_SEPARATOR,
            ],
            'collection' => [
                'related' => 'App\Color',
                'separator' => ',',
                'multiple' => true,
            ],
        ],
        'N' => [
            'title' => 'textures',
            'validate' => [
                self::WRONG_SEPARATOR,
            ],
            'collection' => [
                'related' => 'App\Texture',
                'separator' => ',',
                'multiple' => true,
            ],
        ],
        'O' => [
            'title' => 'transparency_id',
            'validate' => false,
        ],
        'P' => [
            'title' => 'countries',
            'validate' => [
                self::WRONG_SEPARATOR,
            ],
            'collection' => [
                'related' => 'App\Country',
                'separator' => ',',
                'multiple' => true,
            ],
        ],
        'Q' => [
            'title' => 'tree_type_id',
            'validate' => false,
        ],
        'R' => [
            'title' => 'production_duration',
            'validate' => false,
            'lang' => 'ro',
        ],
        'S' => [
            'title' => 'production_duration',
            'validate' => false,
            'lang' => 'ru',
        ],
        'T' => [
            'title' => 'attributes',
            'validate' => false,
            'collection' => [
                'separator' => ',',
                'multiple' => true,
                'pattern' => 'EAV',
            ],
        ],
        'U' => [
            'title' => 'promotion',
            'validate' => false,
        ],
        'V' => [
            'title' => 'common_p',
            'validate' => false,
            'multiple' => true,
        ],
        'W' => [
            'title' => 'popular_p',
            'validate' => false,
            'multiple' => true,
        ],
        'X' => [
            'title' => 'price',
            'validate' => false,
        ],
        'Y' => [
            'title' => 'promo_price',
            'validate' => false,
        ],
        'Z' => [
            'title' => 'show_in_catalog',
            'validate' => false,
        ],
        'AA' => [
            'title' => 'order_in_catalog',
            'validate' => false,
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }

    public function getValues()
    {
        if (!$this->values) {
            $this->values = AttributeValue::all();
        }

        return $this->values;
    }

    public function saveEAVCollection($model, $key, $data, $delimiter, $row)
    {
        $values = $this->getValues();

        $items = explode($delimiter, $data);
        $attributeValues = [];

        foreach ($items as $item) {
            $parts = explode(self::EAV_ATTRIBUTE_VALUE_SEPARATOR, $item);
            $attribute = $parts[0];
            $value = $parts[1];

            if (empty($attributeValues[$parts[0]])) {
                $attributeValues[$attribute] = [];
            }

            if (strpos($value, self::EAV_VALUES_SEPARATOR)) {
                $microParts = explode(self::EAV_VALUES_SEPARATOR, $parts[1]);
                foreach ($microParts as $microItem) {
                    $attributeValues[$attribute][] = $microItem;
                }
            } else {
                $attributeValues[$attribute][] = $value;
            }
        }

        foreach ($attributeValues as $attributeId => $item) {
            foreach ($item as $itemKey) {
                foreach ($values as $value) {
                    if ($value->attribute_id == $attributeId && (int) $itemKey == $value->key) {
                        $attrValue = new ProductAttributeValue();
                        $attrValue->attribute_id = $attributeId;
                        $attrValue->product_id = $model->id;
                        $attrValue->attribute_value_id = $value->id;
                        $attrValue->save();
                    }
                }
            }
        }
    }
}