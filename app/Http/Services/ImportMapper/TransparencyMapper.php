<?php

namespace App\Http\Services\ImportMapper;

use App\Transparency;

class TransparencyMapper extends ImportMapper
{
    const SHEET = 'Светопроницаемость';

    protected $model = 'App\Transparency';
    protected $imageFolder = self::PREFIX_IMAGE_FOLDER.'/transparencies/';

    public $map = [
        'A' => [
            'title' => 'id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'C' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }
}