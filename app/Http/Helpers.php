<?php
namespace App\Http\Helpers;
use App\Translations;
use App\Variables;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App;
use Illuminate\Support\Facades\Lang;
use LaravelLocalization;
class webus_help
{
    public static $variables = null;
    public static $translations = null;

    static function get_variables(){
        if (self::$variables) {
            return new Response(self::$variables);
        }

        $variables = Variables::all();

        $variableMap = [];
        foreach ($variables as $variable) {
            $variableMap[$variable->name] = $variable->value;
        }

        self::$variables = $variableMap;

        return new Response($variableMap);
    }

    static function get_translations(){
        if (self::$translations) {
            return new Response(self::$translations);
        }
        $translations = Translations::all();

        $translationMap = [];
        foreach ($translations as $variable) {
            $translationMap[$variable->key] = $variable->value;
        }

        self::$translations = $translationMap;

        return new Response($translationMap);
    }

    static function menu($name, $template='default', $translatable = false){
        $model_name = 'App\WebusModels\WebusMenu';

        $menu =  $model_name::where('slug', '=', $name)
            ->with('parent_items.children')
            ->first();

        // Check for Menu Existence
        if (!isset($menu)) {
            return false;
        }

        $locale = app()->getLocale();

        return new \Illuminate\Support\HtmlString(
            \Illuminate\Support\Facades\View::make('webus/menu_display.'.$template, [
                'items' => $menu->parent_items,
                'template' => $template,
                'locale' => $locale,
                'translatable' => $translatable
            ])->render()
        );
    }

    static function custom_menu($name, $template='default', $translatable = false){
        $model_name = 'App\WebusModels\WebusMenu';

        $menu =  $model_name::where('slug', '=', $name)
            ->with('parent_items.children')
            ->first();

        // Check for Menu Existence
        if (!isset($menu)) {
            return false;
        }

        $locale = app()->getLocale();

        return new \Illuminate\Support\HtmlString(
            \Illuminate\Support\Facades\View::make('front/partial/top_menu', [
                'items' => $menu->parent_items,
                'template' => $template,
                'locale' => $locale,
                'translatable' => $translatable
            ])->render()
        );
    }

    static function footer_menu($name, $template='default', $translatable = false){
        $model_name = 'App\WebusModels\WebusMenu';

        $menu =  $model_name::where('slug', '=', $name)
            ->with('parent_items.children')
            ->first();

        // Check for Menu Existence
        if (!isset($menu)) {
            return false;
        }

        $locale = app()->getLocale();

        return new \Illuminate\Support\HtmlString(
            \Illuminate\Support\Facades\View::make('front/partial/footer_menu', [
                'items' => $menu->parent_items,
                'template' => $template,
                'locale' => $locale,
                'menu' => $menu,
                'translatable' => $translatable
            ])->render()
        );
    }

    public static function translate_manual($lang,$text){
        $locale = $lang;

        if(isset(self::webus_translatable_split($text)[$locale])){
            $get_json = self::webus_translatable_split($text)[$locale];

            if (!empty($get_json)) {
                return $get_json;
            } else {
                $locale = Lang::getFallback();
                $get_json = self::webus_translatable_split($text)[$locale];
                if ($get_json) {
                    return $get_json;
                }
                return false;
            }
        }
        return false;

    }

    static function translate_automat($text){
        $locale = app()->getLocale();

        if(isset(self::webus_translatable_split($text)[$locale])){
            $get_json = self::webus_translatable_split($text)[$locale];
            if(!empty($get_json)){
                return $get_json;
            }else{
                return false;
            }
        }

        return false;

    }


    static private function webus_translatable_get_language_blocks($text) {
        $split_regex = "#(<!--:[a-z]{2}-->|<!--:-->|\[:[a-z]{2}\]|\[:\]|\{:[a-z]{2}\}|\{:\})#ism";
        return preg_split($split_regex, $text, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
    }


    static private function webus_translatable_split($text) {
        $blocks = self::webus_translatable_get_language_blocks($text);
        return self::webus_translatable_split_blocks($blocks);
    }


    static private function webus_translatable_split_blocks($blocks, &$found = array()) {

        $result = array();
        $all_language = LaravelLocalization::getLocalesOrder();
        foreach($all_language as $language => $properties){
            $result[$language] = '';
        }

        $current_language = false;
        foreach($blocks as $block) {
            // detect c-tags
            if(preg_match("#^<!--:([a-z]{2})-->$#ism", $block, $matches)) {
                $current_language = $matches[1];
                continue;
                // detect b-tags
            }elseif(preg_match("#^\[:([a-z]{2})\]$#ism", $block, $matches)) {
                $current_language = $matches[1];
                continue;
                // detect s-tags @since 3.3.6 swirly bracket encoding added
            }elseif(preg_match("#^\{:([a-z]{2})\}$#ism", $block, $matches)) {
                $current_language = $matches[1];
                continue;
            }
            switch($block){
                case '[:]':
                case '{:}':
                case '<!--:-->':
                    $current_language = false;
                    break;
                default:
                    // correctly categorize text block
                    if($current_language){
                        if(!isset($result[$current_language])) $result[$current_language]='';
                        $result[$current_language] .= $block;
                        $found[$current_language] = true;
                        $current_language = false;
                    }else{
                        foreach($all_language as $language => $properties){
                            $result[$language] .= $block;
                        }
                    }
                    break;
            }
        }
        //it gets trimmed later in webus_translatable_use() anyway, better to do it here
        foreach($result as $lang => $text){
            $result[$lang]=trim($text);
        }
        return $result;
    }


    //META BOX 

    static function get_metabox($post_id, $post_type, $meta_key, $translate = false){
        $query = DB::table('meta_boxes')->where('post_id', $post_id)->where('post_type', $post_type)->where('meta_key',  $meta_key)->select('meta_value')->first();

        if($query){
            if($translate){
                $locale = app()->getLocale();

                $get_json =  self::webus_translatable_split(json_decode($query->meta_value))[$locale];
                if(!empty($get_json)){
                    return $get_json;
                }else{
                    return false;
                }
            }else{
                return json_decode($query->meta_value);
            }

        }else{
            return '';
        }
    }

    static function delete_metabox($post_id, $post_type){
        $delete_all_meta = DB::table('meta_boxes')->where('post_id', $post_id)->where('post_type', $post_type)->delete();
    }

    static function insert_metabox($post_id, $post_type, $meta_key, $meta_value){

        $insert_metabox = DB::table('meta_boxes')->insert([
            'post_id' => $post_id,
            'post_type' => $post_type,
            'meta_key' => $meta_key,
            'meta_value' => json_encode($meta_value, JSON_UNESCAPED_UNICODE)
        ]);
    }

    //Generale ********************************************************************************************

    static function active_class_path($paths, $classes = null)
    {
        foreach ((array) $paths as $path) {
            if (request()->is($path)) {
                return ($classes ? $classes . ' ' : '') . 'active';
            }
        }
        //return $classes ? $classes  : '';
    }

    static function get_post_detail($post_type, $post_id, $fields){
        $query = DB::table($post_type)
            ->select($fields)
            ->where('id', $post_id)
            ->first();

        if($query){
            return $query;
        }else{
            return false;
        }
    }
    
    static function permalink($post_type = false, $slug, $translated = false)
    {
        $locale = '';
        if($translated){
            $locale = LaravelLocalization::getCurrentLocale().'/';
            $default_locale = LaravelLocalization::getDefaultLocale();
            if($locale == $default_locale){
                $locale = '';
            }
        }

        if($post_type){
            $url = url($locale.$post_type.'/'.$slug);
        }else{
            $url = url($locale.$slug);
        }

        return $url;
    }

    static function permalink_by_id($post_type=false, $id, $translated = false){

        $query = DB::table($post_type)->where('id', $id)->first();
        $locale = '';
        if($translated){
            $locale = LaravelLocalization::getCurrentLocale();
        }

        if($query){
            $url = url($locale.'/'.$post_type.'/'.$query->slug);
            if($post_type == 'pages'){
                $url = url($locale.'/'.$query->slug);
            }
        }else{
            $url = 'false';
        }

        return $url;
    }

    static function get_month_name($month)
    {
        $months = array(
            1   =>  webus_help::translate_automat('[:ro]ianuarie[:ru]января[:en]january'),
            2   =>  webus_help::translate_automat('[:ro]februarie[:ru]февраля[:en]february'),
            3   =>  webus_help::translate_automat('[:ro]martie[:ru]марта[:en]march'),
            4   =>  webus_help::translate_automat('[:ro]aprilie[:ru]апреля[:en]april'),
            5   =>  webus_help::translate_automat('[:ro]mai[:ru]мая[:en]may'),
            6   =>  webus_help::translate_automat('[:ro]iunie[:ru]июня[:en]juny'),
            7   =>  webus_help::translate_automat('[:ro]iulie[:ru]июля[:en]july'),
            8   =>  webus_help::translate_automat('[:ro]august[:ru]августа[:en]august'),
            9   =>  webus_help::translate_automat('[:ro]septembrie[:ru]сентября[:en]september'),
            10  =>  webus_help::translate_automat('[:ro]octombrie[:ru]октября[:en]october'),
            11  =>  webus_help::translate_automat('[:ro]noiembrie[:ru]ноября[:en]november'),
            12  =>  webus_help::translate_automat('[:ro]decembrie[:ru]декабря[:en]december'),
        );

        return $months[$month];
    }


    static function get_post_date($date){
        $get_day = date( "d", strtotime($date) ) ;
        $get_month = self::get_month_name(date( "n", strtotime($date))) ;
        $get_year = date( "Y", strtotime($date) ) ;

        return $get_day.' '.$get_month.' '.$get_year;
    }

    static function get_post_terms($name_taxonomy, $post_id, $post_tpe){
        $query = DB::table('taxonomy_relations')
            ->join('taxonomy_terms', 'taxonomy_terms.id', '=', 'taxonomy_relations.term_id')
            ->where('taxonomy_relations.taxonomy', $name_taxonomy)
            ->where('taxonomy_relations.post_id', $post_id)
            ->where('taxonomy_relations.post_type', $post_tpe)
            ->get();

        return $query;
    }

    static function get_terms($taxonomy){
        $query = DB::table('taxonomy_terms')->where('taxonomy', $taxonomy)->orderBy('term_order', 'asc')->get();
        return $query;
    }

    static function get_option($key, $translatable=false){
        $search_option = DB::table('settings')->where('key', $key)->select('value')->first();
        if($search_option != null){
            if($translatable){
                return webus_help::translate_automat($search_option->value);
            }else{
                return $search_option->value;
            }
        }else{
            return '';
        }
    }

    static function get_module($module_id){
        $path_modules = 'App\Http\Controllers\Modules\\';
        $get_info = DB::table('modules')->where('id', $module_id)->first();
        if($get_info != null){
            $code = $get_info->code;
            $call_module = $path_modules.$code;
            return $call_module::front($module_id);
        }else{
            return '';
        }
    }

    static function get_categories(){
        $call = 'App\Http\Controllers\Front\CategoriesController';
        return $call::front();
    }

    static function get_modules($modules){
        if($modules != null){
            foreach ($modules as $module){
                echo webus_help::get_module($module->modul);
            }
        }
    }

    static function get_code_module($module_id){
        $get_info = DB::table('modules')->where('id', $module_id)->select('code')->first();
        if($get_info){
            return $get_info->code;
        }
        return false;
    }

    //WORDPRESS INTEGRATION ********************************************************************************************

    static function get_post_content($id){
        $query = DB::table('wp_posts')
            ->where('ID', $id)
            ->where('post_status', 'publish')
            ->first();

        $data = '';

        if($query){
            $data = $query;
        }

        return $data;
    }


    static function get_post_content_by_slug($slug){
        $query = DB::table('wp_posts')
            ->where('post_name', $slug)
            ->where('post_status', 'publish')
            ->first();

        $data = '';

        if($query){
            $data = $query;
        }

        return $data;
    }


    static function get_permalink($id){

        $query = DB::table('wp_posts')
            ->where('ID', $id)
            ->where('post_status', 'publish')
            ->first();

        $data = 'Post not Found';

        if($query){
            $current_slug =  $query->post_name;
            //get slug of parent page if exist
            $query_slug_parent_of_current_post = DB::table('wp_posts')
                ->where('ID', $query->post_parent)
                ->where('post_status', 'publish')
                ->first();

            $post_parent_slug = '';
            if($query_slug_parent_of_current_post){
                $post_parent_slug = $query_slug_parent_of_current_post->post_name.'/';
            }

            $data = url($post_parent_slug.$current_slug);

        }


        return $data;
    }

    static function get_thumb_by_id($id, $replace = false, $size=false){
        $query = DB::table('wp_postmeta')
            ->select(DB::raw('wp_postmeta.meta_value as THUMB_ID'), 'wp_posts.guid')
            ->join('wp_posts', 'wp_postmeta.meta_value', '=', 'wp_posts.ID')
            ->where('wp_postmeta.post_id', $id)
            ->where('wp_postmeta.meta_key', '_thumbnail_id')
            ->first();
        if($query){

            if($replace){
                //return rawurlencode(str_replace($replace,'', $query->guid));
                return str_replace($replace,'', $query->guid);
            }else{
                //return rawurlencode($query->guid);
                return $query->guid;
            }

        }else{
            return false;
        }

    }

    static function get_thumb_size($url, $size=null){
        if(!$size){
            $size = '150x150';
        }
        //$image = public_path(urldecode($url));
        $image = public_path($url);
        $get_extension = pathinfo($image)['extension'];

        $format = str_replace('.'.$get_extension, '-'.$size.'.'.$get_extension, $image);

        $publicPath = realpath(public_path());
        $relativePart = str_replace($publicPath, '', $format);
        //$new_absolute_url = rawurlencode("/" . trim($relativePart, "/\\"));
        $new_absolute_url =  "/" . trim($relativePart, "/\\");

        //if thumb exist return thumb else return full absolute url
        //$verify = public_path(urldecode($new_absolute_url));
        $verify = public_path($new_absolute_url);
        if(file_exists($verify)){
            return $new_absolute_url;
        }else{
            //return $url;
            return $url;
        }

    }

    static function get_gallery_images($gallery){
        $query = DB::table('wp_posts')
            ->whereIn('ID', array_map('intval', explode(",", $gallery)))
            ->select('wp_posts.guid', 'wp_posts.ID', 'wp_posts.post_mime_type', 'wp_posts.post_name')
            ->get();
        return $query;
    }

    static function atach_thumbnail_to_post($id, $path){

        $add_thumb_post = DB::table('wp_posts')->insertGetId([
            'guid' => $path,
            'post_author' => 1,
            'post_date' => date('Y-m-d'),
            'post_date_gmt' => date('Y-m-d'),
            'post_content' => 'media'. $id,
            'post_title' => 'media'. $id,
            'post_excerpt' => 'media'. $id,
            'post_status' => '',
            'comment_status' => '',
            'ping_status' => '',
            'post_password' => '',
            'post_name' => '',
            'to_ping' => '',
            'pinged' => '',
            'post_modified' => date('Y-m-d'),
            'post_modified_gmt' => date('Y-m-d'),
            'post_content_filtered' => '',
            'post_parent' => '0',
            'post_type' => 'attachment',
            'post_mime_type' => '',
            'comment_count' => '0',
        ]);


        $add_meta = DB::table('wp_postmeta')->insertGetId([
            'post_id' => $id,
            'meta_key' => '_thumbnail_id',
            'meta_value' => $add_thumb_post
        ]);

    }


    static function remove_post_thumbnail($id){

        $query = DB::table('wp_postmeta')
            ->select(DB::raw('wp_postmeta.meta_value as THUMB_ID'), 'wp_posts.guid')
            ->join('wp_posts', 'wp_postmeta.meta_value', '=', 'wp_posts.ID')
            ->where('wp_postmeta.post_id', $id)
            ->where('wp_postmeta.meta_key', '_thumbnail_id')
            ->first();

        if($query){
            $remove_post_thumbnail = DB::table('wp_posts')
                ->where('ID', $query->THUMB_ID)
                ->delete();
        }


        $remove_postmeta = DB::table('wp_postmeta')
            ->where('post_id', $id)
            ->where('meta_key', '_thumbnail_id')
            ->delete();


    }

    static function get_template_wordpress($post_id){
        $query = DB::table('wp_postmeta')
            ->where('meta_key', '_wp_page_template')
            ->where('post_id',$post_id)
            ->first();
        $template = false;
        if($query){
            $template = str_replace('.php', '', $query->meta_value);
        }

        return $template;
    }

    static function atach_template_wordpress($post_id,$value){

        $query = DB::table('wp_postmeta')
            ->where('meta_key', '_wp_page_template')
            ->where('post_id',$post_id)
            ->delete();

        $atach_template = DB::table('wp_postmeta')->insert([
            'post_id' => $post_id,
            'meta_key' => '_wp_page_template',
            'meta_value' => $value
        ]);

    }

}
?>
