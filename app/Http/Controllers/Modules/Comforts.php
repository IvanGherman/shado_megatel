<?php
namespace App\Http\Controllers\Modules;

use App\Comfort;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusModulesController;
use DB;
class Comforts extends WebusModulesController
{

    public function __construct()
    {

        $this->title = 'Comforts';
        $this->module_filename = 'Comforts';

        $this->form_fields = array( //in form template

            //Required field (no translated)
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            'button_text' => [
                'title' => 'Button text',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            'button_url' => [
                'title' => 'Button url',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            //your fields here

            'status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled'
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]

        );

    }

    static function front($module_id){
        $get = DB::table('modules')->where('id', $module_id)->where('module_status', 1)->first();
        if($get != null){
            $comforts = Comfort::where('post_status', 1)->get();
            $module = json_decode($get->setting);
            return view('front.modules.Comforts', [
                'module' => $module,
                'comforts' => $comforts,
            ]);
        }
    }

}


