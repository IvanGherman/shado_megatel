<?php

namespace App\Http\Controllers\Admin;

use App\Http\Services\ImportMapper\CategoryMapper;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusAdminController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CategoriesController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\Category'; //model
        $this->slug = 'categories'; //id controller
        $this->title = 'Categories'; //title controller
        $this->icon = ''; //icon page
        $this->active_modules = false;
        $this->template_post = 'positions'; //template post type (default or positions)
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = [
            'name' => 'translatable',
            'position' => 'translatable',
            'image' => 'image_src',
        ]; //in list template

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'description' => [
                'title' => 'Description',
                'html_type' => 'body',
                'validate' => false,
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'installation' => [
                'title' => 'Measurement and installation',
                'html_type' => 'body',
                'validate' => false,
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'position' => [
                'title' => 'Position',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'image' => [
                'title' => 'Image',
                'html_type' => 'image_input',
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],


        );

        //meta_boxes
        $this->meta_box = false;
    }

    public function import(Request $request)
    {
        $icon_page = '';
        if (isset($this->icon)){
            $icon_page = $this->icon;
        }

        $data = [
            'title' => 'Import category',
            'url_create' => '',
            'form_action' => route($this->slug.'.import_do'),
            'icon' => $icon_page
        ];

        return view('admin.categories.import', ['data' => $data]);
    }

    public function importDo(Request $request)
    {
        $file = $request->file('categories');
        $clear = $request->get('clear') ? true : false;

        try {
            if (!$file) {
                throw new \Exception('File not found', 404);
            }

            if ($clear) {
                DB::statement('DELETE FROM categories');
            }

            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
            $reader->setReadDataOnly(true);
            $reader->setLoadSheetsOnly('Лист1');

            $categoryMapper = new CategoryMapper();
            $spreadsheet = $reader->load($file);
            $worksheetIterator = $spreadsheet->getWorksheetIterator();
            $currentWorksheet = $worksheetIterator->current();
            $counter = 0;

            foreach ($currentWorksheet->getRowIterator(2) as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(FALSE);

                foreach ($cellIterator as $key => $cell) {
                    $cellValue = $cell->getValue();
                    $categoryMapper->store($counter, $key, $cellValue);
                }
                $counter++;
            }

            $categoryMapper->skipEmptyRows();
            $categoryMapper->saveModel();

            $errors = array_merge($categoryMapper->getErrorsDetailed(), $categoryMapper->getDBErrorsDetailed());

            if (!empty($errors)) {
                $this->handleDetailedErrors($errors);
            }

            Session::flash('flash_message', sprintf(
                'Import successfully done. %s categories saved, %s skipped',
                $categoryMapper->getSaved(),
                count($categoryMapper->getErrors()))
            );
        } catch (\Exception $e) {
            Session::flash('flash_message',$e->getMessage());
        }

        return redirect(route($this->slug.'.import', []));
    }

    public function handleDetailedErrors(array $errors)
    {
        Session::flash('flash_message_errors', $errors);
    }
}
