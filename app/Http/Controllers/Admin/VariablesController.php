<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Webus\WebusAdminController;
class VariablesController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\Variables'; //model
        $this->slug = 'variables'; //id controller
        $this->title = 'Variables'; //title controller
        $this->icon = ''; //icon page
        $this->template_post = 'positions'; //template post type (default or positions)
        $this->active_modules = false; //enable modules controll on this page

//      $this->model_taxonomy = 'App\WebusModels\TaxonomyTerms'; //use taxonomy table
//      $taxonomy = $this->model_taxonomy;

        $this->per_page = '25';
        $this->search = 'name'; //field for search
        $this->orderby = 'created_at,desc';
        //Identical to migrated file
        $this->list_field = [
            'name' => true,
            'key' => true,
            'value' => true,
        ]; //in list template

        $this->form_fields = array( //in form template
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'

            ],

            'value' => [
                'title' => 'Value',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'content'
            ],

        );

        //meta_boxes
        $this->meta_box = false;
    }

    public function create()
    {
        echo 'Create action is not available';
        return;
    }

    public function delete($id)
    {
        echo 'Delete action is not available';
        return;
    }
}
