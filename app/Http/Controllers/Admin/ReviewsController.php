<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusAdminController;

class ReviewsController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\Review'; //model
        $this->slug = 'reviews'; //id controller
        $this->title = 'Reviews'; //title controller
        $this->icon = ''; //icon page
        $this->active_modules = false;
        $this->template_post = 'positions'; //template post type (default or positions)
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = [
            'name' => 'translatable',
        ]; //in list template

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'person_name' => [
                'title' => 'Person name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'role' => [
                'title' => 'Role',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'sidebar'
            ],

            'image' => [
                'title' => 'Image',
                'html_type' => 'image_input',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'short_description' => [
                'title' => 'Short description',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'description' => [
                'title' => 'Description',
                'html_type' => 'body',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],


        );

        //meta_boxes
        $this->meta_box = false;
    }
}
