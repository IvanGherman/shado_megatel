<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Webus\WebusAdminController;
class PagesController extends WebusAdminController
{
    public function __construct()
    {
        /******Example Construct for Your Controller (This example works for the menu controller., do not delete)******/
        $this->model_name = 'App\Pages'; //model
        $this->slug = 'pages'; //id controller
        $this->title = 'Pages'; //title controller
        $this->icon = ''; //icon page
        $this->template_post = 'positions'; //template post type (default or positions)
        $this->active_modules = true; //enable modules controll on this page

//        $this->model_taxonomy = 'App\TaxonomyTerms'; //use taxonomy table
//        $taxonomy = $this->model_taxonomy;

        $this->per_page = '25';
        $this->search = 'name'; //field for search
        $this->orderby = 'created_at,desc';
        $templates = array('default' => 'Default');
        foreach (scandir(resource_path('views/front/templates')) as $file)
        {
            if($file != '.' && $file != '..'){ $name_file = str_replace('.blade', '', pathinfo($file)['filename']); if($name_file != 'default') {$templates[$name_file] = studly_case($name_file);}}
        }
        //Identical to migrated file
        $this->list_field = ['name'=>true, 'slug' =>true, 'post_status' => 'status']; //in list template

        $this->form_fields = array( //in form template
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'

            ],

            'body' => [
                'title' => 'Body',
                'html_type' => 'body',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'

            ],

            'meta_title' => [
                'title' => 'Meta title',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'

            ],

            'meta_description' => [
                'title' => 'Meta description',
                'html_type' => 'textarea',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'

            ],

            'meta_keywords' => [
                'title' => 'Meta keywords',
                'html_type' => 'textarea',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'

            ],
            
            'slug' => [
                'title' => 'URL',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'template' => [
                'title' => 'Template',
                'html_type' => 'select',
                'options' => $templates,
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'background' => [
                'title' => 'Background image',
                'html_type' => 'image_input',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],


        );


        //meta_boxes
        $this->meta_box = false;
//        $this->meta_box = array(
//
//            'accent_intro_text' => [
//                'title' => 'Intro text',
//                'html_type' => 'text',
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => true,
//                'show_on' => array(1,'contacts'), //id post or template
//                'metabox' => true,
//            ],
//
//            'accent_intro_text_second' => [
//                'title' => 'Intro text',
//                'html_type' => 'text',
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => false,
//                'metabox' => true,
//            ],
//
//            'select_meta' => [
//                'title' => 'Select meta',
//                'html_type' => 'select',
//                'options' => array(
//                    '1' => 'John',
//                    '2' => 'Marius',
//                ),
//                'multiple' => false,
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => false,
//                'metabox' => true,
//            ],
//
//            'select_meta_multiple' => [
//                'title' => 'Select meta multiple',
//                'html_type' => 'select',
//                'options' => array(
//                    '1' => 'John',
//                    '2' => 'Marius',
//                ),
//                'default_value_not_by_keys' => true,
//                'multiple' => true,
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => false,
//                'metabox' => true,
//            ]
//
//
//        );

    }
}
