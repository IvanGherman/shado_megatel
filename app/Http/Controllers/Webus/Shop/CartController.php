<?php

namespace App\Http\Controllers\Webus\Shop;

use App\Http\Helpers\webus_help;
use App\WebusShop\CustomerAdresses;
use App\WebusShop\Orders;
use App\WebusShop\OrdersProducts;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Products;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrdersMail;
use Redirect;
use App\Http\HelpersShop\webus_shop;
class CartController extends Controller
{

    /**** Cart *****/

    private function get_cart(){
        $products = array();
        $cart = Cart::content();

        foreach ($cart as $product){
            $products[] = array(
                'key' => $product->rowId,
                'id' => $product->id,
                'quantity' => $product->qty,
                'name' => $product->name,
                'image' => Products::image($product->id),
                'price' => webus_shop::calculate_price($product->price),
                'price_total' => webus_shop::calculate_price($product->price*$product->qty)
            );
        }

        return $products;
    }

    public function index(){
        return view('front.shop.cart', [
            'title' => trans('cart.text_heading'),
            'curren_currency' => webus_shop::get_currency(),
            'total' => webus_shop::calculate_price(Cart::total()),
            'totals' =>  webus_shop::get_totals(),
            'products' => self::get_cart()
        ]);
    }
    
    public function add(Request $request){
        $product_quantity = 1;
        $product_id = $request->product_id;
        if(isset($request->product_quantity)){
            $product_quantity = $request->product_quantity;
        }
        if(!empty($product_id)){
            $product_price = Products::price($product_id);
            $product_name = Products::name($product_id);
            if($product_price != 0 && !empty($product_name)){
                Cart::add($product_id, $product_name, $product_quantity, number_format($product_price, 2, '.', ''));
            }
        }
        if($request->ajax()){
            return response()->json([
                'text' => 'Succes',
                'count' => Cart::count()
            ]);
        }else{
           return redirect('/cart');
        }
    }

    public function update(Request $request){
        $product_total_price = array();
        if(isset($request->product_key) && isset($request->product_quantity)){
            $get_product = Cart::get($request->product_key);
            Cart::update($request->product_key, $request->product_quantity);
            if (Cart::count() == 0){
                $response = array(
                    'code' => 'empty-cart',
                    'text_empty_cart' => trans('cart.text_empty_cart')
                );
                $request->session()->forget('coupon');
            }elseif(Cart::count() > 0 && $request->product_quantity == 0){
                $response = array(
                    'code' => 'remove-product'
                );
            }else{
                $product_total_price = $get_product->price*$get_product->qty;
                $response = array(
                    'code' => 'update-product',
                    'product_total_price' => webus_shop::calculate_price($product_total_price)
                );
            }

        }else{
            $response = 'error';
        }

        $response['count'] = Cart::count();
        //$response['total'] = webus_shop::calculate_price(Cart::total());
        $response['totals'] = view('front.shop.totals', ['totals' => webus_shop::get_totals()])->render();
        $response['currency'] = webus_shop::get_currency();

        return response()->json($response);
    }

    public function remove(Request $request){
        if(isset($request->product_key) && !empty($request->product_key)){
            Cart::remove($request->product_key);
            if (Cart::count() == 0){
                $response = array(
                    'code' => 'empty-cart',
                    'text_empty_cart' => trans('cart.text_empty_cart')
                );
                $request->session()->forget('coupon');
            }else{
                $response = array(
                    'code' => 'remove-product'
                );
            }
           
            }else{
                $response = array(
                    'code' => 'error-product'
                );
            }

        $response['count'] = Cart::count();
        //$response['total'] = webus_shop::calculate_price(Cart::total());
        $response['totals'] = view('front.shop.totals', ['totals' => webus_shop::get_totals()])->render();
        $response['currency'] = webus_shop::get_currency();

        return response()->json($response);
    }

    public function apply_coupons(Request $request){
        if (Cart::count() == 0){
            $response = array(
                'code_response' => 'empty-cart',
                'text_empty_cart' => 'Cart is empty'
            );
        }elseif(isset($request->code)){
            $get_coupon = Coupons::where('code', $request->code)->first();
            if($get_coupon){
                $set_code_session_coupon = $request->session()->put('coupon', $request->code);
                $response = array(
                    'code_response' => 'code_exist',
                    'text' => '<div class="place-notification">
                                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> &nbsp;
                                <strong>Success:</strong> You have added coupon code</div>
                                </div>'
                );
            }else{
                $response = array(
                    'code_response' => 'code_not_exist',
                    'text' => '<div class="place-notification">
                    <div class="alert alert-danger"> &nbsp;
                    <strong>Error!</strong> Invalid or expired discount code.</div>
                    </div>');
            }
        }

        $response['total'] = '$'.number_format(Cart::total(),0);
        $response['totals'] = view('front.shop.totals', ['totals' => webus_shop::get_totals()])->render();
        $response['coupon'] = view('front.shop.coupon', ['coupon' => session('coupon')])->render();
        $response['code'] = $request->code;

        return response()->json($response);
    }

    public function remove_coupons(Request $request){
        $request->session()->forget('coupon');
        return redirect(route('cart'));
    }
    
//    public function clear(){
//        Cart::destroy();
//    }

    /**** Currency *****/

    public function change_currency_product(Request $request){
        //save cookie 1 year
        if(isset($request->currency) && isset($request->product_id)){
            $product_price = Products::price($request->product_id);
            $get_price = webus_shop::calculate_price($product_price, $request->currency);
            return response()->json(['new_price' => $get_price])->cookie('currency', $request->currency, 525601);
        }
        return response('error');
    }

    /**** Checkout *****/

    public function login_customer(Request $request, Auth $auth){
        if($auth::guest()){

            $login = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
            if ($login) {
                $response = [
                    'code' => 'success',
                    'redirect_url' => route('checkout')
                ];
            }else{
                $response = [
                    'code' => 'error',
                    'text' => trans('auth.failed')
                ];
            }
            return response()->json($response);

        }
    }
    
    public function checkout(CustomerAdresses $adresses){
        if(Cart::total() > 0){
            return view('front.shop.checkout',[
                'title' => trans('cart.text_heading_checkout'),
                'addresses' => $adresses::customer_addresses(),
                'curren_currency' => webus_shop::get_currency(),
                'payment_methods' => webus_shop::get_payment_methods(),
                'cart' => self::get_cart(),
                'total' => webus_shop::calculate_price(Cart::total())
            ]);
        }else{
            return redirect(route('cart'));
        }

    }
    
    public function add_address(Request $request, CustomerAdresses $customerAdresses, Auth $auth){
        if($auth::check()){
        $record = new $customerAdresses;
        $record->user_id = $auth::id();
        $record->name = $request->name;
        $record->street = $request->street;
        $record->index = $request->index;
        $record->phone = $request->phone;
        $record->country_id = $request->country_id;
        $record->city = $request->city;
        $record->save();
        
        return response()->json([
            'addresses' => view('front.includes.checkout.list-addresses', ['addresses' => $customerAdresses::customer_addresses()])->render()
        ]);
        }
    }

    public function confirm_order(Request $request, Auth $auth, CustomerAdresses $customerAdresses,Orders $orders, OrdersProducts $ordersProducts, User $user){
        $error = false;
        if(Cart::total() == 0){
            $response = array(
                'code' => 'cart_empty',
                'redirect_url' => route('cart')
            );
        }elseif(!array_key_exists($request->method_payment, webus_shop::get_payment_methods())){
            $response = array(
                'code' => 'payment_method_undefined',
                'text' => trans('order.text_payment_method_undefined')
            );
        }else{
            $order_info = array(
                'address_name' => '',
                'address_street' => '',
                'address_index' => '',
                'address_country' => '',
                'address_city' => '',
                'address_phone' => '',
            );
            $order_info['method_payment'] = $request->method_payment;
            $order_info['sub_total'] = webus_shop::calculate_price(Cart::subtotal());
            $order_info['total'] = filter_var(webus_shop::get_totals()['total']['value'], FILTER_SANITIZE_NUMBER_INT);
            $order_info['totals'] = json_encode(webus_shop::get_totals());
            $order_info['currency'] = webus_shop::get_currency();

            if($auth::check()){
                $get_info_address = $customerAdresses::get_address($request->address);
                if($get_info_address != null){
                    $order_info['address_name'] = $get_info_address->name;
                    $order_info['address_street'] = $get_info_address->street;
                    $order_info['address_index'] = $get_info_address->index;
                    $order_info['address_country'] = $get_info_address->country['name'];
                    $order_info['address_city'] = $get_info_address->city;
                    $order_info['address_phone'] = $get_info_address->phone;
                }else{
                    $response = array(
                        'code' => 'address_undefined',
                        'text' => trans('order.text_address_undefined')
                    );
                    $error = true;
                }
            }else{
                if($user::where('email', $request->email)->first() != null){
                    $response = array(
                        'code' => 'exist_user',
                        'text' => trans('order.text_exist_user')
                    );
                    $error = true;
                }else{
                    //register user
                    $register_customer = new $user;
                    $register_customer->name = $request->name;
                    $register_customer->email = $request->email;
                    $register_customer->password = bcrypt($request->confirm_password);
                    $register_customer->save();
                    //add user address
                    $add_customer_address = new $customerAdresses;
                    $add_customer_address->user_id = $register_customer->id;
                    $add_customer_address->name = $request->name;
                    $add_customer_address->street = $request->street;
                    $add_customer_address->index = $request->index;
                    $add_customer_address->phone = $request->phone;
                    $add_customer_address->country_id = $request->country_id;
                    $add_customer_address->city = $request->city;
                    $add_customer_address->save();
                    //prepare order_info
                    $get_info_address = $customerAdresses::get_address($add_customer_address->id);
                    if($get_info_address != null){
                        $order_info['address_name'] = $get_info_address->name;
                        $order_info['address_street'] = $get_info_address->street;
                        $order_info['address_index'] = $get_info_address->index;
                        $order_info['address_country'] = $get_info_address->country['name'];
                        $order_info['address_city'] = $get_info_address->city;
                        $order_info['address_phone'] = $get_info_address->phone;
                    }
                    //login user
                    $auth::attempt(['email' => $request->email, 'password' => $request->confirm_password]);
                    $error = false;
                }
            }

            //add order
            if(!$error){
                $products_cart = self::get_cart();
//                $get_session_order = $request->session()->get('order_id');
//                if($get_session_order){
//                    $order_id = $orders::update_order($get_session_order, $order_info);
//                    $order_products = $ordersProducts::update_products($order_id, $products_cart, webus_shop::get_currency());
//                    $subject = config('app.name').' - Обновленный заказ '.$order_id;
//                }else{
//                    $order_id = $orders::add($order_info);
//                    $order_products = $ordersProducts::add($order_id, $products_cart, webus_shop::get_currency());
//                    $subject = config('app.name').' - Заказ '.$order_id;
//                    $set_order_session = session(['order_id' => $order_id]);
//                }
                $order_id = $orders::add($order_info);
                $order_products = $ordersProducts::add($order_id, $products_cart, webus_shop::get_currency());
                $subject = config('app.name').' - Заказ '.$order_id;

                //mail to admin
                Mail::to(webus_help::get_option('generale_email'))->send(new OrdersMail($order_info, $products_cart, $subject, 'admin'));
                //mail to customer
                Mail::to($auth::user()->email)->send(new OrdersMail($order_info, $products_cart, $subject, 'customer'));

                
                //redirect to selected payment gateway service
                $redirect_url = route('checkout_success');
                if($request->method_payment == 'paypal'){
                   $paypal = 'App\Http\Controllers\Webus\Shop\Payments\PaypalStandart';
                   $redirect_url = $paypal::generate_payment($order_id, self::get_cart(), webus_shop::get_currency());
                }

//                if($request->method_payment == 'paypal'){
//                    $paypal = 'App\Http\Controllers\Webus\Shop\Payments\PaypalExpress';
//                    $redirect_url = $paypal::generate_payment();
//                }
                
                if($request->method_payment == 'yandex'){
                    $set_order_session = session(['order_id' => $order_id]);
                    $redirect_url = route('yandex_generate_pm');
                }
                
                $response = array(
                    'code' => 'pay',
                    'redirect_url' => $redirect_url,
                );
            }

        }

        return response()->json($response);

    }

    //$request->session()->forget('order_id');
    public function success(Request $request){
       return view('front.shop.response-page', [
           'text' => '<span class="alert-success">'.trans('order.text_checkout_success').'</span>'
       ]);
    }  
    
    public function failed(){
        return view('front.shop.response-page', [
            'text' => '<span class="alert-danger">'.trans('order.text_checkout_failed').'</span>'
        ]);
    }
}
