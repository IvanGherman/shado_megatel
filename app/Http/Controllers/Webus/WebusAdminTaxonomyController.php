<?php

namespace App\Http\Controllers\Webus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WebusAdminTaxonomyController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\WebusModels\TaxonomyTerms'; //model
        $this->slug = 'category'; //id controller
        $this->title = 'Category'; //title controller
        $this->icon = ''; //icon page
        $model_tax = $this->model_name;
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = ['name' => 'translatable', 'slug' , 'term_order' => true]; //in list template

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            'slug' => [
                'title' => 'Slug',
                'html_type' => 'text',
                'validate' => 'required|max:255',
                'custom_class' => '',
                'id' => '',
                'slug' => true,
                'translatable' => false
            ],

            'term_order' => [
                'title' => 'Order',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'term_parent' => [
                'title' => 'Parent',
                'html_type' => 'select',
                'options' => $model_tax::get_terms_for_selected($this->slug),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'taxonomy' => [
                'title' => false,
                'html_type' => 'hidden',
                'default' => $this->slug,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],




        );

        $this->meta_box = false;
    }

    public function index(Request $request){
        $model_name = $this->model_name;
        $per_page = 25;
        if(isset($this->per_page))$per_page = $this->per_page;
        $search_field = 'name'; if(isset($this->search)){$search_field = $this->search;}
        $search_key = false; if(isset($request->search)){ $search_key = $request->search;}
        $icon_page = '';
        if(isset($this->icon)){
            $icon_page = $this->icon;
        }

        if($search_key){
            $list = $model_name::with('children')->with('parent')->where($search_field, 'like', '%'.$search_key.'%')->where('taxonomy', $this->slug)->orderBy('term_order', 'asc')->paginate($per_page);
        }else{
            $list = $model_name::with('children')->with('parent')->where($search_field, 'like', '%'.$search_key.'%')->where('taxonomy', $this->slug)->where('term_parent', 0)->orderBy('term_order', 'asc')->paginate($per_page);
        }

        $data = array(
            'title' => $this->title,
            'url_create' => route($this->slug.'.create'),
            'url_edit' => 'admin/'.$this->slug.'/edit/',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_delete_mass' => 'admin/'.$this->slug.'/delete_mass/',
            'url_build' => 'admin/'.$this->slug.'/build/',
            'rows' => $this->list_field,
            'list' => $list,
            'icon' => $icon_page
        );

        $view = 'webus.standart.index';

        if (view()->exists('webus.'.$this->slug.'.index')) {
            $view = 'webus.'.$this->slug.'.index';
        }

        return view($view, ['data' => $data]);
    }
}
