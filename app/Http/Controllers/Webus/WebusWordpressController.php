<?php

namespace App\Http\Controllers\Webus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Http\Helpers\webus_help;

class WebusWordpressController extends Controller
{
    public function __construct()
    {
        /******Example Construct for Your Controller (This example works for the wordpress controller., do not delete)******/
        $this->model_name = 'App\WordpressPosts'; //model for taxonomy use App\WordpressTaxonomy
        $model_name = $this->model_name;
        $this->model_tax = 'App\WordpressTaxonomy';
        $this->old_path = 'http://localhost/cristis/wp-content/';
        $this->post_type = 'post'; //model
        $this->slug = 'news'; //id controller
        $this->title = 'News'; //title controller
        $this->per_page = '25';
        $this->search = 'post_title'; //field for search

        $model_taxonomy =  $this->model_tax;

        $this->taxonomy = array(
            'portfolio_category' => [
                'title' => 'Category',
                'html_type' => 'taxonomy_select',
                'options' => $model_taxonomy::get_terms_array('portfolio_category'),
                'multiple' => true,
                'validate' =>  false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]
        );

        //Identical to migrated file
        $this->list_field = ['ID' => 'translatable', 'post_title' => 'translatable', 'thumbnail']; //in list template

        $this->form_fields = array( //in form template
            'post_title' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'post_name' => [
                'title' => 'Slug',
                'html_type' => 'text',
                'validate' => 'required|max:255',
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            'post_content' => [
                'title' => 'Body',
                'html_type' => 'body',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            //for page post type
//            'post_parent' => [
//                'title' => 'Parent',
//                'html_type' => 'select',
//                'options' => array($model_name::get_posts_parent($this->slug)),
//                'multiple' => false,
//                'validate' =>  false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => false
//            ]

            //for template
//            'template' => [
//                'title' => 'Template',
//                'html_type' => 'select_template',
//                'options' => array(
//                    '0' => 'Default',
//                    'gallery' => 'Gallery',
//                    'contact-template' => 'Contact'
//
//                ),
//                'multiple' => false,
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => false
//            ]

        );



    }

    public function index(Request $request){
        $model_name = $this->model_name;
        $per_page = 25;
        if(isset($this->per_page))$per_page = $per_page;
        $search_field = 'post_title'; if(isset($this->search)){$search_field = $this->search;}
        $search_key = false; if(isset($request->search)){ $search_key = $request->search;}

        $data = array(
            'title' => $this->title,
            'url_create' => route($this->slug.'.create'),
            'url_edit' => 'admin/'.$this->slug.'/edit/',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_build' => 'admin/'.$this->slug.'/build/',
            'rows' => $this->list_field,
            'old_path' => $this->old_path,
            'list' => $model_name::where('post_type', $this->post_type)
                ->where('post_status', 'publish')
                ->where($search_field, 'like', '%'.$search_key.'%')
                ->orderBy('ID', 'post_date')
                ->paginate($per_page)
        );

        $view = 'webus.standart-wordpress.index';

        if (view()->exists('webus.'.$this->slug.'.index')) {
            $view = 'webus.'.$this->slug.'.index';
        }

        return view($view, ['data' => $data]);
    }

    public function create(){

        //check taxonomy
        if(isset($this->taxonomy) && $this->taxonomy){
            //push all taxonomy data in array fields
            foreach ($this->taxonomy as $name_tax=>$tax_settings){
                $this->form_fields[$name_tax] = $tax_settings;
            }
        }

        $data = array(
            'title' => 'Add new '.$this->slug,
            'url_create' => '',
            'form_action' => route($this->slug.'.store'),
            'rows' => $this->form_fields,
        );

        $view = 'webus.standart-wordpress.add-edit';

        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }

        return view($view, ['data' => $data]);
    }

    public function store(Request $request){

        $validate_array = array();
        foreach ($this->form_fields as $key => $field){
            if($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        $this->validate($request, $validate_array);

        $model_name = $this->model_name;
        $post = new $model_name;

        //check taxonomy
        if($this->taxonomy){
            //push all taxonomy data in array fields
            foreach ($this->taxonomy as $name_tax=>$tax_settings){
                $this->form_fields[$name_tax] = $tax_settings;
            }
        }

        foreach ($this->form_fields as $key => $field){
            //translatable
            if($field['translatable']){

                $convert_item_to_translatable = '';
                foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                    //echo 'Lang: '.$key_translatable.' - for:'.$key.' ----'.$item_translatable.'<br>';
                    $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                }
                $post->$key = stripslashes($convert_item_to_translatable);

            }elseif($field['html_type'] == 'file'){

                if(!empty($request->file($key))){
                    $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');

                }

            }elseif($field['html_type'] == 'taxonomy_select'){

                $data_taxonomy  = array(
                    'taxonomy' => $key,
                    'terms' => $request->$key
                );

            }elseif($field['html_type'] == 'select_template'){

                $data_template = $request->$key;

            }elseif($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input'){

                $path_image_input = parse_url($request->$key, PHP_URL_PATH);

            }else{

                //                if($request->$key)
                $post->$key = $request->$key;

            }


        }


        $post->post_type = $this->post_type;

        $post->post_status = 'publish';
        $post->post_date = date('Y-m-d H:i:s');
        $post->post_date_gmt = date('Y-m-d H:i:s');
        $post->post_modified = date('Y-m-d H:i:s');
        $post->post_modified_gmt = date('Y-m-d H:i:s');

        $post->save();

        //atach taxonomy if isset*
        if(isset($data_taxonomy) && $data_taxonomy){
            $model_tax = $this->model_tax;
            $model_tax::atach_terms_to_post($post->ID, $data_taxonomy['terms'], $data_taxonomy['taxonomy']);
        }

        //atach thumb
        if(isset($path)){
            webus_help::atach_thumbnail_to_post($post->ID, preg_replace('/^public\//', '', $path));
        }

        if(isset($path_image_input)){
            webus_help::atach_thumbnail_to_post($post->ID, preg_replace('/^public\//', '', $path_image_input));
        }

        //atach template
        if(isset($data_template)){
            webus_help::atach_template_wordpress($post->ID,$data_template);
        }

        Session::flash('flash_message','Successfully added.');

        return redirect(route($this->slug.'.edit', ['id' => $post->ID]));
    }

    public function edit(Request $request, $id){
        $model_name = $this->model_name;
        $model = $model_name::findOrFail($id);


        $get_thumb = webus_help::get_thumb_by_id($id, $this->old_path);

        if($get_thumb){
            $get_thumb = $get_thumb;
        }else{
            $get_thumb = '';
        }

        //check taxonomy
        if(isset($this->taxonomy) && $this->taxonomy){
            $model_tax = $this->model_tax;
            //push all taxonomy data in array fields
            foreach ($this->taxonomy as $name_tax=>$tax_settings){
                $this->form_fields[$name_tax] = $tax_settings;
                //get selected taxonomy
                $model['selected_'.$name_tax] =  $model_tax::get_selected_relation($id, $name_tax);
            }
        }

        $template_page = false;
        if($this->post_type == 'page'){$template_page = webus_help::get_template_wordpress($id);}

        $data = array(
            'title' => 'Edit '.$this->slug,
            'url_create' => '',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_build' => 'admin/'.$this->slug.'/menu/',
            'form_action' => route($this->slug.'.update', ['id' => $id]),
            'rows' => $this->form_fields,
            'thumbnail' => $get_thumb,
            'page_template' => $template_page
        );

        $view = 'webus.standart-wordpress.add-edit';

        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }


        return view($view, ['data' => $data, 'model' => $model]);

    }

    public function update(Request $request, $id){
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);

        $validate_array = array();
        foreach ($this->form_fields as $key => $field){
            if($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        //check taxonomy
        if(isset($this->taxonomy) && $this->taxonomy){
            //push all taxonomy data in array fields
            foreach ($this->taxonomy as $name_tax=>$tax_settings){
                $this->form_fields[$name_tax] = $tax_settings;
            }
        }

        foreach ($this->form_fields as $key => $field){
            //translatable
            if($field['translatable']){

                $convert_item_to_translatable = '';
                foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                    //echo 'Lang: '.$key_translatable.' - for:'.$key.' ----'.$item_translatable.'<br>';
                    $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                }

                $post->$key = stripslashes($convert_item_to_translatable);

            }elseif($field['html_type'] == 'file'){

                if(!empty($request->file($key))){
                    $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');
                    // $post->$key = preg_replace('/^public\//', '', $path);
                }

                //if is checked for remove from db this file url
                if($request['remove'][$key] == 'on'){
                    webus_help::remove_post_thumbnail($id);
                }

            }elseif($field['html_type'] == 'taxonomy_select'){

                $data_taxonomy  = array(
                    'taxonomy' => $key,
                    'terms' => $request->$key
                );

            }elseif($field['html_type'] == 'select_template'){

                $data_template = $request->$key;

            }elseif($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input'){
                //$post->$key = parse_url($request->$key, PHP_URL_PATH);
                $path_image_input =  parse_url($request->$key, PHP_URL_PATH);

            }else{

                $post->$key = $request->$key;

            }



        }

        $post->post_type = $this->post_type;
        $post->post_status = 'publish';
        $post->post_modified = date('Y-m-d H:i:s');
        $post->post_modified_gmt = date('Y-m-d H:i:s');
        $post->save();

        //atach taxonomy if isset*
        if(isset($data_taxonomy) && $data_taxonomy){
            $model_tax = $this->model_tax;
            $model_tax::atach_terms_to_post($id, $data_taxonomy['terms'], $data_taxonomy['taxonomy']);
        }

        //atach thumb
        if(isset($path)){
            webus_help::remove_post_thumbnail($id);
            webus_help::atach_thumbnail_to_post($id, preg_replace('/^public\//', '', $path));
        }

        if(isset($path_image_input)){
            webus_help::remove_post_thumbnail($id);
            webus_help::atach_thumbnail_to_post($id, preg_replace('/^public\//', '', $path_image_input));
        }

        //atach template
        if(isset($data_template)){
            webus_help::atach_template_wordpress($id,$data_template);
        }

        Session::flash('flash_message','Successfully saved.');
        return redirect(route($this->slug.'.edit', ['id' => $id]));

    }

    public function delete($id){
        webus_help::remove_post_thumbnail($id);

        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);
        $post->delete();

        Session::flash('flash_message','Successfully deleted.');
        return redirect(route($this->slug.'.'.$this->slug));
    }




    //TAXONOMY

    public function index_taxonomy(){
        $mode_name = $this->model_name;

        $data = array(
            'title' => $this->title,
            'url_create' => route($this->slug.'.create'),
            'url_edit' => 'admin/'.$this->slug.'/edit/',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'rows' => $this->list_field,
            'list' => $mode_name::get_terms($this->slug)
        );

        $view = 'webus.standart-wordpress.index-terms';

        if (view()->exists('webus.'.$this->slug.'.index-terms')) {
            $view = 'webus.'.$this->slug.'.index-terms';
        }

        return view($view, ['data' => $data]);


    }


    public function create_taxonomy(){

        $data = array(
            'title' => 'Edit '.$this->slug,
            'url_create' => '',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'form_action' => route($this->slug.'.store'),
            'rows' => $this->form_fields
        );

        $view = 'webus.standart-wordpress.add-edit-terms';

        if (view()->exists('webus.'.$this->slug.'.add-edit-terms')) {
            $view = 'webus.'.$this->slug.'.add-edit-terms';
        }

        return view($view, ['data' => $data]);

    }


    public function store_taxonomy(Request $request){

        $validate_array = array();
        foreach ($this->form_fields as $key => $field){
            if($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        $this->validate($request, $validate_array);

        $model_name = $this->model_name;
        $data = array();

        foreach ($this->form_fields as $key => $field){
            //translatable
            if($field['translatable']){

                $convert_item_to_translatable = '';
                foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                    $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                }
                $data[$key] = $convert_item_to_translatable;

            }else{
                if($request->$key)  $data[$key] = $request->$key;

            }


        }

        $insert_term_to_tax = $model_name::insert_term($data, $this->slug);

        Session::flash('flash_message','Successfully added.');

        return redirect(route($this->slug.'.edit', ['id' => $insert_term_to_tax]));

    }

    public function edit_taxonomy(Request $request, $id){
        $model_name = $this->model_name;
        $model = $model_name::get_term_by_id_taxonomy($this->slug, $id);
        if(!$model) {abort(404);}

        $model = array(
            'term_id' => $model->term_id,
            'name' => $model->name,
            'slug' => $model->slug,
            'parent' => $model->parent
        );

        $data = array(
            'title' => 'Edit '.$this->slug,
            'url_create' => '',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'form_action' => route($this->slug.'.update', ['id' => $id]),
            'rows' => $this->form_fields
        );

        $view = 'webus.standart-wordpress.add-edit-terms';

        if (view()->exists('webus.'.$this->slug.'.add-edit-terms')) {
            $view = 'webus.'.$this->slug.'.add-edit-terms';
        }

        return view($view, ['data' => $data, 'model' => $model]);

    }


    public function update_taxonomy(Request $request, $id){

        $validate_array = array();
        foreach ($this->form_fields as $key => $field){
            if($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        $this->validate($request, $validate_array);

        $model_name = $this->model_name;
        $data = array();

        foreach ($this->form_fields as $key => $field){
            //translatable
            if($field['translatable']){

                $convert_item_to_translatable = '';
                foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                    $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                }
                $data[$key] = $convert_item_to_translatable;

            }else{
                if($request->$key)  $data[$key] = $request->$key;

            }


        }

        $update_term_taxonomy = $model_name::update_term($data, $id, $this->slug);

        Session::flash('flash_message','Successfully added.');

        return redirect(route($this->slug.'.edit', ['id' => $id]));

    }


    public function delete_taxonomy($id){
        $model = $this->model_name;
        $model::delete_term($id, $this->slug);
        Session::flash('flash_message','Successfully deleted.');
        return redirect(route($this->slug.'.'.$this->slug));
    }




}
