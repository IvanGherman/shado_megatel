<?php

namespace App\Http\Controllers\Webus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// start role&permission
use jeremykenedy\LaravelRoles\Models\Role;
use jeremykenedy\LaravelRoles\Models\Permission;
//end role&permission
use App\User;
use Session;
use Auth;
use DB;
class WebusUserController extends WebusAdminController
{
    public function __construct()
    {
        /******Example Construct for Your Controller (This example works for the menu controller., do not delete)******/
        $this->model_name = 'App\User'; //model
        $this->slug = 'users'; //id controller
        $this->title = 'users'; //title controller
        $this->per_page = '25';
        $this->search = 'email'; //field for search
        //Identical to migrated file
        $this->list_field = ['name', 'last_name' => true, 'email' => 'translatable']; //in list template

        $this->form_fields = array( //in form template
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'email' => [
                'title' => 'Email',
                'html_type' => 'text',
                'validate' => 'required|email',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'role' => [
                'title' => 'Role',
                'html_type' => 'select',
                'options' => array(
                    '1 стандрат' => '1 стандрат',
                    '2 Vip' => '2 Vip',
//                    'User' => 'User',
                    'Admin' => 'Admin',
                ),
                'multiple'=> false,
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'password' => [
                'title' => 'Password',
                'html_type' => 'password',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]
        );
    }

    public function index(Request $request){
        $model_name = $this->model_name;
        $per_page = 25;
        if(isset($this->per_page))$per_page = $per_page;
        $search_field = 'name'; if(isset($this->search)){$search_field = $this->search;}
        $search_key = false; if(isset($request->search)){ $search_key = $request->search;}

        $data = array(
            'title' => $this->title,
            'url_create' => route($this->slug.'.create'),
            'url_edit' => 'admin/'.$this->slug.'/edit/',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_delete_mass' => 'admin/'.$this->slug.'/delete_mass/',
            'url_build' => 'admin/'.$this->slug.'/build/',
            'rows' => $this->list_field,
            'list' => $model_name::where($search_field, 'like', '%'.$search_key.'%')->whereNotIn('email', ['d.botezatu@webus.md', 'office@webus.md'])->whereNotIn('id', ['1'])->orderBy('created_at', 'desc')->paginate($per_page)
        );

        $view = 'webus.standart.index';

        if (view()->exists('webus.'.$this->slug.'.index')) {
            $view = 'webus.'.$this->slug.'.index';
        }

        return view($view, ['data' => $data]);
    }

    public function store(Request $request){

        $validate_array = array();
        foreach ($this->form_fields as $key => $field){
            if($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        $this->validate($request, $validate_array);

        $model_name = $this->model_name;
        $post = new $model_name;

        foreach ($this->form_fields as $key => $field){
            //translatable
            if($field['translatable']){

                $convert_item_to_translatable = '';
                foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                    //echo 'Lang: '.$key_translatable.' - for:'.$key.' ----'.$item_translatable.'<br>';
                    $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                }
                $post->$key = stripslashes($convert_item_to_translatable);

            }elseif($field['html_type'] == 'file'){

                if(!empty($request->file($key))){
                    $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');
                    $post->$key = preg_replace('/^public\//', '', $path);
                }


            }elseif($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input'){
                $post->$key = parse_url($request->$key, PHP_URL_PATH);
            }elseif($field['html_type'] == 'password'){
                $post->$key = bcrypt($request->$key);
            }elseif($field['html_type'] == 'select'){
                $role = $request->$key;
            }else{
//                if($request->$key)
                $post->$key = $request->$key;
            }


        }

        $post->save();

        if(isset($role)){
            $find_role = Role::where('name', '=', $role)->first();
            $post->detachAllRoles();
            $post->attachRole($find_role);
        }


        Session::flash('flash_message','Successfully added.');

        return redirect(route($this->slug.'.edit', ['id' => $post->id]));
    }


    public function edit(Request $request, $id){
        if($id == 1){
            abort(404);
        }
        $model_name = $this->model_name;
        $model = $model_name::findOrFail($id);

        $get_role_user = (isset(DB::table('role_user')->where('user_id', $id)->first()->role_id) ? DB::table('role_user')->where('user_id', $id)->first()->role_id : '');
        $data = array(
            'title' => 'Edit '.$this->slug,
            'url_create' => '',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_build' => 'admin/'.$this->slug.'/menu/',
            'form_action' => route($this->slug.'.update', ['id' => $id]),
            'rows' => $this->form_fields,
            'role' => (isset(Role::where('id', $get_role_user)->first()->name) ? Role::where('id', $get_role_user)->first()->name : 'User')
        );

        $view = 'webus.standart.add-edit';

        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }

        return view($view, ['data' => $data, 'model' => $model]);

    }

    public function update(Request $request, $id){
        if($id == 1){
            abort(404);
        }
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);

        $validate_array = array();
        foreach ($this->form_fields as $key => $field){
            if($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        foreach ($this->form_fields as $key => $field){
            //translatable
            if($field['translatable']){

                $convert_item_to_translatable = '';
                foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                    //echo 'Lang: '.$key_translatable.' - for:'.$key.' ----'.$item_translatable.'<br>';
                    $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                }

                $post->$key = stripslashes($convert_item_to_translatable);

            }elseif($field['html_type'] == 'file'){

                if(!empty($request->file($key))){
                    $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');
                    $post->$key = preg_replace('/^public\//', '', $path);
                }

                //if is checked for remove from db this file url
                if($request['remove'][$key] == 'on'){
                    $post->$key = '';
                }

            }elseif($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input'){
                $post->$key = parse_url($request->$key, PHP_URL_PATH);
            }elseif($field['html_type'] == 'password'){
                if($request->$key){ $post->$key = bcrypt($request->$key); }
            }elseif($field['html_type'] == 'select'){
                $role = $request->$key;
            }else{
//                if($request->$key)
                $post->$key = $request->$key;

            }



        }

        $post->save();

        if(isset($role)){
            $find_role = Role::where('name', '=', $role)->first();
            $post->detachAllRoles();
            $post->attachRole($find_role);
        }

        Session::flash('flash_message','Successfully saved.');
        return redirect(route($this->slug.'.edit', ['id' => $id]));

    }

    public function delete($id){
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);
        if(Auth::id() == $post->id){
            return redirect(route($this->slug.'.'.$this->slug))->withErrors(['You can not remove yourself']);
        }else{
            $post->detachAllRoles();
            $post->delete();
            Session::flash('flash_message','Successfully deleted.');
            return redirect(route($this->slug.'.'.$this->slug));
        }

    }

    public function delete_mass(Request $request){
        if($request->posts){
            $ids = $request->posts;
            $model_name = $this->model_name;
            foreach ($ids as $id){
                $post = $model_name::findOrFail($id);
                if(Auth::id() == $post->id){
                    return redirect(route($this->slug.'.'.$this->slug))->withErrors(['You can not remove yourself']);
                }else{
                    $post->detachAllRoles();
                    $post->delete();
                }
            }
        }

        Session::flash('flash_message','Successfully deleted.');
        return redirect(route($this->slug.'.'.$this->slug));
    }

}
