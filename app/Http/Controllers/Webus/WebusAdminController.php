<?php

namespace App\Http\Controllers\Webus;

use App\Http\Controllers\Controller;
use App\Http\Helpers\webus_help;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Route;
use Illuminate\Database\Eloquent\Model;

class WebusAdminController extends Controller
{
    /** @var Model|null $model_name */
    public $model_name = null;
    public $slug = null;
    public $title = null;
    public $list_field = null;
    public $meta_box = null;
    public $form_fields = [];
    public $model_taxonomy = null;

    public $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

    public function __construct()
    {
        /******Example Construct for Your Controller (This example works for the menu controller., do not delete)******/
        $this->model_name = 'App\WebusModels\WebusMenu'; //model
        $this->slug = 'menu'; //id controller
        $this->title = 'Menu'; //title controller
        $this->icon = ''; //icon page
        $this->template_post = 'default'; //template post type (default or positions)
        $this->active_modules = false; //enable modules controll on this page

        $this->model_taxonomy = 'App\WebusModels\TaxonomyTerms'; //use taxonomy table
        $taxonomy = $this->model_taxonomy;

        $this->per_page = '25';
        $this->search = 'name'; //field for search
        $this->orderby = 'created_at,desc';
        //Identical to migrated file
        $this->list_field = ['name', 'slug']; //in list template

        $this->form_fields = array( //in form template
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                //'position' => 'content' //content or sidebar , only for add-edit-positions template, change name template in functions create and edit

            ],

            'slug' => [
                'title' => 'Slug',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '', //datepicker or datetimepicker for select date
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false
            ],


//            'category' => [
//                'title' => 'Categories',
//                'html_type' => 'select_taxonomy',
//                'options' => $taxonomy::get_terms_for_selected('category'),
//                'default_value_not_by_keys' => false, //false for none multiple taxonomy and none multiple value
//                'multiple' => true,
//                'validate' => false,
//                'custom_class' => '',
//                'translatable' => false
//            ],

        );


        //meta_boxes
        $this->meta_box = false; //define metabox false, If you do not want metaboxes
//        $this->meta_box = array(
//
//            'accent_intro_text' => [
//                'title' => 'Intro text',
//                'html_type' => 'text',
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => true,
//                'show_on' => array(2), //id post
//                'metabox' => true,
//            ],
//
//            'accent_intro_text_second' => [
//                'title' => 'Intro text',
//                'html_type' => 'text',
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => false,
//                'metabox' => true,
//            ],
//
//            'select_meta' => [
//                'title' => 'Select meta',
//                'html_type' => 'select',
//                'options' => array(
//                    '1' => 'John',
//                    '2' => 'Marius',
//                ),
//                'multiple' => false,
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => false,
//                'metabox' => true,
//            ],
//
//            'select_meta_multiple' => [
//                'title' => 'Select meta multiple',
//                'html_type' => 'select',
//                'options' => array(
//                    '1' => 'John',
//                    '2' => 'Marius',
//                ),
//                'default_value_not_by_keys' => true,
//                'multiple' => true,
//                'validate' => false,
//                'custom_class' => '',
//                'id' => '',
//                'translatable' => false,
//                'metabox' => true,
//            ]
//
//
//        );

    }

    public function index(Request $request)
    {
        $model_name = $this->model_name;
        $per_page = !empty($this->per_page) ? $this->per_page : 25;

        $search_field = 'name';
        if (isset($this->search)) {
            $search_field = $this->search;
        }

        $search_key = false;
        if (isset($request->search)) {
            $search_key = $request->search;
        }

        $order_by = 'created_at,desc';
        if (isset($this->orderby) && $this->orderby != null){
            $order_by = $this->orderby;
        }

        $list = $model_name::orderBy(explode(',',$order_by)[0], explode(',',$order_by)[1]);

        if (!empty($search_key)) {
            $list = $list->where($search_field, 'like', '%'.$search_key.'%');
        }
        $list = $list->paginate($per_page);

        $icon_page = '';
        if (isset($this->icon)) {
            $icon_page = $this->icon;
        }

        $data = [
            'title' => $this->title,
            'slug' => $this->slug,
            'url_create' => route($this->slug.'.create'),
            'url_edit' => 'admin/'.$this->slug.'/edit/',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_delete_mass' => 'admin/'.$this->slug.'/delete_mass/',
            'url_build' => 'admin/'.$this->slug.'/build/',
            'rows' => $this->list_field,
            'list' => $list,
            'icon' => $icon_page,
        ];

        $view = 'webus.standart.index';

        if (view()->exists('webus.'.$this->slug.'.index')) {
            $view = 'webus.'.$this->slug.'.index';
        }

        return view($view, ['data' => $data]);
    }

    public function create()
    {
        //metaboxes
        $metabox = false;

        if (isset($this->meta_box) && $this->meta_box) {
            $metabox = $this->meta_box;
            $current_route_id_and_template = '';
            $excludeFields = [];
            foreach ($this->meta_box as $key => $field){
                //prepare exclude
                if (!empty($field['show_on']) && !in_array($current_route_id_and_template, $field['show_on'])){
                    $excludeFields[] = $key;
                }
            }

            if (!empty($excludeFields)){
                foreach ($excludeFields as $excludeField){
                    unset($metabox[$excludeField]);
                }
            }
        }

        //Enabled Modules Controller
        if (isset($this->active_modules) && $this->active_modules){
            $metabox['modules'] = self::set_modules();
        }

        $icon_page = '';
        if (isset($this->icon)){
            $icon_page = $this->icon;
        }

        $data = [
            'title' => 'Add '.str_singular($this->title),
            'url_create' => '',
            'form_action' => route($this->slug.'.store'),
            'rows' => $this->form_fields,
            'metabox' => $metabox,
            'icon' => $icon_page
        ];

        $view = 'webus.standart.add-edit';

        if (isset($this->template_post) && $this->template_post == 'positions'){
            $view = 'webus.standart.add-edit-positions';
        }


        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }

        return view($view, ['data' => $data]);
    }

    public function store(Request $request)
    {
        $validate_array = [];
        foreach ($this->form_fields as $key => $field){
            if ($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        $this->validate($request, $validate_array);

        $model_name = $this->model_name;
        $post = new $model_name;
        $data_taxonomies = [];

        foreach ($this->form_fields as $key => $field){
            if ($field['html_type'] != 'info'){
                //translatable
                if ($field['translatable']){
                    $convert_item_to_translatable = '';
                    foreach ($request['translatable'][$key] as $key_translatable => $item_translatable){
                        if (!empty($item_translatable)){
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }
                    }
                    $post->$key = stripslashes($convert_item_to_translatable);
                } elseif ($field['html_type'] == 'schedule') {
                    $schedule = $request->$key;
                    $post->$key = serialize($schedule);
                } elseif ($field['html_type'] == 'file') {
                    if (!empty($request->file($key))) {
                        $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');
                        $post->$key = preg_replace('/^public\//', '', $path);
                    }
                } elseif ($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input') {
                    $post->$key = parse_url($request->$key, PHP_URL_PATH);
                } elseif ($field['html_type'] == 'password') {
                    $post->$key = bcrypt($request->$key);
                } elseif ($field['html_type'] == 'select_taxonomy') {
                    $data_taxonomies[$key] = $request->$key;
                } elseif ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                    $post->$key = null;
                    if (!empty($request->$key)) {
                        $post->$key = json_encode($request->$key);
                    }
                } elseif ($key == 'created_at') {
                    if (!empty($request->created_at)) {
                        $post->$key = Carbon::parse($request->created_at)->format('Y-m-d H:i:s');
                    } else {
                        $post->$key = Carbon::now();
                    }
                } else {
//                if($request->$key)
                    $post->$key = $request->$key;
                }
            }
        }

        $post->save();

        //attach terms taxonomy
        if (isset($data_taxonomies) && $data_taxonomies != null) {
            $model_taxonomy = $this->model_taxonomy;
            foreach ($data_taxonomies as $name_taxonomy=>$terms){
                $model_taxonomy::atach_terms_to_post($name_taxonomy, $terms, $post->id, $this->slug);
            }
        }

        //MetaBox
        $metabox = $this->meta_box;

        //Enabled Modules Controlle
        if (isset($this->active_modules) && $this->active_modules){
            $metabox['modules'] = self::set_modules();
        }

        if (isset($metabox) && $metabox){
            $meta_requests = $request->metabox;
            if ($meta_requests){
                $deleted_all_metabox = true;
                webus_help::delete_metabox($post->id, $this->slug);

                foreach ($meta_requests as $key => $meta) {
                    if (isset($request['meta_translatable'][$key])) {
                        $convert_item_to_translatable = '';
                        foreach ($request['meta_translatable'][$key] as $key_translatable=>$item_translatable){
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }

                        $meta_value = stripslashes($convert_item_to_translatable);
                        webus_help::insert_metabox($post->id, $this->slug, $key, $meta_value);
                    } else {
                        webus_help::insert_metabox($post->id, $this->slug, $key, $meta);
                    }
                }
            }

            //only for repeatable
            if (!isset($deleted_all_metabox)){
                webus_help::delete_metabox($post->id, $this->slug);
            }

            foreach ($metabox as $meta_key => $meta_fields) {
                if ($meta_fields['html_type'] == 'repeatable') {
                    if (isset($request->$meta_key)) {

                        $meta_value = [];
                        foreach ($request->$meta_key as $key => $value){
                            $meta_value[] = $value;
                        }

                        webus_help::insert_metabox($post->id, $this->slug, $meta_key, $meta_value);
                    }
                }
            }

        }

        Session::flash('flash_message','Successfully added.');

        return redirect(route($this->slug.'.edit', ['id' => $post->id]));
    }

    public function edit(Request $request, $id)
    {
        $model_name = $this->model_name;
        $model = $model_name::findOrFail($id);

        foreach ($this->form_fields as $key => $field){
            //get taxonomy default values
            if ($field['html_type'] == 'select_taxonomy'){
                $model_taxonomy = $this->model_taxonomy;
                if (isset($field['multiple']) && $field['multiple'] == false) {
                    $model[$key] = key($model_taxonomy::get_post_terms($key, $id, $this->slug, true));
                } elseif (isset($field['multiple']) && $field['multiple'] == true) {
                    $model['selected_'.$key] = $model_taxonomy::get_post_terms($key, $id, $this->slug, true);
                }
            }
            //get select & multiple from field database
            if ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                if ($model->$key) {
                    $model['selected_'.$key] = json_decode($model->$key);
                } else {
                    $model['selected_'.$key] = [];
                }
            }
        }

        //metaboxes
        $metabox = false;

        if (isset($this->meta_box) && $this->meta_box) {
            $metabox = $this->meta_box;
            $current_route_id = $id;
            $current_template = (isset($model->template) ? $model->template : '');
            $excludeFields = [];
            foreach ($this->meta_box as $key => $field){
                //prepare exclude
                if (!empty($field['show_on']) && !in_array($current_route_id, $field['show_on']) && !in_array($current_template, $field['show_on'])){
                    $excludeFields[] = $key;
                }

                if (isset($field['multiple']) && $field['multiple'] == true) { //for select multiple
                    $model['selected_'.$key] = webus_help::get_metabox($id, $this->slug, $key);
                } else {
                    $model[$key] = webus_help::get_metabox($id, $this->slug, $key);
                }
            }

            if (!empty($excludeFields)) {
                foreach ($excludeFields as $exludeField){
                    unset($metabox[$exludeField]);
                }
            }
        }
        //Enabled Modules Controller
        if (isset($this->active_modules) && $this->active_modules){
            $metabox['modules'] = self::set_modules();
            $model['modules'] = webus_help::get_metabox($id, $this->slug, 'modules');
        }

        $icon_page = '';
        if (isset($this->icon)){
            $icon_page = $this->icon;
        }

        $data = [
            'title' => 'Edit '.str_singular($this->title),
            'url_create' => route($this->slug.'.create'),
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_build' => 'admin/'.$this->slug.'/menu/',
            'form_action' => route($this->slug.'.update', ['id' => $id]),
            'rows' => $this->form_fields,
            'metabox' => $metabox,
            'icon' => $icon_page
        ];

        $view = 'webus.standart.add-edit';

        if (isset($this->template_post) && $this->template_post == 'positions') {
            $view = 'webus.standart.add-edit-positions';
        }

        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }

        return view($view, ['data' => $data, 'model' => $model]);
    }

    public function update(Request $request, $id)
    {
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);

        $validate_array = [];
        foreach ($this->form_fields as $key => $field){
            if ($field['validate']) {
                if ($key == 'slug' && $post->slug !== $request->slug) {
                    $validate_array[$key] = $field['validate'];
                }
            }
        }
        $this->validate($request, $validate_array);

        foreach ($this->form_fields as $key => $field){
            if ($field['html_type'] != 'info'){
                //translatable
                if ($field['translatable']) {

                    $convert_item_to_translatable = '';
                    foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                        if (!empty($item_translatable)) {
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }
                    }

                    $post->$key = stripslashes($convert_item_to_translatable);

                } elseif ($field['html_type'] == 'schedule') {
                    $schedule = $request->$key;
                    $post->$key = serialize($schedule);
                } elseif($field['html_type'] == 'file') {

                    if (!empty($request->file($key))) {
                        $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');
                        $post->$key = preg_replace('/^public\//', '', $path);
                    }

                    //if is checked for remove from db this file url
                    if ($request['remove'][$key] == 'on') {
                        $post->$key = '';
                    }
                } elseif ($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input') {
                    $post->$key = parse_url($request->$key, PHP_URL_PATH);
                } elseif ($field['html_type'] == 'password') {
                    if ($request->$key) {
                        $post->$key = bcrypt($request->$key);
                    }
                } elseif ($field['html_type'] == 'select_taxonomy') {
                    $data_taxonomies[$key] = $request->$key;
                } elseif ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                    if (!empty($request->$key)) {
                        $post->$key = json_encode($request->$key);
                    } else {
                        $post->$key = null;
                    }
                } elseif ($key == 'created_at') {
                    $post->$key = Carbon::parse($request->created_at)->format('Y-m-d H:i');
                } else {
//                if($request->$key)
                    $post->$key = $request->$key;
                }
            }

        }

        $post->save();

        //atach terms taxonomy
        if(isset($data_taxonomies) && $data_taxonomies != null){
            $model_taxonomy = $this->model_taxonomy;
            foreach ($data_taxonomies as $name_taxonomy=>$terms){
                $model_taxonomy::atach_terms_to_post($name_taxonomy, $terms, $post->id, $this->slug);
            }
        }

        //MetaBox
        $metabox = $this->meta_box;
        //Enabled Modules Controller
        if (isset($this->active_modules) && $this->active_modules) {
            $metabox['modules'] = self::set_modules();
        }

        if (isset($metabox) && $metabox) {
            $meta_requests = $request->metabox;
            if($meta_requests){

                $deleted_all_metabox = true;
                webus_help::delete_metabox($id, $this->slug);

                foreach ($meta_requests as $key => $meta){
                    if (isset($request['meta_translatable'][$key])) {
                        $convert_item_to_translatable = '';
                        foreach ($request['meta_translatable'][$key] as $key_translatable=>$item_translatable){
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }

                        $meta_value = stripslashes($convert_item_to_translatable);
                        webus_help::insert_metabox($id, $this->slug, $key, $meta_value);
                    } else {
                        webus_help::insert_metabox($id, $this->slug, $key, $meta);
                    }
                }
            }

            //only for repeatable
            if (!isset($deleted_all_metabox)){
                webus_help::delete_metabox($id, $this->slug);
            }

            foreach ($metabox as $meta_key=>$meta_fields){
                if ($meta_fields['html_type'] == 'repeatable') {
                    if (isset($request->$meta_key)) {
                        $meta_value = [];
                        foreach ($request->$meta_key as $key=>$value){
                            $meta_value[] = $value;
                        }

                        webus_help::insert_metabox($id, $this->slug, $meta_key, $meta_value);
                    }
                }
            }
        }

        Session::flash('flash_message','Successfully saved.');
        return redirect(route($this->slug.'.edit', ['id' => $id]));
    }

    public function delete($id)
    {
        $model_name = $this->model_name;
        //delete metabox
        webus_help::delete_metabox($id, $this->slug);
        //delete taxonomy
        if (isset($this->model_taxonomy)){
            $mode_tax = $this->model_taxonomy;
            $remove_taxonomy = $mode_tax::delete_terms($id, $this->slug);
        }

        $post = $model_name::findOrFail($id);
        $post->delete();
        Session::flash('flash_message','Successfully deleted.');

        return redirect(route($this->slug.'.'.$this->slug));
    }

    public function delete_mass(Request $request)
    {
        if ($request->posts) {
            $ids = $request->posts;
            $model_name = $this->model_name;
            foreach ($ids as $id) {
                //delete metabox
                webus_help::delete_metabox($id, $this->slug);
                //delete taxonomy
                if(isset($this->model_taxonomy)){
                    $mode_tax = $this->model_taxonomy;
                    $remove_taxonomy = $mode_tax::delete_terms($id, $this->slug);
                }

                $post = $model_name::findOrFail($id);
                $post->delete();
            }
        }

        Session::flash('flash_message','Successfully deleted.');

        return redirect(route($this->slug.'.'.$this->slug));
    }


    protected function set_modules()
    {
        $module_model = 'App\WebusModels\Modules';
        $list = $module_model::orderBy('code', 'asc')->get();
        $list_array = [];
        foreach ($list as $item) {
            $list_array[$item->id] = '['.$item->code.'] -'.$item->name;
        }
        $meta = [
            'title' => 'Modules',
            'html_type' => 'repeatable',
            'start_with' => 0,
            'max' => 999,
            'options' => array(
                //fields for repeatable box
                'modul' => array(
                    'title' => 'Module',
                    'html_type' => 'select',
                    'options' => $list_array,
                    'multiple' => false,
                    'validate' => false,
                    'translatable' => false,
                    'custom_class' => 'col-md-12'
                )
            ),
            'validate' => false,
            'custom_class' => '',
            'id' => '',
            'translatable' => false,
            //'show_on' => array(2),
            'metabox' => true

        ];

        return $meta;
    }

    public static function makeMap($array) : array
    {
        $map = ['' => ''];

        foreach ($array as $item) {
            $map[$item['id']] = $item['name'];
        }

        return $map;
    }
}
