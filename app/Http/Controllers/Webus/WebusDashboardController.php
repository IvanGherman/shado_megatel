<?php

namespace App\Http\Controllers\Webus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\webus_help;

class WebusDashboardController extends Controller
{
    
     public function index(){
    
         return view('webus.dashboard.index');

     }

    public function logout(){
        Auth::logout();
        return redirect(url('/'));
    }
    
    public function profile(){
        
        
        return view('webus/profile/index');
    }

    public function update_profile(Request $request){
        $model = 'App\User';
        $user = $model::find(Auth::id());
        $user->name = $request->name;
//        $user->email = $request->email;
        if(!empty($request->password)){
            $user->password = bcrypt($request->password);
        }


//        if(!empty($request->role_id)){
//            $user->attachRole($request->role_id);
//        }
        $user->save();
            return redirect(route('profile'));
    }
   
    
    public function icons(){
        return view('webus.dashboard.icons');
    }
     
}
