<?php

namespace App\Http\Controllers\Webus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use App\WebusModels\Modules;
use Session;
use Illuminate\Support\Facades\Redirect;
class WebusModulesController extends Controller
{
    public $title = null;
    public $module_filename = null;
    public $form_fields = [];

    public function index(){
        $files = File::allFiles(app_path('Http/Controllers/Modules'));
        $files_array = array();
        foreach ($files as $file)
        {
            $name_file = pathinfo($file)['filename'];
            $str_name_file = str_replace('_', ' ', $name_file);
            $modules = Modules::where('code', $name_file)->get();
            $files_array[$str_name_file] = $modules;

        }

        $data = [
            'title' => 'Modules',
            'list' => $files_array,
        ];

        return view('webus.standart.index-modules', [
            'data' => $data,
        ]);

    }

    public function create(){
        $data = [
            'title' => $this->title,
            'url_create' => '',
            'form_action' => route('save_'.$this->module_filename),
            'rows' => $this->form_fields
        ];
        $view = 'webus.standart.add-edit-module';

        if (view()->exists('webus.modules.'.$this->module_filename)) {
            $view = 'webus.modules.'.$this->module_filename;
        }

        return view($view,['data' => $data]);

    }

    public function edit($setting_id){
        $model = Modules::findOrFail($setting_id);
        $get_module_setting = collect(json_decode($model->setting));

        if(!empty($get_module_setting) && $get_module_setting->count()){
            foreach ($get_module_setting as $key=>$setting){
                $model[$key] = $setting;
            }
        }

        $data = [
            'title' => $this->title,
            'url_create' => route('create_'.$this->module_filename),
            'url_delete' => route('delete_'.$this->module_filename, ['setting_id' => $setting_id]),
            'form_action' => route('update_'.$this->module_filename, ['setting_id' => $setting_id]),
            'rows' => $this->form_fields,
        ];

        $view = 'webus.standart.add-edit-module';

        if (view()->exists('webus.modules.'.$this->module_filename)) {
            $view = 'webus.modules.'.$this->module_filename;
        }

        return view($view,['data' => $data, 'model' => $model, 'module_page' => true]);
    }

    public function save(Request $request){
        $record = new Modules();
        $setting = array();
        $fields = $this->form_fields;
        $setting = self::store_data($fields,$request);
        $record->name = $request->name;
        $record->code = $this->module_filename;
        $record->setting = json_encode($setting, JSON_HEX_APOS | JSON_UNESCAPED_UNICODE);
        $record->module_status = $request->status;

        $record->save();

        Session::flash('flash_message','Successfully added.');
        return redirect(route('edit_'.$this->module_filename, ['setting_id' => $record->id]));

    }

    public function update(Request $request, $setting_id){
        $record = Modules::findOrFail($setting_id);
        $fields = $this->form_fields;
        $setting = self::store_data($fields,$request);
        $record->name = $request->name;
        $record->code = $this->module_filename;
        $record->setting = json_encode($setting, JSON_HEX_APOS | JSON_UNESCAPED_UNICODE);
        $record->module_status = $request->status;

        $record->save();

        Session::flash('flash_message','Successfully updated.');
        return redirect(route('edit_'.$this->module_filename, ['setting_id' => $setting_id]));
    }

    protected function store_data($fields, $request){
        if(isset($fields) && count($fields) > 0){
            foreach ($fields as $key=>$field){
                if($field['html_type'] != 'info'){
                    if($field['translatable']){
                        $convert_item_to_translatable = '';
                        foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                            if(!empty($item_translatable)){
                                $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                            }
                        }
                        $setting[$key] = stripslashes($convert_item_to_translatable);
                    }elseif ($key === 'images'){
                        $setting[$key] = json_encode($request->$key);
                    }elseif($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input'){
                        $setting[$key] = parse_url($request->$key, PHP_URL_PATH);
                    }elseif($field['html_type'] == 'password'){
                        $setting[$key] = bcrypt($request->$key);
                    }else{
                        $setting[$key] = $request->$key;
                    }
                }

            }
        }
        return $setting;
    }

    public function delete($setting_id){
        $record = Modules::findOrFail($setting_id);
        $record->delete();

        Session::flash('flash_message','Successfully deleted.');
        return redirect(route('modules'));

    }


    static function front($module_id){

    }

}
