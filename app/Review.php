<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    public function getImage()
    {
        if (!empty($this->image) && file_exists(public_path().$this->image)) {
            return $this->image;
        }

        return 'assets/img/customer-img/no-icon.svg';
    }
}
