<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use App\Http\HelpersShop\webus_shop;
use App\Http\Helpers\webus_help;
class OrdersMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $order_info;
    public $products_cart;
    public $subject;
    public $for;

    public function __construct($order_info, $products_cart, $subject, $for)
    {
        $this->order_info = $order_info;
        $this->products_cart = $products_cart;
        $this->subject = $subject;
        $this->customer_email = Auth::user()->email;
        $this->from_mail = Auth::user()->email;
        if($for == 'customer'){
            $this->from_mail = webus_help::get_option('generale_email');
        }
        $this->for = $for;
        //get description payment method
        $methods = webus_shop::get_payment_methods();
        $this->payment_method = $methods[$order_info['method_payment']];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->from_mail)
            ->subject($this->subject)
            ->markdown('emails.order', [
                'text_for' => trans('order.text_'.$this->for),
                'customer_email' => $this->customer_email,
                'payment_method' => $this->payment_method
            ]);
    }
}
