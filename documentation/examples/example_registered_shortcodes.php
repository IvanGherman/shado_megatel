<?php
//example register shortcodes (RegisterShortcodes.php)
self::add_shortcode( 'gallery', function ($atts, $content = null){
    $atts = self::shortcode_atts( array(
        'ids' => '',
    ), $atts );

    return "ids = {$atts['ids']}";
});

self::add_shortcode( 'one_half', function ($atts, $content = null){
    return '<div class="grid_6">'.shortcodes::do_shortcode($content).'</div>';
});


self::add_shortcode( 'divider', function ($atts, $content = null){
    return '<div class="clear"></div><div class="divider"></div>';
});



//how to use shortcode
echo \App\Http\Shortcodes\shortcodes::do_shortcode('[space]');
?>
