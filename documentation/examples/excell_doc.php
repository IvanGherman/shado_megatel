'providers' => [
...
Maatwebsite\Excel\ExcelServiceProvider::class,
],

'aliases' => [
...
'Excel'     => Maatwebsite\Excel\Facades\Excel::class,
],

<?php

public function excel() {

    // Execute the query used to retrieve the data. In this example
    // we're joining hypothetical users and payments tables, retrieving
    // the payments table's primary key, the user's first and last name, 
    // the user's e-mail address, the amount paid, and the payment
    // timestamp.

    $payments = Payment::get();

    // Initialize the array which will be passed into the Excel
    // generator.
    $paymentsArray = [];

    // Define the Excel spreadsheet headers
    $paymentsArray[] = ['id', 'customer','email','total','created_at'];

    // Convert each member of the returned collection into an array,
    // and append it to the payments array.
    foreach ($payments as $payment) {
        $paymentsArray[] = $payment->toArray();
    }

    // Generate and return the spreadsheet
    Excel::create('payments', function($excel) use ($paymentsArray) {

        // Set the spreadsheet title, creator, and description
        $excel->setTitle('Payments');
        $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
        $excel->setDescription('payments file');

        // Build the spreadsheet, passing in the payments array
        $excel->sheet('sheet1', function($sheet) use ($paymentsArray) {
            $sheet->fromArray($paymentsArray, null, 'A1', false, false);
        });

    })->download('xlsx');
}


//import in db 

Excel::load(storage_path('app/excells/products.xlsx'), function($reader) {
    $results = $reader->all();

//        $fragrances = $results[2];
//        $i=0;
//        foreach ($fragrances as $fragrance){ $i++;
//            $record = new \App\Fragrances;
//            $record->name = $fragrance->nazvanie_atributa;
//            $record->term_parent = $fragrance->roditelskaya_kategoriya;
//            $record->group = $fragrance->gruppa_aromatov1_zhenskie_2_muzhskie;
//            $record->term_order = $i;
//            $record->save();
//        }

});
?>