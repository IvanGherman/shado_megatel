<?php
//routes
Route::get('{primary_slug}', 'SitePageController@index');
Route::get('{secondslug}/{primary_slug}', 'SitePageController@index');

//contoller
public function index(Request $request, $primary_slug = false, $second_slug = false){

    $slug = '';

    if(!$second_slug){
        $slug = $primary_slug;
    }else{
        $slug = $second_slug;
    }


    if(!empty($slug)){

        $post = webus_help::get_post_content_by_slug(urlencode($slug));

        if($post){

            //POST TYPE

            if($post->post_type == 'post'){
                $template = 'cristis.templates.single';

                return view($template, [
                    'title' => webus_help::translate_automat($post->post_title),
                    'post' => $post,
                ]);

                exit();
            }

            //PAGE TYPE
            $template = 'cristis.page';

            $get_template = webus_help::get_template_wordpress($post->ID);
            if($get_template) {$template = 'cristis.templates.'.$get_template;}
            if($get_template == 'news'){
                $get_news =  $query = DB::table('wp_posts')
                    ->where('post_status', 'publish')
                    ->where('post_type', 'post')
                    ->orderby('post_date', 'desc')
                    ->paginate(10);
            }else{
                $get_news = false;
            }



            return view($template, [
                'title' => webus_help::translate_automat($post->post_title),
                'post' => $post,
                'news' => $get_news
            ]);

        }else{
            return response()->view('site.404', [], 404);
        }




    }else{
        return response()->view('site.404', [], 404);
    }

}
?>
