A)
Creates dynamical content. Like FAQ question-answer, etc.

B)
Files it creates:
app/Http/Controllers/Modules/Test1Generator.php
resources/views/admin/Test1Generator/fields/field_text.blade.php
resources/views/admin/Test1Generator/fields/hidden.blade.php
resources/views/admin/Test1Generator/fields/languages.blade.php
resources/views/admin/Test1Generator/templates/1.blade.php
resources/views/front/modules/Test1Generator.blade.php
resources/views/webus/modules/Test1Generator.blade.php

C)
See examples in /documentation.

***

You need to:

1)
After php artisan make:generator Test1, copy file content from docs/examples/generator
to files from (B)

2) in views/webus/modules
edit:
@foreach($model->blocks as $block)...@include('admin.NAME!!!.Templates.

edit:
<noscript id="template_type_1">
        @include('admin.NAME!!!.Templates.1')
</noscript>

3)
edit:
\views\admin\Test1Generator\templates\1.blade.php
@include('admin.NAME!!.fields.field_text

----------------------------------------------------------------------
Manually:

mkdir and create files:
1) resources/views/admin/Generator
+templates+fields

2) resources/views/webus/modules/Generator.blade.php  (!! file name == module name)
3) copy store_data, create, edit functions to app/Http/Controllers/Modules/Generator.php
4) edit @include in resources/views/webus/modules/Generator.blade.php
file example in documentation/example
5)
use App\WebusModels\Modules;
use Session;
app/Http/Controllers/Modules/Generator.php