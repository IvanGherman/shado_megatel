var tinyButtonsList = 'shortcodes';

function tinyButtons(editor) {

    editor.addButton('shortcodes', {
        type: 'menubutton',
        text: 'Shortcodes',
        icon: false,
        menu: [
            {
                text: 'Button',
                onclick: function () {
                    editor.insertContent("[custom_button url=''][/custom_button]");
                }
            },
            {
                text: 'Gallery',
                onclick: function () {
                    editor.insertContent('[gallery][/gallery]');
                }
            },
            {
                text: 'Blockquote',
                onclick: function () {
                    editor.insertContent('[custom_blockquote][/custom_blockquote]');
                }
            },
            {
                text: 'Video',
                onclick: function() {
                    editor.insertContent('[video image="" id="" title=""][/video]')
                }
            }
        ]
    });
}
