$(document).ready(function(){
  var appContainer = $(".app-container"),
      sidebarAnchor = $('#sidebar-anchor'),
      fadedOverlay = $('.fadetoblack'),
      hamburger = $('.hamburger');

  $('.side-menu').perfectScrollbar();

  $('#voyager-loader').fadeOut();

  $(".hamburger, .navbar-expand-toggle, .side-menu .navbar-nav li:not(.dropdown)").on('click', function() {
    if ($(this).is('button')) {
      appContainer.toggleClass("expanded");
      $(this).toggleClass('is-active');
    } else {
      if (!sidebarAnchor.hasClass('active')) {
        appContainer.removeClass("expanded");
        hamburger.toggleClass('is-active');
      }
    }
  });

  fadedOverlay.on('click', function(){
    appContainer.removeClass('expanded');
    hamburger.removeClass('is-active');
  });

  sidebarAnchor.on('click', function(){
    if (appContainer.hasClass('expanded')) {
      if ($(this).hasClass('active')) {
        appContainer.removeClass("expanded");
        $(this).removeClass('active');
        window.localStorage.removeItem('voyager.stickySidebar');
        toastr.success("Sidebar isn't sticky anymore.");

        sidebarAnchor[0].title = sidebarAnchor.data('sticky');
      }
      else {
        $(this).addClass('active');
        window.localStorage.setItem('voyager.stickySidebar', true);
        toastr.success("Sidebar is now sticky");

        sidebarAnchor.data('sticky', sidebarAnchor[0].title);
        sidebarAnchor[0].title = sidebarAnchor.data('unstick');
      }
    }
    else {
      appContainer.addClass("expanded");
      $(this).removeClass('active');
      window.localStorage.removeItem('voyager.stickySidebar');
      toastr.success("Sidebar isn't sticky anymore.");

      sidebarAnchor[0].title = sidebarAnchor.data('sticky');
    }
  });

  $('select.select2').select2({ width: '100%' });

  $('.match-height').matchHeight();

  // $('.datatable').DataTable({
  //   "dom": '<"top"fl<"clear">>rt<"bottom"ip<"clear">>'
  // });

  $(".side-menu .nav .dropdown").on('show.bs.collapse', function() {
    return $(".side-menu .nav .dropdown .collapse").collapse('hide');
  });

  $(document).on('click', '.panel-heading a.panel-action[data-toggle="panel-collapse"]', function(e){
    e.preventDefault();
    var $this = $(this);

    // Toggle Collapse
    if(!$this.hasClass('panel-collapsed')) {
      $this.parents('.panel').find('.panel-body').slideUp();
      $this.addClass('panel-collapsed');
      $this.removeClass('voyager-angle-down').addClass('voyager-angle-up');
    } else {
      $this.parents('.panel').find('.panel-body').slideDown();
      $this.removeClass('panel-collapsed');
      $this.removeClass('voyager-angle-up').addClass('voyager-angle-down');
    }
  });

  //Toggle fullscreen
  $(document).on('click', '.panel-heading a.panel-action[data-toggle="panel-fullscreen"]', function (e) {
    e.preventDefault();
    var $this = $(this);
    if (!$this.hasClass('voyager-resize-full')) {
      $this.removeClass('voyager-resize-small').addClass('voyager-resize-full');
    } else {
      $this.removeClass('voyager-resize-full').addClass('voyager-resize-small');
    }
    $this.closest('.panel').toggleClass('is-fullscreen');
  });

  $('.datetimepicker').datetimepicker();

  $('.datepicker').datetimepicker({
    //defaultDate: new Date(),
    format:'YYYY-MM-DD'
  });

  // Right navbar toggle
  $('.navbar-right-expand-toggle').on('click', function(){
    $('ul.navbar-right').toggleClass('expanded');
  });

  // Save shortcut
  $(document).keydown(function (e){
    if ((e.metaKey || e.ctrlKey) && e.keyCode == 83) { /*ctrl+s or command+s*/
      $(".btn.save").click();
      e.preventDefault();
      return false;
    }
  });

  // Set active menu
  $('.side-menu .navbar-nav a').on('click', function() {
    sessionStorage.setItem('menu', $(this).attr('href'));
  });

  if (!sessionStorage.getItem('menu')) {
    $('.side-menu .navbar-nav li:first-child').addClass('active');
  } else {
    // Sets active and open to selected page in the left column menu.
    $('.side-menu .navbar-nav a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li').addClass('active open');
    $('.side-menu .navbar-nav a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li').find('div').attr('aria-expanded', true).addClass('in');
  }

});



function toggle_all_checkboxes(source) {
  checkboxes = document.getElementsByName('posts[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
  if_is_checked_list_posts()
}

function if_is_checked_list_posts() {
  var checked = $('.checkbox-list-posts').is(":checked");
  if(checked){
    $('.mass_delete').removeAttr('disabled')
  }else{
    $('.mass_delete').attr('disabled', 'disabled')
  }
}