var regName = /dbn/gi
var regPhone = /dbp/gi
var regText = /dbt[0-9]/gi
var regEmail = /dbe/gi
var usersData = [];

fetch('https://jsonplaceholder.typicode.com/users')
    .then(function(response){
        return response.json()
    })
    .then(function(users){
        usersData = users;
        autoFill(users)
    })

var loremText = ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam libero justo, sollicitudin nec efficitur eu, bibendum eu nisl. In hac habitasse platea dictumst. Vestibulum iaculis iaculis tincidunt. Phasellus faucibus sapien at ligula viverra suscipit. Cras lacus est, porttitor sed hendrerit quis, molestie sollicitudin velit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris ut lectus consequat, lacinia lorem a, malesuada orci. Cras pretium condimentum odio sit amet molestie. Vivamus egestas augue suscipit elementum imperdiet. Donec facilisis ornare dui, sit amet luctus elit rutrum a. Fusce vulputate massa eu mi commodo, non dignissim orci dapibus. Maecenas in mi sed quam elementum efficitur.', 'Curabitur elementum lorem eget magna consectetur, non finibus purus eleifend. Morbi eu neque velit. Fusce eget aliquam sapien. Curabitur in condimentum felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam vel semper enim. Ut non enim venenatis, tempus elit ac, ultricies dolor. Cras placerat interdum mi, vel placerat nunc ultrices et. Etiam malesuada id sapien ac molestie. Cras condimentum mi sed elementum pellentesque. In magna nunc, tincidunt id purus non, ornare condimentum tortor. Donec porta quam quis odio rutrum, sit amet mattis orci rhoncus. Vivamus consequat justo sit amet dignissim pellentesque. Cras sit amet bibendum odio, ut porta urna. Morbi laoreet pretium sem et luctus. Nulla ante sem, euismod a eleifend vel, auctor vitae sem.', 'Etiam tristique pulvinar dolor pellentesque viverra. Nam sit amet dolor turpis. Pellentesque congue mauris porttitor elit laoreet malesuada. Sed ultrices, risus ut vestibulum gravida, ligula risus facilisis sapien, eget hendrerit urna felis ut nisl. Curabitur at nulla egestas, aliquet velit pulvinar, pretium ipsum. Aenean scelerisque felis et tortor malesuada tempus. Sed feugiat felis felis, non rhoncus est dapibus sit amet.', 'Nunc pellentesque et turpis id aliquam. Nulla porttitor tincidunt quam ac sodales. Vivamus nec nisl non lorem ullamcorper dictum. In hac habitasse platea dictumst. Nulla vitae purus neque. Mauris eget posuere odio. Sed id tincidunt augue. Vivamus posuere odio ut arcu dictum pulvinar. Vivamus id ipsum lorem.', 'Vivamus cursus condimentum ex, ut fringilla augue condimentum a. Etiam nulla ex, bibendum mattis massa id, hendrerit lobortis lectus. Vestibulum pharetra sagittis velit rhoncus convallis. Mauris scelerisque fringilla augue, eu accumsan magna vestibulum pellentesque. Proin congue eget nibh id efficitur. Donec sed molestie erat. Mauris mattis tincidunt semper. Phasellus a volutpat justo. In tellus dolor, vehicula et aliquam non, suscipit id tellus. Aliquam erat volutpat. Nam vel commodo neque. Mauris volutpat turpis sed ipsum posuere, at luctus risus semper. Quisque a erat non odio mattis elementum. Integer in finibus nibh. Nullam luctus vulputate odio, eu ultrices ante rutrum id.']

function autoFill(users){
    var usersData = users;

    $('input, textarea').on('input', function(){
        var data = $(this).val()

        if (data.match(regName)){
            var randomData = usersData[Math.round(Math.random() * usersData.length - 1)].name
            var newData = data.replace(regName, randomData)
            $(this).val(newData)
        }

        if (data.match(regPhone)){
            var randomData = usersData[Math.round(Math.random() * usersData.length - 1)].phone
            var newData = data.replace(regPhone, randomData)
            $(this).val(newData)
        }

        if (data.match(regEmail)){
            var randomData = usersData[Math.round(Math.random() * usersData.length - 1)].email
            var newData = data.replace(regEmail, randomData)
            $(this).val(newData)
        }

        if (data.match(regText)){
            var number = data.match(regText)[0].slice(-1)[0];
            if (number == 0) {number = 1}
            var newData = '';
            var counter = 0;

            for (var i = 0; i < number; i++){

                if(i == number-1){
                    newData += loremText[counter]
                }else{
                    newData += loremText[counter] + '\n\n'
                }

                if (counter >= loremText.length - 1) {
                    counter = 0
                } else {
                    counter++
                }
            }

            $(this).val(newData)
        }
    })
}