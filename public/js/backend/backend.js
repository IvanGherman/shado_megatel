console.log('Backend');

// S.on('.form', 'submit', function (e) {
//     e.preventDefault();
//     let thisForm = window.App.Validation(this, {
//         prealoder: true
//     })
//
//     if (thisForm.isValid) {
//         thisForm.startPreload()
//
//         setTimeout(() => {
//             thisForm.setSuccess()
//         }, 1000);
//     }
// });

// products page reinit fns


//         App.initSearchit()

//         let colorBoxes = S.find('.js-checkbox-color')
//         App.setActiveColor(colorBoxes)

//         App.initChangeImg()
//         App.initAccordions()
//         App.initFilters()
//


S.on('.review-form', 'submit', function (e) {
    e.preventDefault();
    console.log('here');
    var thisForm = window.App.Validation(this, {
        preloader: true
    });

    var parent = this;

    if (thisForm.isValid) {
        thisForm.startPreload();

        var json = toJSON( parent );

        axios.post('/review', json)
            .then(function (response) {
                console.log(response);
                thisForm.setSuccess();
            })
            .catch(function (error) {
                console.log(error);
            });
    }
});

S.on('.contact_form', 'submit', function (e) {
    e.preventDefault();
    console.log('here');
    var thisForm = window.App.Validation(this, {
        preloader: true
    });

    var parent = this;

    if (thisForm.isValid) {
        thisForm.startPreload();

        var json = toJSON( parent );

        axios.post('/contact', json)
            .then(function (response) {
                console.log(response);
                thisForm.setSuccess();
            })
            .catch(function (error) {
                console.log(error);
            });
    }
});

S.on('.jrequest-form', 'submit', function (e) {
    e.preventDefault();
    var thisForm = window.App.Validation(this, {
        preloader: true
    });

    var parent = this;

    if (thisForm.isValid) {
        thisForm.startPreload();

        var json = toJSON( parent );

        axios.post('/request-master', json)
            .then(function (response) {
                console.log(response);
                thisForm.setSuccess();
            })
            .catch(function (error) {
                console.log(error);
            });
    }
});

$('.no-price').on('click', function () {
    var id = $(this).data('id');
    $('#call-modal [name=product_id]').val(id);
});

var sent = 0;
S.on('#call-modal', 'submit', function (e) {
    e.preventDefault();
    var thisForm = window.App.Validation(this, {
        preloader: true
    });

    if (sent === 1) {
        return;
    }

    var parent = this;

    if (thisForm.isValid) {
        thisForm.startPreload();

        var json = toJSON( parent );
        sent = 1;

        axios.post('/call-request', json)
            .then(function (response) {
                thisForm.setSuccess();
                sent = 0;
            })
            .catch(function (error) {
                console.log(error);
            });
    }
});



function toJSON( form ) {
    var obj = {};
    var elements = form.querySelectorAll( "input, select, textarea" );
    for( var i = 0; i < elements.length; ++i ) {
        var element = elements[i];
        var name = element.name;
        var value = element.value;

        if( name ) {
            obj[ name ] = value;
        }
    }

    return obj;
}