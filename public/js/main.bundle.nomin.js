/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 56);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/*!
 * perfect-scrollbar v1.4.0
 * (c) 2018 Hyunje Jun
 * @license MIT
 */
function get(element) {
  return getComputedStyle(element);
}

function set(element, obj) {
  for (var key in obj) {
    var val = obj[key];
    if (typeof val === 'number') {
      val = val + "px";
    }
    element.style[key] = val;
  }
  return element;
}

function div(className) {
  var div = document.createElement('div');
  div.className = className;
  return div;
}

var elMatches =
  typeof Element !== 'undefined' &&
  (Element.prototype.matches ||
    Element.prototype.webkitMatchesSelector ||
    Element.prototype.mozMatchesSelector ||
    Element.prototype.msMatchesSelector);

function matches(element, query) {
  if (!elMatches) {
    throw new Error('No element matching method supported');
  }

  return elMatches.call(element, query);
}

function remove(element) {
  if (element.remove) {
    element.remove();
  } else {
    if (element.parentNode) {
      element.parentNode.removeChild(element);
    }
  }
}

function queryChildren(element, selector) {
  return Array.prototype.filter.call(element.children, function (child) { return matches(child, selector); }
  );
}

var cls = {
  main: 'ps',
  element: {
    thumb: function (x) { return ("ps__thumb-" + x); },
    rail: function (x) { return ("ps__rail-" + x); },
    consuming: 'ps__child--consume',
  },
  state: {
    focus: 'ps--focus',
    clicking: 'ps--clicking',
    active: function (x) { return ("ps--active-" + x); },
    scrolling: function (x) { return ("ps--scrolling-" + x); },
  },
};

/*
 * Helper methods
 */
var scrollingClassTimeout = { x: null, y: null };

function addScrollingClass(i, x) {
  var classList = i.element.classList;
  var className = cls.state.scrolling(x);

  if (classList.contains(className)) {
    clearTimeout(scrollingClassTimeout[x]);
  } else {
    classList.add(className);
  }
}

function removeScrollingClass(i, x) {
  scrollingClassTimeout[x] = setTimeout(
    function () { return i.isAlive && i.element.classList.remove(cls.state.scrolling(x)); },
    i.settings.scrollingThreshold
  );
}

function setScrollingClassInstantly(i, x) {
  addScrollingClass(i, x);
  removeScrollingClass(i, x);
}

var EventElement = function EventElement(element) {
  this.element = element;
  this.handlers = {};
};

var prototypeAccessors = { isEmpty: { configurable: true } };

EventElement.prototype.bind = function bind (eventName, handler) {
  if (typeof this.handlers[eventName] === 'undefined') {
    this.handlers[eventName] = [];
  }
  this.handlers[eventName].push(handler);
  this.element.addEventListener(eventName, handler, false);
};

EventElement.prototype.unbind = function unbind (eventName, target) {
    var this$1 = this;

  this.handlers[eventName] = this.handlers[eventName].filter(function (handler) {
    if (target && handler !== target) {
      return true;
    }
    this$1.element.removeEventListener(eventName, handler, false);
    return false;
  });
};

EventElement.prototype.unbindAll = function unbindAll () {
    var this$1 = this;

  for (var name in this$1.handlers) {
    this$1.unbind(name);
  }
};

prototypeAccessors.isEmpty.get = function () {
    var this$1 = this;

  return Object.keys(this.handlers).every(
    function (key) { return this$1.handlers[key].length === 0; }
  );
};

Object.defineProperties( EventElement.prototype, prototypeAccessors );

var EventManager = function EventManager() {
  this.eventElements = [];
};

EventManager.prototype.eventElement = function eventElement (element) {
  var ee = this.eventElements.filter(function (ee) { return ee.element === element; })[0];
  if (!ee) {
    ee = new EventElement(element);
    this.eventElements.push(ee);
  }
  return ee;
};

EventManager.prototype.bind = function bind (element, eventName, handler) {
  this.eventElement(element).bind(eventName, handler);
};

EventManager.prototype.unbind = function unbind (element, eventName, handler) {
  var ee = this.eventElement(element);
  ee.unbind(eventName, handler);

  if (ee.isEmpty) {
    // remove
    this.eventElements.splice(this.eventElements.indexOf(ee), 1);
  }
};

EventManager.prototype.unbindAll = function unbindAll () {
  this.eventElements.forEach(function (e) { return e.unbindAll(); });
  this.eventElements = [];
};

EventManager.prototype.once = function once (element, eventName, handler) {
  var ee = this.eventElement(element);
  var onceHandler = function (evt) {
    ee.unbind(eventName, onceHandler);
    handler(evt);
  };
  ee.bind(eventName, onceHandler);
};

function createEvent(name) {
  if (typeof window.CustomEvent === 'function') {
    return new CustomEvent(name);
  } else {
    var evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(name, false, false, undefined);
    return evt;
  }
}

var processScrollDiff = function(
  i,
  axis,
  diff,
  useScrollingClass,
  forceFireReachEvent
) {
  if ( useScrollingClass === void 0 ) useScrollingClass = true;
  if ( forceFireReachEvent === void 0 ) forceFireReachEvent = false;

  var fields;
  if (axis === 'top') {
    fields = [
      'contentHeight',
      'containerHeight',
      'scrollTop',
      'y',
      'up',
      'down' ];
  } else if (axis === 'left') {
    fields = [
      'contentWidth',
      'containerWidth',
      'scrollLeft',
      'x',
      'left',
      'right' ];
  } else {
    throw new Error('A proper axis should be provided');
  }

  processScrollDiff$1(i, diff, fields, useScrollingClass, forceFireReachEvent);
};

function processScrollDiff$1(
  i,
  diff,
  ref,
  useScrollingClass,
  forceFireReachEvent
) {
  var contentHeight = ref[0];
  var containerHeight = ref[1];
  var scrollTop = ref[2];
  var y = ref[3];
  var up = ref[4];
  var down = ref[5];
  if ( useScrollingClass === void 0 ) useScrollingClass = true;
  if ( forceFireReachEvent === void 0 ) forceFireReachEvent = false;

  var element = i.element;

  // reset reach
  i.reach[y] = null;

  // 1 for subpixel rounding
  if (element[scrollTop] < 1) {
    i.reach[y] = 'start';
  }

  // 1 for subpixel rounding
  if (element[scrollTop] > i[contentHeight] - i[containerHeight] - 1) {
    i.reach[y] = 'end';
  }

  if (diff) {
    element.dispatchEvent(createEvent(("ps-scroll-" + y)));

    if (diff < 0) {
      element.dispatchEvent(createEvent(("ps-scroll-" + up)));
    } else if (diff > 0) {
      element.dispatchEvent(createEvent(("ps-scroll-" + down)));
    }

    if (useScrollingClass) {
      setScrollingClassInstantly(i, y);
    }
  }

  if (i.reach[y] && (diff || forceFireReachEvent)) {
    element.dispatchEvent(createEvent(("ps-" + y + "-reach-" + (i.reach[y]))));
  }
}

function toInt(x) {
  return parseInt(x, 10) || 0;
}

function isEditable(el) {
  return (
    matches(el, 'input,[contenteditable]') ||
    matches(el, 'select,[contenteditable]') ||
    matches(el, 'textarea,[contenteditable]') ||
    matches(el, 'button,[contenteditable]')
  );
}

function outerWidth(element) {
  var styles = get(element);
  return (
    toInt(styles.width) +
    toInt(styles.paddingLeft) +
    toInt(styles.paddingRight) +
    toInt(styles.borderLeftWidth) +
    toInt(styles.borderRightWidth)
  );
}

var env = {
  isWebKit:
    typeof document !== 'undefined' &&
    'WebkitAppearance' in document.documentElement.style,
  supportsTouch:
    typeof window !== 'undefined' &&
    ('ontouchstart' in window ||
      (window.DocumentTouch && document instanceof window.DocumentTouch)),
  supportsIePointer:
    typeof navigator !== 'undefined' && navigator.msMaxTouchPoints,
  isChrome:
    typeof navigator !== 'undefined' &&
    /Chrome/i.test(navigator && navigator.userAgent),
};

var updateGeometry = function(i) {
  var element = i.element;
  var roundedScrollTop = Math.floor(element.scrollTop);

  i.containerWidth = element.clientWidth;
  i.containerHeight = element.clientHeight;
  i.contentWidth = element.scrollWidth;
  i.contentHeight = element.scrollHeight;

  if (!element.contains(i.scrollbarXRail)) {
    // clean up and append
    queryChildren(element, cls.element.rail('x')).forEach(function (el) { return remove(el); }
    );
    element.appendChild(i.scrollbarXRail);
  }
  if (!element.contains(i.scrollbarYRail)) {
    // clean up and append
    queryChildren(element, cls.element.rail('y')).forEach(function (el) { return remove(el); }
    );
    element.appendChild(i.scrollbarYRail);
  }

  if (
    !i.settings.suppressScrollX &&
    i.containerWidth + i.settings.scrollXMarginOffset < i.contentWidth
  ) {
    i.scrollbarXActive = true;
    i.railXWidth = i.containerWidth - i.railXMarginWidth;
    i.railXRatio = i.containerWidth / i.railXWidth;
    i.scrollbarXWidth = getThumbSize(
      i,
      toInt(i.railXWidth * i.containerWidth / i.contentWidth)
    );
    i.scrollbarXLeft = toInt(
      (i.negativeScrollAdjustment + element.scrollLeft) *
        (i.railXWidth - i.scrollbarXWidth) /
        (i.contentWidth - i.containerWidth)
    );
  } else {
    i.scrollbarXActive = false;
  }

  if (
    !i.settings.suppressScrollY &&
    i.containerHeight + i.settings.scrollYMarginOffset < i.contentHeight
  ) {
    i.scrollbarYActive = true;
    i.railYHeight = i.containerHeight - i.railYMarginHeight;
    i.railYRatio = i.containerHeight / i.railYHeight;
    i.scrollbarYHeight = getThumbSize(
      i,
      toInt(i.railYHeight * i.containerHeight / i.contentHeight)
    );
    i.scrollbarYTop = toInt(
      roundedScrollTop *
        (i.railYHeight - i.scrollbarYHeight) /
        (i.contentHeight - i.containerHeight)
    );
  } else {
    i.scrollbarYActive = false;
  }

  if (i.scrollbarXLeft >= i.railXWidth - i.scrollbarXWidth) {
    i.scrollbarXLeft = i.railXWidth - i.scrollbarXWidth;
  }
  if (i.scrollbarYTop >= i.railYHeight - i.scrollbarYHeight) {
    i.scrollbarYTop = i.railYHeight - i.scrollbarYHeight;
  }

  updateCss(element, i);

  if (i.scrollbarXActive) {
    element.classList.add(cls.state.active('x'));
  } else {
    element.classList.remove(cls.state.active('x'));
    i.scrollbarXWidth = 0;
    i.scrollbarXLeft = 0;
    element.scrollLeft = 0;
  }
  if (i.scrollbarYActive) {
    element.classList.add(cls.state.active('y'));
  } else {
    element.classList.remove(cls.state.active('y'));
    i.scrollbarYHeight = 0;
    i.scrollbarYTop = 0;
    element.scrollTop = 0;
  }
};

function getThumbSize(i, thumbSize) {
  if (i.settings.minScrollbarLength) {
    thumbSize = Math.max(thumbSize, i.settings.minScrollbarLength);
  }
  if (i.settings.maxScrollbarLength) {
    thumbSize = Math.min(thumbSize, i.settings.maxScrollbarLength);
  }
  return thumbSize;
}

function updateCss(element, i) {
  var xRailOffset = { width: i.railXWidth };
  var roundedScrollTop = Math.floor(element.scrollTop);

  if (i.isRtl) {
    xRailOffset.left =
      i.negativeScrollAdjustment +
      element.scrollLeft +
      i.containerWidth -
      i.contentWidth;
  } else {
    xRailOffset.left = element.scrollLeft;
  }
  if (i.isScrollbarXUsingBottom) {
    xRailOffset.bottom = i.scrollbarXBottom - roundedScrollTop;
  } else {
    xRailOffset.top = i.scrollbarXTop + roundedScrollTop;
  }
  set(i.scrollbarXRail, xRailOffset);

  var yRailOffset = { top: roundedScrollTop, height: i.railYHeight };
  if (i.isScrollbarYUsingRight) {
    if (i.isRtl) {
      yRailOffset.right =
        i.contentWidth -
        (i.negativeScrollAdjustment + element.scrollLeft) -
        i.scrollbarYRight -
        i.scrollbarYOuterWidth;
    } else {
      yRailOffset.right = i.scrollbarYRight - element.scrollLeft;
    }
  } else {
    if (i.isRtl) {
      yRailOffset.left =
        i.negativeScrollAdjustment +
        element.scrollLeft +
        i.containerWidth * 2 -
        i.contentWidth -
        i.scrollbarYLeft -
        i.scrollbarYOuterWidth;
    } else {
      yRailOffset.left = i.scrollbarYLeft + element.scrollLeft;
    }
  }
  set(i.scrollbarYRail, yRailOffset);

  set(i.scrollbarX, {
    left: i.scrollbarXLeft,
    width: i.scrollbarXWidth - i.railBorderXWidth,
  });
  set(i.scrollbarY, {
    top: i.scrollbarYTop,
    height: i.scrollbarYHeight - i.railBorderYWidth,
  });
}

var clickRail = function(i) {
  i.event.bind(i.scrollbarY, 'mousedown', function (e) { return e.stopPropagation(); });
  i.event.bind(i.scrollbarYRail, 'mousedown', function (e) {
    var positionTop =
      e.pageY -
      window.pageYOffset -
      i.scrollbarYRail.getBoundingClientRect().top;
    var direction = positionTop > i.scrollbarYTop ? 1 : -1;

    i.element.scrollTop += direction * i.containerHeight;
    updateGeometry(i);

    e.stopPropagation();
  });

  i.event.bind(i.scrollbarX, 'mousedown', function (e) { return e.stopPropagation(); });
  i.event.bind(i.scrollbarXRail, 'mousedown', function (e) {
    var positionLeft =
      e.pageX -
      window.pageXOffset -
      i.scrollbarXRail.getBoundingClientRect().left;
    var direction = positionLeft > i.scrollbarXLeft ? 1 : -1;

    i.element.scrollLeft += direction * i.containerWidth;
    updateGeometry(i);

    e.stopPropagation();
  });
};

var dragThumb = function(i) {
  bindMouseScrollHandler(i, [
    'containerWidth',
    'contentWidth',
    'pageX',
    'railXWidth',
    'scrollbarX',
    'scrollbarXWidth',
    'scrollLeft',
    'x',
    'scrollbarXRail' ]);
  bindMouseScrollHandler(i, [
    'containerHeight',
    'contentHeight',
    'pageY',
    'railYHeight',
    'scrollbarY',
    'scrollbarYHeight',
    'scrollTop',
    'y',
    'scrollbarYRail' ]);
};

function bindMouseScrollHandler(
  i,
  ref
) {
  var containerHeight = ref[0];
  var contentHeight = ref[1];
  var pageY = ref[2];
  var railYHeight = ref[3];
  var scrollbarY = ref[4];
  var scrollbarYHeight = ref[5];
  var scrollTop = ref[6];
  var y = ref[7];
  var scrollbarYRail = ref[8];

  var element = i.element;

  var startingScrollTop = null;
  var startingMousePageY = null;
  var scrollBy = null;

  function mouseMoveHandler(e) {
    element[scrollTop] =
      startingScrollTop + scrollBy * (e[pageY] - startingMousePageY);
    addScrollingClass(i, y);
    updateGeometry(i);

    e.stopPropagation();
    e.preventDefault();
  }

  function mouseUpHandler() {
    removeScrollingClass(i, y);
    i[scrollbarYRail].classList.remove(cls.state.clicking);
    i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
  }

  i.event.bind(i[scrollbarY], 'mousedown', function (e) {
    startingScrollTop = element[scrollTop];
    startingMousePageY = e[pageY];
    scrollBy =
      (i[contentHeight] - i[containerHeight]) /
      (i[railYHeight] - i[scrollbarYHeight]);

    i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
    i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);

    i[scrollbarYRail].classList.add(cls.state.clicking);

    e.stopPropagation();
    e.preventDefault();
  });
}

var keyboard = function(i) {
  var element = i.element;

  var elementHovered = function () { return matches(element, ':hover'); };
  var scrollbarFocused = function () { return matches(i.scrollbarX, ':focus') || matches(i.scrollbarY, ':focus'); };

  function shouldPreventDefault(deltaX, deltaY) {
    var scrollTop = Math.floor(element.scrollTop);
    if (deltaX === 0) {
      if (!i.scrollbarYActive) {
        return false;
      }
      if (
        (scrollTop === 0 && deltaY > 0) ||
        (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)
      ) {
        return !i.settings.wheelPropagation;
      }
    }

    var scrollLeft = element.scrollLeft;
    if (deltaY === 0) {
      if (!i.scrollbarXActive) {
        return false;
      }
      if (
        (scrollLeft === 0 && deltaX < 0) ||
        (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)
      ) {
        return !i.settings.wheelPropagation;
      }
    }
    return true;
  }

  i.event.bind(i.ownerDocument, 'keydown', function (e) {
    if (
      (e.isDefaultPrevented && e.isDefaultPrevented()) ||
      e.defaultPrevented
    ) {
      return;
    }

    if (!elementHovered() && !scrollbarFocused()) {
      return;
    }

    var activeElement = document.activeElement
      ? document.activeElement
      : i.ownerDocument.activeElement;
    if (activeElement) {
      if (activeElement.tagName === 'IFRAME') {
        activeElement = activeElement.contentDocument.activeElement;
      } else {
        // go deeper if element is a webcomponent
        while (activeElement.shadowRoot) {
          activeElement = activeElement.shadowRoot.activeElement;
        }
      }
      if (isEditable(activeElement)) {
        return;
      }
    }

    var deltaX = 0;
    var deltaY = 0;

    switch (e.which) {
      case 37: // left
        if (e.metaKey) {
          deltaX = -i.contentWidth;
        } else if (e.altKey) {
          deltaX = -i.containerWidth;
        } else {
          deltaX = -30;
        }
        break;
      case 38: // up
        if (e.metaKey) {
          deltaY = i.contentHeight;
        } else if (e.altKey) {
          deltaY = i.containerHeight;
        } else {
          deltaY = 30;
        }
        break;
      case 39: // right
        if (e.metaKey) {
          deltaX = i.contentWidth;
        } else if (e.altKey) {
          deltaX = i.containerWidth;
        } else {
          deltaX = 30;
        }
        break;
      case 40: // down
        if (e.metaKey) {
          deltaY = -i.contentHeight;
        } else if (e.altKey) {
          deltaY = -i.containerHeight;
        } else {
          deltaY = -30;
        }
        break;
      case 32: // space bar
        if (e.shiftKey) {
          deltaY = i.containerHeight;
        } else {
          deltaY = -i.containerHeight;
        }
        break;
      case 33: // page up
        deltaY = i.containerHeight;
        break;
      case 34: // page down
        deltaY = -i.containerHeight;
        break;
      case 36: // home
        deltaY = i.contentHeight;
        break;
      case 35: // end
        deltaY = -i.contentHeight;
        break;
      default:
        return;
    }

    if (i.settings.suppressScrollX && deltaX !== 0) {
      return;
    }
    if (i.settings.suppressScrollY && deltaY !== 0) {
      return;
    }

    element.scrollTop -= deltaY;
    element.scrollLeft += deltaX;
    updateGeometry(i);

    if (shouldPreventDefault(deltaX, deltaY)) {
      e.preventDefault();
    }
  });
};

var wheel = function(i) {
  var element = i.element;

  function shouldPreventDefault(deltaX, deltaY) {
    var roundedScrollTop = Math.floor(element.scrollTop);
    var isTop = element.scrollTop === 0;
    var isBottom =
      roundedScrollTop + element.offsetHeight === element.scrollHeight;
    var isLeft = element.scrollLeft === 0;
    var isRight =
      element.scrollLeft + element.offsetWidth === element.scrollWidth;

    var hitsBound;

    // pick axis with primary direction
    if (Math.abs(deltaY) > Math.abs(deltaX)) {
      hitsBound = isTop || isBottom;
    } else {
      hitsBound = isLeft || isRight;
    }

    return hitsBound ? !i.settings.wheelPropagation : true;
  }

  function getDeltaFromEvent(e) {
    var deltaX = e.deltaX;
    var deltaY = -1 * e.deltaY;

    if (typeof deltaX === 'undefined' || typeof deltaY === 'undefined') {
      // OS X Safari
      deltaX = -1 * e.wheelDeltaX / 6;
      deltaY = e.wheelDeltaY / 6;
    }

    if (e.deltaMode && e.deltaMode === 1) {
      // Firefox in deltaMode 1: Line scrolling
      deltaX *= 10;
      deltaY *= 10;
    }

    if (deltaX !== deltaX && deltaY !== deltaY /* NaN checks */) {
      // IE in some mouse drivers
      deltaX = 0;
      deltaY = e.wheelDelta;
    }

    if (e.shiftKey) {
      // reverse axis with shift key
      return [-deltaY, -deltaX];
    }
    return [deltaX, deltaY];
  }

  function shouldBeConsumedByChild(target, deltaX, deltaY) {
    // FIXME: this is a workaround for <select> issue in FF and IE #571
    if (!env.isWebKit && element.querySelector('select:focus')) {
      return true;
    }

    if (!element.contains(target)) {
      return false;
    }

    var cursor = target;

    while (cursor && cursor !== element) {
      if (cursor.classList.contains(cls.element.consuming)) {
        return true;
      }

      var style = get(cursor);
      var overflow = [style.overflow, style.overflowX, style.overflowY].join(
        ''
      );

      // if scrollable
      if (overflow.match(/(scroll|auto)/)) {
        var maxScrollTop = cursor.scrollHeight - cursor.clientHeight;
        if (maxScrollTop > 0) {
          if (
            !(cursor.scrollTop === 0 && deltaY > 0) &&
            !(cursor.scrollTop === maxScrollTop && deltaY < 0)
          ) {
            return true;
          }
        }
        var maxScrollLeft = cursor.scrollWidth - cursor.clientWidth;
        if (maxScrollLeft > 0) {
          if (
            !(cursor.scrollLeft === 0 && deltaX < 0) &&
            !(cursor.scrollLeft === maxScrollLeft && deltaX > 0)
          ) {
            return true;
          }
        }
      }

      cursor = cursor.parentNode;
    }

    return false;
  }

  function mousewheelHandler(e) {
    var ref = getDeltaFromEvent(e);
    var deltaX = ref[0];
    var deltaY = ref[1];

    if (shouldBeConsumedByChild(e.target, deltaX, deltaY)) {
      return;
    }

    var shouldPrevent = false;
    if (!i.settings.useBothWheelAxes) {
      // deltaX will only be used for horizontal scrolling and deltaY will
      // only be used for vertical scrolling - this is the default
      element.scrollTop -= deltaY * i.settings.wheelSpeed;
      element.scrollLeft += deltaX * i.settings.wheelSpeed;
    } else if (i.scrollbarYActive && !i.scrollbarXActive) {
      // only vertical scrollbar is active and useBothWheelAxes option is
      // active, so let's scroll vertical bar using both mouse wheel axes
      if (deltaY) {
        element.scrollTop -= deltaY * i.settings.wheelSpeed;
      } else {
        element.scrollTop += deltaX * i.settings.wheelSpeed;
      }
      shouldPrevent = true;
    } else if (i.scrollbarXActive && !i.scrollbarYActive) {
      // useBothWheelAxes and only horizontal bar is active, so use both
      // wheel axes for horizontal bar
      if (deltaX) {
        element.scrollLeft += deltaX * i.settings.wheelSpeed;
      } else {
        element.scrollLeft -= deltaY * i.settings.wheelSpeed;
      }
      shouldPrevent = true;
    }

    updateGeometry(i);

    shouldPrevent = shouldPrevent || shouldPreventDefault(deltaX, deltaY);
    if (shouldPrevent && !e.ctrlKey) {
      e.stopPropagation();
      e.preventDefault();
    }
  }

  if (typeof window.onwheel !== 'undefined') {
    i.event.bind(element, 'wheel', mousewheelHandler);
  } else if (typeof window.onmousewheel !== 'undefined') {
    i.event.bind(element, 'mousewheel', mousewheelHandler);
  }
};

var touch = function(i) {
  if (!env.supportsTouch && !env.supportsIePointer) {
    return;
  }

  var element = i.element;

  function shouldPrevent(deltaX, deltaY) {
    var scrollTop = Math.floor(element.scrollTop);
    var scrollLeft = element.scrollLeft;
    var magnitudeX = Math.abs(deltaX);
    var magnitudeY = Math.abs(deltaY);

    if (magnitudeY > magnitudeX) {
      // user is perhaps trying to swipe up/down the page

      if (
        (deltaY < 0 && scrollTop === i.contentHeight - i.containerHeight) ||
        (deltaY > 0 && scrollTop === 0)
      ) {
        // set prevent for mobile Chrome refresh
        return window.scrollY === 0 && deltaY > 0 && env.isChrome;
      }
    } else if (magnitudeX > magnitudeY) {
      // user is perhaps trying to swipe left/right across the page

      if (
        (deltaX < 0 && scrollLeft === i.contentWidth - i.containerWidth) ||
        (deltaX > 0 && scrollLeft === 0)
      ) {
        return true;
      }
    }

    return true;
  }

  function applyTouchMove(differenceX, differenceY) {
    element.scrollTop -= differenceY;
    element.scrollLeft -= differenceX;

    updateGeometry(i);
  }

  var startOffset = {};
  var startTime = 0;
  var speed = {};
  var easingLoop = null;

  function getTouch(e) {
    if (e.targetTouches) {
      return e.targetTouches[0];
    } else {
      // Maybe IE pointer
      return e;
    }
  }

  function shouldHandle(e) {
    if (e.pointerType && e.pointerType === 'pen' && e.buttons === 0) {
      return false;
    }
    if (e.targetTouches && e.targetTouches.length === 1) {
      return true;
    }
    if (
      e.pointerType &&
      e.pointerType !== 'mouse' &&
      e.pointerType !== e.MSPOINTER_TYPE_MOUSE
    ) {
      return true;
    }
    return false;
  }

  function touchStart(e) {
    if (!shouldHandle(e)) {
      return;
    }

    var touch = getTouch(e);

    startOffset.pageX = touch.pageX;
    startOffset.pageY = touch.pageY;

    startTime = new Date().getTime();

    if (easingLoop !== null) {
      clearInterval(easingLoop);
    }
  }

  function shouldBeConsumedByChild(target, deltaX, deltaY) {
    if (!element.contains(target)) {
      return false;
    }

    var cursor = target;

    while (cursor && cursor !== element) {
      if (cursor.classList.contains(cls.element.consuming)) {
        return true;
      }

      var style = get(cursor);
      var overflow = [style.overflow, style.overflowX, style.overflowY].join(
        ''
      );

      // if scrollable
      if (overflow.match(/(scroll|auto)/)) {
        var maxScrollTop = cursor.scrollHeight - cursor.clientHeight;
        if (maxScrollTop > 0) {
          if (
            !(cursor.scrollTop === 0 && deltaY > 0) &&
            !(cursor.scrollTop === maxScrollTop && deltaY < 0)
          ) {
            return true;
          }
        }
        var maxScrollLeft = cursor.scrollLeft - cursor.clientWidth;
        if (maxScrollLeft > 0) {
          if (
            !(cursor.scrollLeft === 0 && deltaX < 0) &&
            !(cursor.scrollLeft === maxScrollLeft && deltaX > 0)
          ) {
            return true;
          }
        }
      }

      cursor = cursor.parentNode;
    }

    return false;
  }

  function touchMove(e) {
    if (shouldHandle(e)) {
      var touch = getTouch(e);

      var currentOffset = { pageX: touch.pageX, pageY: touch.pageY };

      var differenceX = currentOffset.pageX - startOffset.pageX;
      var differenceY = currentOffset.pageY - startOffset.pageY;

      if (shouldBeConsumedByChild(e.target, differenceX, differenceY)) {
        return;
      }

      applyTouchMove(differenceX, differenceY);
      startOffset = currentOffset;

      var currentTime = new Date().getTime();

      var timeGap = currentTime - startTime;
      if (timeGap > 0) {
        speed.x = differenceX / timeGap;
        speed.y = differenceY / timeGap;
        startTime = currentTime;
      }

      if (shouldPrevent(differenceX, differenceY)) {
        e.preventDefault();
      }
    }
  }
  function touchEnd() {
    if (i.settings.swipeEasing) {
      clearInterval(easingLoop);
      easingLoop = setInterval(function() {
        if (i.isInitialized) {
          clearInterval(easingLoop);
          return;
        }

        if (!speed.x && !speed.y) {
          clearInterval(easingLoop);
          return;
        }

        if (Math.abs(speed.x) < 0.01 && Math.abs(speed.y) < 0.01) {
          clearInterval(easingLoop);
          return;
        }

        applyTouchMove(speed.x * 30, speed.y * 30);

        speed.x *= 0.8;
        speed.y *= 0.8;
      }, 10);
    }
  }

  if (env.supportsTouch) {
    i.event.bind(element, 'touchstart', touchStart);
    i.event.bind(element, 'touchmove', touchMove);
    i.event.bind(element, 'touchend', touchEnd);
  } else if (env.supportsIePointer) {
    if (window.PointerEvent) {
      i.event.bind(element, 'pointerdown', touchStart);
      i.event.bind(element, 'pointermove', touchMove);
      i.event.bind(element, 'pointerup', touchEnd);
    } else if (window.MSPointerEvent) {
      i.event.bind(element, 'MSPointerDown', touchStart);
      i.event.bind(element, 'MSPointerMove', touchMove);
      i.event.bind(element, 'MSPointerUp', touchEnd);
    }
  }
};

var defaultSettings = function () { return ({
  handlers: ['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch'],
  maxScrollbarLength: null,
  minScrollbarLength: null,
  scrollingThreshold: 1000,
  scrollXMarginOffset: 0,
  scrollYMarginOffset: 0,
  suppressScrollX: false,
  suppressScrollY: false,
  swipeEasing: true,
  useBothWheelAxes: false,
  wheelPropagation: true,
  wheelSpeed: 1,
}); };

var handlers = {
  'click-rail': clickRail,
  'drag-thumb': dragThumb,
  keyboard: keyboard,
  wheel: wheel,
  touch: touch,
};

var PerfectScrollbar = function PerfectScrollbar(element, userSettings) {
  var this$1 = this;
  if ( userSettings === void 0 ) userSettings = {};

  if (typeof element === 'string') {
    element = document.querySelector(element);
  }

  if (!element || !element.nodeName) {
    throw new Error('no element is specified to initialize PerfectScrollbar');
  }

  this.element = element;

  element.classList.add(cls.main);

  this.settings = defaultSettings();
  for (var key in userSettings) {
    this$1.settings[key] = userSettings[key];
  }

  this.containerWidth = null;
  this.containerHeight = null;
  this.contentWidth = null;
  this.contentHeight = null;

  var focus = function () { return element.classList.add(cls.state.focus); };
  var blur = function () { return element.classList.remove(cls.state.focus); };

  this.isRtl = get(element).direction === 'rtl';
  this.isNegativeScroll = (function () {
    var originalScrollLeft = element.scrollLeft;
    var result = null;
    element.scrollLeft = -1;
    result = element.scrollLeft < 0;
    element.scrollLeft = originalScrollLeft;
    return result;
  })();
  this.negativeScrollAdjustment = this.isNegativeScroll
    ? element.scrollWidth - element.clientWidth
    : 0;
  this.event = new EventManager();
  this.ownerDocument = element.ownerDocument || document;

  this.scrollbarXRail = div(cls.element.rail('x'));
  element.appendChild(this.scrollbarXRail);
  this.scrollbarX = div(cls.element.thumb('x'));
  this.scrollbarXRail.appendChild(this.scrollbarX);
  this.scrollbarX.setAttribute('tabindex', 0);
  this.event.bind(this.scrollbarX, 'focus', focus);
  this.event.bind(this.scrollbarX, 'blur', blur);
  this.scrollbarXActive = null;
  this.scrollbarXWidth = null;
  this.scrollbarXLeft = null;
  var railXStyle = get(this.scrollbarXRail);
  this.scrollbarXBottom = parseInt(railXStyle.bottom, 10);
  if (isNaN(this.scrollbarXBottom)) {
    this.isScrollbarXUsingBottom = false;
    this.scrollbarXTop = toInt(railXStyle.top);
  } else {
    this.isScrollbarXUsingBottom = true;
  }
  this.railBorderXWidth =
    toInt(railXStyle.borderLeftWidth) + toInt(railXStyle.borderRightWidth);
  // Set rail to display:block to calculate margins
  set(this.scrollbarXRail, { display: 'block' });
  this.railXMarginWidth =
    toInt(railXStyle.marginLeft) + toInt(railXStyle.marginRight);
  set(this.scrollbarXRail, { display: '' });
  this.railXWidth = null;
  this.railXRatio = null;

  this.scrollbarYRail = div(cls.element.rail('y'));
  element.appendChild(this.scrollbarYRail);
  this.scrollbarY = div(cls.element.thumb('y'));
  this.scrollbarYRail.appendChild(this.scrollbarY);
  this.scrollbarY.setAttribute('tabindex', 0);
  this.event.bind(this.scrollbarY, 'focus', focus);
  this.event.bind(this.scrollbarY, 'blur', blur);
  this.scrollbarYActive = null;
  this.scrollbarYHeight = null;
  this.scrollbarYTop = null;
  var railYStyle = get(this.scrollbarYRail);
  this.scrollbarYRight = parseInt(railYStyle.right, 10);
  if (isNaN(this.scrollbarYRight)) {
    this.isScrollbarYUsingRight = false;
    this.scrollbarYLeft = toInt(railYStyle.left);
  } else {
    this.isScrollbarYUsingRight = true;
  }
  this.scrollbarYOuterWidth = this.isRtl ? outerWidth(this.scrollbarY) : null;
  this.railBorderYWidth =
    toInt(railYStyle.borderTopWidth) + toInt(railYStyle.borderBottomWidth);
  set(this.scrollbarYRail, { display: 'block' });
  this.railYMarginHeight =
    toInt(railYStyle.marginTop) + toInt(railYStyle.marginBottom);
  set(this.scrollbarYRail, { display: '' });
  this.railYHeight = null;
  this.railYRatio = null;

  this.reach = {
    x:
      element.scrollLeft <= 0
        ? 'start'
        : element.scrollLeft >= this.contentWidth - this.containerWidth
          ? 'end'
          : null,
    y:
      element.scrollTop <= 0
        ? 'start'
        : element.scrollTop >= this.contentHeight - this.containerHeight
          ? 'end'
          : null,
  };

  this.isAlive = true;

  this.settings.handlers.forEach(function (handlerName) { return handlers[handlerName](this$1); });

  this.lastScrollTop = Math.floor(element.scrollTop); // for onScroll only
  this.lastScrollLeft = element.scrollLeft; // for onScroll only
  this.event.bind(this.element, 'scroll', function (e) { return this$1.onScroll(e); });
  updateGeometry(this);
};

PerfectScrollbar.prototype.update = function update () {
  if (!this.isAlive) {
    return;
  }

  // Recalcuate negative scrollLeft adjustment
  this.negativeScrollAdjustment = this.isNegativeScroll
    ? this.element.scrollWidth - this.element.clientWidth
    : 0;

  // Recalculate rail margins
  set(this.scrollbarXRail, { display: 'block' });
  set(this.scrollbarYRail, { display: 'block' });
  this.railXMarginWidth =
    toInt(get(this.scrollbarXRail).marginLeft) +
    toInt(get(this.scrollbarXRail).marginRight);
  this.railYMarginHeight =
    toInt(get(this.scrollbarYRail).marginTop) +
    toInt(get(this.scrollbarYRail).marginBottom);

  // Hide scrollbars not to affect scrollWidth and scrollHeight
  set(this.scrollbarXRail, { display: 'none' });
  set(this.scrollbarYRail, { display: 'none' });

  updateGeometry(this);

  processScrollDiff(this, 'top', 0, false, true);
  processScrollDiff(this, 'left', 0, false, true);

  set(this.scrollbarXRail, { display: '' });
  set(this.scrollbarYRail, { display: '' });
};

PerfectScrollbar.prototype.onScroll = function onScroll (e) {
  if (!this.isAlive) {
    return;
  }

  updateGeometry(this);
  processScrollDiff(this, 'top', this.element.scrollTop - this.lastScrollTop);
  processScrollDiff(
    this,
    'left',
    this.element.scrollLeft - this.lastScrollLeft
  );

  this.lastScrollTop = Math.floor(this.element.scrollTop);
  this.lastScrollLeft = this.element.scrollLeft;
};

PerfectScrollbar.prototype.destroy = function destroy () {
  if (!this.isAlive) {
    return;
  }

  this.event.unbindAll();
  remove(this.scrollbarX);
  remove(this.scrollbarY);
  remove(this.scrollbarXRail);
  remove(this.scrollbarYRail);
  this.removePsClasses();

  // unset elements
  this.element = null;
  this.scrollbarX = null;
  this.scrollbarY = null;
  this.scrollbarXRail = null;
  this.scrollbarYRail = null;

  this.isAlive = false;
};

PerfectScrollbar.prototype.removePsClasses = function removePsClasses () {
  this.element.className = this.element.className
    .split(' ')
    .filter(function (name) { return !name.match(/^ps([-_].+|)$/); })
    .join(' ');
};

/* harmony default export */ __webpack_exports__["a"] = (PerfectScrollbar);


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as the `TypeError` message for "Functions" methods. */
var FUNC_ERROR_TEXT = 'Expected a function';

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        result = wait - timeSinceLastCall;

    return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

/**
 * Creates a throttled function that only invokes `func` at most once per
 * every `wait` milliseconds. The throttled function comes with a `cancel`
 * method to cancel delayed `func` invocations and a `flush` method to
 * immediately invoke them. Provide `options` to indicate whether `func`
 * should be invoked on the leading and/or trailing edge of the `wait`
 * timeout. The `func` is invoked with the last arguments provided to the
 * throttled function. Subsequent calls to the throttled function return the
 * result of the last `func` invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the throttled function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.throttle` and `_.debounce`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to throttle.
 * @param {number} [wait=0] The number of milliseconds to throttle invocations to.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=true]
 *  Specify invoking on the leading edge of the timeout.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new throttled function.
 * @example
 *
 * // Avoid excessively updating the position while scrolling.
 * jQuery(window).on('scroll', _.throttle(updatePosition, 100));
 *
 * // Invoke `renewToken` when the click event is fired, but not more than once every 5 minutes.
 * var throttled = _.throttle(renewToken, 300000, { 'trailing': false });
 * jQuery(element).on('click', throttled);
 *
 * // Cancel the trailing throttled invocation.
 * jQuery(window).on('popstate', throttled.cancel);
 */
function throttle(func, wait, options) {
  var leading = true,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  if (isObject(options)) {
    leading = 'leading' in options ? !!options.leading : leading;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }
  return debounce(func, wait, {
    'leading': leading,
    'maxWait': wait,
    'trailing': trailing
  });
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && objectToString.call(value) == symbolTag);
}

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = throttle;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(1)))

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! nouislider - 12.1.0 - 10/25/2018 */
!function(t){ true?!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):undefined}(function(){"use strict";var et="12.1.0";function s(t){return null!=t}function rt(t){t.preventDefault()}function i(t){return"number"==typeof t&&!isNaN(t)&&isFinite(t)}function nt(t,e,r){0<r&&(at(t,e),setTimeout(function(){st(t,e)},r))}function it(t){return Math.max(Math.min(t,100),0)}function ot(t){return Array.isArray(t)?t:[t]}function e(t){var e=(t=String(t)).split(".");return 1<e.length?e[1].length:0}function at(t,e){t.classList?t.classList.add(e):t.className+=" "+e}function st(t,e){t.classList?t.classList.remove(e):t.className=t.className.replace(new RegExp("(^|\\b)"+e.split(" ").join("|")+"(\\b|$)","gi")," ")}function lt(t){var e=void 0!==window.pageXOffset,r="CSS1Compat"===(t.compatMode||"");return{x:e?window.pageXOffset:r?t.documentElement.scrollLeft:t.body.scrollLeft,y:e?window.pageYOffset:r?t.documentElement.scrollTop:t.body.scrollTop}}function c(t,e){return 100/(e-t)}function p(t,e){return 100*e/(t[1]-t[0])}function f(t,e){for(var r=1;t>=e[r];)r+=1;return r}function r(t,e,r){if(r>=t.slice(-1)[0])return 100;var n,i,o=f(r,t),a=t[o-1],s=t[o],l=e[o-1],u=e[o];return l+(i=r,p(n=[a,s],n[0]<0?i+Math.abs(n[0]):i-n[0])/c(l,u))}function n(t,e,r,n){if(100===n)return n;var i,o,a=f(n,t),s=t[a-1],l=t[a];return r?(l-s)/2<n-s?l:s:e[a-1]?t[a-1]+(i=n-t[a-1],o=e[a-1],Math.round(i/o)*o):n}function o(t,e,r){var n;if("number"==typeof e&&(e=[e]),!Array.isArray(e))throw new Error("noUiSlider ("+et+"): 'range' contains invalid value.");if(!i(n="min"===t?0:"max"===t?100:parseFloat(t))||!i(e[0]))throw new Error("noUiSlider ("+et+"): 'range' value isn't numeric.");r.xPct.push(n),r.xVal.push(e[0]),n?r.xSteps.push(!isNaN(e[1])&&e[1]):isNaN(e[1])||(r.xSteps[0]=e[1]),r.xHighestCompleteStep.push(0)}function a(t,e,r){if(!e)return!0;r.xSteps[t]=p([r.xVal[t],r.xVal[t+1]],e)/c(r.xPct[t],r.xPct[t+1]);var n=(r.xVal[t+1]-r.xVal[t])/r.xNumSteps[t],i=Math.ceil(Number(n.toFixed(3))-1),o=r.xVal[t]+r.xNumSteps[t]*i;r.xHighestCompleteStep[t]=o}function l(t,e,r){var n;this.xPct=[],this.xVal=[],this.xSteps=[r||!1],this.xNumSteps=[!1],this.xHighestCompleteStep=[],this.snap=e;var i=[];for(n in t)t.hasOwnProperty(n)&&i.push([t[n],n]);for(i.length&&"object"==typeof i[0][0]?i.sort(function(t,e){return t[0][0]-e[0][0]}):i.sort(function(t,e){return t[0]-e[0]}),n=0;n<i.length;n++)o(i[n][1],i[n][0],this);for(this.xNumSteps=this.xSteps.slice(0),n=0;n<this.xNumSteps.length;n++)a(n,this.xNumSteps[n],this)}l.prototype.getMargin=function(t){var e=this.xNumSteps[0];if(e&&t/e%1!=0)throw new Error("noUiSlider ("+et+"): 'limit', 'margin' and 'padding' must be divisible by step.");return 2===this.xPct.length&&p(this.xVal,t)},l.prototype.toStepping=function(t){return t=r(this.xVal,this.xPct,t)},l.prototype.fromStepping=function(t){return function(t,e,r){if(100<=r)return t.slice(-1)[0];var n,i=f(r,e),o=t[i-1],a=t[i],s=e[i-1],l=e[i];return n=[o,a],(r-s)*c(s,l)*(n[1]-n[0])/100+n[0]}(this.xVal,this.xPct,t)},l.prototype.getStep=function(t){return t=n(this.xPct,this.xSteps,this.snap,t)},l.prototype.getNearbySteps=function(t){var e=f(t,this.xPct);return{stepBefore:{startValue:this.xVal[e-2],step:this.xNumSteps[e-2],highestStep:this.xHighestCompleteStep[e-2]},thisStep:{startValue:this.xVal[e-1],step:this.xNumSteps[e-1],highestStep:this.xHighestCompleteStep[e-1]},stepAfter:{startValue:this.xVal[e],step:this.xNumSteps[e],highestStep:this.xHighestCompleteStep[e]}}},l.prototype.countStepDecimals=function(){var t=this.xNumSteps.map(e);return Math.max.apply(null,t)},l.prototype.convert=function(t){return this.getStep(this.toStepping(t))};var u={to:function(t){return void 0!==t&&t.toFixed(2)},from:Number};function d(t){if("object"==typeof(e=t)&&"function"==typeof e.to&&"function"==typeof e.from)return!0;var e;throw new Error("noUiSlider ("+et+"): 'format' requires 'to' and 'from' methods.")}function h(t,e){if(!i(e))throw new Error("noUiSlider ("+et+"): 'step' is not numeric.");t.singleStep=e}function m(t,e){if("object"!=typeof e||Array.isArray(e))throw new Error("noUiSlider ("+et+"): 'range' is not an object.");if(void 0===e.min||void 0===e.max)throw new Error("noUiSlider ("+et+"): Missing 'min' or 'max' in 'range'.");if(e.min===e.max)throw new Error("noUiSlider ("+et+"): 'range' 'min' and 'max' cannot be equal.");t.spectrum=new l(e,t.snap,t.singleStep)}function g(t,e){if(e=ot(e),!Array.isArray(e)||!e.length)throw new Error("noUiSlider ("+et+"): 'start' option is incorrect.");t.handles=e.length,t.start=e}function v(t,e){if("boolean"!=typeof(t.snap=e))throw new Error("noUiSlider ("+et+"): 'snap' option must be a boolean.")}function b(t,e){if("boolean"!=typeof(t.animate=e))throw new Error("noUiSlider ("+et+"): 'animate' option must be a boolean.")}function S(t,e){if("number"!=typeof(t.animationDuration=e))throw new Error("noUiSlider ("+et+"): 'animationDuration' option must be a number.")}function w(t,e){var r,n=[!1];if("lower"===e?e=[!0,!1]:"upper"===e&&(e=[!1,!0]),!0===e||!1===e){for(r=1;r<t.handles;r++)n.push(e);n.push(!1)}else{if(!Array.isArray(e)||!e.length||e.length!==t.handles+1)throw new Error("noUiSlider ("+et+"): 'connect' option doesn't match handle count.");n=e}t.connect=n}function x(t,e){switch(e){case"horizontal":t.ort=0;break;case"vertical":t.ort=1;break;default:throw new Error("noUiSlider ("+et+"): 'orientation' option is invalid.")}}function y(t,e){if(!i(e))throw new Error("noUiSlider ("+et+"): 'margin' option must be numeric.");if(0!==e&&(t.margin=t.spectrum.getMargin(e),!t.margin))throw new Error("noUiSlider ("+et+"): 'margin' option is only supported on linear sliders.")}function E(t,e){if(!i(e))throw new Error("noUiSlider ("+et+"): 'limit' option must be numeric.");if(t.limit=t.spectrum.getMargin(e),!t.limit||t.handles<2)throw new Error("noUiSlider ("+et+"): 'limit' option is only supported on linear sliders with 2 or more handles.")}function C(t,e){if(!i(e)&&!Array.isArray(e))throw new Error("noUiSlider ("+et+"): 'padding' option must be numeric or array of exactly 2 numbers.");if(Array.isArray(e)&&2!==e.length&&!i(e[0])&&!i(e[1]))throw new Error("noUiSlider ("+et+"): 'padding' option must be numeric or array of exactly 2 numbers.");if(0!==e){if(Array.isArray(e)||(e=[e,e]),!(t.padding=[t.spectrum.getMargin(e[0]),t.spectrum.getMargin(e[1])])===t.padding[0]||!1===t.padding[1])throw new Error("noUiSlider ("+et+"): 'padding' option is only supported on linear sliders.");if(t.padding[0]<0||t.padding[1]<0)throw new Error("noUiSlider ("+et+"): 'padding' option must be a positive number(s).");if(100<=t.padding[0]+t.padding[1])throw new Error("noUiSlider ("+et+"): 'padding' option must not exceed 100% of the range.")}}function N(t,e){switch(e){case"ltr":t.dir=0;break;case"rtl":t.dir=1;break;default:throw new Error("noUiSlider ("+et+"): 'direction' option was not recognized.")}}function U(t,e){if("string"!=typeof e)throw new Error("noUiSlider ("+et+"): 'behaviour' must be a string containing options.");var r=0<=e.indexOf("tap"),n=0<=e.indexOf("drag"),i=0<=e.indexOf("fixed"),o=0<=e.indexOf("snap"),a=0<=e.indexOf("hover"),s=0<=e.indexOf("unconstrained");if(i){if(2!==t.handles)throw new Error("noUiSlider ("+et+"): 'fixed' behaviour must be used with 2 handles");y(t,t.start[1]-t.start[0])}if(s&&(t.margin||t.limit))throw new Error("noUiSlider ("+et+"): 'unconstrained' behaviour cannot be used with margin or limit");t.events={tap:r||o,drag:n,fixed:i,snap:o,hover:a,unconstrained:s}}function k(t,e){if(!1!==e)if(!0===e){t.tooltips=[];for(var r=0;r<t.handles;r++)t.tooltips.push(!0)}else{if(t.tooltips=ot(e),t.tooltips.length!==t.handles)throw new Error("noUiSlider ("+et+"): must pass a formatter for all handles.");t.tooltips.forEach(function(t){if("boolean"!=typeof t&&("object"!=typeof t||"function"!=typeof t.to))throw new Error("noUiSlider ("+et+"): 'tooltips' must be passed a formatter or 'false'.")})}}function P(t,e){d(t.ariaFormat=e)}function A(t,e){d(t.format=e)}function M(t,e){if("boolean"!=typeof(t.keyboardSupport=e))throw new Error("noUiSlider ("+et+"): 'keyboardSupport' option must be a boolean.")}function V(t,e){t.documentElement=e}function O(t,e){if("string"!=typeof e&&!1!==e)throw new Error("noUiSlider ("+et+"): 'cssPrefix' must be a string or `false`.");t.cssPrefix=e}function L(t,e){if("object"!=typeof e)throw new Error("noUiSlider ("+et+"): 'cssClasses' must be an object.");if("string"==typeof t.cssPrefix)for(var r in t.cssClasses={},e)e.hasOwnProperty(r)&&(t.cssClasses[r]=t.cssPrefix+e[r]);else t.cssClasses=e}function ut(e){var r={margin:0,limit:0,padding:0,animate:!0,animationDuration:300,ariaFormat:u,format:u},n={step:{r:!1,t:h},start:{r:!0,t:g},connect:{r:!0,t:w},direction:{r:!0,t:N},snap:{r:!1,t:v},animate:{r:!1,t:b},animationDuration:{r:!1,t:S},range:{r:!0,t:m},orientation:{r:!1,t:x},margin:{r:!1,t:y},limit:{r:!1,t:E},padding:{r:!1,t:C},behaviour:{r:!0,t:U},ariaFormat:{r:!1,t:P},format:{r:!1,t:A},tooltips:{r:!1,t:k},keyboardSupport:{r:!0,t:M},documentElement:{r:!1,t:V},cssPrefix:{r:!0,t:O},cssClasses:{r:!0,t:L}},i={connect:!1,direction:"ltr",behaviour:"tap",orientation:"horizontal",keyboardSupport:!0,cssPrefix:"noUi-",cssClasses:{target:"target",base:"base",origin:"origin",handle:"handle",handleLower:"handle-lower",handleUpper:"handle-upper",horizontal:"horizontal",vertical:"vertical",background:"background",connect:"connect",connects:"connects",ltr:"ltr",rtl:"rtl",draggable:"draggable",drag:"state-drag",tap:"state-tap",active:"active",tooltip:"tooltip",pips:"pips",pipsHorizontal:"pips-horizontal",pipsVertical:"pips-vertical",marker:"marker",markerHorizontal:"marker-horizontal",markerVertical:"marker-vertical",markerNormal:"marker-normal",markerLarge:"marker-large",markerSub:"marker-sub",value:"value",valueHorizontal:"value-horizontal",valueVertical:"value-vertical",valueNormal:"value-normal",valueLarge:"value-large",valueSub:"value-sub"}};e.format&&!e.ariaFormat&&(e.ariaFormat=e.format),Object.keys(n).forEach(function(t){if(!s(e[t])&&void 0===i[t]){if(n[t].r)throw new Error("noUiSlider ("+et+"): '"+t+"' is required.");return!0}n[t].t(r,s(e[t])?e[t]:i[t])}),r.pips=e.pips;var t=document.createElement("div"),o=void 0!==t.style.msTransform,a=void 0!==t.style.transform;r.transformRule=a?"transform":o?"msTransform":"webkitTransform";return r.style=[["left","top"],["right","bottom"]][r.dir][r.ort],r}function z(t,f,o){var l,u,s,a,c,e,p,i,d=window.navigator.pointerEnabled?{start:"pointerdown",move:"pointermove",end:"pointerup"}:window.navigator.msPointerEnabled?{start:"MSPointerDown",move:"MSPointerMove",end:"MSPointerUp"}:{start:"mousedown touchstart",move:"mousemove touchmove",end:"mouseup touchend"},h=window.CSS&&CSS.supports&&CSS.supports("touch-action","none")&&function(){var t=!1;try{var e=Object.defineProperty({},"passive",{get:function(){t=!0}});window.addEventListener("test",null,e)}catch(t){}return t}(),y=t,m=[],g=[],v=0,E=f.spectrum,b=[],S={},w=t.ownerDocument,x=f.documentElement||w.documentElement,C=w.body,N=-1,U=0,k=1,P=2,A="rtl"===w.dir||1===f.ort?0:100;function M(t,e){var r=w.createElement("div");return e&&at(r,e),t.appendChild(r),r}function V(t,e){return!!e&&M(t,f.cssClasses.connect)}function r(t,e){return!!f.tooltips[e]&&M(t.firstChild,f.cssClasses.tooltip)}function O(e,i,o){var a=w.createElement("div"),s=[];s[U]=f.cssClasses.valueNormal,s[k]=f.cssClasses.valueLarge,s[P]=f.cssClasses.valueSub;var l=[];l[U]=f.cssClasses.markerNormal,l[k]=f.cssClasses.markerLarge,l[P]=f.cssClasses.markerSub;var u=[f.cssClasses.valueHorizontal,f.cssClasses.valueVertical],c=[f.cssClasses.markerHorizontal,f.cssClasses.markerVertical];function p(t,e){var r=e===f.cssClasses.value,n=r?s:l;return e+" "+(r?u:c)[f.ort]+" "+n[t]}return at(a,f.cssClasses.pips),at(a,0===f.ort?f.cssClasses.pipsHorizontal:f.cssClasses.pipsVertical),Object.keys(e).forEach(function(t){!function(t,e,r){if((r=i?i(e,r):r)!==N){var n=M(a,!1);n.className=p(r,f.cssClasses.marker),n.style[f.style]=t+"%",U<r&&((n=M(a,!1)).className=p(r,f.cssClasses.value),n.setAttribute("data-value",e),n.style[f.style]=t+"%",n.innerHTML=o.to(e))}}(t,e[t][0],e[t][1])}),a}function L(){var t;c&&((t=c).parentElement.removeChild(t),c=null)}function z(t){L();var m,g,v,b,e,r,S,w,x,n=t.mode,i=t.density||1,o=t.filter||!1,a=function(t,e,r){if("range"===t||"steps"===t)return E.xVal;if("count"===t){if(e<2)throw new Error("noUiSlider ("+et+"): 'values' (>= 2) required for mode 'count'.");var n=e-1,i=100/n;for(e=[];n--;)e[n]=n*i;e.push(100),t="positions"}return"positions"===t?e.map(function(t){return E.fromStepping(r?E.getStep(t):t)}):"values"===t?r?e.map(function(t){return E.fromStepping(E.getStep(E.toStepping(t)))}):e:void 0}(n,t.values||!1,t.stepped||!1),s=(m=i,g=n,v=a,b={},e=E.xVal[0],r=E.xVal[E.xVal.length-1],w=S=!1,x=0,(v=v.slice().sort(function(t,e){return t-e}).filter(function(t){return!this[t]&&(this[t]=!0)},{}))[0]!==e&&(v.unshift(e),S=!0),v[v.length-1]!==r&&(v.push(r),w=!0),v.forEach(function(t,e){var r,n,i,o,a,s,l,u,c,p,f=t,d=v[e+1],h="steps"===g;if(h&&(r=E.xNumSteps[e]),r||(r=d-f),!1!==f&&void 0!==d)for(r=Math.max(r,1e-7),n=f;n<=d;n=(n+r).toFixed(7)/1){for(u=(a=(o=E.toStepping(n))-x)/m,p=a/(c=Math.round(u)),i=1;i<=c;i+=1)b[(s=x+i*p).toFixed(5)]=[E.fromStepping(s),0];l=-1<v.indexOf(n)?k:h?P:U,!e&&S&&(l=0),n===d&&w||(b[o.toFixed(5)]=[n,l]),x=o}}),b),l=t.format||{to:Math.round};return c=y.appendChild(O(s,o,l))}function j(){var t=l.getBoundingClientRect(),e="offset"+["Width","Height"][f.ort];return 0===f.ort?t.width||l[e]:t.height||l[e]}function F(n,i,o,a){var e=function(t){return!!(t=function(t,e,r){var n,i,o=0===t.type.indexOf("touch"),a=0===t.type.indexOf("mouse"),s=0===t.type.indexOf("pointer");0===t.type.indexOf("MSPointer")&&(s=!0);if(o){var l=function(t){return t.target===r||r.contains(t.target)};if("touchstart"===t.type){var u=Array.prototype.filter.call(t.touches,l);if(1<u.length)return!1;n=u[0].pageX,i=u[0].pageY}else{var c=Array.prototype.find.call(t.changedTouches,l);if(!c)return!1;n=c.pageX,i=c.pageY}}e=e||lt(w),(a||s)&&(n=t.clientX+e.x,i=t.clientY+e.y);return t.pageOffset=e,t.points=[n,i],t.cursor=a||s,t}(t,a.pageOffset,a.target||i))&&(!(y.hasAttribute("disabled")&&!a.doNotReject)&&(e=y,r=f.cssClasses.tap,!((e.classList?e.classList.contains(r):new RegExp("\\b"+r+"\\b").test(e.className))&&!a.doNotReject)&&(!(n===d.start&&void 0!==t.buttons&&1<t.buttons)&&((!a.hover||!t.buttons)&&(h||t.preventDefault(),t.calcPoint=t.points[f.ort],void o(t,a))))));var e,r},r=[];return n.split(" ").forEach(function(t){i.addEventListener(t,e,!!h&&{passive:!0}),r.push([t,e])}),r}function H(t){var e,r,n,i,o,a,s=100*(t-(e=l,r=f.ort,n=e.getBoundingClientRect(),i=e.ownerDocument,o=i.documentElement,a=lt(i),/webkit.*Chrome.*Mobile/i.test(navigator.userAgent)&&(a.x=0),r?n.top+a.y-o.clientTop:n.left+a.x-o.clientLeft))/j();return s=it(s),f.dir?100-s:s}function D(t,e){"mouseout"===t.type&&"HTML"===t.target.nodeName&&null===t.relatedTarget&&R(t,e)}function T(t,e){if(-1===navigator.appVersion.indexOf("MSIE 9")&&0===t.buttons&&0!==e.buttonsProperty)return R(t,e);var r=(f.dir?-1:1)*(t.calcPoint-e.startCalcPoint);$(0<r,100*r/e.baseSize,e.locations,e.handleNumbers)}function R(t,e){e.handle&&(st(e.handle,f.cssClasses.active),v-=1),e.listeners.forEach(function(t){x.removeEventListener(t[0],t[1])}),0===v&&(st(y,f.cssClasses.drag),J(),t.cursor&&(C.style.cursor="",C.removeEventListener("selectstart",rt))),e.handleNumbers.forEach(function(t){Y("change",t),Y("set",t),Y("end",t)})}function q(t,e){var r;if(1===e.handleNumbers.length){var n=u[e.handleNumbers[0]];if(n.hasAttribute("disabled"))return!1;r=n.children[0],v+=1,at(r,f.cssClasses.active)}t.stopPropagation();var i=[],o=F(d.move,x,T,{target:t.target,handle:r,listeners:i,startCalcPoint:t.calcPoint,baseSize:j(),pageOffset:t.pageOffset,handleNumbers:e.handleNumbers,buttonsProperty:t.buttons,locations:m.slice()}),a=F(d.end,x,R,{target:t.target,handle:r,listeners:i,doNotReject:!0,handleNumbers:e.handleNumbers}),s=F("mouseout",x,D,{target:t.target,handle:r,listeners:i,doNotReject:!0,handleNumbers:e.handleNumbers});i.push.apply(i,o.concat(a,s)),t.cursor&&(C.style.cursor=getComputedStyle(t.target).cursor,1<u.length&&at(y,f.cssClasses.drag),C.addEventListener("selectstart",rt,!1)),e.handleNumbers.forEach(function(t){Y("start",t)})}function n(t){t.stopPropagation();var n,i,o,e=H(t.calcPoint),r=(n=e,o=!(i=100),u.forEach(function(t,e){if(!t.hasAttribute("disabled")){var r=Math.abs(m[e]-n);(r<i||100===r&&100===i)&&(o=e,i=r)}}),o);if(!1===r)return!1;f.events.snap||nt(y,f.cssClasses.tap,f.animationDuration),K(r,e,!0,!0),J(),Y("slide",r,!0),Y("update",r,!0),Y("change",r,!0),Y("set",r,!0),f.events.snap&&q(t,{handleNumbers:[r]})}function B(t){var e=H(t.calcPoint),r=E.getStep(e),n=E.fromStepping(r);Object.keys(S).forEach(function(t){"hover"===t.split(".")[0]&&S[t].forEach(function(t){t.call(a,n)})})}function X(t,e){S[t]=S[t]||[],S[t].push(e),"update"===t.split(".")[0]&&u.forEach(function(t,e){Y("update",e)})}function Y(r,n,i){Object.keys(S).forEach(function(t){var e=t.split(".")[0];r===e&&S[t].forEach(function(t){t.call(a,b.map(f.format.to),n,b.slice(),i||!1,m.slice())})})}function _(t){return t+"%"}function I(t,e,r,n,i,o){return 1<u.length&&!f.events.unconstrained&&(n&&0<e&&(r=Math.max(r,t[e-1]+f.margin)),i&&e<u.length-1&&(r=Math.min(r,t[e+1]-f.margin))),1<u.length&&f.limit&&(n&&0<e&&(r=Math.min(r,t[e-1]+f.limit)),i&&e<u.length-1&&(r=Math.max(r,t[e+1]-f.limit))),f.padding&&(0===e&&(r=Math.max(r,f.padding[0])),e===u.length-1&&(r=Math.min(r,100-f.padding[1]))),!((r=it(r=E.getStep(r)))===t[e]&&!o)&&r}function W(t,e){var r=f.ort;return(r?e:t)+", "+(r?t:e)}function $(t,n,r,e){var i=r.slice(),o=[!t,t],a=[t,!t];e=e.slice(),t&&e.reverse(),1<e.length?e.forEach(function(t,e){var r=I(i,t,i[t]+n,o[e],a[e],!1);!1===r?n=0:(n=r-i[t],i[t]=r)}):o=a=[!0];var s=!1;e.forEach(function(t,e){s=K(t,r[t]+n,o[e],a[e])||s}),s&&e.forEach(function(t){Y("update",t),Y("slide",t)})}function G(t,e){return f.dir?100-t-e:t}function J(){g.forEach(function(t){var e=50<m[t]?-1:1,r=3+(u.length+e*t);u[t].style.zIndex=r})}function K(t,e,r,n){return!1!==(e=I(m,t,e,r,n,!1))&&(function(t,e){m[t]=e,b[t]=E.fromStepping(e);var r="translate("+W(_(G(e,0)-A),"0")+")";u[t].style[f.transformRule]=r,Q(t),Q(t+1)}(t,e),!0)}function Q(t){if(s[t]){var e=0,r=100;0!==t&&(e=m[t-1]),t!==s.length-1&&(r=m[t]);var n=r-e,i="translate("+W(_(G(e,n)),"0")+")",o="scale("+W(n/100,"1")+")";s[t].style[f.transformRule]=i+" "+o}}function Z(t,e){var n=ot(t),r=void 0===m[0];e=void 0===e||!!e,f.animate&&!r&&nt(y,f.cssClasses.tap,f.animationDuration),g.forEach(function(t){var e,r;K(t,(e=n[t],r=t,null===e||!1===e||void 0===e?m[r]:("number"==typeof e&&(e=String(e)),e=f.format.from(e),!1===(e=E.toStepping(e))||isNaN(e)?m[r]:e)),!0,!1)}),g.forEach(function(t){K(t,m[t],!0,!0)}),J(),g.forEach(function(t){Y("update",t),null!==n[t]&&e&&Y("set",t)})}function tt(){var t=b.map(f.format.to);return 1===t.length?t[0]:t}return at(e=y,f.cssClasses.target),0===f.dir?at(e,f.cssClasses.ltr):at(e,f.cssClasses.rtl),0===f.ort?at(e,f.cssClasses.horizontal):at(e,f.cssClasses.vertical),l=M(e,f.cssClasses.base),function(t,e){var r,n,i,o=M(e,f.cssClasses.connects);u=[],(s=[]).push(V(o,t[0]));for(var a=0;a<f.handles;a++)u.push((r=a,i=void 0,n=M(e,f.cssClasses.origin),(i=M(n,f.cssClasses.handle)).setAttribute("data-handle",r),f.keyboardSupport&&i.setAttribute("tabindex","0"),i.setAttribute("role","slider"),i.setAttribute("aria-orientation",f.ort?"vertical":"horizontal"),0===r?at(i,f.cssClasses.handleLower):r===f.handles-1&&at(i,f.cssClasses.handleUpper),n)),g[a]=a,s.push(V(o,t[a+1]))}(f.connect,l),(p=f.events).fixed||u.forEach(function(t,e){F(d.start,t.children[0],q,{handleNumbers:[e]})}),p.tap&&F(d.start,l,n,{}),p.hover&&F(d.move,l,B,{hover:!0}),p.drag&&s.forEach(function(t,e){if(!1!==t&&0!==e&&e!==s.length-1){var r=u[e-1],n=u[e],i=[t];at(t,f.cssClasses.draggable),p.fixed&&(i.push(r.children[0]),i.push(n.children[0])),i.forEach(function(t){F(d.start,t,q,{handles:[r,n],handleNumbers:[e-1,e]})})}}),Z(f.start),a={destroy:function(){for(var t in f.cssClasses)f.cssClasses.hasOwnProperty(t)&&st(y,f.cssClasses[t]);for(;y.firstChild;)y.removeChild(y.firstChild);delete y.noUiSlider},steps:function(){return m.map(function(t,e){var r=E.getNearbySteps(t),n=b[e],i=r.thisStep.step,o=null;!1!==i&&n+i>r.stepAfter.startValue&&(i=r.stepAfter.startValue-n),o=n>r.thisStep.startValue?r.thisStep.step:!1!==r.stepBefore.step&&n-r.stepBefore.highestStep,100===t?i=null:0===t&&(o=null);var a=E.countStepDecimals();return null!==i&&!1!==i&&(i=Number(i.toFixed(a))),null!==o&&!1!==o&&(o=Number(o.toFixed(a))),[o,i]})},on:X,off:function(t){var n=t&&t.split(".")[0],i=n&&t.substring(n.length);Object.keys(S).forEach(function(t){var e=t.split(".")[0],r=t.substring(e.length);n&&n!==e||i&&i!==r||delete S[t]})},get:tt,set:Z,setHandle:function(t,e,r){var n=[];if(!(0<=(t=Number(t))&&t<g.length))throw new Error("noUiSlider ("+et+"): invalid handle number, got: "+t);for(var i=0;i<g.length;i++)n[i]=null;n[t]=e,Z(n,r)},reset:function(t){Z(f.start,t)},__moveHandles:function(t,e,r){$(t,e,m,r)},options:o,updateOptions:function(e,t){var r=tt(),n=["margin","limit","padding","range","animate","snap","step","format"];n.forEach(function(t){void 0!==e[t]&&(o[t]=e[t])});var i=ut(o);n.forEach(function(t){void 0!==e[t]&&(f[t]=i[t])}),E=i.spectrum,f.margin=i.margin,f.limit=i.limit,f.padding=i.padding,f.pips&&z(f.pips),m=[],Z(e.start||r,t)},target:y,removePips:L,pips:z},f.pips&&z(f.pips),f.tooltips&&(i=u.map(r),X("update",function(t,e,r){if(i[e]){var n=t[e];!0!==f.tooltips[e]&&(n=f.tooltips[e].to(r[e])),i[e].innerHTML=n}})),X("update",function(t,e,a,r,s){g.forEach(function(t){var e=u[t],r=I(m,t,0,!0,!0,!0),n=I(m,t,100,!0,!0,!0),i=s[t],o=f.ariaFormat.to(a[t]);r=E.fromStepping(r).toFixed(1),n=E.fromStepping(n).toFixed(1),i=E.fromStepping(i).toFixed(1),e.children[0].setAttribute("aria-valuemin",r),e.children[0].setAttribute("aria-valuemax",n),e.children[0].setAttribute("aria-valuenow",i),e.children[0].setAttribute("aria-valuetext",o)})}),a}return{__spectrum:l,version:et,create:function(t,e){if(!t||!t.nodeName)throw new Error("noUiSlider ("+et+"): create requires a single element, got: "+t);if(t.noUiSlider)throw new Error("noUiSlider ("+et+"): Slider was already initialized.");var r=z(t,ut(e),e);return t.noUiSlider=r}}});

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

/*! @license ScrollReveal v4.0.5

	Copyright 2018 Fisssion LLC.

	Licensed under the GNU General Public License 3.0 for
	compatible open source projects and non-commercial use.

	For commercial sites, themes, projects, and applications,
	keep your source code private/proprietary by purchasing
	a commercial license from https://scrollrevealjs.org/
*/
(function (global, factory) {
	 true ? module.exports = factory() :
	undefined;
}(this, (function () { 'use strict';

var defaults = {
	delay: 0,
	distance: '0',
	duration: 600,
	easing: 'cubic-bezier(0.5, 0, 0, 1)',
	interval: 0,
	opacity: 0,
	origin: 'bottom',
	rotate: {
		x: 0,
		y: 0,
		z: 0
	},
	scale: 1,
	cleanup: false,
	container: document.documentElement,
	desktop: true,
	mobile: true,
	reset: false,
	useDelay: 'always',
	viewFactor: 0.0,
	viewOffset: {
		top: 0,
		right: 0,
		bottom: 0,
		left: 0
	},
	afterReset: function afterReset() {},
	afterReveal: function afterReveal() {},
	beforeReset: function beforeReset() {},
	beforeReveal: function beforeReveal() {}
}

function failure() {
	document.documentElement.classList.remove('sr');

	return {
		clean: function clean() {},
		destroy: function destroy() {},
		reveal: function reveal() {},
		sync: function sync() {},
		get noop() {
			return true
		}
	}
}

function success() {
	document.documentElement.classList.add('sr');

	if (document.body) {
		document.body.style.height = '100%';
	} else {
		document.addEventListener('DOMContentLoaded', function () {
			document.body.style.height = '100%';
		});
	}
}

var mount = { success: success, failure: failure }

/*! @license is-dom-node v1.0.4

	Copyright 2018 Fisssion LLC.

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

*/
function isDomNode(x) {
	return typeof window.Node === 'object'
		? x instanceof window.Node
		: x !== null &&
				typeof x === 'object' &&
				typeof x.nodeType === 'number' &&
				typeof x.nodeName === 'string'
}

/*! @license is-dom-node-list v1.2.1

	Copyright 2018 Fisssion LLC.

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

*/
function isDomNodeList(x) {
	var prototypeToString = Object.prototype.toString.call(x);
	var regex = /^\[object (HTMLCollection|NodeList|Object)\]$/;

	return typeof window.NodeList === 'object'
		? x instanceof window.NodeList
		: x !== null &&
				typeof x === 'object' &&
				typeof x.length === 'number' &&
				regex.test(prototypeToString) &&
				(x.length === 0 || isDomNode(x[0]))
}

/*! @license Tealight v0.3.6

	Copyright 2018 Fisssion LLC.

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

*/
function tealight(target, context) {
  if ( context === void 0 ) { context = document; }

  if (target instanceof Array) { return target.filter(isDomNode); }
  if (isDomNode(target)) { return [target]; }
  if (isDomNodeList(target)) { return Array.prototype.slice.call(target); }
  if (typeof target === "string") {
    try {
      var query = context.querySelectorAll(target);
      return Array.prototype.slice.call(query);
    } catch (err) {
      return [];
    }
  }
  return [];
}

function isObject(x) {
	return (
		x !== null &&
		x instanceof Object &&
		(x.constructor === Object ||
			Object.prototype.toString.call(x) === '[object Object]')
	)
}

function each(collection, callback) {
	if (isObject(collection)) {
		var keys = Object.keys(collection);
		return keys.forEach(function (key) { return callback(collection[key], key, collection); })
	}
	if (collection instanceof Array) {
		return collection.forEach(function (item, i) { return callback(item, i, collection); })
	}
	throw new TypeError('Expected either an array or object literal.')
}

function logger(message) {
	var details = [], len = arguments.length - 1;
	while ( len-- > 0 ) details[ len ] = arguments[ len + 1 ];

	if (this.constructor.debug && console) {
		var report = "%cScrollReveal: " + message;
		details.forEach(function (detail) { return (report += "\n — " + detail); });
		console.log(report, 'color: #ea654b;'); // eslint-disable-line no-console
	}
}

function rinse() {
	var this$1 = this;

	var struct = function () { return ({
		active: [],
		stale: []
	}); };

	var elementIds = struct();
	var sequenceIds = struct();
	var containerIds = struct();

	/**
	 * Take stock of active element IDs.
	 */
	try {
		each(tealight('[data-sr-id]'), function (node) {
			var id = parseInt(node.getAttribute('data-sr-id'));
			elementIds.active.push(id);
		});
	} catch (e) {
		throw e
	}
	/**
	 * Destroy stale elements.
	 */
	each(this.store.elements, function (element) {
		if (elementIds.active.indexOf(element.id) === -1) {
			elementIds.stale.push(element.id);
		}
	});

	each(elementIds.stale, function (staleId) { return delete this$1.store.elements[staleId]; });

	/**
	 * Take stock of active container and sequence IDs.
	 */
	each(this.store.elements, function (element) {
		if (containerIds.active.indexOf(element.containerId) === -1) {
			containerIds.active.push(element.containerId);
		}
		if (element.hasOwnProperty('sequence')) {
			if (sequenceIds.active.indexOf(element.sequence.id) === -1) {
				sequenceIds.active.push(element.sequence.id);
			}
		}
	});

	/**
	 * Destroy stale containers.
	 */
	each(this.store.containers, function (container) {
		if (containerIds.active.indexOf(container.id) === -1) {
			containerIds.stale.push(container.id);
		}
	});

	each(containerIds.stale, function (staleId) {
		var stale = this$1.store.containers[staleId].node;
		stale.removeEventListener('scroll', this$1.delegate);
		stale.removeEventListener('resize', this$1.delegate);
		delete this$1.store.containers[staleId];
	});

	/**
	 * Destroy stale sequences.
	 */
	each(this.store.sequences, function (sequence) {
		if (sequenceIds.active.indexOf(sequence.id) === -1) {
			sequenceIds.stale.push(sequence.id);
		}
	});

	each(sequenceIds.stale, function (staleId) { return delete this$1.store.sequences[staleId]; });
}

function clean(target) {
	var this$1 = this;

	var dirty;
	try {
		each(tealight(target), function (node) {
			var id = node.getAttribute('data-sr-id');
			if (id !== null) {
				dirty = true;
				var element = this$1.store.elements[id];
				if (element.callbackTimer) {
					window.clearTimeout(element.callbackTimer.clock);
				}
				node.setAttribute('style', element.styles.inline.generated);
				node.removeAttribute('data-sr-id');
				delete this$1.store.elements[id];
			}
		});
	} catch (e) {
		return logger.call(this, 'Clean failed.', e.message)
	}

	if (dirty) {
		try {
			rinse.call(this);
		} catch (e) {
			return logger.call(this, 'Clean failed.', e.message)
		}
	}
}

function destroy() {
	var this$1 = this;

	/**
	 * Remove all generated styles and element ids
	 */
	each(this.store.elements, function (element) {
		element.node.setAttribute('style', element.styles.inline.generated);
		element.node.removeAttribute('data-sr-id');
	});

	/**
	 * Remove all event listeners.
	 */
	each(this.store.containers, function (container) {
		var target =
			container.node === document.documentElement ? window : container.node;
		target.removeEventListener('scroll', this$1.delegate);
		target.removeEventListener('resize', this$1.delegate);
	});

	/**
	 * Clear all data from the store
	 */
	this.store = {
		containers: {},
		elements: {},
		history: [],
		sequences: {}
	};
}

/*! @license Rematrix v0.3.0

	Copyright 2018 Julian Lloyd.

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
/**
 * @module Rematrix
 */

/**
 * Transformation matrices in the browser come in two flavors:
 *
 *  - `matrix` using 6 values (short)
 *  - `matrix3d` using 16 values (long)
 *
 * This utility follows this [conversion guide](https://goo.gl/EJlUQ1)
 * to expand short form matrices to their equivalent long form.
 *
 * @param  {array} source - Accepts both short and long form matrices.
 * @return {array}
 */
function format(source) {
	if (source.constructor !== Array) {
		throw new TypeError('Expected array.')
	}
	if (source.length === 16) {
		return source
	}
	if (source.length === 6) {
		var matrix = identity();
		matrix[0] = source[0];
		matrix[1] = source[1];
		matrix[4] = source[2];
		matrix[5] = source[3];
		matrix[12] = source[4];
		matrix[13] = source[5];
		return matrix
	}
	throw new RangeError('Expected array with either 6 or 16 values.')
}

/**
 * Returns a matrix representing no transformation. The product of any matrix
 * multiplied by the identity matrix will be the original matrix.
 *
 * > **Tip:** Similar to how `5 * 1 === 5`, where `1` is the identity.
 *
 * @return {array}
 */
function identity() {
	var matrix = [];
	for (var i = 0; i < 16; i++) {
		i % 5 == 0 ? matrix.push(1) : matrix.push(0);
	}
	return matrix
}

/**
 * Returns a 4x4 matrix describing the combined transformations
 * of both arguments.
 *
 * > **Note:** Order is very important. For example, rotating 45°
 * along the Z-axis, followed by translating 500 pixels along the
 * Y-axis... is not the same as translating 500 pixels along the
 * Y-axis, followed by rotating 45° along on the Z-axis.
 *
 * @param  {array} m - Accepts both short and long form matrices.
 * @param  {array} x - Accepts both short and long form matrices.
 * @return {array}
 */
function multiply(m, x) {
	var fm = format(m);
	var fx = format(x);
	var product = [];

	for (var i = 0; i < 4; i++) {
		var row = [fm[i], fm[i + 4], fm[i + 8], fm[i + 12]];
		for (var j = 0; j < 4; j++) {
			var k = j * 4;
			var col = [fx[k], fx[k + 1], fx[k + 2], fx[k + 3]];
			var result =
				row[0] * col[0] + row[1] * col[1] + row[2] * col[2] + row[3] * col[3];

			product[i + k] = result;
		}
	}

	return product
}

/**
 * Attempts to return a 4x4 matrix describing the CSS transform
 * matrix passed in, but will return the identity matrix as a
 * fallback.
 *
 * > **Tip:** This method is used to convert a CSS matrix (retrieved as a
 * `string` from computed styles) to its equivalent array format.
 *
 * @param  {string} source - `matrix` or `matrix3d` CSS Transform value.
 * @return {array}
 */
function parse(source) {
	if (typeof source === 'string') {
		var match = source.match(/matrix(3d)?\(([^)]+)\)/);
		if (match) {
			var raw = match[2].split(', ').map(parseFloat);
			return format(raw)
		}
	}
	return identity()
}

/**
 * Returns a 4x4 matrix describing X-axis rotation.
 *
 * @param  {number} angle - Measured in degrees.
 * @return {array}
 */
function rotateX(angle) {
	var theta = Math.PI / 180 * angle;
	var matrix = identity();

	matrix[5] = matrix[10] = Math.cos(theta);
	matrix[6] = matrix[9] = Math.sin(theta);
	matrix[9] *= -1;

	return matrix
}

/**
 * Returns a 4x4 matrix describing Y-axis rotation.
 *
 * @param  {number} angle - Measured in degrees.
 * @return {array}
 */
function rotateY(angle) {
	var theta = Math.PI / 180 * angle;
	var matrix = identity();

	matrix[0] = matrix[10] = Math.cos(theta);
	matrix[2] = matrix[8] = Math.sin(theta);
	matrix[2] *= -1;

	return matrix
}

/**
 * Returns a 4x4 matrix describing Z-axis rotation.
 *
 * @param  {number} angle - Measured in degrees.
 * @return {array}
 */
function rotateZ(angle) {
	var theta = Math.PI / 180 * angle;
	var matrix = identity();

	matrix[0] = matrix[5] = Math.cos(theta);
	matrix[1] = matrix[4] = Math.sin(theta);
	matrix[4] *= -1;

	return matrix
}

/**
 * Returns a 4x4 matrix describing 2D scaling. The first argument
 * is used for both X and Y-axis scaling, unless an optional
 * second argument is provided to explicitly define Y-axis scaling.
 *
 * @param  {number} scalar    - Decimal multiplier.
 * @param  {number} [scalarY] - Decimal multiplier.
 * @return {array}
 */
function scale(scalar, scalarY) {
	var matrix = identity();

	matrix[0] = scalar;
	matrix[5] = typeof scalarY === 'number' ? scalarY : scalar;

	return matrix
}

/**
 * Returns a 4x4 matrix describing X-axis translation.
 *
 * @param  {number} distance - Measured in pixels.
 * @return {array}
 */
function translateX(distance) {
	var matrix = identity();
	matrix[12] = distance;
	return matrix
}

/**
 * Returns a 4x4 matrix describing Y-axis translation.
 *
 * @param  {number} distance - Measured in pixels.
 * @return {array}
 */
function translateY(distance) {
	var matrix = identity();
	matrix[13] = distance;
	return matrix
}

var getPrefixedCssProp = (function () {
	var properties = {};
	var style = document.documentElement.style;

	function getPrefixedCssProperty(name, source) {
		if ( source === void 0 ) source = style;

		if (name && typeof name === 'string') {
			if (properties[name]) {
				return properties[name]
			}
			if (typeof source[name] === 'string') {
				return (properties[name] = name)
			}
			if (typeof source[("-webkit-" + name)] === 'string') {
				return (properties[name] = "-webkit-" + name)
			}
			throw new RangeError(("Unable to find \"" + name + "\" style property."))
		}
		throw new TypeError('Expected a string.')
	}

	getPrefixedCssProperty.clearCache = function () { return (properties = {}); };

	return getPrefixedCssProperty
})();

function style(element) {
	var computed = window.getComputedStyle(element.node);
	var position = computed.position;
	var config = element.config;

	/**
	 * Generate inline styles
	 */
	var inline = {};
	var inlineStyle = element.node.getAttribute('style') || '';
	var inlineMatch = inlineStyle.match(/[\w-]+\s*:\s*[^;]+\s*/gi) || [];

	inline.computed = inlineMatch ? inlineMatch.map(function (m) { return m.trim(); }).join('; ') + ';' : '';

	inline.generated = inlineMatch.some(function (m) { return m.match(/visibility\s?:\s?visible/i); })
		? inline.computed
		: inlineMatch.concat( ['visibility: visible']).map(function (m) { return m.trim(); }).join('; ') + ';';

	/**
	 * Generate opacity styles
	 */
	var computedOpacity = parseFloat(computed.opacity);
	var configOpacity = !isNaN(parseFloat(config.opacity))
		? parseFloat(config.opacity)
		: parseFloat(computed.opacity);

	var opacity = {
		computed: computedOpacity !== configOpacity ? ("opacity: " + computedOpacity + ";") : '',
		generated: computedOpacity !== configOpacity ? ("opacity: " + configOpacity + ";") : ''
	};

	/**
	 * Generate transformation styles
	 */
	var transformations = [];

	if (parseFloat(config.distance)) {
		var axis = config.origin === 'top' || config.origin === 'bottom' ? 'Y' : 'X';

		/**
		 * Let’s make sure our our pixel distances are negative for top and left.
		 * e.g. { origin: 'top', distance: '25px' } starts at `top: -25px` in CSS.
		 */
		var distance = config.distance;
		if (config.origin === 'top' || config.origin === 'left') {
			distance = /^-/.test(distance) ? distance.substr(1) : ("-" + distance);
		}

		var ref = distance.match(/(^-?\d+\.?\d?)|(em$|px$|%$)/g);
		var value = ref[0];
		var unit = ref[1];

		switch (unit) {
			case 'em':
				distance = parseInt(computed.fontSize) * value;
				break
			case 'px':
				distance = value;
				break
			case '%':
				/**
				 * Here we use `getBoundingClientRect` instead of
				 * the existing data attached to `element.geometry`
				 * because only the former includes any transformations
				 * current applied to the element.
				 *
				 * If that behavior ends up being unintuitive, this
				 * logic could instead utilize `element.geometry.height`
				 * and `element.geoemetry.width` for the distaince calculation
				 */
				distance =
					axis === 'Y'
						? element.node.getBoundingClientRect().height * value / 100
						: element.node.getBoundingClientRect().width * value / 100;
				break
			default:
				throw new RangeError('Unrecognized or missing distance unit.')
		}

		if (axis === 'Y') {
			transformations.push(translateY(distance));
		} else {
			transformations.push(translateX(distance));
		}
	}

	if (config.rotate.x) { transformations.push(rotateX(config.rotate.x)); }
	if (config.rotate.y) { transformations.push(rotateY(config.rotate.y)); }
	if (config.rotate.z) { transformations.push(rotateZ(config.rotate.z)); }
	if (config.scale !== 1) {
		if (config.scale === 0) {
			/**
			 * The CSS Transforms matrix interpolation specification
			 * basically disallows transitions of non-invertible
			 * matrixes, which means browsers won't transition
			 * elements with zero scale.
			 *
			 * That’s inconvenient for the API and developer
			 * experience, so we simply nudge their value
			 * slightly above zero; this allows browsers
			 * to transition our element as expected.
			 *
			 * `0.0002` was the smallest number
			 * that performed across browsers.
			 */
			transformations.push(scale(0.0002));
		} else {
			transformations.push(scale(config.scale));
		}
	}

	var transform = {};
	if (transformations.length) {
		transform.property = getPrefixedCssProp('transform');
		/**
		 * The default computed transform value should be one of:
		 * undefined || 'none' || 'matrix()' || 'matrix3d()'
		 */
		transform.computed = {
			raw: computed[transform.property],
			matrix: parse(computed[transform.property])
		};

		transformations.unshift(transform.computed.matrix);
		var product = transformations.reduce(multiply);

		transform.generated = {
			initial: ((transform.property) + ": matrix3d(" + (product.join(', ')) + ");"),
			final: ((transform.property) + ": matrix3d(" + (transform.computed.matrix.join(
				', '
			)) + ");")
		};
	} else {
		transform.generated = {
			initial: '',
			final: ''
		};
	}

	/**
	 * Generate transition styles
	 */
	var transition = {};
	if (opacity.generated || transform.generated.initial) {
		transition.property = getPrefixedCssProp('transition');
		transition.computed = computed[transition.property];
		transition.fragments = [];

		var delay = config.delay;
		var duration = config.duration;
		var easing = config.easing;

		if (opacity.generated) {
			transition.fragments.push({
				delayed: ("opacity " + (duration / 1000) + "s " + easing + " " + (delay / 1000) + "s"),
				instant: ("opacity " + (duration / 1000) + "s " + easing + " 0s")
			});
		}

		if (transform.generated.initial) {
			transition.fragments.push({
				delayed: ((transform.property) + " " + (duration / 1000) + "s " + easing + " " + (delay /
					1000) + "s"),
				instant: ((transform.property) + " " + (duration / 1000) + "s " + easing + " 0s")
			});
		}

		/**
		 * The default computed transition property should be one of:
		 * undefined || '' || 'all 0s ease 0s' || 'all 0s 0s cubic-bezier()'
		 */
		if (transition.computed && !transition.computed.match(/all 0s/)) {
			transition.fragments.unshift({
				delayed: transition.computed,
				instant: transition.computed
			});
		}

		var composed = transition.fragments.reduce(
			function (composition, fragment, i) {
				composition.delayed +=
					i === 0 ? fragment.delayed : (", " + (fragment.delayed));
				composition.instant +=
					i === 0 ? fragment.instant : (", " + (fragment.instant));
				return composition
			},
			{
				delayed: '',
				instant: ''
			}
		);

		transition.generated = {
			delayed: ((transition.property) + ": " + (composed.delayed) + ";"),
			instant: ((transition.property) + ": " + (composed.instant) + ";")
		};
	} else {
		transition.generated = {
			delayed: '',
			instant: ''
		};
	}

	return {
		inline: inline,
		opacity: opacity,
		position: position,
		transform: transform,
		transition: transition
	}
}

function animate(element, force) {
	if ( force === void 0 ) force = {};

	var pristine = force.pristine || this.pristine;
	var delayed =
		element.config.useDelay === 'always' ||
		(element.config.useDelay === 'onload' && pristine) ||
		(element.config.useDelay === 'once' && !element.seen);

	var shouldReveal = element.visible && !element.revealed;
	var shouldReset = !element.visible && element.revealed && element.config.reset;

	if (force.reveal || shouldReveal) {
		return triggerReveal.call(this, element, delayed)
	}

	if (force.reset || shouldReset) {
		return triggerReset.call(this, element)
	}
}

function triggerReveal(element, delayed) {
	var styles = [
		element.styles.inline.generated,
		element.styles.opacity.computed,
		element.styles.transform.generated.final
	];
	if (delayed) {
		styles.push(element.styles.transition.generated.delayed);
	} else {
		styles.push(element.styles.transition.generated.instant);
	}
	element.revealed = element.seen = true;
	element.node.setAttribute('style', styles.filter(function (s) { return s !== ''; }).join(' '));
	registerCallbacks.call(this, element, delayed);
}

function triggerReset(element) {
	var styles = [
		element.styles.inline.generated,
		element.styles.opacity.generated,
		element.styles.transform.generated.initial,
		element.styles.transition.generated.instant
	];
	element.revealed = false;
	element.node.setAttribute('style', styles.filter(function (s) { return s !== ''; }).join(' '));
	registerCallbacks.call(this, element);
}

function registerCallbacks(element, isDelayed) {
	var this$1 = this;

	var duration = isDelayed
		? element.config.duration + element.config.delay
		: element.config.duration;

	var beforeCallback = element.revealed
		? element.config.beforeReveal
		: element.config.beforeReset;

	var afterCallback = element.revealed
		? element.config.afterReveal
		: element.config.afterReset;

	var elapsed = 0;
	if (element.callbackTimer) {
		elapsed = Date.now() - element.callbackTimer.start;
		window.clearTimeout(element.callbackTimer.clock);
	}

	beforeCallback(element.node);

	element.callbackTimer = {
		start: Date.now(),
		clock: window.setTimeout(function () {
			afterCallback(element.node);
			element.callbackTimer = null;
			if (element.revealed && !element.config.reset && element.config.cleanup) {
				clean.call(this$1, element.node);
			}
		}, duration - elapsed)
	};
}

var nextUniqueId = (function () {
	var uid = 0;
	return function () { return uid++; }
})();

function sequence(element, pristine) {
	if ( pristine === void 0 ) pristine = this.pristine;

	/**
	 * We first check if the element should reset.
	 */
	if (!element.visible && element.revealed && element.config.reset) {
		return animate.call(this, element, { reset: true })
	}

	var seq = this.store.sequences[element.sequence.id];
	var i = element.sequence.index;

	if (seq) {
		var visible = new SequenceModel(seq, 'visible', this.store);
		var revealed = new SequenceModel(seq, 'revealed', this.store);

		seq.models = { visible: visible, revealed: revealed };

		/**
		 * If the sequence has no revealed members,
		 * then we reveal the first visible element
		 * within that sequence.
		 *
		 * The sequence then cues a recursive call
		 * in both directions.
		 */
		if (!revealed.body.length) {
			var nextId = seq.members[visible.body[0]];
			var nextElement = this.store.elements[nextId];

			if (nextElement) {
				cue.call(this, seq, visible.body[0], -1, pristine);
				cue.call(this, seq, visible.body[0], +1, pristine);
				return animate.call(this, nextElement, { reveal: true, pristine: pristine })
			}
		}

		/**
		 * If our element isn’t resetting, we check the
		 * element sequence index against the head, and
		 * then the foot of the sequence.
		 */
		if (
			!seq.blocked.head &&
			i === [].concat( revealed.head ).pop() &&
			i >= [].concat( visible.body ).shift()
		) {
			cue.call(this, seq, i, -1, pristine);
			return animate.call(this, element, { reveal: true, pristine: pristine })
		}

		if (
			!seq.blocked.foot &&
			i === [].concat( revealed.foot ).shift() &&
			i <= [].concat( visible.body ).pop()
		) {
			cue.call(this, seq, i, +1, pristine);
			return animate.call(this, element, { reveal: true, pristine: pristine })
		}
	}
}

function Sequence(interval) {
	var i = Math.abs(interval);
	if (!isNaN(i)) {
		this.id = nextUniqueId();
		this.interval = Math.max(i, 16);
		this.members = [];
		this.models = {};
		this.blocked = {
			head: false,
			foot: false
		};
	} else {
		throw new RangeError('Invalid sequence interval.')
	}
}

function SequenceModel(seq, prop, store) {
	var this$1 = this;

	this.head = [];
	this.body = [];
	this.foot = [];

	each(seq.members, function (id, index) {
		var element = store.elements[id];
		if (element && element[prop]) {
			this$1.body.push(index);
		}
	});

	if (this.body.length) {
		each(seq.members, function (id, index) {
			var element = store.elements[id];
			if (element && !element[prop]) {
				if (index < this$1.body[0]) {
					this$1.head.push(index);
				} else {
					this$1.foot.push(index);
				}
			}
		});
	}
}

function cue(seq, i, direction, pristine) {
	var this$1 = this;

	var blocked = ['head', null, 'foot'][1 + direction];
	var nextId = seq.members[i + direction];
	var nextElement = this.store.elements[nextId];

	seq.blocked[blocked] = true;

	setTimeout(function () {
		seq.blocked[blocked] = false;
		if (nextElement) {
			sequence.call(this$1, nextElement, pristine);
		}
	}, seq.interval);
}

function initialize() {
	var this$1 = this;

	rinse.call(this);

	each(this.store.elements, function (element) {
		var styles = [element.styles.inline.generated];

		if (element.visible) {
			styles.push(element.styles.opacity.computed);
			styles.push(element.styles.transform.generated.final);
			element.revealed = true;
		} else {
			styles.push(element.styles.opacity.generated);
			styles.push(element.styles.transform.generated.initial);
			element.revealed = false;
		}

		element.node.setAttribute('style', styles.filter(function (s) { return s !== ''; }).join(' '));
	});

	each(this.store.containers, function (container) {
		var target =
			container.node === document.documentElement ? window : container.node;
		target.addEventListener('scroll', this$1.delegate);
		target.addEventListener('resize', this$1.delegate);
	});

	/**
	 * Manually invoke delegate once to capture
	 * element and container dimensions, container
	 * scroll position, and trigger any valid reveals
	 */
	this.delegate();

	/**
	 * Wipe any existing `setTimeout` now
	 * that initialization has completed.
	 */
	this.initTimeout = null;
}

function isMobile(agent) {
	if ( agent === void 0 ) agent = navigator.userAgent;

	return /Android|iPhone|iPad|iPod/i.test(agent)
}

function deepAssign(target) {
	var sources = [], len = arguments.length - 1;
	while ( len-- > 0 ) sources[ len ] = arguments[ len + 1 ];

	if (isObject(target)) {
		each(sources, function (source) {
			each(source, function (data, key) {
				if (isObject(data)) {
					if (!target[key] || !isObject(target[key])) {
						target[key] = {};
					}
					deepAssign(target[key], data);
				} else {
					target[key] = data;
				}
			});
		});
		return target
	} else {
		throw new TypeError('Target must be an object literal.')
	}
}

function reveal(target, options, syncing) {
	var this$1 = this;
	if ( options === void 0 ) options = {};
	if ( syncing === void 0 ) syncing = false;

	var containerBuffer = [];
	var sequence$$1;
	var interval = options.interval || defaults.interval;

	try {
		if (interval) {
			sequence$$1 = new Sequence(interval);
		}

		var nodes = tealight(target);
		if (!nodes.length) {
			throw new Error('Invalid reveal target.')
		}

		var elements = nodes.reduce(function (elementBuffer, elementNode) {
			var element = {};
			var existingId = elementNode.getAttribute('data-sr-id');

			if (existingId) {
				deepAssign(element, this$1.store.elements[existingId]);

				/**
				 * In order to prevent previously generated styles
				 * from throwing off the new styles, the style tag
				 * has to be reverted to its pre-reveal state.
				 */
				element.node.setAttribute('style', element.styles.inline.computed);
			} else {
				element.id = nextUniqueId();
				element.node = elementNode;
				element.seen = false;
				element.revealed = false;
				element.visible = false;
			}

			var config = deepAssign({}, element.config || this$1.defaults, options);

			if ((!config.mobile && isMobile()) || (!config.desktop && !isMobile())) {
				if (existingId) {
					clean.call(this$1, element);
				}
				return elementBuffer // skip elements that are disabled
			}

			var containerNode = tealight(config.container)[0];
			if (!containerNode) {
				throw new Error('Invalid container.')
			}
			if (!containerNode.contains(elementNode)) {
				return elementBuffer // skip elements found outside the container
			}

			var containerId;
			{
				containerId = getContainerId(
					containerNode,
					containerBuffer,
					this$1.store.containers
				);
				if (containerId === null) {
					containerId = nextUniqueId();
					containerBuffer.push({ id: containerId, node: containerNode });
				}
			}

			element.config = config;
			element.containerId = containerId;
			element.styles = style(element);

			if (sequence$$1) {
				element.sequence = {
					id: sequence$$1.id,
					index: sequence$$1.members.length
				};
				sequence$$1.members.push(element.id);
			}

			elementBuffer.push(element);
			return elementBuffer
		}, []);

		/**
		 * Modifying the DOM via setAttribute needs to be handled
		 * separately from reading computed styles in the map above
		 * for the browser to batch DOM changes (limiting reflows)
		 */
		each(elements, function (element) {
			this$1.store.elements[element.id] = element;
			element.node.setAttribute('data-sr-id', element.id);
		});
	} catch (e) {
		return logger.call(this, 'Reveal failed.', e.message)
	}

	/**
	 * Now that element set-up is complete...
	 * Let’s commit any container and sequence data we have to the store.
	 */
	each(containerBuffer, function (container) {
		this$1.store.containers[container.id] = {
			id: container.id,
			node: container.node
		};
	});
	if (sequence$$1) {
		this.store.sequences[sequence$$1.id] = sequence$$1;
	}

	/**
	 * If reveal wasn't invoked by sync, we want to
	 * make sure to add this call to the history.
	 */
	if (syncing !== true) {
		this.store.history.push({ target: target, options: options });

		/**
		 * Push initialization to the event queue, giving
		 * multiple reveal calls time to be interpreted.
		 */
		if (this.initTimeout) {
			window.clearTimeout(this.initTimeout);
		}
		this.initTimeout = window.setTimeout(initialize.bind(this), 0);
	}
}

function getContainerId(node) {
	var collections = [], len = arguments.length - 1;
	while ( len-- > 0 ) collections[ len ] = arguments[ len + 1 ];

	var id = null;
	each(collections, function (collection) {
		each(collection, function (container) {
			if (id === null && container.node === node) {
				id = container.id;
			}
		});
	});
	return id
}

/**
 * Re-runs the reveal method for each record stored in history,
 * for capturing new content asynchronously loaded into the DOM.
 */
function sync() {
	var this$1 = this;

	each(this.store.history, function (record) {
		reveal.call(this$1, record.target, record.options, true);
	});

	initialize.call(this);
}

var polyfill = function (x) { return (x > 0) - (x < 0) || +x; };
var mathSign = Math.sign || polyfill

/*! @license miniraf v1.0.0

	Copyright 2018 Fisssion LLC.

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

*/
var polyfill$1 = (function () {
	var clock = Date.now();

	return function (callback) {
		var currentTime = Date.now();
		if (currentTime - clock > 16) {
			clock = currentTime;
			callback(currentTime);
		} else {
			setTimeout(function () { return polyfill$1(callback); }, 0);
		}
	}
})();

var index = window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	polyfill$1;

function getGeometry(target, isContainer) {
	/**
	 * We want to ignore padding and scrollbars for container elements.
	 * More information here: https://goo.gl/vOZpbz
	 */
	var height = isContainer ? target.node.clientHeight : target.node.offsetHeight;
	var width = isContainer ? target.node.clientWidth : target.node.offsetWidth;

	var offsetTop = 0;
	var offsetLeft = 0;
	var node = target.node;

	do {
		if (!isNaN(node.offsetTop)) {
			offsetTop += node.offsetTop;
		}
		if (!isNaN(node.offsetLeft)) {
			offsetLeft += node.offsetLeft;
		}
		node = node.offsetParent;
	} while (node)

	return {
		bounds: {
			top: offsetTop,
			right: offsetLeft + width,
			bottom: offsetTop + height,
			left: offsetLeft
		},
		height: height,
		width: width
	}
}

function getScrolled(container) {
	var top, left;
	if (container.node === document.documentElement) {
		top = window.pageYOffset;
		left = window.pageXOffset;
	} else {
		top = container.node.scrollTop;
		left = container.node.scrollLeft;
	}
	return { top: top, left: left }
}

function isElementVisible(element) {
	if ( element === void 0 ) element = {};

	var container = this.store.containers[element.containerId];
	if (!container) { return }

	var viewFactor = Math.max(0, Math.min(1, element.config.viewFactor));
	var viewOffset = element.config.viewOffset;

	var elementBounds = {
		top: element.geometry.bounds.top + element.geometry.height * viewFactor,
		right: element.geometry.bounds.right - element.geometry.width * viewFactor,
		bottom: element.geometry.bounds.bottom - element.geometry.height * viewFactor,
		left: element.geometry.bounds.left + element.geometry.width * viewFactor
	};

	var containerBounds = {
		top: container.geometry.bounds.top + container.scroll.top + viewOffset.top,
		right: container.geometry.bounds.right + container.scroll.left - viewOffset.right,
		bottom:
			container.geometry.bounds.bottom + container.scroll.top - viewOffset.bottom,
		left: container.geometry.bounds.left + container.scroll.left + viewOffset.left
	};

	return (
		(elementBounds.top < containerBounds.bottom &&
			elementBounds.right > containerBounds.left &&
			elementBounds.bottom > containerBounds.top &&
			elementBounds.left < containerBounds.right) ||
		element.styles.position === 'fixed'
	)
}

function delegate(
	event,
	elements
) {
	var this$1 = this;
	if ( event === void 0 ) event = { type: 'init' };
	if ( elements === void 0 ) elements = this.store.elements;

	index(function () {
		var stale = event.type === 'init' || event.type === 'resize';

		each(this$1.store.containers, function (container) {
			if (stale) {
				container.geometry = getGeometry.call(this$1, container, true);
			}
			var scroll = getScrolled.call(this$1, container);
			if (container.scroll) {
				container.direction = {
					x: mathSign(scroll.left - container.scroll.left),
					y: mathSign(scroll.top - container.scroll.top)
				};
			}
			container.scroll = scroll;
		});

		/**
		 * Due to how the sequencer is implemented, it’s
		 * important that we update the state of all
		 * elements, before any animation logic is
		 * evaluated (in the second loop below).
		 */
		each(elements, function (element) {
			if (stale) {
				element.geometry = getGeometry.call(this$1, element);
			}
			element.visible = isElementVisible.call(this$1, element);
		});

		each(elements, function (element) {
			if (element.sequence) {
				sequence.call(this$1, element);
			} else {
				animate.call(this$1, element);
			}
		});

		this$1.pristine = false;
	});
}

function transformSupported() {
	var style = document.documentElement.style;
	return 'transform' in style || 'WebkitTransform' in style
}

function transitionSupported() {
	var style = document.documentElement.style;
	return 'transition' in style || 'WebkitTransition' in style
}

var version = "4.0.5";

var boundDelegate;
var boundDestroy;
var boundReveal;
var boundClean;
var boundSync;
var config;
var debug;
var instance;

function ScrollReveal(options) {
	if ( options === void 0 ) options = {};

	var invokedWithoutNew =
		typeof this === 'undefined' ||
		Object.getPrototypeOf(this) !== ScrollReveal.prototype;

	if (invokedWithoutNew) {
		return new ScrollReveal(options)
	}

	if (!ScrollReveal.isSupported()) {
		logger.call(this, 'Instantiation failed.', 'This browser is not supported.');
		return mount.failure()
	}

	var buffer;
	try {
		buffer = config
			? deepAssign({}, config, options)
			: deepAssign({}, defaults, options);
	} catch (e) {
		logger.call(this, 'Invalid configuration.', e.message);
		return mount.failure()
	}

	try {
		var container = tealight(buffer.container)[0];
		if (!container) {
			throw new Error('Invalid container.')
		}
	} catch (e) {
		logger.call(this, e.message);
		return mount.failure()
	}

	config = buffer;

	if ((!config.mobile && isMobile()) || (!config.desktop && !isMobile())) {
		logger.call(
			this,
			'This device is disabled.',
			("desktop: " + (config.desktop)),
			("mobile: " + (config.mobile))
		);
		return mount.failure()
	}

	mount.success();

	this.store = {
		containers: {},
		elements: {},
		history: [],
		sequences: {}
	};

	this.pristine = true;

	boundDelegate = boundDelegate || delegate.bind(this);
	boundDestroy = boundDestroy || destroy.bind(this);
	boundReveal = boundReveal || reveal.bind(this);
	boundClean = boundClean || clean.bind(this);
	boundSync = boundSync || sync.bind(this);

	Object.defineProperty(this, 'delegate', { get: function () { return boundDelegate; } });
	Object.defineProperty(this, 'destroy', { get: function () { return boundDestroy; } });
	Object.defineProperty(this, 'reveal', { get: function () { return boundReveal; } });
	Object.defineProperty(this, 'clean', { get: function () { return boundClean; } });
	Object.defineProperty(this, 'sync', { get: function () { return boundSync; } });

	Object.defineProperty(this, 'defaults', { get: function () { return config; } });
	Object.defineProperty(this, 'version', { get: function () { return version; } });
	Object.defineProperty(this, 'noop', { get: function () { return false; } });

	return instance ? instance : (instance = this)
}

ScrollReveal.isSupported = function () { return transformSupported() && transitionSupported(); };

Object.defineProperty(ScrollReveal, 'debug', {
	get: function () { return debug || false; },
	set: function (value) { return (debug = typeof value === 'boolean' ? value : debug); }
});

ScrollReveal();

return ScrollReveal;

})));


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	autosize 4.0.2
	license: MIT
	http://www.jacklmoore.com/autosize
*/
!function(e,t){if(true)!(__WEBPACK_AMD_DEFINE_ARRAY__ = [module,exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else { var n; }}(this,function(e,t){"use strict";var n,o,p="function"==typeof Map?new Map:(n=[],o=[],{has:function(e){return-1<n.indexOf(e)},get:function(e){return o[n.indexOf(e)]},set:function(e,t){-1===n.indexOf(e)&&(n.push(e),o.push(t))},delete:function(e){var t=n.indexOf(e);-1<t&&(n.splice(t,1),o.splice(t,1))}}),c=function(e){return new Event(e,{bubbles:!0})};try{new Event("test")}catch(e){c=function(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!1),t}}function r(r){if(r&&r.nodeName&&"TEXTAREA"===r.nodeName&&!p.has(r)){var e,n=null,o=null,i=null,d=function(){r.clientWidth!==o&&a()},l=function(t){window.removeEventListener("resize",d,!1),r.removeEventListener("input",a,!1),r.removeEventListener("keyup",a,!1),r.removeEventListener("autosize:destroy",l,!1),r.removeEventListener("autosize:update",a,!1),Object.keys(t).forEach(function(e){r.style[e]=t[e]}),p.delete(r)}.bind(r,{height:r.style.height,resize:r.style.resize,overflowY:r.style.overflowY,overflowX:r.style.overflowX,wordWrap:r.style.wordWrap});r.addEventListener("autosize:destroy",l,!1),"onpropertychange"in r&&"oninput"in r&&r.addEventListener("keyup",a,!1),window.addEventListener("resize",d,!1),r.addEventListener("input",a,!1),r.addEventListener("autosize:update",a,!1),r.style.overflowX="hidden",r.style.wordWrap="break-word",p.set(r,{destroy:l,update:a}),"vertical"===(e=window.getComputedStyle(r,null)).resize?r.style.resize="none":"both"===e.resize&&(r.style.resize="horizontal"),n="content-box"===e.boxSizing?-(parseFloat(e.paddingTop)+parseFloat(e.paddingBottom)):parseFloat(e.borderTopWidth)+parseFloat(e.borderBottomWidth),isNaN(n)&&(n=0),a()}function s(e){var t=r.style.width;r.style.width="0px",r.offsetWidth,r.style.width=t,r.style.overflowY=e}function u(){if(0!==r.scrollHeight){var e=function(e){for(var t=[];e&&e.parentNode&&e.parentNode instanceof Element;)e.parentNode.scrollTop&&t.push({node:e.parentNode,scrollTop:e.parentNode.scrollTop}),e=e.parentNode;return t}(r),t=document.documentElement&&document.documentElement.scrollTop;r.style.height="",r.style.height=r.scrollHeight+n+"px",o=r.clientWidth,e.forEach(function(e){e.node.scrollTop=e.scrollTop}),t&&(document.documentElement.scrollTop=t)}}function a(){u();var e=Math.round(parseFloat(r.style.height)),t=window.getComputedStyle(r,null),n="content-box"===t.boxSizing?Math.round(parseFloat(t.height)):r.offsetHeight;if(n<e?"hidden"===t.overflowY&&(s("scroll"),u(),n="content-box"===t.boxSizing?Math.round(parseFloat(window.getComputedStyle(r,null).height)):r.offsetHeight):"hidden"!==t.overflowY&&(s("hidden"),u(),n="content-box"===t.boxSizing?Math.round(parseFloat(window.getComputedStyle(r,null).height)):r.offsetHeight),i!==n){i=n;var o=c("autosize:resized");try{r.dispatchEvent(o)}catch(e){}}}}function i(e){var t=p.get(e);t&&t.destroy()}function d(e){var t=p.get(e);t&&t.update()}var l=null;"undefined"==typeof window||"function"!=typeof window.getComputedStyle?((l=function(e){return e}).destroy=function(e){return e},l.update=function(e){return e}):((l=function(e,t){return e&&Array.prototype.forEach.call(e.length?e:[e],function(e){return r(e)}),e}).destroy=function(e){return e&&Array.prototype.forEach.call(e.length?e:[e],i),e},l.update=function(e){return e&&Array.prototype.forEach.call(e.length?e:[e],d),e}),t.default=l,e.exports=t.default});

/***/ }),
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */
/***/ (function(module, exports) {

// Disabled buttons


document.addEventListener('click', function (e) {
    if (e.target.closest('.btn--disabled')) {
        e.preventDefault()
        e.stopPropagation()
        return false
    }
})

/***/ }),
/* 14 */
/***/ (function(module, exports) {

// JS

(function () {
    let nav = document.querySelector('#nav');
    if (!nav) return // Exit if #nav doesn't exist
   

    let toggleBtn = nav.parentNode.querySelectorAll('.nav__toggle');

    let navBG = nav.querySelector('.nav__bg')
    let navOpen = new CustomEvent('navOpen')
    let navClose = new CustomEvent('navClose')
    let isOpen = false;
    let navList = nav.querySelector('.nav__list')


    toggleBtn.forEach(element => {
        element.addEventListener('click', toggleNav);
    });

    navBG.addEventListener('click', toggleNav);

    if(window.innerWidth > 1200){
        new ps(navList, {
            maxScrollbarLength: 100,
            wheelSpeed: .1,
            swipeEasing: true,
            wheelPropagation: false
        });
    }

    function toggleNav() {
        if (isOpen) {
            
            
            document.dispatchEvent(navClose)
            nav.classList.remove('open')
            // toggleBtn.classList.remove('open')
            toggleBtn.forEach(element => {
                element.classList.remove('open')
            });
        } else {
            
            
            document.dispatchEvent(navOpen)
            nav.classList.add('open')
            // toggleBtn.classList.add('open')
            toggleBtn.forEach(element => {
                element.classList.add('open')
            });
        }

        isOpen = !isOpen
    }


})()

/***/ }),
/* 15 */
/***/ (function(module, exports) {

(function () {
    let dropdownBtn = S.find('.has-dropdown');
    S.on(dropdownBtn, 'click', toggleDropdown)
    

    function toggleDropdown(e) {
        e.stopPropagation();

        let nextDropdown = S.find(this, '.dropdown')[0]
        let nextDropdownScrollHeight = nextDropdown.scrollHeight


        let hasDropdown = S.find(this)
        let clickedOnIcon = S.find(e.target)[0].closest('.dropdown__icon') > 0 || S.hasClass(e.target, 'dropdown__icon')

        if (clickedOnIcon == false) {
            if (S.hasClass(hasDropdown, 'open')) {
                S.removeClass(hasDropdown, 'open')
                nextDropdown.style.maxHeight = nextDropdownScrollHeight + 'px'
                setTimeout(() => {
                    nextDropdown.style.maxHeight = '0px'
                }, 250);
            } else {
                S.addClass(hasDropdown, 'open')
                nextDropdown.style.maxHeight = nextDropdownScrollHeight + 'px'
                nextDropdown.addEventListener('transitionend', removeStyles)
            }
        } else {
            if (S.hasClass(this, 'open')) {
                nextDropdown.style.maxHeight = nextDropdownScrollHeight + 'px'
                setTimeout(() => {
                    nextDropdown.style.maxHeight = '0px'
                }, 250);
            } else {
                nextDropdown.style.maxHeight = nextDropdownScrollHeight + 'px'
                nextDropdown.addEventListener('transitionend', removeStyles)
            }

            let hasDropdownElm = S.find(e.target)[0].closest('.has-dropdown')
            S.toggleClass(hasDropdownElm, 'open')
        }
    }

    function removeStyles() {
        this.style.maxHeight = 'none'
        this.removeEventListener('transitionend', removeStyles)
    }
})()

/***/ }),
/* 16 */
/***/ (function(module, exports) {



/***/ }),
/* 17 */
/***/ (function(module, exports) {

let allInputs = S.find('input, textarea');

checkInputs();
S.on(allInputs, 'change', checkInputs)


function checkInputs() {
    S.each(allInputs, function () {
        if (this.value != '') {
            S.addClass(this, 'active')
        } else {
            S.removeClass(this, 'active')
        }
    })
}

/***/ }),
/* 18 */
/***/ (function(module, exports) {

class Acc {
    constructor(el) {
        this.parent = el
        this.items = S.find(this.parent, '.accordion__item').map(el => {
            return {
                $item: el,
                $content: S.first(el, '.accordion__content'),
                $title: S.first(el, '.accordion__title'),
                isOpen: S.hasClass(el, 'active')
            }
        })
        this.activeItemIndex = this.items.map(item => item.isOpen).indexOf(true)
        this.collapse = this.parent.dataset.collapse

        // Events
        this.parent.addEventListener('click', (e) => {
            // e.stopPropagation()
            // if(!e.target.closest('[data-modal]')) {
            //     e.stopPropagation()
            // }
            if (S.closest(e.target, '.accordion__title')) {
                let clickedIndex = this.getClickedIndex(e.target)
                this.toggleItem(clickedIndex)
            }
        })
    }

    getClickedIndex(target) {
        let clickedItem = S.closest(target, '.accordion__item')
        let indexOfClickedItem = this.items.map(item => {
            return item.$item == clickedItem
        }).indexOf(true)

        return indexOfClickedItem
    }

    toggleItem(index) {
        let clickedItem = this.items[index]

        // Collapse active slide
        if (this.collapse && this.activeItemIndex != -1) {
            this.collapseActive()
        }

        // Toggle clicked slide
        if (clickedItem.isOpen) {
            S.slideCloseCss(clickedItem.$content)
            S.removeClass(clickedItem.$item, 'active')
        } else {
            let accordionItemOpened = new CustomEvent('accordionItemOpened', {
                detail: {
                    item: clickedItem
                }
            })
            document.dispatchEvent(accordionItemOpened)
            S.slideOpenCss(clickedItem.$content)
            S.addClass(clickedItem.$item, 'active')
        }

        clickedItem.isOpen = !clickedItem.isOpen
        this.activeItemIndex = index
    }

    collapseActive() {
        S.slideCloseCss(this.items[this.activeItemIndex].$content)
        this.items[this.activeItemIndex].isOpen = !this.items[this.activeItemIndex].isOpen
    }
}

// Accordion initialization
let allAccs = []
S.each('.accordion', function() {
    allAccs.push(new Acc(this))
})

// console.log(allAccs);

// S.makeGlobal('initAccordions', initAccordions)

/***/ }),
/* 19 */
/***/ (function(module, exports) {

// JS
// import Swiper from '../../components/swiper/swiper'

// Init all gallery swipers
if (S.find('.gallery-swiper').length > 0) {
    S.find('.gallery-swiper').forEach(swiper => {
        if(swiper.closest('.accordion')) return
        new Swiper(swiper, {
            navigation: {
                nextEl: '.gallery-swiper__btn-next',
                prevEl: '.gallery-swiper__btn-prev',
            },
        })
    });
    // new Swiper('.gallery-swiper', {
    //     navigation: {
    //         nextEl: '.gallery-swiper__btn-next',
    //         prevEl: '.gallery-swiper__btn-prev',
    //     },
    // })
}

// var swiperItems = S.find('.gallery-swiper')
// if (swiperItems.length > 0) {

//     S.each(swiperItems, function () {
//         console.log(this);

//         var nextEl = S.first(this, '.swiper-button-next')
//         var prevEl = S.first(this, '.swiper-button-prev')
//         var swiper = new Swiper('.gallery-swiper', {
//             navigation: {
//                 nextEl: nextEl,
//                 prevEl: prevEl,
//             },
//         })

//         console.log(swiper.navigation);


//         // console.log(swiper.navigation);
//     })

// }

// Create class
class Gallery {
    constructor(parent) {
        let self = this;
        this.parent = parent;
        this.modalSwiperElm = this.parent.querySelector('.modal .swiper-container')
        this.modalElm = this.parent.querySelector('.modal')

        this.parent.addEventListener('click', function (e) {
            let target = e.target.closest('.gallery__item')
            if (target) {
                self.showGallery(+target.getAttribute('data-index'))
            }
        })
    }
    showGallery(index) {
        this.modalElm.wsModal.openModal()
        this.modalSwiperElm.swiper.slideTo(index)
    }
}


if (S.find('.gallery').length > 0) {
    S.each('.gallery', function () {
        new Gallery(this)
    })
}

/***/ }),
/* 20 */
/***/ (function(module, exports) {



/***/ }),
/* 21 */
/***/ (function(module, exports) {



/***/ }),
/* 22 */
/***/ (function(module, exports) {

class Rating {
    constructor(parent) {
        this.elm = parent
        this.inputs = [...parent.querySelectorAll('input')]
        this.icons = [...parent.querySelectorAll('.icon')]
        this.checkedIndex;
        this.elm.addEventListener('click', this.clickHandler.bind(this))
    }

    clickHandler() {
        //- shoots twice
        this.setIndexOfChecked()
        this.changeActiveItem()
    }

    setIndexOfChecked() {
        this.inputs.forEach(input => {
            if (input.checked) {
                this.checkedIndex = S.index(input.parentElement);
            }
        })
    }

    changeActiveItem() {

        this.icons.forEach((icon, i) => {
            if (i <= this.checkedIndex) {
                icon.classList.add('active')
            } else {
                icon.classList.remove('active')
            }
        })
    }
}

S.each('.raiting', function () {

    new Rating(this)

})

/***/ }),
/* 23 */
/***/ (function(module, exports) {

class Scroll {
    constructor(container, options) {
        console.log(this.inited);
        
        var self = this;

        var options = options || {}

        if(this.inited) return

        this.inited = true
        console.log(this.inited);

        self.container = S.find(container);
        S.on(self.container, 'click', this.scrollX.bind(self))

        Object.assign(this, {
            breackpoint: 1200,
            speed: 1
        })

        Object.assign(this, options)
    }

    scrollX(e) {
        if (window.innerWidth <= this.breackpoint) {
            var scrollElm = e.target.closest('.scroll-x__item')

            var xCoord = (scrollElm.offsetLeft - this.container[0].offsetLeft) - (this.container[0].offsetWidth / 2) + (scrollElm.offsetWidth / 2)
            // var xCoord = scrollElm.offsetLeft - (this.container[0].offsetWidth - scrollElm.offsetWidth) / 2;


            if (scrollElm) {
                TweenMax.to(this.container[0], this.speed, {
                    scrollTo: {
                        x: xCoord
                    },
                    ease: Power2.easeInOut
                });
            }
        }
    }


    scrollTest() {

    }
}

var scrolls = S.find('.scroll-x:not(.product-info__details-img-wrapper)')

if (scrolls.length > 0) {
    console.log(scrolls.length);
    

    S.each(scrolls, function () {

        var f = new Scroll(this, {
            breackpoint: 1200,
            speed: 0.5
        })

    })
}

// var desktopScrolls = S.find('.scroll-x.product-info__details-img-wrapper')

// console.log(desktopScrolls.length);

// if (desktopScrolls.length > 0) {
//     console.log(desktopScrolls.length);
    

//     S.each(desktopScrolls, function () {

//         var f = new Scroll(this, {
//             breackpoint: 1920,
//             speed: 0.5
//         })

//     })
// }


/***/ }),
/* 24 */
/***/ (function(module, exports) {

// import Header from '../header/header'
class Searchfield {
    constructor(selector) {
        var self = this
        this.$parent = S.first(selector)
        this.searchField = S.first(this.$parent, '.searchfield__wrapper')
        this.searchForm = S.first(this.$parent, '.searchfield__form')
        this.searchInput = S.first(this.searchForm, 'input')

        this.searchBg = S.first(this.$parent, '.searchfield__bg')

        this.searchBtn = S.first(this.$parent, '.searchfield__btn')
        this.closeSearchBtn = S.first(this.$parent, '.searchfield__close-btn')

        this.searchingFiltersList = S.first(this.$parent, '.searchfield__filters-list-wrapper')
        if (this.searchingFiltersList) {
            this.searchingFiltersListParent = S.closest(this.searchingFiltersList, '.searchfield__filters-wrapper')
            this.openFiltersListBtn = S.first(this.$parent, '.searchfield__filters-open-btn')
        }

        this.searchingResultsList = S.first(this.$parent, '.searchfield__results-list-wrapper')
        this.searchingResultsListStub = S.first(this.$parent, '.searchfield__results-list-wrapper--no-results')
        this.searchingResultsListParent = S.first(this.searchingResultsList, '.searchfield__results-list-wrapper')

        if (document.body.clientWidth > 991) {
            if (this.searchingFiltersList) {
                var filtersList = S.first(this.searchingFiltersList, '.searchfield__filters-list')
                this.psFilters = new ps(filtersList, {
                    maxScrollbarLength: 50,
                    wheelSpeed: .25,
                    swipeEasing: true,
                    wheelPropagation: false
                });
            }
            if (this.searchingResultsList) {
                this.psResults = new ps('.searchfield__results-list', {
                    maxScrollbarLength: 50,
                    wheelSpeed: .25,
                    swipeEasing: true,
                    wheelPropagation: false
                });
            }

        }

        // listeners


        this.searchForm.addEventListener('click', this.showSearch.bind(this))


        this.searchBtn.addEventListener('click', this.showSearch.bind(this)) // mobile - form is hidden

        this.searchInput.addEventListener('input', this.showSearchResults.bind(this))
        if (this.searchingFiltersList) {

            this.searchingFiltersList.addEventListener('mouseenter', this.hideListGradient)
        }

        this.searchingResultsList.addEventListener('mouseenter', this.hideListGradient)


        if (document.body.clientWidth <= 991) {
            if (this.searchingFiltersList) {

                this.openFiltersListBtn.addEventListener('click', this.toggleFiltersList.bind(this))
            }
            this.searchingResultsList.addEventListener('scroll', this.hideListGradient)
        }

        if (this.searchBg) {
            this.searchBg.addEventListener('click', this.hideSearch.bind(this))
        } else {
            document.body.addEventListener('click', this.hideSearch.bind(this))
        }

        if (this.closeSearchBtn) {
            this.closeSearchBtn.addEventListener('click', this.hideSearch.bind(this)) // mobile
        }

    }

    showSearch() {
        if (this.searchField.matches('.open')) return

        S.addClass(this.searchField, 'open')
        if (this.searchInput.value == '') {

            S.addClass(this.searchingResultsListStub, 'open')
        } else {
            this.showSearchResults()
        }

        var evt = new CustomEvent('searcField-opened')
        document.dispatchEvent(evt)

        // this is needed if searchfield is in header and we need to switch scrolling propagation
        // Header.searchOpen = true


    }

    hideSearch(e) {
        if (e.target.closest('.form')) {
            return
        }

        // this is needed if searchfield is in header and we need to switch scrolling propagation
        // Header.searchOpen = false
        S.removeClass('.searchfield .open', 'open')
        var evt = new CustomEvent('searcField-closed')
        document.dispatchEvent(evt)

    }

    hideListGradient() {
        var parent = this.parentElement

        if (parent.matches('.no-gradient')) return
        S.addClass(parent, 'no-gradient')
    }

    toggleFiltersList() {
        if (this.searchingFiltersListParent.matches('.open')) {
            S.removeClass(this.searchingFiltersListParent, 'open')

        } else {
            S.addClass(this.searchingFiltersListParent, 'open')
        }
    }

    showSearchResults() {
        if (this.searchingResultsList.matches('.open')) return
        S.removeClass(this.searchingResultsListStub, 'open')
        S.addClass(this.searchingResultsList, 'open')

    }

}

function initSearchfield() {
    S.each('.searchfield', function () {
        new Searchfield(this)
    })
}

initSearchfield()

S.makeGlobal('initSearchfield', initSearchfield)

/***/ }),
/* 25 */
/***/ (function(module, exports) {



/***/ }),
/* 26 */
/***/ (function(module, exports) {

(function () {
  let allLeaferArr = [];
  let debounce = (a, b = 250, c) => (...d) =>
    clearTimeout(c, (c = setTimeout(a, b, ...d)));

  // Global store for window.resize function
  window.addEventListener(
    "resize",
    debounce(function () {
      allLeaferArr.forEach(function (el) {
        el.resize();
      });
    }, 200)
  );

  class Leafer {
    constructor(selector, options) {
      this.parentElm = S.first(selector);


      // Check If parent undefind or null
      if (!this.parentElm) {
        throw new Error("Leafer: Selector is undefined or null");
      }

      // Detect if this DOM element already inited as Wgallery
      // Return and burn all code blow =D
      if (this.parentElm.isLeaferInstance) {
        return;
      }
      this.parentElm.isLeaferInstance = true;
      this.wrapElm = [...this.parentElm.children].filter(elm =>
        elm.classList.contains("leafer__wrap")
      )[0];
      this.navigation;
      // this.navigation = S.first(this.parentElm, '.leafer__btn--prev')
      this.allSlidesArr = [...this.wrapElm.children].filter(elm =>
        elm.classList.contains("leafer__slide")
      );
      if(this.allSlidesArr.length <=0) return
      this.totalSlides = this.allSlidesArr.length;
      this.speed = 0.3;
      this.activeSlide = 0;
      this.currentSlidePos = 0;
      this.vertical = false;
      this.onSlideChange = function () {};
      this.onInit = function () {};
      this.direction = {
        margin: "marginRight",
        way: "X",
        clientSizeCheck: "clientWidth",
        scrollSizeCheck: "scrollWidth",
        offsetSizeCheck: "offsetWidth"
      };

      Object.assign(this, options);
      allLeaferArr.push(this); // Push new instance to allLeaferArr[] for resize storage event
      this.allLeaferArr = allLeaferArr;

      // Init state
      S.addClass(this.allSlidesArr[this.activeSlide], "active");
      if (this.vertical) {
        this.direction = {
          margin: "marginBottom",
          way: "Y",
          clientSizeCheck: "clientHeight",
          scrollSizeCheck: "scrollHeight",
          offsetSizeCheck: "offsetHeight"
        };
      }
      
      this.slidesPerView = this.getSlidesPerView();
      
      this.navigation = this.getNavigationBtns(options);



      this.maxRange = this.getMaxRange();
      this.slideSize = this.getSlideSize();

      // Init classes
      if (this.vertical) {
        S.addClass(this.wrapElm, "leafer--vertical");
      }
      this.setDisabledBtns();

      // Events
      if (this.navigation.next) {
        S.on(this.navigation.next, "click", this.slideNext.bind(this));
      }
      if (this.navigation.prev) {
        S.on(this.navigation.prev, "click", this.slidePrev.bind(this));
      }
      // Events: Touch
      if (this.isTouchDevice() && !this.vertical) {
        this.tX;
        this.tStartX;
        this.tDistance;
        this.parentElm.addEventListener(
          "touchstart",
          this.touchStartHandle.bind(this)
        );
        this.parentElm.addEventListener(
          "touchend",
          this.touchEndHandle.bind(this)
        );
        this.parentElm.addEventListener(
          "touchmove",
          this.touchMoveHandle.bind(this)
        );
      }

      this.onInit();
    }

    slideNext() {
      if (this.stopSlideNext()) {
        return;
      }



      this.beforeNextSlide ? this.beforeNextSlide() : function () {};

      this.activeSlide++;
      this.changeActiveSlideClass();

      let scrollRange = this.getScrollRange();
      this.animateSlide(scrollRange);
      this.changeSlideCurrentPos(scrollRange);
      this.setDisabledBtns();
      this.onSlideChange();
    }

    slidePrev() {
      if (this.activeSlide <= 0) {
        return;
      }

      this.activeSlide--;
      this.changeActiveSlideClass();

      if (this.canAnimate()) {
        this.animateSlide(this.getScrollRange());
        this.changeSlideCurrentPos(this.getScrollRange());
      }

      this.setDisabledBtns();
      this.onSlideChange();
    }

    slideTo(index) {
      index =
        index > this.allSlidesArr.length ? this.allSlidesArr.length - 1 : index;
      index = index <= 0 ? 0 : index;
      this.activeSlide =
        index >= this.totalSlides - this.slidesPerView ?
        this.totalSlides - this.slidesPerView :
        index;



      this.changeActiveSlideClass();

      this.animateSlide(this.getScrollRange());
      this.changeSlideCurrentPos(this.getScrollRange());

      this.setDisabledBtns();
      this.onSlideChange();
    }

    slideClickHandler(e) {
      // console.log('slide click handler');
      let slide = e.target.closest(".leafer__slide");
      this.activeSlide = this.allSlidesArr.indexOf(slide);
      this.slideTo(this.activeSlide);
    }

    touchStartHandle(e) {
      e.stopPropagation();

      this.tStartX = e.touches[0].clientX - this.parentElm.offsetLeft;
      this.tStartY = e.touches[0].clientY - this.parentElm.offsetTop;
    }

    touchEndHandle(e) {
      // console.log('touch end');

      let closestToEdgeSlideIndex = 0;
      // closestToEdgeSlideIndex = -Math.round((this.tDistance - this.slideSize) / this.slideSize) - 1;
      // closestToEdgeSlideIndex;
      if (this.tDistance <= -50) {
        closestToEdgeSlideIndex = 1;
      } else if (this.tDistance >= 50) {
        closestToEdgeSlideIndex = -1;
      }


      this.activeSlide += closestToEdgeSlideIndex;
      this.wrapElm.style.transition = ``;
      this.slideTo(this.activeSlide);
    }

    touchMoveHandle(e) {
      // 1. Detect Y scroll amount
      // 2. Detect X scroll amount
      // 3. Early exit if is vertical scrolling (Y > X). Otherwise prevent scrolling (e.preventDefault)

      // 1.
      let tY = e.touches[0].clientY - this.parentElm.offsetTop;
      let tYDistance = tY - this.tStartY;
      let deltaY = Math.abs(tYDistance)

      // 2.
      this.tX = e.touches[0].clientX - this.parentElm.offsetLeft;
      let range = this.currentSlidePos + (this.tX - this.tStartX);
      this.tDistance = this.tX - this.tStartX;
      let deltaX = Math.abs(this.tDistance)

      // 3.
      if (deltaY > deltaX) {
        return
      } else if (deltaY <= deltaX && e.cancelable) {
        e.preventDefault()
      }


      if (-range >= this.maxRange) {
        return;
      }

      if (!this.wrapElm.transition) {
        this.wrapElm.style.transition = `none`;
      }

      this.wrapElm.style.transform = `translateX(${range}px)`;

      // let tY = e.touches[0].clientY - this.parentElm.offsetTop;
      // let tYDistance = tY - this.tStartY;

      // console.log(Math.abs(tYDistance));
      // if (Math.abs(tYDistance) > 10) {
      //   console.log("prevent");
      //   // this.prevent(e)
      //   this.isScrollVertical = true
      // } else {
      //   this.isScrollVertical = false
      // }

      // if (!this.isScrollVertical) {
      //   console.log('continue');


      // }

    }

    animateSlide(scrollRange) {
      this.wrapElm.style.transform = `translate${
        this.direction.way
      }(${-scrollRange}px)`;
    }

    stopSlideNext() {
      return this.activeSlide > this.totalSlides - 1 - this.slidesPerView;
    }

    canAnimate() {
      return this.activeSlide <= this.allSlidesArr.length - this.slidesPerView;
    }

    resize() {
      this.slidesPerView = this.getSlidesPerView();
      this.maxRange = this.getMaxRange();
      this.slideSize = this.getSlideSize();
      this.animateSlide(this.getScrollRange());
    }

    // Get
    getScrollRange() {
      let result = this.slideSize * this.activeSlide;

      if (result > Math.abs(this.maxRange)) {
        return this.maxRange;
      }

      return result;
    }

    getSlidesPerView() {
      let slideSize = this.allSlidesArr[0][this.direction.scrollSizeCheck];
      let parentSize = this.parentElm[this.direction.offsetSizeCheck];

      let result = Number(Math.floor(parentSize / slideSize)) || 1;
      return result;
    }

    getMaxRange() {
      let firstSlide = this.allSlidesArr[0];
      let slideSize = firstSlide[this.direction.scrollSizeCheck];
      let margin = +window
        .getComputedStyle(firstSlide)[this.direction.margin].replace("px", "") * 2;

      return (slideSize + margin) * (this.totalSlides - this.slidesPerView);
    }

    getSlideSize() {
      let slideSize = this.allSlidesArr[0][this.direction.clientSizeCheck];

      let margin = +window
        .getComputedStyle(this.allSlidesArr[0])[this.direction.margin].replace("px", "") * 2;
      return slideSize + margin;
    }

    getNavigationBtns() {
      return {
        next: this.nextBtn ?
          S.first(this.nextBtn) : S.first(this.parentElm, ".leafer__btn--next"),
        prev: this.prevBtn ?
          S.first(this.prevBtn) : S.first(this.parentElm, ".leafer__btn--prev")
      };
    }

    // Set
    setDisabledBtns() {
      if (this.totalSlides < this.slidesPerView) {
        S.addClass(this.navigation.prev, "leafer--invisible");
        S.addClass(this.navigation.next, "leafer--invisible");
        return
      }
      // Maybe need to do it more performant?
      if (this.activeSlide <= 0) {
        // console.log(this.prevBtn);

        S.addClass(this.navigation.prev, "leafer--disabled");
        S.removeClass(this.navigation.next, "leafer--disabled");
      } else {
        S.removeClass(this.navigation.prev, "leafer--disabled");
      }

      if (this.stopSlideNext()) {
        S.addClass(this.navigation.next, "leafer--disabled");
        S.removeClass(this.navigation.prev, "leafer--disabled");
      } else {
        S.removeClass(this.navigation.next, "leafer--disabled");
      }
    }

    isTouchDevice() {
      return "ontouchstart" in document.documentElement;
    }

    // Changers
    changeSlideCurrentPos(scrollRange) {
      this.currentSlidePos = -scrollRange;
    }

    changeActiveSlideClass() {
      S.removeClass(this.allSlidesArr, "active");
      S.addClass(this.allSlidesArr[this.activeSlide], "active");
    }
  }

  window.Leafer = Leafer;
})();

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

!function(e){ false?undefined:module.exports=e}(function(e,n){"use strict";n=n||{};var t=n.tagName||"span",o=null!=n.classPrefix?n.classPrefix:"char",r=1,a=function(e){for(var n=e.parentNode,a=e.nodeValue,c=a.length,l=-1;++l<c;){var d=document.createElement(t);o&&(d.className=o+r,r++),d.appendChild(document.createTextNode(a[l])),n.insertBefore(d,e)}n.removeChild(e)};return function c(e){for(var n=[].slice.call(e.childNodes),t=n.length,o=-1;++o<t;)c(n[o]);e.nodeType===Node.TEXT_NODE&&a(e)}(e),e});

/***/ }),
/* 28 */
/***/ (function(module, exports) {



/***/ }),
/* 29 */
/***/ (function(module, exports) {

class changeImg {
    constructor(selector, options) {
        var defaults = {
            changeSpeed: 1000,
            container: '.resultImg'
        }
        this.params = Object.assign({}, defaults, options)
        this.cardEl = selector;
        this.current = 1
        this.interval;
        this.isLoaded = true;
        this.resultImg = S.first(this.cardEl, this.params.container);
        this.allThumbsArr = [...this.cardEl.querySelectorAll('.thumb')];
        this.srcImagesArr = this.createSrcArr();        
        this.originalImg = S.first(this.cardEl, '.img--main')
        this.imgFirst = S.first(this.cardEl, '.img--first')
        this.imgSecond = S.first(this.cardEl, '.img--second')
        this.imagesToChange = [this.imgFirst, this.imgSecond]
        this.currentImg = this.imagesToChange[0]
        this.prevImage = this.imagesToChange[1]
        this.isFirstImageActive = true

        this.hoveredThumbIndex = 0
        
        



        if (this.resultImg) {
            this.mainImageSrc = S.attr(this.resultImg.querySelector('img'), 'src')
            this.cardEl.addEventListener('mouseenter', this.changeImage.bind(this), true)
            this.cardEl.addEventListener('mouseleave', this.removeEvent.bind(this), true)
        }
    }
    changeImage(e) {

        
        
        
        let target = e.target
        


        if (!target.classList.contains('thumb')) {
            return
        }

        


        if(this.allThumbsArr.indexOf(target) == this.hoveredThumbIndex) return



        let index = this.allThumbsArr.indexOf(target)
        let srcArr = this.srcImagesArr[index]

        this.hoveredThumbIndex = this.allThumbsArr.indexOf(target)

        if (srcArr.length == 1) {
            srcArr[0]
            
            
            
            

            if (this.isLoaded) {
                if (this.isFirstImageActive) {
                    this.currentImg = this.imagesToChange[0]
                    this.prevImage = this.imagesToChange[1]
                } else {
                    this.currentImg = this.imagesToChange[1]
                    this.prevImage = this.imagesToChange[0]
                }

                if (this.current >= srcArr.length) {
                    this.current = 0;
                }

                this.animate(srcArr[this.current])
                this.isLoaded = false
            }

            

            // this.currentImg = this.imagesToChange[0]
            clearInterval(this.interval)

            return

        }

        this.preloadImages(srcArr)
        this.animate(srcArr[this.current])

        this.interval = setInterval(() => {
            if (this.isLoaded) {
                if (this.isFirstImageActive) {
                    this.currentImg = this.imagesToChange[0]
                    this.prevImage = this.imagesToChange[1]
                } else {
                    this.currentImg = this.imagesToChange[1]
                    this.prevImage = this.imagesToChange[0]
                }

                if (this.current >= srcArr.length) {
                    this.current = 0;
                }

                this.animate(srcArr[this.current])
                this.isLoaded = false
            }

        }, this.params.changeSpeed);

        
    }

    removeEvent(e) {
        let target = e.target
        
        if (!target.classList.contains('thumb')) {
            return
        }
        if(this.allThumbsArr.indexOf(target) == this.hoveredThumbIndex) return
        if(!this.interval) return;
        clearInterval(this.interval);
        if (target.closest('.nav')) {
            return
        }
        S.addClass(this.imagesToChange, 'invisible')
        S.removeClass(this.originalImg, 'invisible')
        this.currentImg = this.imagesToChange[0]
        this.prevImage = this.imagesToChange[1]
        this.current = 1
        this.isFirstImageActive = true

        // this.animate(this.mainImageSrc)
    }

    animate(src) {
        this.current++
        this.isFirstImageActive = !this.isFirstImageActive

        this.currentImg.src = src
        this.currentImg.addEventListener('load', () => {
            if (!S.hasClass(this.originalImg, 'invisible')) {
                S.addClass(this.originalImg, 'invisible')
            }

            S.removeClass(this.currentImg, 'invisible')
            S.addClass(this.prevImage, 'invisible')
            this.isLoaded = true
        })


    }
    createSrcArr() {
        var arrOfSrc = []
        this.allThumbsArr.forEach(elm => {
            let stringToArr = JSON.parse(elm.getAttribute('data-select'))
            arrOfSrc.push(stringToArr)
        })


        return arrOfSrc
    }

    preloadImages(arr) {
        arr.forEach(src => {
            let img = new Image()
            img.src = src
        })
    }
}


initChangeImg('.preview-card', '.preview-card__slider-container', 1000)

initLeafers()

function initLeafers() {
    if(window.innerWidth < 1200) return
    
    var thumbs = S.find('.preview-card .js-leafer-thumbs')
    if(thumbs.length <=0) return

    S.each(thumbs, function () {


        new Leafer(this, {
            vertical: true,
            nextBtn: S.first(this.closest('.preview-card'), '.leafer__btn--next'),
            prevBtn: S.first(this.closest('.preview-card'), '.leafer__btn--prev'),
        })
    })
}

function initChangeImg(imgCont, srcsCont, speed) {

    var items = document.querySelectorAll(imgCont);

    items.forEach(function (item) {

        new changeImg(item, {
            changeSpeed: speed,
            container: srcsCont
        })
    })
}

S.makeGlobal('initChangeImg', initChangeImg)
S.makeGlobal('initLeafers', initLeafers)

/***/ }),
/* 30 */
/***/ (function(module, exports) {

// var isActive = false


class ToggleHidden {
    constructor(elm, options) {
        var self = this

        this.speed = 0.7
        this.parent = elm
        this.parent.wsToggleHidden = this
        this.hiddenContent = S.first(this.parent, '.toggle-hidden__content--hidden')
        


        // console.log(this, this.hiddenContent);


        if (!this.hiddenContent) return

        var def = {
            useDefaultFunc: true,
            onTriggerOpen: function () {},
            onTriggerClose: function () {},
        }

        Object.assign(this, def, options)
        this.hiddenHTML = S.html(this.hiddenContent)


        if (!this.useDefaultFunc) {
            S.html(this.hiddenContent, '')
        }

        this.hiddenContentHeight = this.hiddenContent.scrollHeight
        // this.openBtn = S.find(this.parent, '.toggle-hidden__btn--open')
        // this.closeBtn = S.first(this.parent, '.toggle-hidden__btn--close')
        this.toggleBtn = S.first(this.parent, '.toggle-hidden__btn')
        this.btnTexts = S.find(this.toggleBtn, '.toggle-hidden__btn-text' )
        this.toggleCheckbox = S.first(this.parent, '.toggle-checkbox__input')
        this.opened = false

        // console.log(this.toggleCheckbox);


        if (!this.toggleCheckbox) {
            // S.each(this.openBtn, function () {
            //     this.addEventListener('click', self.toggleItem.bind(self))
            // })
            // this.closeBtn.addEventListener('click', this.closeItem.bind(this))
            this.toggleBtn.addEventListener('click', this.toggleItem.bind(this))
        }

    }

    openItem() {
        if (this.useDefaultFunc) {
            S.slideOpenCss(this.hiddenContent)
        } else {
            this.onTriggerOpen()
        }

        S.toggleClass(this.btnTexts, 'visible')

        this.opened = true
    }
    closeItem() {
        if (this.useDefaultFunc) {
            S.slideCloseCss(this.hiddenContent)
        } else {
            this.onTriggerClose()
        }
        S.toggleClass(this.btnTexts, 'visible')


        this.opened = false
    }

    toggleItem() {
        if (this.opened) {
            this.closeItem()
        } else {
            this.openItem()
        }


    }

}

initToggleHidden()


function initToggleHidden() {
    S.each('.toggle-hidden:not(.toggle-display)', function () {
        new ToggleHidden(this)
    })
    S.each('.toggle-hidden.toggle-display', function () {

        new ToggleHidden(this, {
            useDefaultFunc: false,
            onTriggerOpen: function () {
                S.html(this.hiddenContent, this.hiddenHTML)
                this.hiddenContent.style = 'display: block'

            },
            onTriggerClose: function () {
                S.html(this.hiddenContent, '')
                this.hiddenContent.style = ''

            },

        })
    })
}

S.makeGlobal('initToggleHidden', initToggleHidden)

/***/ }),
/* 31 */
/***/ (function(module, exports) {

let testimonials = S.find('.testimonial .static')


if(testimonials.length>0) {

    S.each(testimonials, function(){
   
        
    
        new ps(this, {
            maxScrollbarLength: 50,
            wheelSpeed: .1,
            swipeEasing: true,
            wheelPropagation: false
        })
    })
}

/***/ }),
/* 32 */
/***/ (function(module, exports) {



/***/ }),
/* 33 */
/***/ (function(module, exports) {

class ProductsFilters {
    constructor(parent, options) {

        var self = this;
        self.container = parent
        self.slider = S.first(parent, '.js-filters-slider')
        self.navigation = {
            prevEl: S.first(parent, '.products-filters__btn-prev'),
            nextEl: S.first(parent, '.products-filters__btn-next')
        },

        Object.assign(this, options)

     
        this.initSwiper.call(this)
        

    }

    initSwiper(){
        
        let _this = this
        
        new Swiper(_this.slider, {
            slidesPerView: 'auto',
            spaceBetween: 16,
            navigation: _this.navigation,
            on: {         
                slideChange: function(){
                    if(window.innerWidth >= 1366){
                        
                        

                        if(this.activeIndex >= this.slides.length - 9){
                            S.addClass(this.navigation.nextEl, 'swiper-button-disabled')
                        } else {
                            S.removeClass(this.navigation.nextEl, 'swiper-button-disabled')
                        }
                    } 
                    


                }

            },
          
            
        })
    }

}


function initProductsFilters(selector){
    S.each(selector, function(){
        new ProductsFilters(this)
    })
}

if(window.innerWidth > 1200) {
    initProductsFilters('.products-filters')

}


/***/ }),
/* 34 */
/***/ (function(module, exports) {

function initTestimonialsSlider(){
    
    
    if(window.innerWidth > 580) {
        let staticGalleryDesktop = S.first('.testimonials__gallery-desktop .testimonials-gallery')
        var kd = new Swiper(staticGalleryDesktop, {
            slidesPerView: 3,
            loop: true,
            centeredSlides: true,
            navigation: {
                prevEl: '.testimonials__gallery-desktop .testimonials-gallery__btn-prev',
                nextEl: '.testimonials__gallery-desktop .testimonials-gallery__btn-next'
            },
            breakpoints: {
                1200: {
                    slidesPerView: 2,
                },
                780: {
                    slidesPerView: 1,
                }
            }
        })
    } else {
        let km = null
        document.addEventListener('accordionItemOpened', function(e){        
            let staticGalleryMobile = S.first(e.detail.item.$content, '.testimonials-gallery') 
            let prevEl = S.first(e.detail.item.$content, '.testimonials-gallery__btn-prev')         
            let nextEl = S.first(e.detail.item.$content, '.testimonials-gallery__btn-next') 
                    
            if(staticGalleryMobile && km == null){  
                setTimeout(() => {
                    km = new Swiper(staticGalleryMobile, {
                        slidesPerView: 1,
                        navigation: {
                            prevEl: prevEl,
                            nextEl: nextEl
                        },
                    })
                    let gallerySwiper = S.first('.testimonials__gallery-mobile .gallery-swiper')
                    
                    
                    new Swiper(gallerySwiper, {
                        navigation: {
                            nextEl: '.gallery-swiper__btn-next',
                            prevEl: '.gallery-swiper__btn-prev',
                        },
                    })

                }, 300);              
            }
        })
    }
}

initTestimonialsSlider()

S.makeGlobal('initTestimonialsSlider', initTestimonialsSlider)


document.addEventListener('click', function(e){
    if(!e.target.closest('.header__lang') && S.first('.header__lang').matches('.open')){
        S.removeClass( S.first('.header__lang'), 'open')        
    }
})

// footer btn

class Fixed {
    constructor(elm, options) {
        let self = this;
        this.fixed = elm;
        this.lastScroll = 0;
        this.timer = null;
        this.isScrollDetect = false;
        this.pageHeight = document.body.scrollHeight

        this.def = {
            throttle: 100,
            scrollPosHidden: this.pageHeight - 100,
            allBreakpoints: false,
            breakpointUp: true,
            breakpoint: 991,
            onInit: function () {},
            onHide: function () {}
        }

        this.params = Object.assign(this, this.def, options)
  
        

        this.checkScrollPos();

        S.on(window, 'scroll', throttle(this.scrollStart.bind(this), this.params.throttle))

        this.onInit()

        // Listen or custom events
        document.addEventListener('modalOpen', this.hide.bind(this))
        document.addEventListener('modalClose', this.show.bind(this))
        document.addEventListener('filter-opened', this.hide.bind(this))
        document.addEventListener('filter-closed', this.show.bind(this))


    }



    scrollStart() {
        this.notWorkingOnMobile = window.innerWidth < this.breakpoint && this.breakpointUp && !this.allBreakpoints
        this.notWorkingOnDesktop = window.innerWidth > this.breakpoint && !this.breakpointUp && !this.allBreakpoints
        if(this.notWorkingOnMobile) return
        if(this.notWorkingOnDesktop) return
        this.isScrollDetect = true
        var st = window.pageYOffset      

        this.changeFixedVisibility(st)
    }

    checkScrollPos() {
        this.notWorkingOnMobile = window.innerWidth < this.breakpoint && this.breakpointUp && !this.allBreakpoints
        this.notWorkingOnDesktop = window.innerWidth > this.breakpoint && !this.breakpointUp && !this.allBreakpoints
        if(this.notWorkingOnMobile) return
        if(this.notWorkingOnDesktop) return
        this.isScrollDetect = false
        var st = window.pageYOffset;

        this.changeFixedVisibility(st)
    }

    changeFixedVisibility(st) {    
        
        // Show Fixed
        if (st < this.lastScroll) {       

            if (st < this.params.scrollPosHidden && this.isScrollDetect) {
                this.hide()
            }

        }
        // Hide Fixed
        else if (st > this.lastScroll) {
           

            this.show()
        }

        this.lastScroll = st
    }

    hide() {
        if (!S.hasClass(this.fixed, 'is-hidden')) {
            S.addClass(this.fixed, 'is-hidden')
        }
        this.onHide()
    }

    show() {
        if (S.hasClass(this.fixed, 'is-hidden')) {
            S.removeClass(this.fixed, 'is-hidden')
        }
    }
}

setTimeout(() => {
    new Fixed(S.first('.btn--bottom-fixed'), {
        breakpointUp: true,
        breakpoint: 580
    })
    new Fixed(S.first('.footer__fixed-btns'), {
        breakpointUp: false,
        breakpoint: 580
    })
    
    
}, 100);


S.bodyOnClick('.js-scroll-to-top', function(){

    
    S.scrollTo(window, {
        top: 0,
        offsetY: -65
    })
})



/***/ }),
/* 35 */
/***/ (function(module, exports) {



/***/ }),
/* 36 */
/***/ (function(module, exports) {

// import './no-ie.scss';

/***/ }),
/* 37 */
/***/ (function(module, exports) {

// import './article.scss';

/***/ }),
/* 38 */
/***/ (function(module, exports) {

class loadMoreNews {
	constructor(params) {
		this.loadBtn = S.first('.js-load-news');
		
		console.log(this.loadBtn);

		if (!this.loadBtn) {
			return
		}

		this.renderContainer = S.first(params.renderContainer);
		this.items = S.find(this.renderContainer, '.js-news-item')
		this.hiddenItems = this.items.filter(item=> {
			return item.matches('.hidden-md')
		})
		this.url = params.url;
		this.onClick = params.onClick;

		S.on('.js-load-news', 'click', () => {
			console.log('click');
			
			if(this.hiddenItems.length> 0) {
				S.removeClass(this.hiddenItems, 'hidden-md')
				this.hiddenItems = this.items.filter(item=> {
					return item.matches('.hidden-md')
				})

			} else {

				this.onClick.apply(this);
			}
			
		});
	}
}

S.makeGlobal('loadMoreNews', loadMoreNews);

/***/ }),
/* 39 */
/***/ (function(module, exports) {

// import './ui.scss';

/***/ }),
/* 40 */
/***/ (function(module, exports) {



/***/ }),
/* 41 */
/***/ (function(module, exports) {

let productSwipers = S.find('.js-product-slider');
if (productSwipers.length > 0) {
	S.each(productSwipers, function() {
		let nav = {
			prevEl: S.first(
				S.closest(this, '.product-slider__wrapper'),
				'.product-slider__btn-prev'
			),
			nextEl: S.first(
				S.closest(this, '.product-slider__wrapper'),
				'.product-slider__btn-next'
			)
		};

		new Swiper(this, {
			centered: true,
			slidesPerView: 'auto',
			navigation: nav,

			on: {
				init: function() {
					let _this = this;
					if (window.innerWidth > 1200) {
						Array.from(this.slides).forEach(slide => {
							slide.addEventListener('mouseenter', hidePrevBtn);
							slide.addEventListener('mouseleave', showPrevBtn);
						});
					}

					function hidePrevBtn() {
						if (this.matches('.swiper-slide-active')) {
							S.addClass(_this.navigation.prevEl, 'hidden');
						}
					}
					function showPrevBtn() {
						if (this.matches('.swiper-slide-active')) {
							S.removeClass(_this.navigation.prevEl, 'hidden');
						}
					}
				},
				slideChange: function() {
					if (window.innerWidth > 1366) {
						if (this.activeIndex >= this.slides.length - 6) {
							S.addClass(this.navigation.nextEl, 'swiper-button-disabled');
						} else {
							S.removeClass(this.navigation.nextEl, 'swiper-button-disabled');
						}
					} else if (window.innerWidth <= 1440 && window.innerWidth > 1200) {
						if (this.activeIndex >= this.slides.length - 5) {
							S.addClass(this.navigation.nextEl, 'swiper-button-disabled');
						} else {
							S.removeClass(this.navigation.nextEl, 'swiper-button-disabled');
						}
					} else if (window.innerWidth <= 1200 && window.innerWidth > 991) {
						if (this.activeIndex >= this.slides.length - 5) {
							S.addClass(this.navigation.nextEl, 'swiper-button-disabled');
						} else {
							S.removeClass(this.navigation.nextEl, 'swiper-button-disabled');
						}
					}
				}
			}
		});
	});
}


/***/ }),
/* 42 */
/***/ (function(module, exports) {

class Product {
	constructor(elm) {
		this.elm = elm;
		this.parent = S.closest(this.elm, '.product-info__wrapper');
		this.videoModal = S.first('#product-video-modal');
		this.videoIframe = S.first(this.videoModal, 'iframe');
		this.modalWrapper;

		if (S.closest(this.parent, '.modal')) {
			this.modalWrapper = S.first(
				S.closest(this.parent, '.modal'),
				'.modal__content'
			);
		}

		this.itemThumbsGallery;

		if (this.elm.innerHTML != '') {
			this.prevBtn = S.first(this.parent, '.product-info__slider-btn-prev');
			this.nextBtn = S.first(this.parent, '.product-info__slider-btn-next');
			this.itemThumbsSlider = S.first(
				this.parent,
				'.product-info-slider-thumbs'
			);
			this.itemThumbs = S.find(this.itemThumbsSlider, '.swiper-slide');
			this.itemMainSlider = S.first(this.parent, '.product-info-slider');
			this.mainImgContainer = S.first(this.parent, '.product-info-slider-main');
			this.mainImgContainerParent = S.first(
				this.parent,
				'.product-info__img-container'
			);
			this.zoomImgContainer = S.first(
				this.parent,
				'.product-info__zoom-img-container'
			);
			this.mainImg = S.first(this.mainImgContainer, '.product-info__img');
			this.zoomImg = S.first(this.zoomImgContainer, '.product-info__zoom-img');
			let fakeImgChecker = new Image();
			this.mainSrc = '';
			this.zoomSrc = '';

			if (this.parent) {
				if (window.innerWidth >= 991) {
					this.initThumbsSlider();
					this.itemThumbsSlider.addEventListener(
						'click',
						this.changeMainImg.bind(this)
					);

					fakeImgChecker.addEventListener('load', () => {
						this.mainImgContainerClientHeight = this.mainImgContainer.clientHeight;
						this.mainImgContainerClientWidth = this.mainImgContainer.clientWidth;
						this.zoomImgContainerScrollHeight = this.zoomImgContainer.scrollHeight;
						this.zoomImgContainerClientHeight = this.zoomImgContainer.clientHeight;
						this.zoomImgContainerScrollWidth = this.zoomImgContainer.scrollWidth;
						this.zoomImgContainerClientWidth = this.zoomImgContainer.clientWidth;
						this.difX =
							this.zoomImgContainerScrollWidth -
							this.zoomImgContainerClientWidth;
						this.difY =
							this.zoomImgContainerScrollHeight -
							this.zoomImgContainerClientHeight;
						this.mainImg.addEventListener(
							'mouseenter',
							this.revealZoomImg.bind(this)
						);
						this.mainImg.addEventListener(
							'mouseleave',
							this.hideZoomImg.bind(this)
						);
						this.mainImg.addEventListener(
							'mousemove',
							this.moveZoomImg.bind(this)
						);
					});

					fakeImgChecker.src = this.zoomImg.src;

					document.addEventListener(
						'HeaderOpened',
						this.adjustImage.bind(this)
					);
					document.addEventListener(
						'HeaderClosed',
						this.adjustImage.bind(this)
					);
				} else {
					this.initSlider();
				}
			}
		}
	}

	adjustImage(e) {
		if (
			e.type == 'HeaderOpened' &&
			e.detail.headerHeight >
				this.mainImgContainerParent.getBoundingClientRect().y
		) {
			// var topPos = 150
			this.mainImgContainerParent.style = `top: 170px`;
		} else if (e.type == 'HeaderClosed') {
			this.mainImgContainerParent.style = '';
		}
	}

	initSlider() {
		var self = this;
		// console.log('991');

		this.itemThumbsGallery = new Swiper(this.itemThumbsSlider, {
			spaceBetween: 15,
			slidesPerView: 1,
			autoheight: true,
			pagination: {
				el: S.first(self.itemThumbsSlider, '.swiper-pagination'),
				type: 'bullets'
			},
			watchSlidesVisibility: true,
			watchSlidesProgress: true
		});

		// this.itemMainGallery = new Swiper(this.itemMainSlider, {
		//     slidesPerView: 1,
		//     effect: 'fade',
		//     // thumbs: {
		//     //     swiper: this.itemThumbsGallery
		//     // },
		//     navigation: {
		//         prevEl: this.prevBtn,
		//         nextEl: this.nextBtn
		//     },

		// })

		S.each(this.itemThumbs, function() {
			this.addEventListener('click', function() {
				var thumb = S.first(this, '.product-info__img-thumb-slide');
				if (S.attr(thumb, 'data-video-src')) {
					// console.log(self);
					self.setModalIframeSrc(S.attr(thumb, 'data-video-src'));
				}
			});
		});
	}

	initThumbsSlider() {
		// console.log('> 991');

		this.itemThumbsGallery = new Swiper(this.itemThumbsSlider, {
			spaceBetween: 15,
			slidesPerView: 4,
			direction: 'vertical',
			freeMode: true,
			watchSlidesVisibility: true,
			watchSlidesProgress: true,
			navigation: {
				prevEl: '.product-info__thumbs-btn-prev',
				nextEl: '.product-info__thumbs-btn-next'
			},
			breakpoints: {
				1440: {
					slidesPerView: 3
				},
				1366: {
					slidesPerView: 3,
					spaceBetween: 10
				}
			}
		});
	}

	changeMainImg(e) {
		var thumb = e.target.closest('.product-info__img-thumb-slide');
		if (S.attr(thumb, 'data-video-src')) {
			this.setModalIframeSrc(S.attr(thumb, 'data-video-src'));
			return;
		}

		this.getData(e);
		this.setnewData();
	}

	setModalIframeSrc(src) {
		S.attr(this.videoIframe, 'src', src);
	}

	getData(e) {
		if (e.target.closest('.swiper-slide')) {
			var activeSlideImg = S.first(
				e.target.closest('.swiper-slide'),
				'.product-info__img'
			);
			this.mainSrc = S.attr(activeSlideImg, 'data-src');
			this.zoomSrc = S.attr(activeSlideImg, 'data-zoom');
		}
	}

	setnewData() {
		if (window.innerWidth >= 991) {
			var newZoomImg = document.createElement('img');

			S.attr(newZoomImg, 'src', this.zoomSrc);
			S.addClass(newZoomImg, 'product-info__zoom-img');
			S.append(this.zoomImgContainer, newZoomImg);
			S.remove(this.zoomImg);
			this.zoomImg = newZoomImg;
		}

		var newImg = document.createElement('img');
		S.attr(newImg, 'src', this.mainSrc);
		S.addClass(newImg, 'product-info__img');
		S.addClass(newImg, 'hidden');
		S.addClass(newImg, 'on-top');
		S.append(this.mainImgContainer, newImg);

		setTimeout(() => {
			S.removeClass(newImg, 'hidden');
		}, 0);

		setTimeout(() => {
			S.remove(this.mainImg);
			S.removeClass(newImg, 'on-top');
			this.mainImg = newImg;
			if (window.innerWidth >= 991) {
				this.mainImg.addEventListener(
					'mouseenter',
					this.revealZoomImg.bind(this)
				);
				this.mainImg.addEventListener(
					'mouseleave',
					this.hideZoomImg.bind(this)
				);
				this.mainImg.addEventListener('mousemove', this.moveZoomImg.bind(this));
			}
		}, 0);
	}

	revealZoomImg() {
		console.log('zoom');

		S.addClass(this.zoomImgContainer, 'visible');
	}

	hideZoomImg() {
		S.removeClass(this.zoomImgContainer, 'visible');
	}

	moveZoomImg(e) {
		var mainImgPosX = this.mainImg.getBoundingClientRect().x;
		var mainImgPosY = this.mainImg.getBoundingClientRect().y;
		var cursorPosX = e.clientX;
		var cursorPosY = e.clientY;
		var posX = cursorPosX - mainImgPosX;
		var posY = cursorPosY - mainImgPosY;

		var resultX = (posX / this.mainImgContainerClientWidth) * -1 * this.difX;
		var resultY = (posY / this.mainImgContainerClientHeight) * -1 * this.difY;

		this.zoomImg.style.transform = `translate(${resultX}px, ${resultY}px)`;
	}
}

function initProduct() {
	var productSlider = S.find('.product-info__img-container');
	// console.log(productSlider);

	if (productSlider.length > 0) {
		S.each(productSlider, function() {
			new Product(this);
		});
		App.initScrollOnProductLinks();
	}
}

document.addEventListener('DOMContentLoaded', function() {
	initProduct();
});

S.makeGlobal('initProduct', initProduct);
// S.makeGlobal('initProducts', initProducts)

S.makeGlobal('Product', Product);


/***/ }),
/* 43 */
/***/ (function(module, exports) {



/***/ }),
/* 44 */
/***/ (function(module, exports) {



/***/ }),
/* 45 */
/***/ (function(module, exports) {

var aboutGallery = S.first('.about-gallery')

if (aboutGallery) {

	new Swiper(aboutGallery, {
		slidesPerView: 1,
		direction: 'vertical',
		navigation: {
			prevEl: '.about-gallery__btn-prev',
			nextEl: '.about-gallery__btn-next'
        },
        breakpoints: {
            991: {
                direction: 'horizontal',
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true
                }
            }
        }
		
	});
}

/***/ }),
/* 46 */
/***/ (function(module, exports) {



/***/ }),
/* 47 */
/***/ (function(module, exports) {



/***/ }),
/* 48 */
/***/ (function(module, exports) {



/***/ }),
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/scrollreveal/dist/scrollreveal.js
var scrollreveal = __webpack_require__(6);
var scrollreveal_default = /*#__PURE__*/__webpack_require__.n(scrollreveal);

// EXTERNAL MODULE: ./dev/components/button/button.js
var button_button = __webpack_require__(13);

// EXTERNAL MODULE: ./dev/components/nav/nav.js
var nav = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/lodash.throttle/index.js
var lodash_throttle = __webpack_require__(3);
var lodash_throttle_default = /*#__PURE__*/__webpack_require__.n(lodash_throttle);

// CONCATENATED MODULE: ./dev/components/header/header.js
// JS


// Config



// Class
class header_Header {
    constructor(elm, options) {
        let self = this;
        this.header = elm;
        this.lastScroll = 0;
        this.timer = null;
        this.isScrollDetect = false;



        this.def = {
            throttle: 100,
            scrollPosTiny: 70,
            scrollPosHidden: 300,
            onInit: function () {},
            onHide: function () {}
        }



        this.params = Object.assign(this, this.def, options)

        this.checkScrollPos();

        S.on(window, 'scroll', lodash_throttle_default()(this.scrollStart.bind(this), this.params.throttle))

        this.onInit()



        // Listen or custom events
        document.addEventListener('modalOpen', this.hide.bind(this))
        document.addEventListener('modalClose', this.show.bind(this))


        document.addEventListener('filter-opened', this.hide.bind(this))
        document.addEventListener('filter-closed', this.show.bind(this))
    }



    scrollStart() {
        this.isScrollDetect = true
        var st = window.pageYOffset


        this.changeHeaderVisibility(st)
    }

    checkScrollPos() {
        this.isScrollDetect = false
        var st = window.pageYOffset;

        this.changeHeaderVisibility(st)
    }

    changeHeaderVisibility(st) {
        // Show header
        if (st > this.lastScroll && st > this.params.scrollPosTiny) {

            if (!S.hasClass(this.header, 'header--tiny')) {
                S.addClass(this.header, 'header--tiny')
            }

            if (st > this.params.scrollPosHidden && this.isScrollDetect) {
                this.hide()
            }

        }
        // Hide header
        else if (st < this.lastScroll) {
            if (st < this.params.scrollPosTiny && S.hasClass(this.header, 'header--tiny')) {
                S.removeClass(this.header, 'header--tiny')
            }

            this.show()
        }

        this.lastScroll = st
    }

    hide() {
        if (!S.hasClass(this.header, 'is-hidden')) {
            S.addClass(this.header, 'is-hidden')
        }
        this.onHide()
    }

    show() {
        if (S.hasClass(this.header, 'is-hidden')) {
            S.removeClass(this.header, 'is-hidden')
        }
    }
}

// Init

setTimeout(() => {
    var elm = S.first('#header')
    var wrapper = S.first(elm, '.header__wrapper')
    var langDropdown = S.first(elm, '.header__lang')
    var openMenuBtn = S.first(elm, '.header__mega-menu-open')
    var megaMenu = S.first(elm, '.header__mega-menu')

    let config = {
        throttle: 100,
        scrollPosTiny: 70,
        scrollPosHidden: 600,
        menuOpen: false,
        wrapper,
        openMenuBtn,
        megaMenu,
        onInit: function () {
            if (openMenuBtn) {
                S.on(openMenuBtn, 'click', toggleMenu.bind(this))
                
                let _this = this
                
                document.addEventListener('click', function(e){
                    
                    if(!e.target.closest('.header') && _this.menuOpen) {
                        toggleMenu.call(_this)
                    }
                })
            }
        },
        onHide: function () {
            S.removeClass(langDropdown, 'open')
            if (megaMenu.matches('.open')) {
                
                toggleMenu.call(this)
            }
        },

    }
    var header = new header_Header(elm, config)

    function toggleMenu() {
        if (this.menuOpen) {
            S.removeClass(this.wrapper, 'shadow')
            S.removeClass(this.megaMenu, 'open')
            this.menuOpen = false
        } else {
            S.addClass(this.wrapper, 'shadow')
            S.addClass(this.megaMenu, 'open')
            this.menuOpen = true
        }
    }
}, 100);
// EXTERNAL MODULE: ./dev/components/dropdown/dropdown.js
var dropdown = __webpack_require__(15);

// CONCATENATED MODULE: ./dev/components/iframe/iframe.js
function initIframe(iframe) {

    if (iframe.getAttribute('src')) {
        iframe.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*')
    } else {
        let modalIframeSrc = iframe.getAttribute('data-src')
        let paused = iframe.getAttribute('data-paused')
        let enableJs = modalIframeSrc.includes('?') ? '&enablejsapi=1' : '?enablejsapi=1';

        if (modalIframeSrc.includes(enableJs)) {
            modalIframeSrc += '&autoplay=1'
        } else {
            modalIframeSrc += enableJs + '&autoplay=1'
        }



        if (!paused) {
            iframe.setAttribute('src', modalIframeSrc)
        }

    }
}

/* harmony default export */ var iframe = (initIframe);
// CONCATENATED MODULE: ./dev/components/modal/modal.js
// Custom Events


class modal_Modal {
    constructor(elm) {

        let self = this;
        this.modal = elm;
        this.openedModal = document.querySelector('.modal.open');
        this.iframe = null;
        this.allIframes = [];
        this.hasSwiper = false;
        this.hasScrolableContent = false
        this.openModalEvt = new CustomEvent('modalOpen')
        this.closeModalEvt = new CustomEvent('modalClose')
        // this.clbks = {}

        this.modal.wsModal = this;

        this.modal.addEventListener('click', function (e) {
            e.stopPropagation()

            /**
             * If has swiper inside, then ignore click on wrapper and close modal.
             * Prevent closing modal when click on swiper items (next, prev, img, modal__media-wrapper)
             * else if has wrapper, then close modal only when click outsude .modal__wrapper
             */




            if (self.hasSwiper && e.target.closest('.modal__media-wrapper, .swiper-container, img, .btn--next, .btn--prev') && !e.target.closest('[data-modal]')) {
                
                return
            } else if (self.hasSwiper && e.target.closest('.modal__media-wrapper, .swiper-container, img, .btn--next, .btn--prev') && e.target.closest('[data-modal]')) {
                // self.closeModal(e)
                
                
                let target = e.target.closest('[data-modal]')
                let selector = target.dataset.modal
                document.querySelector(selector).wsModal.openModal()
                
                return
            } else if (!self.hasSwiper && self.hasScrolableContent && e.target.closest('.modal__wrapper') && !e.target.closest('.modal__close')) {
                return
            } else if (e.target.closest('.modal, .modal__close')) {
                self.closeModal(e)
            }
        })
    }

    openModal() {
        // Open modal
        this.modal.classList.add('open')

        // Get modal params

        this.hasSwiper = this.modal.querySelector('.swiper-container')


        this.hasScrolableContent = this.modal.matches('.modal--scrollable')

        this.openedModal = this.modal;
        this.allIframes = [...this.modal.querySelectorAll('iframe')]
        this.iframe = this.modal.querySelector('.swiper-container') != null ?
            this.modal.querySelector('.swiper-slide:first-child iframe') :
            this.modal.querySelector('iframe');

        // Start iframe
        if (this.iframe) {
            this.startIframe()
        }

        document.dispatchEvent(this.openModalEvt)
        // this.clbks.openModal()
    }

    closeModal() {

        // Close modal
        this.modal.classList.remove('open')

        // Pause iframe
        if (this.allIframes.length > 0) {
            this.pauseIframe()
        }

        document.dispatchEvent(this.closeModalEvt)
        // this.clbks.closeModal()
    }

    startIframe() {
        iframe(this.iframe)
    }

    pauseIframe() {
        // Loop through iframes and pause it
        this.allIframes.forEach(iframe => {
            iframe.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')
        })
    }

    on(method, clbk) {
        if (!S.isFunction(clbk)) {
            throw 'Modal.on -> Second argument [clbk] must be Function'
        }
        // this.clbks[method] = clbk
    }
}

initModals()

function initModals() {
    S.each('.modal', function () {
        // Create class if NOT inited.
        if (!this.wsModal) {
            new modal_Modal(this)
        }
    })

    if (document.body.clientWidth > 991) {

        S.each('.modal__content--scrollable', function () {
            var scrollPs = new ps(this, {
                maxScrollbarLength: 100,
                wheelSpeed: .25,
                swipeEasing: true,
                wheelPropagation: false
            })
        })


    }
}

// Make global window.App.initModals
S.makeGlobal('initModals', initModals)


// Events
let target;
document.body.addEventListener('click', function (e) {
    target = e.target.closest('[data-modal]')
    

    
    if (target) {
        let selector = target.dataset.modal
        document.querySelector(selector).wsModal.openModal()
    }
})
// EXTERNAL MODULE: ./dev/components/icon/icon.js
var icon = __webpack_require__(16);

// EXTERNAL MODULE: ./dev/js/input.js
var input = __webpack_require__(17);

// CONCATENATED MODULE: ./dev/components/template/template.js
function renderVar(tpl, obj) {


    var html = tpl.innerHTML;


    for (const key in obj) {



        if (obj.hasOwnProperty(key)) {



            const element = obj[key];

            html = html.replace('{{' + key + '}}', element)
        }
    }

    return html;
};

function useTemplate(template, dataCont, resCont) {

    let data = dataCont.dataset ? dataCont.dataset.arr : dataCont

    let result = ''

    if (typeof (data) == 'string') {
        data = JSON.parse(data);
    }


    S.each(data, function () {

        result += renderVar(template, this)
    })

    resCont.insertAdjacentHTML('beforeend', result)

}


/* harmony default export */ var template = (useTemplate);
// CONCATENATED MODULE: ./dev/components/form/upload-file.js

class upload_file_UploadFile {
  constructor(parent) {
    this.parent = parent;
    this.input = this.parent.querySelector('input[type="file"]')
    this.uploadedContainer = this.parent.querySelector('.uploaded-files')
    this.arr = Array.from(this.input.files)
    this.template = S.first('#upload-item-template')
    this.data = []

    this.input.addEventListener('change', this.addFiles.bind(this))
    this.uploadedContainer.addEventListener('click', this.removeFile.bind(this))


  }

  addFiles() {
    for (var i = 0; i < this.input.files.length; i++) {
      var fileNameString = this.input.files[i].name
      var fileNameStringArr = fileNameString.split('.')
      var fileNameStringEnd = fileNameStringArr[fileNameStringArr.length - 1]
      var fileNameStringBeginning, fileName
      if (fileNameStringArr[0].length > 11) {
        fileNameStringBeginning = fileNameString.slice(0, 10)
        fileName = fileNameStringBeginning + '...' + fileNameStringEnd
      } else {
        fileName = fileNameString
      }

      this.data.push({
        'i': i,
        'fileName': fileName
      })

      this.arr.push(this.input.files[i])
      this.input.filesArr = this.arr
    }

    template(this.template, this.data, this.uploadedContainer)

    /**
     * Input must be cleared
     * If we try to upload same file twice and input will be filled with that file,
     * we will have no effect
     */
    this.input.value = ''

  }

  removeFile(e) {
    var elm = S.closest(e.target, '.js-remove-file')

    if (elm) {
      var li = S.closest(elm, 'li')
      var i = S.index(li)
      S.remove(li)
      this.arr.splice(i, 1)
      this.data.splice(i, 1)
    }

    this.input.filesArr = this.arr
  }

  clearAllFiles() {
    this.uploadedContainer.innerHTML = ''
    this.arr = []
    this.input.filesArr = []
    this.data = []
  }
}

let allUploadArr = []

// Get all forms with input[type="file"]
S.each('input[type="file"]', function () {
  let form = this.closest('.form')
  allUploadArr.push(new upload_file_UploadFile(form))
})

/* harmony default export */ var upload_file = (allUploadArr);
// EXTERNAL MODULE: ./node_modules/autosize/dist/autosize.min.js
var autosize_min = __webpack_require__(7);
var autosize_min_default = /*#__PURE__*/__webpack_require__.n(autosize_min);

// CONCATENATED MODULE: ./dev/components/form/textarea.js


autosize_min_default()(S.find('textarea'));

// Textarea counter
let BACKSPACE = 8
S.find('.js-textarea-counter').forEach( elm => {
    let countHolder = S.find(S.closest(elm, '.form__group'), '.js-form-count')
    let startCount = +S.html(countHolder)
    let totalSymbols;

    S.on(elm, 'input', function (e) {
        totalSymbols = S.val(elm).replace(/\s/g, '').length

        if (totalSymbols >= startCount) {
            S.val(elm, getString(S.val(elm)))
        }
        
        S.html(countHolder, startCount - S.val(elm).replace(/\s/g, '').length)
        
        if (totalSymbols >= startCount) {
            S.html(countHolder, 0)
        }
    })

    function getString(str) {
        return str.substring(0, startCount + str.split(' ').length - 1)
    }
})

// CONCATENATED MODULE: ./dev/components/form/form.js




// Settings
let formAutofill = true;

// Validation
function Validation(parent, options) {
    var parent = S.find(parent);

    var defaults = {
        formBtn: S.find(parent, '.js-form-submit'),
        preloader: false,
        offset: 0,
        scroll: true,
        speed: 1000,
        checkOnInput: false,
        onComplete: function () {}
    }


    var params = Object.assign({}, defaults, options);



    var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var required = S.find(parent, '[required]')
    var groupRequired = S.find(parent, '[data-group-required]')
    var result = [];
    var ifSucces = [];
    var succesValue = false;
    var uploadForm = upload_file.filter( item => {
        return item.parent === parent[0]
    })[0]

    /**
     * Live input validation when user cahnge value in input
     * else
     * Check all form with all inputs
     */
    if (params.checkOnInput) {
        required.on('input', checkInput)
    } else {
        checkInputs()
    }



    function checkInputs() {

        // cheking all inputs that have required
        for (var i = 0; i < required.length; i++) {
            var arr = [];

            // cheking empty. Ecception: input[type="file"]
            if (S.attr(required[i], 'type') !== 'file') {
                arr.push(checkEmpty(required[i]))
            }

            // cheking the Email
            if (S.attr(required[i], 'type') === 'email') {
                arr.push(checkEmail(required[i]))
            }

            // cheking minimal symbols in string
            if (S.attr(required[i], 'data-min')) {
                arr.push(checkMin(required[i]))
            }

            // cheking maximal symbols in string

            if (S.attr(required[i], 'data-max')) {
                arr.push(checkMax(required[i]))
            }

            // cheking if checkbox is cheked
            if (S.attr(required[i], 'type') == 'checkbox') {
                arr.push(checkCheckbox(required[i]))
            }

            // One lowercase letter
            if (S.attr(required[i], 'data-lowercase') != null) {
                arr.push(checkLowercase(required[i]))
            }

            // One uppercase letter
            if (S.attr(required[i], 'data-uppercase') != null) {
                arr.push(checkUppercase(required[i]))
            }

            // One number exist
            if (S.attr(required[i], 'data-number') != null) {
                arr.push(checkNumberExist(required[i]))
            }

            // Only digits
            if (S.attr(required[i], 'data-digits') != null) {
                arr.push(checkDigits(required[i]))
            }

            // Compare with equal input
            if (S.attr(required[i], 'data-equal') != null) {
                let compareValue = S.val(S.attr(required[i], 'data-equal'))
                arr.push(checkEqual(required[i], compareValue))
            }

            // Compare input with files
            if (S.attr(required[i], 'type') === 'file') {
                arr.push(checkFileExist(required[i]))
            }

            // adding object with the containing results
            result.push({
                elm: required[i],
                value: arr.indexOf(false) == -1 ? true : false
            })

            // adding in mass to check if all inputs pass validation
            ifSucces.push(result[i].value)

            // adding error class to inputs that didnt pass valid
            let formGroup = S.closest(required[i], '.form__group')
            if (arr.indexOf(false) == -1) {
                S.removeClass(formGroup, 'error');
            } else {
                S.addClass(formGroup, 'error');
            }
        }

        // Check group of checboxes or radios
        if (groupRequired.length > 0) {
            groupRequired.forEach(groupParent => {
                ifSucces.push(checkGroup(groupParent))
            })
        }

        params.onComplete()

        // Scroll to error
        if (params.scroll) {
            scrollOnError()
        }

        succesValue = ifSucces.indexOf(false) == -1 ? true : false

    }

    function checkInput() {
        var arr = [];
        var errors = {};
        arr.push('empty: ' + checkEmpty(this))
        errors.empty = checkEmpty(this)
        console.log(this);
        


        // cheking the Email
        if (S.attr(this, 'type') === 'email') {
            arr.push(checkEmail(this))
            errors.email = checkEmail(this)
        }

        // cheking minimal symbols in string

        if (S.attr(this, 'data-min')) {
            arr.push('min: ' + checkMin(this))
            errors.min = checkMin(this)
        }

        // cheking maximal symbols in string
        if (S.attr(this, 'data-max')) {
            arr.push(checkMax(this))
            errors.max = checkMax(this)
        }

        // One lowercase letter
        if (S.attr(this, 'data-lowercase') != null) {
            arr.push('lowercase: ' + checkLowercase(this))
            errors.lowercase = checkLowercase(this)
        }

        // One uppercase letter
        if (S.attr(this, 'data-uppercase') != null) {
            arr.push('uppercase: ' + checkUppercase(this))
            errors.uppercase = checkUppercase(this)
        }

        // One number exist
        if (S.attr(this, 'data-number') != null) {
            arr.push('number: ' + checkNumberExist(this))
            errors.number = checkNumberExist(this)
        }

        // Digits only
        if (S.attr(this, 'data-digits') != null) {
            console.log(checkDigits(this));
            
            arr.push('digits: ' + checkDigits(this))
            errors.number = checkDigits(this)
        }

        // Compare with old password
        if (S.attr(this, 'data-compare') != null) {
            let compareValue = S.val(S.attr(this, 'data-compare'))
            arr.push('compare: ' + checkCompare(this, compareValue))
            errors.compare = checkCompare(this, compareValue)
        }

        // Compare with equal input
        if (S.attr(this, 'data-equal') != null) {
            let compareValue = S.val(S.attr(this, 'data-equal'))
            arr.push('equal: ' + checkEqual(this, compareValue))
        }

        // adding object with the containing results
        result.push({
            elm: this,
            value: arr.indexOf(false) == -1 ? true : false
        })

        // adding in mass to check if all inputs pass validation
        ifSucces.push(result.value)

        // adding error class to inputs that didnt pass valid
        let formGroup = S.closest(this, '.form__group')
        if (arr.indexOf(false) == -1) {
            S.removeClass(formGroup, 'error');
        } else {
            S.addClass(formGroup, 'error');
        }

        params.onComplete(errors)
    }

    function setSuccess() {
        S.addClass(parent, 'success')

        setTimeout(function () {
            S.removeClass(parent, 'success')
        }, 2000);

        stopPreload()
        clearInputs()
    }

    function clearInputs() {
        parent[0].reset()
        S.removeClass(S.find(parent, '.active'), 'active')

        if (uploadForm) {
            uploadForm.clearAllFiles()
        }
    }

    function scrollOnError() {
        let firstError = S.find(parent, '.error')[0]

        if (!firstError) {
            return
        }

        let scrollingContainer
        if (document.body.clientWidth > 991) {
            scrollingContainer = S.attr(parent[0], 'data-scrolling-container') ? S.closest(parent[0], '.ps') : window;

            firstError.scrollTop = 100
        } else {
            scrollingContainer = S.attr(parent[0], 'data-scrolling-container') ? S.first(S.closest(parent[0], '.modal--scrollable'), S.attr(parent[0], 'data-scrolling-container')) : window;
            // S.scrollTo(scrollingContainer, {
            //     top: firstError,
            //     offsetY: 100,
            //     behavior:'smooth'
            // })
        }

        console.log({error: firstError});
        

        // Add focus to first input element
        // setTimeout(() => {
        //     S.find(firstError, 'input')[0].focus()
        // }, 500);

        // S.scrollTo(scrollingContainer, {
        //     top: firstError,
        //     offsetY: 100,
            
        // })

        // Scroll to first error
        
    }


    // validation functions
    function checkEmpty(elem) {
        var str = S.val(elem)

        str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

        if (S.val(elem).length <= 0) {
            return false
        }
        return true
    }

    function checkEmail(elem) {
        var text = S.val(elem)
        if (emailRegExp.test(text)) {
            return true
        }
        return false
    }

    function checkMin(elem) {
        if (S.val(elem).length <= S.attr(elem, 'data-min') - 1) {
            return false
        }
        return true
    }

    function checkMax(elem) {
        if (S.val(elem).length > S.attr(elem, 'data-max')) {
            return false
        }
        return true
    }

    function checkLowercase(elem) {
        return /[a-z]/.test(S.val(elem))
    }

    function checkUppercase(elem) {
        return /[A-Z]/.test(S.val(elem))
    }

    function checkNumberExist(elem) {
        return /[0-9]/.test(S.val(elem))
    }

    function checkDigits(elem) {
        return /^\d+$/.test(S.val(elem))
    }

    function checkCompare(elem, val) {
        return S.val(elem) !== val
    }

    function checkEqual(elem, val) {
        return S.val(elem) === val
    }

    function checkCheckbox(elem) {
        if (S.prop(elem, 'checked')) {
            return true
        }

        return false
    }

    function checkGroup(group) {
        let inputs = S.find(group, 'input[type="checkbox"], input[type="radio"]')
        let results = []

        S.each(inputs, function () {
            results.push(S.prop(this, 'checked'))
        })

        if (results.indexOf(true) == -1) {
            S.addClass(group, 'error')
        } else {
            S.removeClass(group, 'error')
        }

        return results.indexOf(true) != -1
    }

    function checkFileExist(elem) {
        if (!elem.filesArr) {
            return false
        }

        return elem.filesArr.length > 0
    }



    function startPreload() {
        S.addClass(params.formBtn, 'btn--preload').attr(params.formBtn, 'disabled', true)
        S.addClass(parent, 'form--preload')
    }

    function stopPreload() {
        S.removeClass(params.formBtn, 'btn--preload').attr(params.formBtn, 'disabled', false)
        S.removeClass(parent, 'form--preload')
    }

    return {
        isValid: succesValue,
        clearInputs: clearInputs,
        startPreload: startPreload,
        stopPreload: stopPreload,
        setSuccess: setSuccess
    }

}


// Autofill form
if (formAutofill) {
    S.on('.form input', 'keyup', function (e) {
        if (e.altKey == true && e.keyCode == 86) {
            let thisForm = S.closest(this, '.form')
            let textInps = S.find(thisForm, 'input[type="text"]')
            let passInps = S.find(thisForm, 'input[type="password"]')
            let numberInps = S.find(thisForm, 'input[type="number"], input[type="tel"]')
            let emailInps = S.find(thisForm, 'input[type="email"]')
            let textarea = S.find(thisForm, 'textarea')

            S.val(textInps, 'Auto filled input')
            S.val(passInps, 'autofill')
            S.val(numberInps, '123456789')
            S.val(emailInps, 'autofill@form.com')
            S.val(textarea, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultricies lobortis felis, eu fermentum tortor aliquet eu. Nunc neque nibh, accumsan non placerat at, imperdiet nec libero. Proin nec nulla et risus accumsan viverra sed dapibus diam. Ut vel mauris commodo, posuere tortor id, pharetra libero. Nulla faucibus sem eu massa malesuada aliquet. Integer ultrices libero venenatis elit egestas aliquam. Morbi viverra nisl eros, vitae eleifend enim vulputate ac. Vestibulum venenatis eu lacus et ultricies. In hac habitasse platea dictumst. Etiam justo ipsum, viverra quis pretium a, gravida quis metus.')
        }
    })
}


if (!window.App) {
    window.App = {}
    window.App.Validation = Validation
} else {
    window.App.Validation = Validation
}
// EXTERNAL MODULE: ./dev/components/accordion/accordion.js
var accordion = __webpack_require__(18);

// CONCATENATED MODULE: ./dev/components/tab/tab.js
class Tab {
    constructor(parent, options) {

        var self = this;
        self.container = S.first(parent);
        self.btnsContainer = S.first(self.container, '.tab-btns')
        self.btns = S.find(self.btnsContainer, '.tab-btns__toggle-tab')
        self.wrapper = S.closest(self.container, '.tab-wrapper')
        self.tabItemsContainer = S.first(self.wrapper, '.tab__container')
        self.tabItems = [...this.tabItemsContainer.children]

        self.activeIndex = 0;
        self.itemsWrapper = null // DOM Element which contains tab-items if content is outside of tabs


        self.url = location.href
        self.urlTabIndex = self.url.split('!#')[1]



        Object.assign(this, options)

        self.tabItems = this.getTabItems()

        if (self.urlTabIndex) {


            window.addEventListener('hashchange', function () {
                this.changeTabByUrl(self)

            })

            this.changeTabByUrl(self)
        }



        S.on(self.btns, 'click', function (e) {
            self.setActiveTab(this, e)
        })

        if (self.activeIndex > 0) {
            S.trigger(self.btns[self.activeIndex], 'click')

        }
    }

    changeTabByUrl(self) {
        self.clickedBtn = self.btns.filter(btn => {
            var btnId = self.urlTabIndex
            return S.attr(btn, 'data-tab') == btnId
        })[0]


        if (self.clickedBtn) {
            S.on(self.clickedBtn, 'click', self.setActiveTab.bind(self, self.clickedBtn))
            S.trigger(self.clickedBtn, 'click')
        }

    }

    getTabItems() {
        let items = []
        if (this.itemsWrapper) {
            let itemsContainerArr = S.find(this.itemsWrapper, '.tab__other-place-content-container');

            S.each(itemsContainerArr, function () {
                let itemsContainer = this.firstChild
                let itemsArr = [...itemsContainer.children]
                items.push(itemsArr)
            })

        } else {
            items.push([...this.tabItemsContainer.children])
        }

        return items
    }
    setActiveTab(elm, e) {
        let clickedBtn = elm
        let index = [...this.btns].indexOf(clickedBtn)
        let activeIndex = this.activeIndex

        // Change button
        this.btns[this.activeIndex].classList.remove('js-tab-active', 'active')
        this.btns[index].classList.add('js-tab-active', 'active')

        // change items 

        S.each(this.tabItems, function () {
            this[index].classList.add('js-tab-active', 'active')

            if (index != activeIndex) {
                this[activeIndex].classList.remove('js-tab-active', 'active')
            }
        })

        this.activeIndex = index
    }
}


/* harmony default export */ var tab = (Tab);


if (S.find('.tab').length > 0) {
    S.each('.tab', function () {
        var f = new Tab(this, {
            itemsWrapper: S.first(this.closest('.tab-wrapper'), '.tab__other-place-content-wrapper')
        })

    })

}
// EXTERNAL MODULE: ./dev/components/gallery/gallery.js
var gallery = __webpack_require__(19);

// EXTERNAL MODULE: ./dev/components/pagination/pagination.js
var pagination = __webpack_require__(20);

// EXTERNAL MODULE: ./dev/components/breadscrumbs/breadscrumbs.js
var breadscrumbs = __webpack_require__(21);

// EXTERNAL MODULE: ./dev/components/raiting/raiting.js
var raiting = __webpack_require__(22);

// EXTERNAL MODULE: ./dev/components/scroll-x/scroll-x.js
var scroll_x = __webpack_require__(23);

// EXTERNAL MODULE: ./dev/components/searchfield/searchfield.js
var searchfield = __webpack_require__(24);

// EXTERNAL MODULE: ./dev/components/picture/picture.js
var picture = __webpack_require__(25);

// EXTERNAL MODULE: ./dev/components/leafer/leafer.js
var leafer = __webpack_require__(26);

// EXTERNAL MODULE: ./dev/js/vendors/charming.min.js
var charming_min = __webpack_require__(27);

// EXTERNAL MODULE: ./node_modules/perfect-scrollbar/dist/perfect-scrollbar.esm.js
var perfect_scrollbar_esm = __webpack_require__(2);

// CONCATENATED MODULE: ./dev/components/slideshow/slideshow.js


// import imagesLoaded from '../../js/vendors/imagesloaded.pkgd.min'

const getMousePos = (e) => {
    let posx = 0;
    let posy = 0;
    if (!e) e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    } else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    return {
        x: posx,
        y: posy
    }
};
// Gets a random integer.
const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
// Equation of a line (y = mx + b ).
const lineEq = (y2, y1, x2, x1, currentVal) => {
    const m = (y2 - y1) / (x2 - x1);
    const b = y1 - m * x1;
    return m * currentVal + b;
};

// Some random chars.
const chars = ['$', '%', '#', '&', '=', '*', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.', ':', ',', '^'];
const charsTotal = chars.length;

// Randomize letters function. Used when navigating the slideshow to switch the curretn slide´s texts.
const randomizeLetters = (letters) => {
    return new Promise((resolve, reject) => {
        const lettersTotal = letters.length;
        let cnt = 0;

        letters.forEach((letter, pos) => {
            let loopTimeout;
            const loop = () => {
                letter.innerHTML = chars[getRandomInt(0, charsTotal - 1)];
                loopTimeout = setTimeout(loop, getRandomInt(50, 500));
            };
            loop();

            const timeout = setTimeout(() => {
                clearTimeout(loopTimeout);
                letter.style.opacity = 1;
                letter.innerHTML = letter.dataset.initial;
                ++cnt;
                if (cnt === lettersTotal) {
                    resolve();
                }
            }, pos * lineEq(40, 0, 8, 200, lettersTotal));
        });
    });
};

// Hide each of the letters with random delays. Used when showing the current slide´s content.
const disassembleLetters = (letters) => {
    return new Promise((resolve, reject) => {
        const lettersTotal = letters.length;
        let cnt = 0;

        letters.forEach((letter, pos) => {
            setTimeout(() => {
                letter.style.opacity = 0;
                ++cnt;
                if (cnt === lettersTotal) {
                    resolve();
                }
            }, pos * 30);
        });
    });
}

// The Slide class.
class SlideshowSlide {
    constructor(el) {
        this.DOM = {
            el: el
        };
        // The image wrap element.
        this.DOM.imgWrap = this.DOM.el.querySelector('.slideshow__slide-img-wrap');
        // The image element.
        this.DOM.img = this.DOM.imgWrap.querySelector('.slideshow__slide-img');
        // The texts: the parent wrap, title, number and side text.
        this.DOM.texts = {
            // wrap: this.DOM.el.querySelector('.slideshow__slide-title-wrap'),
            // title: this.DOM.el.querySelector('.slideshow__slide-title'),
            // number: this.DOM.el.querySelector('.slideshow__slide-number'),
            side: this.DOM.el.querySelector('.slideshow__slide-side'),
        };
        // Split the title and side texts into spans, one per letter. Sort these so we can later animate then with the 
        // randomizeLetters or disassembleLetters functions when navigating and showing the content.
        // charming(this.DOM.texts.title);
        // charming(this.DOM.texts.side);
        // this.DOM.titleLetters = Array.from(this.DOM.texts.title.querySelectorAll('span')).sort(() => 0.5 - Math.random());
        // this.DOM.sideLetters = Array.from(this.DOM.texts.side.querySelectorAll('span')).sort(() => 0.5 - Math.random());
        // this.DOM.titleLetters.forEach(letter => letter.dataset.initial = letter.innerHTML);
        // this.DOM.sideLetters.forEach(letter => letter.dataset.initial = letter.innerHTML);
        // Calculate the sizes of the image wrap. 
        this.calcSizes();
        // And also the transforms needed per position. 
        // We have 5 different possible positions for a slide: center, bottom right, top left and outside the viewport (top left or bottom right).
        this.calcTransforms();
        // Init/Bind events.
        this.initEvents();
    }
    // Gets the size of the image wrap.
    calcSizes() {
        this.width = this.DOM.imgWrap.offsetWidth;
        this.height = this.DOM.imgWrap.offsetHeight;
    }
    // Gets the transforms per slide position.
    calcTransforms() {
        /*
        Each position corresponds to the position of a given slide:
        0: left top corner outside the viewport
        1: left top corner (prev slide position)
        2: center (current slide position)
        3: right bottom corner (next slide position)
        4: right bottom corner outside the viewport
        5: left side, for when the content is shown
        */
       if(window.innerWidth > 991) {
        this.transforms = [{
                   x: -1 * (winsize.width / 2 + this.width),
                   y: -1 * (winsize.height / 2 + this.height),
                   rotation: -30
               },
               {
                   x: -1 * (winsize.width / 2 - this.width / 5.5),
                   y: -1 * (winsize.height / 2 - this.height / 3),
                   rotation: 0
               },
               {
                   x: 0,
                   y: 0,
                   rotation: 0
               },
               {
                   x: winsize.width / 2 - this.width / 5.5,
                   y: winsize.height / 2 - this.height / 3,
                   rotation: 0
               },
               {
                   x: winsize.width / 2 + this.width,
                   y: winsize.height / 2 + this.height,
                   rotation: 30
               },
               {
                   // x: -1 * (winsize.width / 4 - this.width / 4),
                   x: -1 * (winsize.width / 4),
                   y: 0,
                   rotation: 0
               }
           ];
       } else {
        this.transforms = [{
                   x: -1 * (winsize.width / 2 + this.width),
                   y: -1 * (winsize.height / 2 + this.height),
                   rotation: -30
               },
               {
                   x: -1 * (winsize.width / 2 + this.width / 2),
                   y: -1 * (winsize.height / 2 + this.height / 2),
                   rotation: 0
               },
               {
                   x: 0,
                   y: 0,
                   rotation: 0
               },
               {
                   x: winsize.width / 2 + this.width / 2,
                   y: winsize.height / 2 + this.height / 2,
                   rotation: 0
               },
               {
                   x: winsize.width / 2 + this.width,
                   y: winsize.height / 2 + this.height,
                   rotation: 30
               },
               {
                   // x: -1 * (winsize.width / 4 - this.width / 4),
                   x: -1 * (winsize.width / 4),
                   y: 0,
                   rotation: 0
               }
           ];
       }
        
    }
    // Init events:
    // Mouseevents for mousemove/tilt/scale on the current image, and window resize.
    initEvents() {
        this.mouseenterFn = () => {
            if (!this.isPositionedCenter() || !allowTilt) return;
            clearTimeout(this.mousetime);
            this.mousetime = setTimeout(() => {
                // Scale the image.
                TweenMax.to(this.DOM.img, 0.8, {
                    ease: Power3.easeOut,
                    scale: 1.1
                });
            }, 40);
        };
        this.mousemoveFn = ev => requestAnimationFrame(() => {
            // Tilt the current slide.
            if (!allowTilt || !this.isPositionedCenter()) return;
            this.tilt(ev);
        });
        this.mouseleaveFn = (ev) => requestAnimationFrame(() => {
            if (!allowTilt || !this.isPositionedCenter()) return;
            clearTimeout(this.mousetime);

            // Reset tilt and image scale.
            TweenMax.to([this.DOM.imgWrap, this.DOM.texts.wrap], 1.8, {
                ease: 'Power4.easeOut',
                x: 0,
                y: 0,
                rotationX: 0,
                rotationY: 0
            });
            TweenMax.to(this.DOM.img, 1.8, {
                ease: 'Power4.easeOut',
                scale: 1
            });
        });
        // When resizing the window recalculate size and transforms, since both will depend on the window size.
        this.resizeFn = () => {
            this.calcSizes();
            this.calcTransforms();
        };
        this.DOM.imgWrap.addEventListener('mouseenter', this.mouseenterFn);
        this.DOM.imgWrap.addEventListener('mousemove', this.mousemoveFn);
        this.DOM.imgWrap.addEventListener('mouseleave', this.mouseleaveFn);
        window.addEventListener('resize', this.resizeFn);
    }
    // Tilt the image wrap and texts when mouse moving the current slide.
    tilt(ev) {
        const mousepos = getMousePos(ev);
        // Document scrolls.
        const docScrolls = {
            left: document.body.scrollLeft + document.documentElement.scrollLeft,
            top: document.body.scrollTop + document.documentElement.scrollTop
        };
        const bounds = this.DOM.imgWrap.getBoundingClientRect();;
        // Mouse position relative to the main element (this.DOM.el).
        const relmousepos = {
            x: mousepos.x - bounds.left - docScrolls.left,
            y: mousepos.y - bounds.top - docScrolls.top
        };

        // Move the element from -20 to 20 pixels in both x and y axis.
        // Rotate the element from -15 to 15 degrees in both x and y axis.
        let t = {
                x: [-20, 20],
                y: [-20, 20]
            },
            r = {
                x: [-15, 15],
                y: [-15, 15]
            };

        const transforms = {
            translation: {
                x: (t.x[1] - t.x[0]) / bounds.width * relmousepos.x + t.x[0],
                y: (t.y[1] - t.y[0]) / bounds.height * relmousepos.y + t.y[0]
            },
            rotation: {
                x: (r.x[1] - r.x[0]) / bounds.height * relmousepos.y + r.x[0],
                y: (r.y[1] - r.y[0]) / bounds.width * relmousepos.x + r.y[0]
            }
        };

        // Move the image wrap.
        TweenMax.to(this.DOM.imgWrap, 1.5, {
            ease: 'Power1.easeOut',
            x: transforms.translation.x,
            y: transforms.translation.y,
            rotationX: transforms.rotation.x,
            rotationY: transforms.rotation.y
        });

        // Move the texts wrap.
        // TweenMax.to(this.DOM.texts.wrap, 1.5, {
        //     ease: 'Power1.easeOut',
        //     x: -1 * transforms.translation.x,
        //     y: -1 * transforms.translation.y
        // });
    }
    // Positions one slide (left, right or current) in the viewport.
    position(pos) {
        TweenMax.set(this.DOM.imgWrap, {
            x: this.transforms[pos].x,
            y: this.transforms[pos].y,
            rotationX: 0,
            rotationY: 0,
            opacity: 1,
            rotationZ: this.transforms[pos].rotation
        });
    }
    // Sets it as current.
    setCurrent(isContentOpen) {
        this.isCurrent = true;
        this.DOM.el.classList.add('slideshow__slide--current', 'slideshow__slide--visible');
        // Position it on the current´s position.
        this.position(isContentOpen ? 5 : 2);
    }
    // Position the slide on the left side.
    setLeft(isContentOpen) {
        this.isRight = this.isCurrent = false;
        this.isLeft = true;
        this.DOM.el.classList.add('slideshow__slide--visible');
        // Position it on the left position.
        this.position(isContentOpen ? 0 : 1);
    }
    // Position the slide on the right side.
    setRight(isContentOpen) {
        this.isLeft = this.isCurrent = false;
        this.isRight = true;
        this.DOM.el.classList.add('slideshow__slide--visible');
        // Position it on the right position.
        this.position(isContentOpen ? 4 : 3);
    }
    // Check if the slide is positioned on the right side (if it´s the next slide in the slideshow).
    isPositionedRight() {
        return this.isRight;
    }
    // Check if the slide is positioned on the left side (if it´s the previous slide in the slideshow).
    isPositionedLeft() {
        return this.isLeft;
    }
    // Check if the slide is the current one.
    isPositionedCenter() {
        return this.isCurrent;
    }
    // Reset classes and state.
    reset() {
        this.isRight = this.isLeft = this.isCurrent = false;
        this.DOM.el.classList = 'slideshow__slide';
    }
    hide() {
        TweenMax.set(this.DOM.imgWrap, {
            x: 0,
            y: 0,
            rotationX: 0,
            rotationY: 0,
            rotationZ: 0,
            opacity: 0
        });
    }
    // Moves a slide to a specific position defined in settings.position.
    // Also, moves it from position settings.from and if we need to reset the image scale when moving the slide then settings.resetImageScale should be true.
    moveToPosition(settings) {



        return new Promise((resolve, reject) => {
            /*
            Moves the slide to a specific position:
            -2: left top corner outside the viewport
            -1: left top corner (prev slide position)
            0: center (current slide position)
            1: right bottom corner (next slide position)
            2: right bottom corner outside the viewport
            3: left side, for when the content is shown
            */
            TweenMax.to(this.DOM.imgWrap, .8, {
                ease: Power4.easeInOut,
                delay: settings.delay || 0,
                startAt: settings.from !== undefined ? {
                    x: this.transforms[settings.from + 2].x,
                    y: this.transforms[settings.from + 2].y,
                    rotationX: 0,
                    rotationY: 0,
                    rotationZ: this.transforms[settings.from + 2].rotation
                } : {},
                x: this.transforms[settings.position + 2].x,
                y: this.transforms[settings.position + 2].y,
                rotationX: 0,
                rotationY: 0,
                rotationZ: this.transforms[settings.position + 2].rotation,
                onStart: settings.from !== undefined ? () => TweenMax.set(this.DOM.imgWrap, {
                    opacity: 1
                }) : null,
                onComplete: resolve
            });

            // Reset image scale when showing the content of the current slide.
            if (settings.resetImageScale) {
                TweenMax.to(this.DOM.img, .8, {
                    ease: Power4.easeInOut,
                    scale: 1
                });
            }
        });
    }
    // Hides the current slide´s texts.
    hideTexts(animation, direction) {



        if (animation && direction == 'next') {

            TweenMax.to(this.DOM.texts.side, .4, {
                opacity: 0,
                y: '-50%',
                x: '50%'

            });
            setTimeout(() => {
                TweenMax.to(this.DOM.texts.side, 0, {

                    // transform: 'none'
                    y: 0,
                    x: '50%'

                });
            }, 400);
        } else if (animation && direction == 'prev') {

            TweenMax.to(this.DOM.texts.side, .4, {
                opacity: 0,
                y: '50%',
                x: '50%'

            });
            setTimeout(() => {
                TweenMax.to(this.DOM.texts.side, 0, {

                    // transform: 'none'
                    y: 0,
                    x: '50%'

                });
            }, 400);
        } else {

            TweenMax.to(this.DOM.texts.side, .4, {
                opacity: 0,
                // transform: 'none'
                y: 0,
                x: '50%'
            });
        }
    }
    // Shows the current slide´s texts.
    showTexts(animation, direction) {
        
        

        if (animation && direction == 'prev') {

            TweenMax.to(this.DOM.texts.side, 0, {
                y: '-50%',
                x: '50%'
            });
            setTimeout(() => {
                TweenMax.to(this.DOM.texts.side, 0.4, {
                    opacity: 1,
                    // transform: 'none'
                    y: '0%',
                    x: '50%'
                });
            }, 0);
        } else if (animation && direction == 'next') {

            TweenMax.to(this.DOM.texts.side, 0, {
                y: '50%',
                x: '50%'
            });
            setTimeout(() => {

                
                TweenMax.to(this.DOM.texts.side, 0.4, {
                    opacity: 1,
                    x: '50%',
                    y: '0%',
                });
            }, 0);
        } else if (animation) {

            

            setTimeout(() => {
                TweenMax.to(this.DOM.texts.side, 0.4, {
                    opacity: 1,
                    // transform: 'none'
                    y: 0,
                    x: '50%'
                });
            }, 300);
        } else {

            TweenMax.to(this.DOM.texts.side, 0, {
                opacity: 1,
                // transform: 'none'
                y: 0,
                x: '50%'
            });
        }








        // if (animation) {
        //     randomizeLetters(this.DOM.titleLetters);
        //     randomizeLetters(this.DOM.sideLetters);
        //     TweenMax.to(this.DOM.texts.number, 0.6, {
        //         ease: Elastic.easeOut.config(1, 0.5),
        //         startAt: {
        //             x: '-10%',
        //             opacity: 0
        //         },
        //         x: '0%',
        //         opacity: 1
        //     });
        // }
    }
}

// The Content class. Represents one content item per slide.
class SlideshowContent {
    constructor(el) {
        this.DOM = {
            el: el
        };
        this.DOM.content = this.DOM.el.querySelector('.slideshow__content-item');
        this.DOM.info = this.DOM.el.querySelector('.product-info__details-container')
        // this.DOM.title = this.DOM.el.querySelector('.slideshow__content-title');
        // this.DOM.subtitle = this.DOM.el.querySelector('.slideshow__content-subtitle');
        // this.DOM.textBlock = this.DOM.el.querySelector('.slideshow__content-textblock');
        this.DOM.backCtrl = this.DOM.el.parentNode.querySelector('.slideshow__content-close');
        this.DOM.backCtrl.addEventListener('click', () => slideshow.hideContent());

        new ps(this.DOM.info, {
            maxScrollbarLength: 50,
            wheelSpeed: .1,
            swipeEasing: true,
            wheelPropagation: false
        })
    }
    show() {
        this.DOM.el.classList.add('slideshow__content-item--current');

        TweenMax.to([this.DOM.backCtrl, this.DOM.el], 0.8, {
            ease: Power4.easeOut,
            delay: 0.4,
            opacity: 1,
        });
    }
    hide() {
        this.DOM.el.classList.remove('slideshow__content-item--current');

        TweenMax.to([this.DOM.backCtrl, this.DOM.el].reverse(), 0.3, {
            ease: Power3.easeIn,
            opacity: 0,

        });
    }
}

// The Slideshow class.
class Slideshow {
    constructor(el) {
        this.DOM = {
            el: el
        };
        // The slides.
        this.slides = [];
        this.navBtns = S.find(this.DOM.el, '.slideshow-nav')
        Array.from(this.DOM.el.querySelectorAll('.slideshow__slide')).forEach(slideEl => this.slides.push(new SlideshowSlide(slideEl)));
        // The total number of slides.
        this.slidesTotal = this.slides.length;
        // At least 4 slides to continue...
        if (this.slidesTotal < 4) {
            return false;
        }
        // Current slide position.
        this.current = 0;
        this.DOM.deco = this.DOM.el.querySelector('.slideshow__deco');
        this.DOM.decoBg = this.DOM.el.querySelector('.slideshow__deco-bg');
        this.DOM.decoImg = this.DOM.deco.querySelector('.slideshow__deco-bg-img');





        this.contents = [];
        Array.from(document.querySelectorAll('.slideshow__content > .slideshow__content-item')).forEach(contentEl => this.contents.push(new SlideshowContent(contentEl)));

        // Set the current/next/previous slides. 
        this.render();
        this.currentSlide.showTexts(false);

        this.setBgColor()

        // Init/Bind events.
        this.initEvents();
    }
    render() {
        // The current, next, and previous slides.
        this.currentSlide = this.slides[this.current];
        this.nextSlide = this.slides[this.current + 1 <= this.slidesTotal - 1 ? this.current + 1 : 0];
        this.prevSlide = this.slides[this.current - 1 >= 0 ? this.current - 1 : this.slidesTotal - 1];
        this.currentSlide.setCurrent();
        this.nextSlide.setRight();
        this.prevSlide.setLeft();


    }
    initEvents() {
        // Clicking the next and previous slide starts the navigation / clicking the current shows its content..
        if(window.innerWidth >991) {
            this.clickFn = (slide, e) => {
            
                e.preventDefault()
                if (slide.isPositionedRight()) {
                    this.navigate('next');
                    this.setBgColor()
                } else if (slide.isPositionedLeft()) {
                    this.navigate('prev');
                    this.setBgColor()
                } else {
                    this.showContent();                  

                }
            }            
        } else {
            this.clickFn = (navBtn) => {

                
                if (navBtn.matches('.slideshow-nav--next')) {
                    this.navigate('next');
                    this.setBgColor()
                } else{
                    this.navigate('prev');
                    this.setBgColor()
                } 
            }     
        }
        if(window.innerWidth >991) {
            for (let slide of this.slides) {
                slide.DOM.imgWrap.addEventListener('click', (e) => this.clickFn(slide, e));
            }
        } else {
            for (let navBtn of this.navBtns) {
                
                
                navBtn.addEventListener('click', () => this.clickFn(navBtn));
            }
        }
        
      

        this.resizeFn = () => {
            // Reposition the slides.
            this.nextSlide.setRight(this.isContentOpen);
            this.prevSlide.setLeft(this.isContentOpen);
            this.currentSlide.setCurrent(this.isContentOpen);

            if (this.isContentOpen) {

                TweenMax.set(this.DOM.deco, {
                    scaleX: winsize.width / this.DOM.deco.offsetWidth,
                    scaleY: winsize.width / this.DOM.deco.offsetWidth,
                    x: -(winsize.width / 2),
                    y: 20
                });

                TweenMax.to(this.DOM.decoBg, {
                    ease: Power4.easeInOut,
                    scaleX: 1,
                    scaleY: 1,
                });

            }
        };
        window.addEventListener('resize', this.resizeFn);
    }

    setBgColor() {
        var currentbgColor = this.currentSlide.DOM.el.dataset.bgcolor
        this.DOM.deco.style.color = currentbgColor
        this.DOM.decoBg.style.backgroundColor = currentbgColor

    }

    showContent() {


        if (this.isContentOpen || this.isAnimating) return;
        allowTilt = false;
        this.isContentOpen = true;
        this.DOM.el.classList.add('slideshow--previewopen');
        var contentOpen = new CustomEvent('contentOpen')
        document.dispatchEvent(contentOpen)

        TweenMax.to(this.DOM.deco, .8, {
            ease: Power4.easeInOut,
            scaleX: winsize.width / this.DOM.deco.offsetWidth,
            scaleY: winsize.width / this.DOM.deco.offsetWidth,
            x: -(winsize.width / 2),
            y: 20
        });
        TweenMax.to(this.DOM.decoBg, .8, {
            ease: Power4.easeInOut,
            scaleX: 1,
            scaleY: 1,
        });




        // Move away right/left slides.
        this.prevSlide.moveToPosition({
            position: -2
        });
        this.nextSlide.moveToPosition({
            position: 2
        });
        // Position the current slide and reset its image scale value.
        this.currentSlide.moveToPosition({
            position: 3,
            resetImageScale: true
        });
        // Show content and back arrow (to close the content).
        this.contents[this.current].show();
        // Hide texts.
        this.currentSlide.hideTexts(false);
    }
    hideContent() {
        if (!this.isContentOpen || this.isAnimating) return;

        this.DOM.el.classList.remove('slideshow--previewopen');

        // Hide content.
        this.contents[this.current].hide();

        var contentClose = new CustomEvent('contentClose')
        document.dispatchEvent(contentClose)

        TweenMax.to(this.DOM.deco, .8, {
            ease: Power4.easeInOut,
            scaleX: 1,
            scaleY: 1,
            x: 0,
            y: 0
        });
        TweenMax.to(this.DOM.decoBg, .8, {
            ease: Power4.easeInOut,
            scaleX: .3,
            scaleY: .3,
        });


        // Move in right/left slides.
        this.prevSlide.moveToPosition({
            position: -1
        });
        this.nextSlide.moveToPosition({
            position: 1
        });
        // Position the current slide.
        this.currentSlide.moveToPosition({
            position: 0
        }).then(() => {
            allowTilt = false;
            this.isContentOpen = false;
        });
        // Show texts.
        this.currentSlide.showTexts(true);
    }
    // Animates the element behind the current slide.
    bounceDeco(direction, delay) {
        TweenMax.to(this.DOM.deco, .4, {
            ease: 'Power2.easeIn',
            delay: delay + delay * 0.2,
            x: direction === 'next' ? -40 : 40,
            y: direction === 'next' ? -40 : 40,
            onComplete: () => {
                TweenMax.to(this.DOM.deco, 0.6, {
                    //ease: Elastic.easeOut.config(1, 0.65),
                    ease: 'Power2.easeOut',
                    x: 0,
                    y: 0
                });
            }
        });
    }
    // Navigate the slideshow.
    navigate(direction) {
        // If animating return.
        if (this.isAnimating) return;
        this.isAnimating = true;
        allowTilt = false;

        const upcomingPos = direction === 'next' ?
            this.current < this.slidesTotal - 2 ? this.current + 2 : Math.abs(this.slidesTotal - 2 - this.current) :
            this.current >= 2 ? this.current - 2 : Math.abs(this.slidesTotal - 2 + this.current);

        this.upcomingSlide = this.slides[upcomingPos];

        // Update current.
        this.current = direction === 'next' ?
            this.current < this.slidesTotal - 1 ? this.current + 1 : 0 :
            this.current > 0 ? this.current - 1 : this.slidesTotal - 1;

        // Move slides (the previous, current, next and upcoming slide).
        this.prevSlide.moveToPosition({
            position: direction === 'next' ? -2 : 0,
            delay: direction === 'next' ? 0 : 0.14
        }).then(() => {
            if (direction === 'next') {
                this.prevSlide.hide();
            }
        });

        this.currentSlide.moveToPosition({
            position: direction === 'next' ? -1 : 1,
            delay: 0.07
        });

        this.currentSlide.hideTexts(true, direction);

        // this.bounceDeco(direction, 0.07);

        this.nextSlide.moveToPosition({
            position: direction === 'next' ? 0 : 2,
            delay: direction === 'next' ? 0.14 : 0
        }).then(() => {
            if (direction === 'prev') {
                this.nextSlide.hide();
            }
        });

        setTimeout(() => {
            if (direction === 'next') {
                this.nextSlide.showTexts(true, direction);
            } else {
                this.prevSlide.showTexts(true, direction);
            }

        }, 700);


        this.upcomingSlide.moveToPosition({
            position: direction === 'next' ? 1 : -1,
            from: direction === 'next' ? 2 : -2,

        }).then(() => {
            // Reset classes.
            [this.nextSlide, this.currentSlide, this.prevSlide].forEach(slide => slide.reset());
            this.render();
            this.setBgColor()
            allowTilt = false;
            this.isAnimating = false;

        });


    }
}

// Window sizes.
let winsize;
const calcWinsize = () => winsize = {
    width: window.innerWidth,
    height: window.innerHeight
};

calcWinsize();

window.addEventListener('resize', calcWinsize);

let allowTilt = false;

var slideshow


initSlideshow()

function initSlideshow() {
    // Init slideshow.
    var slider = document.querySelector('.slideshow')
    if (slider) {
        slideshow = new Slideshow(slider);
    }
}

// Preload all the images in the page..
// const loader = document.querySelector('.loader');


// if (loader) {
//     imagesLoaded(document.querySelectorAll('.slideshow__slide-img'), {
//         background: true
//     }, () => document.body.classList.remove('loading'));
// }
// EXTERNAL MODULE: ./dev/components/news-card/news-card.js
var news_card = __webpack_require__(28);

// CONCATENATED MODULE: ./dev/components/selection/selection.js




class selection_SelectionInit {
    constructor(selector, options) {
        const parent = selector;
        var options = options || {}
        var self = this
        this.parent = parent
        this.isMobile = window.innerWidth <= 991
        this.closeBtn = parent.querySelector('.js-selection-results-close-btn')
        this.dataContainer = parent.querySelector('.js-selection-results-data')
        this.resultsShown = false





        this.data = [] // array of objects
        if (this.dataContainer) {
            S.each(JSON.parse(this.dataContainer.dataset.arr), function () {
                self.data.push(this['value'])
            })


        }




        this.modal = S.closest(this.parent, '.modal')

        this.resultsSelectedElms = []
        this.inputs = []


        this.resultsElmHeight = 0
        this.resultsElmPos = 0
        this.resultItems = []
        this.resultInputs = []
        this.isBottom = false
        this.clearBtn = parent.querySelector('.js-selection-clear-selected')
        this.multiple = false
        // this.templateSelector = '#template-selection-result'

        this.inputLim = 25
        this.resultsLength = 0

        if (!parent) return

        const preloaderElm = parent.querySelector('.preloader')

        // Defaults
        Object.assign(this, {
            input: parent.querySelector('.js-selection-main-input'),
            searchInput: parent.querySelector('.js-selection-search-input'),
            resultsElm: parent.querySelector('.js-selection-results'),
            noResultsElm: parent.querySelector('.js-selection-no-results'),
            resultsListElm: parent.querySelector('.js-selection-results-list'),
            templateSelector: '#template-selection-result',
            preloaderElm: preloaderElm,
            minChars: 3,
            activeIndex: 0,
            debounce: null,
            search: true,
            ajax: false,
            // ajax: {
            //     debounce: null,
            //     onInput: null
            // },
            onInput: function () {},
            onSelected: function () {},
            onClearedSelected: function () {}
        }, options)


        Object.assign(this, this.ajax, options.ajax)

        console.log(this, this.templateSelector, this.multiple);




        this.template = document.querySelector(this.templateSelector)



        // Bind events
        if (this.ajax) {
            this.input.addEventListener('input', this.togglePreloaderAndResults.bind(this))

            if (this.searchInput) {
                this.searchInput.addEventListener('input', this.getData.bind(this, this.ajax.debounce))
            }
        } else {

            this.render(this.dataContainer)
            if (this.searchInput) {

                this.searchInput.addEventListener('input', this.searching.bind(this))
            }
        }



        this.input.addEventListener('focus', this.showResultsElm.bind(this))

        this.closeBtn.addEventListener('click', this.hideResultsElm.bind(this))


        if (this.modal) {
            this.modal.addEventListener('scroll', this.hideResultsElm.bind(this))
        }

        this.clearBtn.addEventListener('click', this.clearSelected.bind(this))


        if (window.innerWidth > 991) {
            window.addEventListener('scroll', this.hideResultsElm.bind(this))
            document.addEventListener('click', function (e) {
                var clickedOnThisSelection = e.target.closest('.js-selection') == self.parent

                if (!clickedOnThisSelection) {
                    self.hideResultsElm()
                }

            })

        } else {
            window.addEventListener('resize', this.setPosition.bind(this))
        }


        // Custom events
        this.event = new CustomEvent('selected');
        this.resultOpenedEvent = new CustomEvent('resultOpened')
        this.resultClosedEvent = new CustomEvent('resultClosed')

        if (!this.isMobile) {


            this.ps = new perfect_scrollbar_esm["a" /* default */](this.resultsListElm, {
                maxScrollbarLength: 50,
                wheelSpeed: .5,
                swipeEasing: true,
                wheelPropagation: false
            })
        }

        this.setPosition()

    }


    getData(deb) {

        // Debounce 
        if (deb) {
            this.showPreloader()
            this.resultsElm.classList.add('disabled')

            clearTimeout(deb.timeout)
            deb.timeout = setTimeout(() => {


                // Call external function and send current this state
                this.ajax.onInput.apply(this)

                this.hidePreloader()
                this.resultsElm.classList.remove('disabled')
                this.showResultsElm()
            }, deb.delay);
        }

    }

    searching() {
        self = this

        let val = this.searchInput.value.toLowerCase()


        let filteredData

        filteredData = this.data.filter(item => {
            return item.toLowerCase().indexOf(val) != -1
        })

        S.each(this.resultItems, function () {
            this.hidden = true
        })

        S.each(filteredData, function () {
            var searchResultElms = S.find(self.resultsElm, `[data-value="${this}"]`)
            S.each(searchResultElms, function () {
                this.hidden = false
            })

        });

        if (!this.isMobile) {
            this.ps.update()
        }

    }

    render(data) {
        self = this

        console.log(this.templateSelector, this.multiple);



        if (this.ajax) {
            // clear results because with every click data appended not updated
            this.resultItems = S.find(this.resultsListElm, '.selection__result')

            if (this.resultItems.length > 0) {
                S.each(this.resultItems, function () {
                    S.remove(this)
                })
            }
        }



        template(this.template, data, this.resultsListElm)

        this.resultItems = S.find(this.resultsListElm, '.selection__result')
        this.inputs = S.find(this.resultsListElm, 'input')



        S.each(this.inputs, function () {
            this.addEventListener('change', self.selectResults.bind(self))
        })

        this.hidePreloader()
    }
    selectResults(e) {

        var results = []
        var lim = this.inputLim
        var length = 0
        var symbolsNumber = 0
        var resultsFirstArr = []
        var resultsSecArr = []
        S.removeClass(this.resultItems, 'active')

        this.resultsSelectedElms = this.resultItems.filter(elm => {
            return S.first(elm, 'input:checked')
        })

        this.resultInputs = this.inputs.filter(input => {
            return input.checked
        })

        S.each(this.resultsSelectedElms, function () {
            S.addClass(this, 'active')

            results.push(S.attr(this, 'data-value'));
        });


        if (results.length == 0) {
            this.clearSelected()

            return
        }

        if (results.join('').length >= lim) {
            let tempArr = []

            results.forEach((element, i) => {
                if (i < this.resultsLength) {
                    symbolsNumber += element.length
                    resultsFirstArr.push(element)
                    tempArr.push(element)
                }
            });

            var sliceIndex = lim - symbolsNumber - 5 > 0 ? lim - symbolsNumber - 5 : 0

            if (results[this.resultsLength].length > sliceIndex) {
                var str = results[this.resultsLength].slice(0, sliceIndex)

                if (str.length > 0) {
                    resultsFirstArr.push(str + '...')
                    tempArr.push(results[this.resultsLength])
                }
            }


            resultsSecArr = results.filter(item => {
                return tempArr.indexOf(item) == -1
            })

            let num = resultsSecArr.length

            length = resultsSecArr.length == 0 ? '' : ' + ' + num
            this.input.value = resultsFirstArr.join(', ') + length

        } else {
            S.each(results, function () {

                resultsFirstArr.push(this)
            })
            this.resultsLength = resultsFirstArr.length
            this.input.value = resultsFirstArr.join(', ')
        }

        if (!this.multiple) {
            this.hideResultsElm()
        }

        this.onSelected()

    }

    clearSelected() {
        if (this.input.value = '') return

        S.each(this.resultInputs, function () {
            this.checked = false
        })
        S.each(this.resultsSelectedElms, function () {
            S.removeClass(this, 'active')

        });

        this.resultsSelectedElms = []
        this.input.value = ''


        this.onClearedSelected()
    }


    showPreloader() {
        if (this.input.value.length < this.minChars) return
        this.preloaderElm.classList.add('show')
    }

    hidePreloader() {
        this.preloaderElm.classList.remove('show')
    }

    showResultsElm() {
        console.log('opened');

        if (this.input.value.length < this.minChars && this.ajax == true) return

        if (window.innerWidth < 991) {

            var selectionOpened = new CustomEvent('selectionOpened')
            document.dispatchEvent(selectionOpened)
        }

        this.setPosition()

        this.resultsElm.classList.add('show')
        this.parent.classList.add('show')
        this.resultsShown = true
    }

    hideResultsElm() {
        if (!this.resultsShown) return
        console.log('closed');
        this.input.blur()


        this.resultsElm.classList.remove('show')
        if (this.searchInput) {
            this.searchInput.value = ''
            this.searchInput.blur()
            if (!this.ajax) {

                // after hiding animation ends
                setTimeout(() => {
                    S.trigger(this.searchInput, 'input');

                }, 300);
            }
        }


        this.parent.classList.remove('show')



        if (window.innerWidth < 991) {

            var selectionClosed = new CustomEvent('selectionClosed')
            document.dispatchEvent(selectionClosed)
        }
        this.resultsShown = false

    }

    togglePreloaderAndResults() {
        if (this.input.value.length < 3) {
            this.hidePreloader()
            this.hideResultsElm()
        } else {
            this.showPreloader()
        }
    }

    on(method, fn, options) {
        this.input.addEventListener(method, fn.bind(this), options)
    }

    setPosition() {

        this.searchitBottomPos = this.parent.getBoundingClientRect().y + this.parent.getBoundingClientRect().height
        this.resultsElmHeight = this.resultsElm.scrollHeight
        this.isBottom = this.noResultsElm ? window.innerHeight - this.searchitBottomPos < this.noResultsElm.scrollHeight : window.innerHeight - this.searchitBottomPos < this.resultsElmHeight

        if (this.isBottom) {
            S.addClass(this.parent, 'selection--bottom')
        } else {
            S.removeClass(this.parent, 'selection--bottom')
        }

        if (window.innerWidth > 991) {
            this.resultsElmPos = this.parent.getBoundingClientRect().height

            if (this.isBottom) {
                this.resultsElm.style = `top: auto; bottom: ${this.resultsElmPos}px`
                if (!this.noResultsElm) return
                this.noResultsElm.style = `top: auto; bottom: ${this.resultsElmPos}px`

            } else {
                this.resultsElm.style = `bottom: auto; top: ${this.resultsElmPos}px`
                if (!this.noResultsElm) return
                this.noResultsElm.style = `bottom: auto; top: ${this.resultsElmPos}px`
            }

        } else {
            this.resultsElmPos = this.parent.getBoundingClientRect().y * -1

            // setTimeout(() => {
            // }, 300);
            this.resultsElm.style = `height: ${window.innerHeight}px; top: ${this.resultsElmPos}px`
            if (!this.noResultsElm) return
            this.noResultsElm.style = `height: ${(window.innerHeight - 60)}px; top: ${(this.resultsElmPos + 60)}px`
        }

    }
}


function Selection(selector, options) {
    var obj = {}
    let elms = []

    if (typeof selector === 'string') {
        elms = [].slice.call(document.querySelectorAll(selector))
    } else if (selector instanceof SS) {
        elms.push(selector[0])
    }

    elms.forEach((elm, i) => {
        obj[i] = new selection_SelectionInit(elm, options)
    })

    if (elms.length === 1) {
        return obj[0]
    }

    return obj
}




/* harmony default export */ var selection = (Selection);

initSelection()


function initSelection() {
    var simpleSelection = new Selection('.js-selection-single:not(.js-selection-ajax)', {
        templateSelector: '#template-selection-result'
    })

    var multipleSelection = new Selection('.js-selection-multiple:not(.js-selection-ajax)', {
        multiple: true,
        templateSelector: '#template-selection-result-multiple'
    })


}

// for ajax uncomment and put the code below in backend.js
// var s1 = App.Selection('.js-selection-ajax', {
//     minChars: 3,
//     preloader: true,
//     ajax: {
//         debounce: {
//             delay: 500
//         },
//         onInput: function () {
//             var self = this;
//             axios.get('../../assets/data/data.json', {
//                     body: self.searchInput.value
//                 })
//                 .then(function (response) {
//                     self.render(response.data)
//                     self.setPosition()
//                     if (response.data.length > 0) {
//                         S.removeClass(self.noResultsElm, 'show')
//                     } else {
//                         S.addClass(self.noResultsElm, 'show')
//                     }
//                 })

//         }
//     },
//     templateSelector: '#template-selection-result'
// })

window.App.Selection = Selection

S.makeGlobal('Selection', Selection)
S.makeGlobal('initSelection', initSelection)
// EXTERNAL MODULE: ./dev/components/range/nouislider/nouislider.min.js
var nouislider_min = __webpack_require__(4);
var nouislider_min_default = /*#__PURE__*/__webpack_require__.n(nouislider_min);

// CONCATENATED MODULE: ./dev/components/range/range.js


function rangeSlider(range, obj) {

    var parent = range
    var minValueInput = parent.querySelector('.range__min-val')
    var maxValueInput = parent.querySelector('.range__max-val')
    var sliderElm = parent.querySelector('.range__slider')
    var sliderType = obj.type
    var step = +(obj.step)
    var minValue = +(obj.min)
    var maxValue = +(obj.max)
    var slider
    var initialStartValue = +(obj.start)
    var initialEndValue

    if (obj.end == null || undefined) {
        initialEndValue = null
    } else {
        initialEndValue = +(obj.end)
    }

    if (sliderType == 'double') {
        slider = nouislider_min_default.a.create(sliderElm, {
            start: [initialStartValue, initialEndValue],
            connect: true,
            step: step,
            range: {
                'min': minValue,
                'max': maxValue
            }
        });



        slider.on('update', function (values, handle) {

            var leftV = +(values[0])
            var rightV = +(values[1])
            minValueInput.value = leftV.toFixed()
            maxValueInput.value = rightV.toFixed()

        });

        slider.on('slide', function (values, handle) {
            if (handle == 0) {
                minValueInput.classList.add('focus')
                maxValueInput.classList.remove('focus')
            } else if (handle == 1) {
                maxValueInput.classList.add('focus')
                minValueInput.classList.remove('focus')
            }
        })

        minValueInput.addEventListener('change', function () {
            slider.set([this.value, maxValueInput.value])
        })

        maxValueInput.addEventListener('change', function () {
            slider.set([minValueInput.value, this.value])
        })

    } else if (sliderType == 'single') {
        slider = nouislider_min_default.a.create(sliderElm, {
            start: initialStartValue,
            step: step,
            range: {
                'min': minValue,
                'max': maxValue
            }
        });

        minValueInput.value = minValue.toFixed();

        slider.on('update', function () {

            var leftV = +(slider.get())
            minValueInput.value = leftV.toFixed()

        });

        slider.on('change', function () {


            if (minValueInput.value != minValue) {
                minValueInput.classList.add('active')
            } else {
                minValueInput.classList.remove('active')
            }
        })

        minValueInput.addEventListener('change', function () {
            slider.set([this.value])
        })

        slider.on('slide', function (values, handle) {
            if (handle == 0) {
                minValueInput.classList.add('focus')

            } else if (handle == 1) {

                minValueInput.classList.remove('focus')
            }
        })
    }

    return slider

}

/* harmony default export */ var range_range = (rangeSlider);
// CONCATENATED MODULE: ./dev/components/filters/filters.js


// import Acc from '../accordion/accordion'

var pscroll;

let filtersSection;
let filtersSectionPosition;

initFilters();

function initFilters() {
	let windowHeight = window.innerHeight;

	let filter = S.first('.js-filters');
	let filterFixedWrap = S.first('.filters__fixed-wrap');
	let filterOpenBtn = S.first('.js-filters__btn-open');

	// let accordionItems = S.find(filter, '.accordion__item')

	let activeAccordionItems = S.find(filter, '.accordion__item.active');

	if (filter) {
		if (activeAccordionItems.length > 0) {
			initAllScrolls(filter);
		}

		initRange(filter);

		// bodyOnClick - from utils
		S.bodyOnClick('.js-filters__btn-open', function() {
			openFilters(filter, filterFixedWrap);
		});
		S.bodyOnClick(
			'.js-filters__btn-close, .js-filter-bg, .filters__form-btn',
			function() {
				closeFilters(filter, filterFixedWrap);
			}
		);
		// S.bodyOnClick('.js-filter-bg', function () {
		//     closeFilters(filter)
		// })

		// S.bodyOnClick('.filters__form-btn', function () {

		//     closeFilters(filter)
		// })

		filtersSection = S.closest(filter, 'section');

		hideShowOpenBtn(filterOpenBtn, windowHeight);
		document.addEventListener(
			'scroll',
			throttle(function() {
				hideShowOpenBtn(filterOpenBtn, windowHeight);
			}, 100)
		);

		if (document.body.clientWidth < 1200) {
			// var accordion = S.first(filter, '.accordion')
			// accordion.click()
			// accordionItems = S.find(filter, '.accordion__item').slice(0)
			// S.each(accordionItems, function () {
			//     if (this.classList.contains('active')) {
			//         var index = S.index(this)
			//         S.trigger(accordion, 'click', {
			//             detail: {
			//                 itemIndex: index
			//             }
			//         })
			//     };
			// })
			// console.log(accordionItems);
			// S.each(accordionItems, function () {
			//     if (this.classList.contains('active')) {
			//         var index = S.index(this)
			//         Acc.prototype.toggleItem(index)
			//     };
			// })
			// accordionItems = S.find(filter, '.accordion__item').slice(2).map(el => {
			//     return {
			//         $el: el,
			//         $content: S.first(el, '.accordion__content'),
			//         isOpen: S.hasClass(el, 'active')
			//     }
			// })
			// S.each(accordionItems, function () {
			//     this.isOpen = false
			//     console.log(this);
			//     S.slideCloseCss(this.$content)
			//     S.removeClass(this.$el, 'active')
			// })
			// destroyAllScrolls()
		}
	}
}

function initRange(filter) {
	var productsPriceRange = S.first(filter, '.range');
	var inputs = S.find(productsPriceRange, 'input');

	if (productsPriceRange) {
		// only for one range, for few ranges make loop
		var slider = range_range(productsPriceRange, {
			type: productsPriceRange.dataset.type,
			step: productsPriceRange.dataset.step,
			min: productsPriceRange.dataset.min,
			max: productsPriceRange.dataset.max,
			start: productsPriceRange.dataset.start,
			end: productsPriceRange.dataset.end
		});

		slider.on('change', function() {
			let values = slider.get();
			var sliderChanged = new CustomEvent('sliderChanged', {
				detail: {
					data: values
				}
			});

			document.dispatchEvent(sliderChanged);
		});

		S.each(inputs, function() {
			S.on(this, 'change', function() {
				let values = slider.get();
				var sliderChanged = new CustomEvent('sliderChanged', {
					detail: {
						data: values
					}
				});

				document.dispatchEvent(sliderChanged);
			});
		});

		S.bodyOnClick('.js-filters-reset-btn', function() {
			if (window.innerWidth > 1200) {
				S.scrollTo(window, {
					offset: 0,
					behavior: 'smooth'
				});
			} else {
				S.scrollTo(filter, {
					offset: 0,
					behavior: 'smooth'
				});
			}

			slider.set([0, 0]);
		});
	}
}

function openFilters(filter, filterFixedWrap) {
	S.addClass(filter, 'open');
	S.addClass(filterFixedWrap, 'open');
	var evt = new CustomEvent('filter-opened');
	document.dispatchEvent(evt);
}

function closeFilters(filter, filterFixedWrap) {
	S.removeClass(filter, 'open');
	S.removeClass(filterFixedWrap, 'open');
	var evt = new CustomEvent('filter-closed');
	document.dispatchEvent(evt);
}

function hideShowOpenBtn(btn, windowHeight) {
	filtersSectionPosition =
		filtersSection.getBoundingClientRect().y +
		filtersSection.getBoundingClientRect().height;

	if (filtersSectionPosition <= windowHeight - 100) {
		S.addClass(btn, 'hide');
	} else {
		S.removeClass(btn, 'hide');
	}
}

function initAllScrolls(filter) {
	if (document.body.clientWidth > 1200) {
		var scrollElements = S.find(filter, '.js-filters-list'); //- add this class to list of filter items

		S.each(scrollElements, function() {
			initScrollBar(this);
		});
	}
}

S.makeGlobal('initFilters', initFilters);

function initScrollBar(elm) {
	if (elm.matches('.ps')) {
		return;
	}

	console.log(elm.scrollHeight);

	pscroll = new ps(elm, {
		maxScrollbarLength: 50,
		wheelSpeed: 0.1,
		swipeEasing: true,
		wheelPropagation: true
	});
	elm.ps = pscroll;
}

// set active color to color filter
var colorBoxes = S.find('.js-checkbox-color');

setActiveColor(colorBoxes);

function setActiveColor(arr) {
	arr.forEach(elm => {
		var label = S.first(elm, '.form__wrapper');
		var bgColor = label.style.backgroundColor;
		var color = bgColor.substring(4, bgColor.length - 1).split(', ');
		setActiveClass(color, label);
	});
}

function setActiveClass(color, elm) {
	var colorIndex = Math.round(
		(parseInt(color[0]) * 299 +
			parseInt(color[1]) * 587 +
			parseInt(color[2]) * 114) /
			1000
	);
	var activeClassStr = colorIndex > 125 ? '' : '--white';
	S.addClass(elm, `active${activeClassStr}`);
}

S.makeGlobal('setActiveColor', setActiveColor);

// EXTERNAL MODULE: ./dev/components/preview-card/preview-card.js
var preview_card = __webpack_require__(29);

// EXTERNAL MODULE: ./dev/components/toggle-hidden/toggle-hidden.js
var toggle_hidden = __webpack_require__(30);

// EXTERNAL MODULE: ./dev/components/testimonial/testimonial.js
var testimonial = __webpack_require__(31);

// EXTERNAL MODULE: ./dev/components/avatar/avatar.js
var avatar = __webpack_require__(32);

// EXTERNAL MODULE: ./dev/components/products-filters/products-filters.js
var products_filters = __webpack_require__(33);

// CONCATENATED MODULE: ./dev/pages/lib/lib.js
// CSS
// import './lib.scss';

// import '../../components/form/form';
// import '../../components/accordion/accordion';
// import '../../components/scroll-x/scroll-x'
// import '../../components/tab/tab'

// import Swiper from '../../components/swiper/swiper';


// JS
var btnCopy = document.querySelectorAll('.code-copy');
btnCopy.forEach(function (btn) {
	btn.addEventListener('click', copyOnClick);
});

function copyOnClick() {
	var mixinCont = this.closest('.lib__actions').querySelector('pre');
	console.log(mixinCont);
	window.getSelection().selectAllChildren(mixinCont);
	document.execCommand('copy');
}

// Swiper init
var libSwiper1 = new Swiper('#lib-swiper-1', {
	autoHeight: true,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev'
	},
	pagination: {
		el: '.swiper-pagination',
		type: 'bullets',
		clickable: true
	}
});



///  Components

////  Template component example

let dataCont = S.first('.template__default-data')
let lib_template = S.first('#template-default')
let resCont = S.first('.example-list')

if (dataCont) {

	template(lib_template, dataCont, resCont)
}
// EXTERNAL MODULE: ./dev/pages/index/index.js
var index = __webpack_require__(34);

// EXTERNAL MODULE: ./dev/pages/404/404.js
var _404 = __webpack_require__(35);

// EXTERNAL MODULE: ./dev/pages/no-ie/no-ie.js
var no_ie = __webpack_require__(36);

// EXTERNAL MODULE: ./dev/pages/article/article.js
var article = __webpack_require__(37);

// EXTERNAL MODULE: ./dev/pages/news/news.js
var news = __webpack_require__(38);

// EXTERNAL MODULE: ./dev/pages/ui/ui.js
var ui = __webpack_require__(39);

// EXTERNAL MODULE: ./dev/pages/tab-test/tab-test.js
var tab_test = __webpack_require__(40);

// EXTERNAL MODULE: ./dev/pages/products/products.js
var products = __webpack_require__(41);

// EXTERNAL MODULE: ./dev/pages/product/product.js
var product = __webpack_require__(42);

// EXTERNAL MODULE: ./dev/pages/contacts/contacts.js
var contacts = __webpack_require__(43);

// EXTERNAL MODULE: ./dev/pages/faq/faq.js
var faq = __webpack_require__(44);

// EXTERNAL MODULE: ./dev/pages/about/about.js
var about = __webpack_require__(45);

// EXTERNAL MODULE: ./dev/sections/sidebar-menu/sidebar-menu.js
var sidebar_menu = __webpack_require__(46);

// EXTERNAL MODULE: ./dev/sections/products-main__list/products-main__list.js
var products_main_list = __webpack_require__(47);

// EXTERNAL MODULE: ./dev/sections/info/info.js
var info = __webpack_require__(48);

// CONCATENATED MODULE: ./dev/js/main.js


// Components


















// import '../components/searchit/searchit';

// import '../components/searchit2/searchit2';









// End: Components

// Pages













// End: Pages

// Sections
// import '../sections/footer/footer'



// End: Sections

var body = S.first('body');

// document.addEventListener('contentOpen', function () {
//     console.log('contentOpen');

//     S.addClass(body, 'overflow-hidden')
// })

// document.addEventListener('contentClose', function () {
//     console.log('contentClose');

//     S.removeClass(body, 'overflow-hidden')
// })

// document.addEventListener('navOpen', function () {
//     console.log('navOpen');

//     S.addClass(body, 'overflow-hidden')
// })

// document.addEventListener('navClose', function () {
//     console.log('navClose');

//     S.removeClass(body, 'overflow-hidden')
// })

document.addEventListener('filter-opened', function() {
	console.log('filter-opened');

	S.addClass(body, 'overflow-hidden');
});

document.addEventListener('filter-closed', function() {
	console.log('filter-closed');

	S.removeClass(body, 'overflow-hidden');
});

function initSortBtns() {
	// toggle active sort btn
	S.bodyOnClick('.sort-btn', function() {
		S.removeClass(S.siblings(this), 'active');
		S.addClass(this, 'active');
	});
}

function initPreviewModalSlider() {
	let previewSwipers = S.find('.js-product-preview-slider');

	if (previewSwipers.length > 0) {
		S.each(previewSwipers, function() {
			let nav = {
				prevEl: S.first(
					S.closest(this, '.product-preview'),
					'.product-preview__slider-btn-prev'
				),
				nextEl: S.first(
					S.closest(this, '.product-preview'),
					'.product-preview__slider-btn-next'
				)
			};
			new Swiper(this, {
				navigation: nav
			});
		});
	}
}

initPreviewModalSlider();

initSortBtns();

var slideUpMultiple = {
	interval: 500,
	afterReveal(el) {
		el.classList.add('sr-show');
	}
};

scrollreveal_default()().reveal('.sr-up-multiple', slideUpMultiple);

S.makeGlobal('initSortBtns', initSortBtns);
S.makeGlobal('initPreviewModalSlider', initPreviewModalSlider);

let staticGallery = S.first('.js-static-gallery');
if (staticGallery) {
	new Swiper(staticGallery, {
		slidesPerView: 3,
		spaceBetween: 16,
		navigation: {
			prevEl: '.static__gallery-btn-prev',
			nextEl: '.static__gallery-btn-next'
		},
		breakpoints: {
			768: {
				slidesPerView: 2
			},
			580: {
				slidesPerView: 1
			}
		}
	});
}

S.bodyOnClick('[data-slide-change]', function() {
	let id = '#' + this.dataset.slideChange;
	let modal = S.first(this.dataset.modal);
	let swiper = S.first(modal, '.swiper-container');
	let slide = S.first(swiper, id);
	let swiperChildren = swiper.swiper.slides;
	let slides = [];
	for (let index = 0; index < swiperChildren.length; index++) {
		slides.push(swiperChildren[index]);
	}

	let index = slides.indexOf(slide);

	swiper.swiper.slideTo(index);
});

var resetBtn = S.find('.js-form-reset-btn');
// S.on(resetBtn, 'click', clearInputs)
S.each(resetBtn, function() {
	var form = S.first(S.closest(this, '.filters'), 'form');
	var checkboxes = S.find(form, 'input[type="checkbox"');

	this.addEventListener('click', function() {
		S.each(checkboxes, function() {
			S.attr(this, 'checked', null);
		});
		form.reset();
	});
});

function initScrollOnProductLinks() {
	S.each('.product-info__details-img-wrapper .swiper-container', function() {
		var swiperScrollBar = S.first(this, '.swiper-scrollbar');
		var scrollPs = new Swiper(this, {
			slidesPerView: 'auto',
			scrollbar: {
				el: swiperScrollBar,
				draggable: true
			},
			freeMode: true,
			watchOverflow: true
		});
	});
}

// initScrollOnProductLinks();
S.makeGlobal('initScrollOnProductLinks', initScrollOnProductLinks);

document.fonts.ready.then(function() {
	S.addClass('.preloader', 'shown');
	setTimeout(() => {
		S.remove('.preloader');
	}, 300);
});


/***/ })
/******/ ]);