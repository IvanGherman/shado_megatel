<?php

return [

    //Cart page
    'text_heading' => 'Корзина',
    'text_empty_cart' => 'Ваша корзина пуста',
    'text_count' => 'Количество товаров в корзине:',
    'text_total' => 'Сумма:',
    'text_checkout' => 'Подтверждение заказа',
    'text_error_remove_product' => 'Ошибка! Попробуйте обновить страницу.',

    //Checkout page
    'text_heading_checkout' => 'Оформление заказа'

];
