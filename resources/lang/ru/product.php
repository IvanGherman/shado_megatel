<?php

return [
    'text_all' => 'Все',
    'text_price' => 'Цена',
    'text_currency' => 'Валюта',
    'text_quantity' => 'Количество',
    'text_checkout' => 'Купить',
    'text_add_to_cart' => 'Добавить в корзину',
    'text_read_more' => 'Читать больше',
    'text_no_products' => 'Нет товаров в этой категории',
    'text_attention' => 'Внимание!',
    'text_recommandation_use' => 'Рекомендации по применению',
    'text_product_added_to_cart' => 'Товар добавлен в корзину'
];
