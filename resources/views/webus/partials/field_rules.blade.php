@if($item['html_type'] == 'text')
    @include('webus.fields.text')
@endif

@if($item['html_type'] == 'textarea')
    @include('webus.fields.textarea')
@endif

@if($item['html_type'] == 'body')
    @include('webus.fields.body')
@endif

@if($item['html_type'] == 'password')
    @include('webus.fields.password')
@endif

@if($item['html_type'] == 'select')
    @include('webus.fields.select',['options' => $item['options']])
@endif

@if($item['html_type'] == 'select_no_role')
    @include('webus.fields.select_no_role',['options' => $item['options']])
@endif

@if($item['html_type'] == 'select_taxonomy')
    @include('webus.fields.select',['options' => $item['options']])
@endif

@if($item['html_type'] == 'hidden')
    @include('webus.fields.hidden')
@endif

@if($item['html_type'] == 'schedule')
    @include('webus.fields.schedule')
@endif

@if($item['html_type'] == 'file')
    @include('webus.fields.file')
@endif

@if($item['html_type'] == 'file_input')
    @include('webus.fields.file_input')
@endif

@if($item['html_type'] == 'image_input')
    @include('webus.fields.image_input')
@endif

@if($item['html_type'] == 'gallery_repeatable')
    @include('webus.fields.gallery_repeatable')
@endif

@if($item['html_type'] == 'gallery')
    @include('webus.fields.gallery')
@endif

@if($item['html_type'] == 'repeatable')
    @include('webus.fields.repeatable')
@endif

@if($item['html_type'] == 'checkbox')
    @include('webus.fields.checkbox')
@endif

@if($item['html_type'] == 'options')
    @include('webus.fields.options')
@endif
