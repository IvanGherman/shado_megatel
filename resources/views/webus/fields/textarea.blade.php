@if(isset($name_option))
        <textarea class="col-md-12 form-control {{ $classes }}"
                  name="{{ $key }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $name_option }}]"
                  id="{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>"
                  @if($item['validate']) required @endif><?php if(isset($model[$key]) && !empty($model[$key])) {if(isset($repeatable->$name_option)){echo htmlentities($repeatable->$name_option);}} ?></textarea>

@else
        <textarea
                @if(isset($item['metabox'])) name="metabox[{{ $key }}]" @else name="{{ $key }}" @endif
        placeholder="{{ $item['title'] }}" id="{{ $key }}" class="{{ $classes }} form-control" @if(isset($item['rows'])) rows="{{$item['rows']}}" @endif @if($item['validate']) required @endif>@if(isset($model) && !empty($model[$key]) && $item['translatable'] == false){{ $model[$key] }}@else{{''}}@if(isset($item['default'])){{ $item['default'] }}@endif{{''}}@endif</textarea>
@endif
