@if (!empty($model))
    @php $schedule = unserialize($model->$key); @endphp
    @foreach($schedule as $day => $item)
        <div class="row schedule-day">
            <div class="heading_field plus">{{ $day }}</div>
            @foreach($item as $key => $time)
            <div class="col-md-6">
                <label>
                    {{ $key }}
                </label>
                <input name="schedule[{{ $day }}][{{ $key }}]" class="form-control timepicker" value="{{ $time }}">
            </div>
            @endforeach
        </div>
    @endforeach
@else
    @php $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']; @endphp
    @foreach($days as $day)
        <div class="row">
            <div>{{ $day }}</div>
            <div class="col-md-6">
                <label>
                    from
                </label>
                <input name="schedule[{{ $day }}][from]" class="form-control timepicker">
            </div>
            <div class="col-md-6">
                <label>
                    to
                </label>
                <input name="schedule[{{ $day }}][to]" class="form-control timepicker">
            </div>
        </div>
    @endforeach
@endif
<script src="{{ asset('webus/admin_assets/js/timepicker.js') }}"></script>
<script>
    $('.timepicker').timepicker({
        timeFormat: 'HH:mm',
        interval: 60,
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
</script>

