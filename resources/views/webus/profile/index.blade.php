@extends('layouts.webus')

@section('content')

    <h1 class="page-title">
        <i class="voyager-person"></i> Edit profile
    </h1>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form"
                          action="{{ route('update_profile') }}"
                          method="POST" enctype="multipart/form-data">

                        <!-- PUT Method if we are editing -->
                        {{ method_field("POST") }}

                        <!-- CSRF TOKEN -->
                       {{csrf_field()}}

                        <div class="panel-body">


                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name"
                                       placeholder="Name" id="name"
                                       value="{{Auth::user()->name}}">
                            </div>

                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="text" class="form-control" name="email"
                                       placeholder="Email" id="email"
                                       value="{{Auth::user()->email}}" disabled>
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <br>
                                <small>Leave empty to keep the same</small>
                                <input type="password" class="form-control" name="password"
                                       placeholder="Password" id="password">
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="password">Avatar</label>--}}
                                {{--<img src="http://localhost:1995/storage/users/March2017/1zSt7R7FJX6ULVG0rgkd.png"--}}
                                     {{--style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">--}}
                                {{--<input type="file" name="avatar">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="role">User Role</label>--}}
                                {{--<select name="role_id" id="role" class="form-control">--}}
                                    {{--<option value="1"  @if(Auth::user()->hasRole('admin'))selected @endif >Administrator</option>--}}
                                    {{--<option value="2" @if(Auth::user()->hasRole('user'))selected @endif>Normal User</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}



                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Update profile</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>

@endsection
