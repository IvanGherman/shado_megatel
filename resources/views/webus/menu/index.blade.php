@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-list-add"></i> {{ $data['title'] }}
        <a href="{{ $data['url_create'] }}" class="btn btn-success">
            <i class="voyager-plus"></i> Add New
        </a>
    </h1>

    <div class="page-content with_padding">

        <table class="row table table-hover webus-table">

            <thead>
            <tr>
                @foreach($data['rows'] as $row)
                    <th>{{ $row }}</th>
                @endforeach
                <th class="actions">Actions</th>
            </tr>

            </thead>

            <tbody>
            @foreach($data['list'] as $item)
                <tr>

                    @foreach($data['rows'] as $key=>$row)
                        <td>
                            @if($row == 'translatable')
                                {{ \App\Http\Helpers\webus_help::translate_automat($item->$key) }}
                            @else
                                {{ $item->$row }}
                            @endif

                        </td>
                    @endforeach

                    <td class="no-sort no-click" id="bread-actions">
                        <form action="{{ url($data['url_delete'].$item->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?');">
                            {{ method_field("POST") }}
                            {{ csrf_field() }}
                            <button title="Delete" class="btn btn-sm btn-danger pull-right delete">
                                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                            </button>
                        </form>

                        <a href="{{ url($data['url_edit'].$item->id) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                        </a>

                        <a href="{{ url($data['url_build'].$item->id) }}" title="Build" class="btn btn-sm btn-warning pull-right">
                            <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">Build</span>
                        </a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $data['list']->links() }}

    </div>
@endsection
