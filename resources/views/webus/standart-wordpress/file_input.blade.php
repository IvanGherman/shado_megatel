<input id="input_{{ $key }}" type="text" name="{{ $key }}" value="@if(isset($model) && !empty($data['thumbnail'])){{config('app.url').$data['thumbnail'] }}@endif" class="form-control" @if($item['validate']) required @endif>
<button type="button" class="input_image btn btn-dark" data-input="input_{{ $key }}" data-preview="preview_{{ $key }}">Select file</button>
@if(isset($model) && !empty($data['thumbnail']))
    <a class="btn-animate btn btn-success" href="{{ $data['thumbnail'] }}" download>Download file</a>
@endif
