<select name="{{ $key }}" placeholder="{{ $item['title'] }}" id="{{ $key }}" class="form-control" @if($item['validate']) required @endif>
    @if($options)
        @foreach($options as $key_option=>$option)
            <option value="{{ $key_option }}" @if(isset($data['page_template']) && $data['page_template'] == $key_option) selected @endif>{{ $option }}</option>
        @endforeach
    @endif
</select>

