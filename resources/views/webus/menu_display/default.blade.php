@if($items)
    <ol class="dd-list">

        @foreach($items->sortBy('item_order') as $item)

            <li class="dd-item" data-id="{{ $item->id }}">
                <div class="pull-right item_actions">
                    <div class="btn-sm btn-danger pull-right delete" data-id="{{ $item->id }}">
                        <i class="voyager-trash"></i> Delete
                    </div>
                    <div class="btn-sm btn-primary pull-right edit"
                         data-id="{{ $item->id }}"
                         data-parent_id="{{ $item->parent_id }}"
                         data-title="{{ $item->name }}"
                         @if($translatable)
                         @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                         data-lang_{{$localeCode}}="{{\App\Http\Helpers\webus_help::translate_manual($localeCode, $item->name)}}"
                         @endforeach
                         @endif
                         data-url="{{ $item->url }}"
                         data-target="{{ $item->target }}"
                         data-icon_class="{{ $item->icon }}"
                         data-class="{{ $item->class }}" >
                        <i class="voyager-edit"></i> Edit
                    </div>
                </div>

                <div class="dd-handle">
                    <span>{{ $item->name }}</span> <small class="url">{{ $item->url }}</small>
                </div>



                @if(!$item->children->isEmpty())
                    @include('webus/menu_display.'.$template, ['items' => $item->children, 'template' => $template])
                @endif



            </li>
        @endforeach



    </ol>
@endif
