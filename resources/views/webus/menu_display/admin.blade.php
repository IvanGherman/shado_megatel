@if($items)
<ul class="nav navbar-nav">
    @foreach($items->sortBy('item_order') as $item)
    <li @if(!$item->children->isEmpty())class="dropdown"@endif>
        <a @if(!$item->children->isEmpty())href="#{{ $item['id'] }}-dropdown-element" data-toggle="collapse" aria-expanded="false" @else href="@if(!empty($item['url'])){{ url($item['url']) }}@endif" @endif target="{{ $item['target'] }}">
            <span class="icon {{ $item['icon'] }}"></span>
            <span class="title">{{ $item['name'] }}</span>
        </a>
        @if(!$item->children->isEmpty())
        <div id="{{ $item['id'] }}-dropdown-element" class="panel-collapse collapse ">
            <div class="panel-body">
                @include('webus/menu_display.'.$template, ['items' => $item->children, 'template' => $template])
            </div>
        </div>
        @endif
    </li>
    @endforeach
</ul>
@endif
