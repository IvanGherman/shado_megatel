<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>

    <title>@if(isset($data['title'])){{ $data['title'] }} | {{ config('app.admin_name', 'Webus Admin') }}@else{{ config('app.admin_name', 'Webus Admin') }}@endif</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('webus/partials.head')
            <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

        var path_absolute = '{{ route('dashboard') }}';
        var site_url = '{{ url('/') }}';

    </script>
</head>

<body class="flat-blue">

<div id="voyager-loader">
    <img src="/webus/admin_assets/images/logo-icon.png" alt="WebusAdmin Loader">
</div>


<div class="app-container">
    <div class="fadetoblack visible-xs"></div>
    <div class="row content-container">

        @include('webus/partials.nav')
        @include('webus/partials.sidebar')

                <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">
                @yield('content')
                        <!-- .page-content container-fluid -->

            </div>
        </div>

    </div>
</div>
</div>

@include('webus/partials.footer')

</body>
</html>
