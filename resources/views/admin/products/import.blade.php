@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-@if(!empty($data['icon'])){{ $data['icon'] }}@else{{ 'list-add' }}@endif"></i> {{ $data['title'] }}
        @if(!empty($data['url_create']))
            <a href="{{ $data['url_create'] }}" class="btn btn-success">
                <i class="voyager-plus"></i> Add New
            </a>
        @endif
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message_errors'))
                    @php  $messages = \Illuminate\Support\Facades\Session::get('flash_message_errors') @endphp
                    @if (is_array($messages))
                        @foreach($messages as $message)
                            <div class="alert alert-danger">{!! $message !!}</div>
                        @endforeach
                    @else
                        <div class="alert alert-danger">{!! $message !!}</div>
                    @endif
                @endif

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" id="import-products" action="{{ $data['form_action'] }}" method="POST" enctype="multipart/form-data">
                        {{ method_field("POST") }}
                        {{ csrf_field() }}
                        <input type="file" name="products">
                        <br/>
                        <label>
                            <input type="checkbox" name="clear">Clear
                        </label>

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div
                        id="import-results"
                        style="height: 300px; overflow: scroll"
                        class="panel panel-bordered">
                    <div class="alert alert-success msg__" id="started_" style="display: none">Started</div>
                    <div class="alert alert-success msg__" id="success_" style="display: none"></div>
                    <div class="alert alert-danger msg__" id="failure_" style="display: none"></div>
                    <div class="alert alert-success msg__" id="finished_" style="display: none">Finished</div>
                </div>
            </div>
        </div>

    </div>
    <script>
        var saved = 0;
        var skipped = 0;
        var total = 0;
        var offset = 0;
        var limit = 5;

        var perPage = 5;

        $('#import-products').on('submit', function (e) {
            e.preventDefault();
            $(this).find('button').attr('disabled', 'disabled');
            save(limit, offset, saved, skipped, 0);
        });

        function save(currentLimit, currentOffset, saved, skipped, noOptions) {
            var el = $('#import-products')
            var data = new FormData(el[0]);
            data.append('limit', currentLimit);
            data.append('offset', currentOffset);
            data.append('noOptions', noOptions);
            $('#started_').show();
            $.ajax({
                type: "POST",
                url: '{{ $data['form_action'] }}',
                data: data,
                success: function (data) {
                    skipped += +data.errors;
                    saved += +data.saved;
                    total = data.total;

                    var messageContainer = $('#import-results');

                    if (data.hasOwnProperty('error')) {
                        var failure = $('<div></div>').addClass('alert alert-danger msg__').html('Error: ' + data.error);
                        messageContainer.append(failure);
                    } else {
                        $('#success_').html('Total saved: ' + saved).show();
                        $('#failure_').html('Total skipped: ' + skipped).show();

                        if (data.hasOwnProperty('errors_detailed') && data.errors_detailed.length > 0) {
                            for (var i = 0; i < data.errors_detailed.length; i++) {
                                var failure = $('<div></div>').addClass('alert alert-danger msg__').html(data.errors_detailed[i]);
                                messageContainer.append(failure);
                            }
                        }

                        if (total > skipped + saved) {
                            var diff = total - skipped - saved;
                            if (diff < currentLimit) {
                                currentLimit = diff - currentLimit;
                            }
                            save(currentLimit, currentOffset + perPage, saved, skipped, 1);
                        } else {
                            $('#finished_').show();
                        }
                    }
                },
                processData: false,
                contentType: false
            });
        }
    </script>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/nestable.css">
    <style>
        .msg__{
            margin: 0px!important;
            padding: 5px!important;
        }
    </style>
@endsection
@section('javascript')
    <script src="/webus/admin_assets/lib/js/tinymce/tinymce.min.js"></script>
    <script src="/webus/admin_assets/js/voyager_tinymce.js"></script>
    <script type="text/javascript" src="/webus/admin_assets/js/jquery.nestable.js"></script>
    @stack('scripts')
    @stack('gallery_field')
@endsection

