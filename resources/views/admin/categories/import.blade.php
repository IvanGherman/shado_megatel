@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-@if(!empty($data['icon'])){{ $data['icon'] }}@else{{ 'list-add' }}@endif"></i> {{ $data['title'] }}
        @if(!empty($data['url_create']))
            <a href="{{ $data['url_create'] }}" class="btn btn-success">
                <i class="voyager-plus"></i> Add New
            </a>
        @endif
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success msg__">{!! session('flash_message') !!}</div>
                @endif

                @if(Session::has('flash_message_errors'))
                    @php $messages = \Illuminate\Support\Facades\Session::get('flash_message_errors') @endphp
                    @if ($messages && is_array($messages))
                        @foreach($messages as $message)
                            <div class="alert alert-danger msg__">{!! $message !!}</div>
                        @endforeach
                    @endif
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" action="{{ $data['form_action'] }}" method="POST" enctype="multipart/form-data">
                        {{ method_field("POST") }}
                        {{ csrf_field() }}
                        <input type="file" name="categories">
                        <br/>
                        <label>
                            <input type="checkbox" name="clear">Clear
                        </label>

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/nestable.css">
    <style>
        .msg__{
            margin: 0px!important;
            padding: 5px!important;
        }
    </style>
@endsection
@section('javascript')
    <script src="/webus/admin_assets/lib/js/tinymce/tinymce.min.js"></script>
    <script src="/webus/admin_assets/js/voyager_tinymce.js"></script>
    <script type="text/javascript" src="/webus/admin_assets/js/jquery.nestable.js"></script>
    @stack('scripts')
    @stack('gallery_field')
@endsection

