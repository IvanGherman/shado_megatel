@extends('layouts.front')
@section('content')
    @php
        $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
        $variables = json_decode($variables);

        $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
        $translations = json_decode($translations);

        $locale = Config::get('app.locale');
    @endphp
    <ul class="breadscrumbs-list scroll-x" itemscope="itemscope" itemtype="http://schema.org/BreadcrumbList">
        <li class="breadscrumbs-item scroll-x__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="{{ route('home') }}">Главная<span class="visually-hidden" itemprop="name">Главная</span>
                <meta itemprop="position" content="1"/></a>
        </li>
        <li class="breadscrumbs-item scroll-x__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="{{ route('product_list') }}">Каталог<span class="visually-hidden" itemprop="name">Каталог</span>
                <meta itemprop="position" content="2"/></a>
        </li>
        <li class="breadscrumbs-item scroll-x__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
            <a class="visually-hidden" itemprop="item" href="#">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->title) !!}</a><span itemprop="name">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->title) !!}</span>
            <meta itemprop="position" content="3"/>
        </li>
    </ul>
    <section class="product">
        <div class="product-info">
            <div class="product-info__first">
                <div class="container">
                    <div class="product-info__wrapper dflex jcsb ais">
                        <div class="product-info__details-container hidden-lg-up">
                            <div class="product-info__details-header">
                                <div class="product-info__details-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->title) !!}</div>
                                @if ($product->title)
                                    <div class="product-info__details-label">
                                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->product_code) !!}</span>
                                        <span class="product-info__details-label-text">{{ $product->code }}</span>
                                    </div>
                                @endif
                            </div>
                            @include('front.partial.price', ['item' => $product])
                        </div>
                        <div class="product-info__img-container dflex ais">
                            <div class="product-info__thumbs-wrapper">
                                <div class="swiper-container product-info-slider-thumbs">
                                    <div class="swiper-wrapper">
                                        @php
                                            $images = json_decode($product->images);
                                        @endphp
                                        @if (!empty($images))
                                            @foreach($images as $image)
                                                @if ($image->image)
                                                    <div class="swiper-slide">
                                                        <div class="product-info__img-thumb-slide">
                                                            <img class="product-info__img"
                                                                 src="{{ \App\Product::getAssetOrDefault($image->image) }}"
                                                                 data-src="{{ \App\Product::getAssetOrDefault($image->image) }}"
                                                                 data-zoom="{{ \App\Product::getAssetOrDefault($image->image) }}"/>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                        {{--<div class="swiper-slide">--}}
                                            {{--<div class="product-info__img-thumb-slide product-info__img-thumb-slide--video-btn"--}}
                                                 {{--data-modal="#product-video-modal" data-video-src="https://www.youtube.com/embed/m9kvbpZhwq4">                               <img src="assets/img/customer-img/product-img-2.jpg"/>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="swiper-pagination"></div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-button-next"></div>
                                </div>
                                <div class="product-info__thumbs-btns">
                                    <button class="btn btn-icon-only btn--link product-info__thumbs-btn-prev btn--prev">
                                        <svg class="icon stroke-current" width="24" height="24">
                                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                        </svg>
                                    </button>
                                    <button class="btn btn-icon-only btn--link product-info__thumbs-btn-next btn--next">
                                        <svg class="icon stroke-current" width="24" height="24">
                                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="product-info__image-wrapper mr-30 hidden-lg">
                                <button class="btn btn-icon-only btn--link product-info__slider-btn-prev hidden-lg-up">
                                    <svg class="icon stroke-primary" width="12" height="12">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                    </svg>
                                </button>
                                <button class="btn btn-icon-only btn--link product-info__slider-btn-next hidden-lg-up">
                                    <svg class="icon stroke-primary" width="12" height="12">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                    </svg>
                                </button>
                                <div class="product-info-slider-main">
                                    <img class="product-info__img" src="{{ asset($product->getFirstImage()) }}"/>
                                    <div class="product-info-slider-main__pseudo-btn">
                                        <img src="{{ asset('assets/img/zoom.svg') }}" alt="" width="24" height="24"/>
                                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->zoom) !!}</span>
                                    </div>
                                </div>
                                <div class="product-info__zoom-img-container">
                                    <img class="product-info__zoom-img" src="{{ asset($product->getFirstImage()) }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="product-info__details-container hidden-lg">
                            <div class="product-info__details-header">
                                <div class="product-info__details-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->title) !!}</div>
                                <div class="product-info__details-label">
                                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->product_code) !!}</span>
                                    <span class="product-info__details-label-text">{{ $product->code }}</span>
                                </div>
                            </div>
                            @include('front.partial.price', ['item' => $product, 'no_price' => true])

                            <div class="product-info__details-item">
                                @include('front.pages.product_attributes')
                            </div>
                            @if ($by_model->count() > 0)
                            <div class="product-info__details-item">
                                <div class="product-info__details-img-wrapper">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                @foreach($by_model as $itemModel)
                                                    <a class="product-info__details-img" href="{{ url('product', $itemModel->id) }}">
                                                        <img src="{{ asset($itemModel->getModelImage()) }}" alt=""/>
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="swiper-scrollbar"></div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="product-info__details-footer">
                                <button class="btn btn--primary btn--lg" data-modal="#request-modal">
                                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->request_check) !!}</span>
                                </button>
                            </div>
                        </div>
                        <div class="product-info__details-container hidden-lg-up">
                            <div class="product-info__details-title--secondary">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->choose_color) !!}</div>
                            <div class="product-info__details-img-wrapper">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            @foreach($by_model as $itemModel)
                                                <a class="product-info__details-img" href="{{ url('product', $itemModel->id) }}">
                                                    <img src="{{ asset($itemModel->getModelImage()) }}" alt=""/>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="swiper-scrollbar"></div>
                                </div>
                            </div>
                            <div class="product-info__details-footer hidden-lg">
                                <button class="btn btn--primary btn--lg" data-modal="#request-modal">
                                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->request_check) !!}</span>
                                </button>
                            </div>
                        </div>
                        <div class="accordion hidden-lg-up">
                            <div class="accordion__item">
                                <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->attributes) !!}
                                    <div class="accordion__icon"></div>
                                </div>
                                <div class="accordion__content">
                                    <div class="some-content">
                                        <div class="product-info__details-item"></div>
                                        @include('front.pages.product_attributes')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-info__main">
                <div class="container container--medium">
                    <div class="tab-wrapper">
                        <div class="tab hidden-lg">
                            <div class="tab-btns scroll-x">
                                <button class="btn btn--white product-info__main-tab-btn tab-btns__toggle-tab  scroll-x__item  js-tab-active active"
                                        data-tab="">
                                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->about) !!}</span>
                                </button>
                                <button class="btn btn--white product-info__main-tab-btn tab-btns__toggle-tab  scroll-x__item" data-tab="">
                                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->check_install) !!}</span>
                                </button>
                            </div>
                            <ul class="tab__container">
                                <li class="tab__item js-tab-active active">
                                    <h2 class="product-info__main-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->description) !!}</h2>
                                    <div class="static">
                                        <p>
                                            {!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->description) !!}
                                        </p>
                                    </div>
                                </li>
                                <li class="tab__item">
{{--                                    <h2 class="product-info__main-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->how_to_check_jaluzi) !!}</h2>--}}
                                    <div class="static">
                                        @foreach($product->categories as $category)
                                            <p>{!! $category->getInstallation() !!}</p>
                                        @endforeach
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="accordion hidden-lg-up">
                        <div class="accordion__item">
                            <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->about) !!}
                                <div class="accordion__icon"></div>
                            </div>
                            <div class="accordion__content">
                                <div class="some-content">
                                    <h2 class="product-info__main-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->description) !!}</h2>
                                    <div class="static">
                                        <p>
                                            {!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->description) !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion__item">
                            <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->check_install) !!}
                                <div class="accordion__icon"></div>
                            </div>
                            <div class="accordion__content">
                                <div class="some-content">
                                    <div class="static">
                                        {{--                                    <h2 class="product-info__main-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->how_to_check_jaluzi) !!}</h2>--}}
                                        @foreach($product->categories as $category)
                                            <p>{!! $category->getInstallation() !!}</p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! \App\Http\Helpers\webus_help::get_modules($modules) !!}
    @if ($common and $common->count() > 0)
    @include('front.pages.product_slider', ['title' => \App\Http\Helpers\webus_help::translate_manual($locale, $translations->common), 'items' => $common, 'modal_id' => 'product-preview-modal-similar'])
    @endif
    @if ($popular and $popular->count() > 0)
    @include('front.pages.product_slider', ['title' => \App\Http\Helpers\webus_help::translate_manual($locale, $translations->popular), 'items' => $popular, 'modal_id' => 'product-preview-modal-popular'])
    @endif
@endsection

@section('modal_content')
    @if ($common)
    <div class="modal product-preview-modal hidden-xl" id="product-preview-modal-similar">
        <div class="modal__wrapper">
            <div class="modal__close">
                <svg class="icon stroke-black" width="20" height="20">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                </svg>
            </div>
            <div class="modal__content">
                <div class="product-preview">
                    <div class="swiper-container product-preview__slider js-product-preview-slider">
                        <div class="swiper-wrapper" id="jproduct-modals">
                            @include('front.pages.products_modals', ['products' => $common])
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <button class="btn btn-icon-only btn--link product-preview__slider-btn-prev btn--prev">
                        <svg class="icon stroke-current" width="52" height="52">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                    <button class="btn btn-icon-only btn--link product-preview__slider-btn-next btn--next">
                        <svg class="icon stroke-current" width="52" height="52">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if ($popular)
    <div class="modal product-preview-modal hidden-xl" id="product-preview-modal-popular">
        <div class="modal__wrapper">
            <div class="modal__close">
                <svg class="icon stroke-black" width="20" height="20">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                </svg>
            </div>
            <div class="modal__content">
                <div class="product-preview">
                    <div class="swiper-container product-preview__slider js-product-preview-slider">
                        <div class="swiper-wrapper" id="jproduct-modals">
                            @include('front.pages.products_modals', ['products' => $popular])
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <button class="btn btn-icon-only btn--link product-preview__slider-btn-prev btn--prev">
                        <svg class="icon stroke-current" width="52" height="52">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                    <button class="btn btn-icon-only btn--link product-preview__slider-btn-next btn--next">
                        <svg class="icon stroke-current" width="52" height="52">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection