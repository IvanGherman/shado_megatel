@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<section class="product-slider">
    <h2 class="title--xxl ff-secondary section-title">{{ $title }}</h2>
    <div class="product-slider__wrapper">
        <div class="product-slider__container">
            <div class="swiper-container product-slider__swiper js-product-slider">
                <div class="swiper-wrapper">
                    @foreach($items as $item)
                        @php
                            $images = json_decode($item->images);
                            $newImages = [];
                            if (!empty($images)) {
                                foreach ($images as $image) {
                                    $newImages[] = \App\Product::getAssetOrDefault($image->image);
                                }
                            }
                            $imagesImploded = implode("','", $newImages);
                        @endphp
                        <div class="swiper-slide">
                            <div class="preview-card dflex f-d-col preview-card--product hidden-xl">
                                @if (!empty($newImages))
                                    <div class="preview-card__img-slider-container bg-white br-8 dflex f-d-col">
                                        <div class="preview-card__img-thumb-slider leafer js-leafer-thumbs">
                                            <div class="leafer__wrap leafer--vertical">
                                                <div class="leafer__slide thumb"
                                                     data-select="[&quot;{{ $item->getFirstImage() }}&quot;]">
                                                    <img class="preview-card__img" src="{{ $item->getFirstImage() }}"/></div>
                                                <div class="leafer__slide thumb"></div>
                                            </div>
                                        </div>
                                        <div class="leafer__btns">
                                            <button class="btn btn-icon-only btn--nopadding btn--link preview-card__img-slider-btn-prev leafer__btn leafer__btn--prev">
                                                <svg class="icon stroke-primary" width="20" height="20">
                                                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                                </svg>
                                            </button>
                                            <button class="btn btn-icon-only btn--nopadding btn--link preview-card__img-slider-btn-next leafer__btn leafer__btn--next">
                                                <svg class="icon stroke-primary" width="20" height="20">
                                                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                @endif

                                <div class="preview-card__img-container">
                                    <a class="preview-card__slider-container" href="{{ url('product', $item->getId()) }}">
                                        <img class="preview-card__img img--main" src="{{ $item->getFirstImage() }}"/>
                                        <img class="on-top img--first invisible" alt=""/>
                                        <img class="on-top img--second invisible" alt=""/></a>
                                    <button class="btn btn--reverse btn--with-icon btn--white preview-card__modal-btn hidden-xl"
                                            data-modal="#{{ $modal_id }}" data-slide-change="G{{ $item->getId() }}">
                                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->preview) !!}</span>
                                        <svg class="icon icon--mright stroke-primary" width="24" height="24">
                                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-eye') }}"></use>
                                        </svg>
                                    </button>
                                    @if ($item->promotion)
                                        <div class="preview-card__product-label">
                                            <div class="preview-card__product-label-container br-24 bg-primary">
                                                <svg class="icon" width="12" height="12">
                                                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-percent') }}"></use>
                                                </svg>
                                                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->discount) !!}</span>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="preview-card__text-container">
                                    <div class="preview-card__img-secondary">
                                        <img class="icon" src="{{ asset($item->getModelImage()) }}" alt="" width="32" height="32"/>
                                    </div><a class="product-item__title align--left" href="{{ url('product', $item->getId()) }}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->title) !!}</a>

                                    <p class="product-item__text align--left">{!! $item->getImploded('categories', $locale) !!}</p>
                                    @include('front.partial.price', ['item' => $item])
                                </div>
                            </div>
                            <div class="preview-card dflex f-d-col preview-card--product hidden-xl-up">
                                <div class="preview-card__img-slider-container bg-white br-8 dflex f-d-col">
                                    <div class="preview-card__img-thumb-slider leafer js-leafer-thumbs">
                                        <div class="leafer__wrap leafer--vertical">
                                            <div class="leafer__slide thumb"
                                                 data-select="[&quot;{{ $item->getFirstImage() }}&quot;]">
                                                <img class="preview-card__img" src="{{ $item->getFirstImage() }}"/>
                                            </div>
                                            <div class="leafer__slide thumb"></div>
                                        </div>
                                    </div>
                                    <div class="leafer__btns">
                                        <button class="btn btn-icon-only btn--nopadding btn--link preview-card__img-slider-btn-prev leafer__btn leafer__btn--prev">
                                            <svg class="icon stroke-primary" width="20" height="20">
                                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                            </svg>
                                        </button>
                                        <button class="btn btn-icon-only btn--nopadding btn--link preview-card__img-slider-btn-next leafer__btn leafer__btn--next">
                                            <svg class="icon stroke-primary" width="20" height="20">
                                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                                <div class="preview-card__img-container">
                                    <a class="preview-card__slider-container" href="{{ url('product', $item->getId()) }}">
                                        <img class="preview-card__img img--main"
                                             src="{{ $item->getFirstImage() }}"/>
                                        <img class="on-top img--first invisible" alt=""/>
                                        <img class="on-top img--second invisible" alt=""/>
                                    </a>
                                    <button class="btn btn--reverse btn--with-icon btn--white preview-card__modal-btn hidden-xl"
                                            data-modal="#{{ $modal_id }}" data-slide-change="G{{ $item->getId() }}">
                                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->preview) !!}</span>
                                        <svg class="icon icon--mright stroke-primary" width="24" height="24">
                                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-eye') }}"></use>
                                        </svg>
                                    </button>
                                    <div class="preview-card__product-label">
                                        <div class="preview-card__product-label-container br-24 bg-primary">
                                            <svg class="icon" width="12" height="12">
                                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-percent') }}"></use>
                                            </svg>
                                            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->discount) !!}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="preview-card__text-container">
                                    <div class="preview-card__img-secondary">
                                        <img class="icon" src="{{ asset($item->getModelImage()) }}" alt="" width="32" height="32"/>
                                    </div>
                                    <a class="product-item__title align--left" href="{{ url('product', $item->getId()) }}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->title) !!}</a>

                                    <p class="product-item__text align--left">{!! $item->getImploded('categories', $locale) !!}</p>
                                    @include('front.partial.price', ['item' => $item])

                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="swiper-slide swiper-slide--empty">
                    </div>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
        <div class="product-slider__btns">
            <button class="btn btn-icon-only btn--link btn--prev product-slider__btn-prev"><span></span>
                <svg class="icon stroke-current" width="24" height="24">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                </svg>
            </button>
            <button class="btn btn-icon-only btn--link btn--next product-slider__btn-next"><span></span>
                <svg class="icon stroke-current" width="24" height="24">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                </svg>
            </button>
        </div>
    </div>
</section>