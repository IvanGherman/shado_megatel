@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
@foreach($products as $product)
    @php
        $images = json_decode($product->images);
        $newImages = [];
        if (!empty($images)) {
            foreach ($images as $image) {
                $newImages[] = \App\Product::getAssetOrDefault($image->image);
            }
        }
        $imagesImploded = implode("','", $newImages);
    @endphp
    <div class="swiper-slide product-preview__slide" id="G{{ $product->getId() }}">
        <div class="product-info__first">
            <div class="product-info__wrapper dflex jcsb ais">
                <div class="product-info__img-container dflex ais">
                    <div class="product-info__thumbs-wrapper">
                        <div class="swiper-container product-info-slider-thumbs">
                            <div class="swiper-wrapper">
                                @foreach($newImages as $newImage)
                                    <div class="swiper-slide" data-exid="{{ $product->getId() }}">
                                        <div class="product-info__img-thumb-slide">
                                            <img class="product-info__img"
                                                 src="{{ $newImage }}"
                                                 data-src="{{ $newImage }}"
                                                 data-zoom="{{ $newImage }}"/>
                                        </div>
                                    </div>
                                @endforeach
                                {{--for video if needed--}}
                                {{--<div class="swiper-slide">--}}
                                {{--<div class="product-info__img-thumb-slide product-info__img-thumb-slide--video-btn"--}}
                                {{--data-modal="#product-video-modal"--}}
                                {{--data-video-src="{{ asset('https://www.youtube.com/embed/m9kvbpZhwq4') }}">--}}
                                {{--<img src="{{ asset('assets/img/customer-img/product-img-2.jpg') }}"/>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <div class="product-info__thumbs-btns">
                            <button class="btn btn-icon-only btn--link product-info__thumbs-btn-prev btn--prev">
                                <svg class="icon stroke-current" width="24" height="24">
                                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                </svg>
                            </button>
                            <button class="btn btn-icon-only btn--link product-info__thumbs-btn-next btn--next">
                                <svg class="icon stroke-current" width="24" height="24">
                                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <!-- delete .hidden-lg on mobile-->
                    <div class="product-info__image-wrapper mr-30 hidden-lg">
                        <button class="btn btn-icon-only btn--link product-info__slider-btn-prev hidden-lg-up">
                            <svg class="icon stroke-primary" width="12" height="12">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                            </svg>
                        </button>
                        <button class="btn btn-icon-only btn--link product-info__slider-btn-next hidden-lg-up">
                            <svg class="icon stroke-primary" width="12" height="12">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                            </svg>
                        </button>
                        <div class="product-info-slider-main">
                            <img class="product-info__img" src="{{ $product->getFirstImage() }}"/>
                            <div class="product-info-slider-main__pseudo-btn">
                                <img src="{{ asset('assets/img/zoom.svg') }}" alt="" width="24" height="24"/>
                                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->zoom) !!}</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-info__zoom-img-container">
                        <img class="product-info__zoom-img"
                             src="{{ asset($product->getFirstImage()) }}"/>
                    </div>
                </div>
                <div class="product-info__details-container">
                    <div class="product-info__details-header">
                        <div class="product-info__details-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->title) !!}</div>
                        <div class="product-info__details-label">
                            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->product_code) !!}</span>
                            <span class="product-info__details-label-text">{{ $product->code }}</span>
                        </div>
                    </div>
                    @include('front.partial.price', ['item' => $product, 'no_price' => true])

                    <div class="product-info__details-item">
                        @include('front.pages.product_attributes')
                    </div>
                    <div class="product-info__details-item">
                        <div class="product-info__details-img-wrapper">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        @foreach($product->getByModel() as $productByModel)
                                            <a class="product-info__details-img" href="{{ route('product', ['id' => $productByModel->id]) }}">
                                                <img src="{{ asset($productByModel->getModelImage()) }}" alt=""/>
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="swiper-scrollbar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-info__details-footer">
                        <button class="btn btn--primary btn--lg" data-modal="#request-modal">
                            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->request_check) !!}</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach