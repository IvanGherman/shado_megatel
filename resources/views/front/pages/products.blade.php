@extends('layouts.front')
@section('content')
    @php
        $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
        $variables = json_decode($variables);

        $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
        $translations = json_decode($translations);

        $locale = Config::get('app.locale');
    @endphp
    <ul class="breadscrumbs-list scroll-x" itemscope="itemscope" itemtype="http://schema.org/BreadcrumbList">
        <li class="breadscrumbs-item scroll-x__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem"><a itemprop="item" href="{{ route('home') }}">Главная<span class="visually-hidden" itemprop="name">Главная</span>
                <meta itemprop="position" content="1"/></a>
        </li>
        <li class="breadscrumbs-item scroll-x__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem"><a class="visually-hidden" itemprop="item" href="#">Каталог</a><span itemprop="name">Каталог</span>
            <meta itemprop="position" content="2"/>
        </li>
    </ul>
    <section class="products-main">
        <div class="products-main__wrapper">
            @include('front.pages.products_filters')
            <div class="products-main__content">
                <div class="products-main__btns">
                    <div class="filters__open js-filters__btn-open hidden-xl-up">
                        <svg class="icon" width="40" height="40">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-filters') }}"></use>
                        </svg><span class="filters__open-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->filters) !!}</span>
                        <button class="filters__btn-open" type="button">
                            <svg class="icon" width="24" height="24">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-chevron-right') }}"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="products-main__sort-btns hidden-md">
                        <span class="products-main__sort-btns-label">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->show_by) !!}:</span>
                        @foreach ($allowedLimits as $allowed)
                            <button class="show-by btn btn--nopadding btn--link sort-btn @if ($allowed === $limit) active @endif"
                                    data-id="{{ $allowed }}">
                                <span>{{ $allowed }}</span>
                            </button>
                        @endforeach
                    </div>
                </div>
                <div class="container container--large">
                    <ul class="products-main__list row" id="products-container">
                        @include('front.pages.products_partial')
                    </ul>
                </div>
                <div id="products_pagination">
                    @include('front.pages.products_pagination')
                </div>
            </div>
        </div>
    </section>
    @if ($categoryEntity)
        <section class="promo-text">
            <span class="promo-text__wrapper">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $categoryEntity->description) !!}
            </span>
        </section>
    @endif
    <script>
        $(document).ready(function(){var e=null;$.ajaxSetup({cache:!1});var n='{{ route('product_list') }}',o=+'{{ $limit }}',a=null,i=null,t='{!! json_encode($filters_predefined) !!}';function r(t){if(t)r=t;else{var e=$("form.filters__form").serialize(),r=n;r=-1===n.indexOf("?")?n+"?limit="+o:n+"&limit="+o,a&&(r=r+"&price_from="+a),i&&(r=r+"&price_to="+i),r=r+"&"+e}$('.products-main__list').addClass('is-loading');$.ajax({url:r}).done(function(t){if(t.hasOwnProperty("error"))alert(t.error);else{var e=(e=r.split("?"))[1];o=e="?"+e,a=$("form.filters__form").serializeArray(),window.history.pushState({isXMLHttpRequest:!0,urlPath:o,form_data:a},"",o),$("#products-container").html(t.html),$(".filterable").html(0),$.each(t.counters.categories,function(t,e){$("#categories-amount-"+e.id).html(e.amount)}),$.each(t.counters.colors,function(t,e){$("#colors-amount-"+e.id).html(e.amount)}),$.each(t.counters.textures,function(t,e){$("#textures-amount-"+e.id).html(e.amount)}),$.each(t.counters.transparencies,function(t,e){$("#transparencies-amount-"+e.id).html(e.amount)}),$.each(t.counters.countries,function(t,e){$("#countries-amount-"+e.id).html(e.amount)}),$.each(t.counters.treeType,function(t,e){$("#tree-types-amount-"+e.id).html(e.amount)});for(var n=0;n<t.counters.attributes.length;n++)$("#attribute-value-"+t.counters.attributes[n].attribute_value_id+"-amount").html(t.counters.attributes[n].amount);$("#jproduct-modals").html(t.modals),$("#products_pagination").html(t.pagination),App.initPreviewModalSlider(),App.initProduct(),App.initChangeImg(".preview-card",".preview-card__slider-container",1e3)}var o,a;$('.products-main__list').removeClass('is-loading');})}function c(t){e&&clearTimeout(e),e=setTimeout(function(){r(t),e=null},0)}function u(){var t=$("form.filters__form");t.find("[type=checkbox]").removeAttr("checked"),t.find("[name=keyword]").val("")}t=$.parseJSON(t),$.each(t,function(a,t){for(var e=0;e<t.length;e++)$("#"+a+t[e]).prop("checked",!0),$("#"+a+t[e]).closest(".accordion__item").addClass("active");"attribute"===a&&$.each(t,function(o,t){$.each(t,function(t,e){var n=$("#"+a+"-"+o+"-"+e);n.prop("checked",!0),n.closest(".accordion__item").addClass("active show")})})}),$("form.filters__form input, form.filters__form select").on("change",function(){n='{{ route('product_list') }}',c(null)}),$("input[name=keyword]").keydown(function(){2<=$(this).val().length&&c(null)}),$("#products_pagination").on("click",".paginator",function(t){t.preventDefault(),n=$(this).data("href"),c(null)}),$(".show-by").on("click",function(){o=+$(this).data("id"),$(".show-by").removeClass("active"),$(this).toggleClass("active"),c(null)}),document.addEventListener("sliderChanged",function(t){a=t.detail.data[0],i=t.detail.data[1],c(null)}),$(".js-filters-reset-btn").on("click",function(){u(),c(null)}),$(".js-noUi-reset-btn").on("click",function(){u(),c(null)}),window.onpopstate=function(t){var e=window.history.state;e&&e.hasOwnProperty("form_data")&&($("[type=checkbox]").prop("checked",!1),$("[type=text]").val(""),$.each(e.form_data,function(t,e){var n=$('[name="'+e.name+'"]');"checkbox"===n.attr("type")?n.prop("checked",!0):"text"===n.attr("type")&&n.val(e.value)}))}});
    </script>
@endsection

@section('modal_content')
    <div class="modal product-preview-modal hidden-xl" id="product-preview-modal">
        <div class="modal__wrapper">
            <div class="modal__close">
                <svg class="icon stroke-black" width="20" height="20">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                </svg>
            </div>
            <div class="modal__content">
                <div class="product-preview">
                    <div class="swiper-container product-preview__slider js-product-preview-slider">
                        <div class="swiper-wrapper" id="jproduct-modals">
                            @include('front.pages.products_modals')
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <button class="btn btn-icon-only btn--link product-preview__slider-btn-prev btn--prev">
                        <svg class="icon stroke-current" width="52" height="52">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                    <button class="btn btn-icon-only btn--link product-preview__slider-btn-next btn--next">
                        <svg class="icon stroke-current" width="52" height="52">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection