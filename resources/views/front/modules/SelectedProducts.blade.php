@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<div class="hero">
    <div class="slideshow">
        <div class="slideshow__deco">
            <div class="slideshow__deco-bg" style="background-color: #00A0AF"></div>
            <svg class="slideshow__deco-bg-img" width="518" height="733" viewbox="0 0 518 733" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.7401 22.8227L517.762 0V732.563L0 677.52L10.7401 22.8227Z"></path>
            </svg>
        </div>
        @foreach($selected as $item)
            <a class="slideshow__slide" href="{{ route('product', ['id' => $item->product_id]) }}" data-bgColor="{{ $item->color }}">
                <div class="slideshow__slide-img-wrap">
                    <div class="slideshow__slide-img" style="background-image: url({{ asset($item->getImage()) }});"></div>
                </div>
                <div class="slideshow__slide-side">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->title) !!}</div>
            </a>
        @endforeach
        <button class="slideshow-nav slideshow-nav--prev">
            <svg class="icon stroke-white slideshow-icon slideshow-icon--navarrow-prev" width="16" height="16">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-slideshow-arrow') }}"></use>
            </svg>
        </button>
        <button class="slideshow-nav slideshow-nav--next">
            <svg class="icon stroke-white slideshow-icon slideshow-icon--navarrow-next" width="16" height="16">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-slideshow-arrow') }}"></use>
            </svg>
        </button>
    </div>
    <div class="slideshow__content">
        @foreach($selected as $item)
            <div class="slideshow__content-item">
                <div class="product-info__details-container">
                    <div class="product-info__details-header">
                        <div class="product-info__details-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->product->title) !!}</div>
                        <div class="product-info__details-label">
                            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->product_code) !!}</span>
                            <span class="product-info__details-label-text">{{ $item->product->code }}</span>
                        </div>
                    </div>
                    @include('front.partial.price', ['item' => $item->product])
                    <div class="product-info__details-item">
                        @include('front.pages.product_attributes', ['product' => $item->product])
                    </div>
                    @php $byModel = $item->product->getByModel(); @endphp
                    <div class="product-info__details-item">
                        <div class="product-info__details-img-wrapper">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        @foreach($byModel as $itemByModel)
                                            <a class="product-info__details-img" href="{{ route('product', ['id' => $itemByModel->id]) }}">
                                                <img src="{{ asset($itemByModel->getModelImage()) }}" alt=""/>
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="swiper-scrollbar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-info__details-footer">
                        <a class="btn btn--primary btn--lg" href="{{ route('product', ['id' => $item->product->id]) }}">
                            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->detailed) !!}</span>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
        <button class="slideshow__content-close">
            <svg class="icon stroke-primary" width="32" height="32">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-ix') }}"></use>
            </svg>
        </button>
    </div>
    <h2 class="title--xxl hero__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->made_in_moldova) !!}</h2>
</div>