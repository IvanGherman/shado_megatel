@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
@if ($reviews->count() > 0)
<section class="testimonials">
    <div class="testimonials__wrapper bg-grey-0 hidden-sm">
        <h2 class="title--xxl ff-secondary section-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->reviews_about_us) !!}</h2>
        <div class="gallery testimonials__gallery-desktop">
            <div class="swiper-container testimonials-gallery">
                <div class="swiper-wrapper">
                    @foreach($reviews as $key => $review)
                        <div class="swiper-slide">
                            <div class="gallery__item" data-index="{{ $key }}">
                                <div class="testimonial">
                                    <div class="testimonial__wrapper">
                                        <div class="testimonial__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->short_description) !!}</div>
                                    </div>
                                    <div class="testimonial__label">
                                        <div class="avatar avatar--complex">
                                            <div class="avatar__img-container">
                                                <img class="avatar__img" src="{{ asset($review->getImage()) }}" alt="alt"/>
                                            </div>
                                            <div class="avatar__text">
                                                <div class="avatar__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->person_name) !!}</div>
                                                <div class="avatar__subtitle">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->role) !!}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
            <div class="gallery__btns testimonials-gallery__btns">
                <button class="btn btn-icon-only btn--link btn--prev testimonials-gallery__btn-prev"><span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
                <button class="btn btn-icon-only btn--link btn--next testimonials-gallery__btn-next"><span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
            </div>
            <div class="modal">
                <div class="modal__wrapper">
                    <div class="modal__close">
                        <svg class="icon stroke-black" width="20" height="20">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                        </svg>
                    </div>
                    <div class="modal__content">
                        <div class="swiper-container gallery-swiper">
                            <div class="swiper-wrapper">
                                @foreach($reviews as $review)
                                    <div class="swiper-slide">
                                        <div class="testimonial">
                                            <div class="testimonial__wrapper">
                                                <div class="static">
                                                    <div class="clearfix">
                                                        {!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->description) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="testimonial__label">
                                                <div class="avatar avatar--complex">
                                                    <div class="avatar__img-container">
                                                        <img class="avatar__img" src="{{ asset($review->getImage()) }}" alt="alt"/>
                                                    </div>
                                                    <div class="avatar__text">
                                                        <div class="avatar__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->person_name) !!}</div>
                                                        <div class="avatar__subtitle">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->role) !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="align--center">
            <button class="btn btn--primary btn--custom testimonials__link" data-modal="#testimonial-modal">
                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->add_review) !!}</span>
            </button>
        </div>
    </div>
    <div class="testimonials__wrapper hidden-sm-up">
        <div class="accordion">
            <div class="accordion__item">
                <div class="accordion__title accordion__title--section-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->reviews_about_us) !!}
                    <div class="accordion__icon"></div>
                </div>
                <div class="accordion__content">
                    <div class="some-content">
                        <div class="gallery testimonials__gallery-mobile">
                            <div class="swiper-container testimonials-gallery">
                                <div class="swiper-wrapper">
                                    @foreach($reviews as $key => $review)
                                        <div class="swiper-slide">
                                            <div class="gallery__item" data-index="{{ $key }}">
                                                <div class="testimonial">
                                                    <div class="testimonial__wrapper">
                                                        <div class="testimonial__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->short_description) !!}</div>
                                                    </div>
                                                    <div class="testimonial__label">
                                                        <div class="avatar avatar--complex">
                                                            <div class="avatar__img-container">
                                                                <img class="avatar__img" src="{{ asset($review->getImage()) }}" alt="alt"/>
                                                            </div>
                                                            <div class="avatar__text">
                                                                <div class="avatar__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->person_name) !!}</div>
                                                                <div class="avatar__subtitle">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->role) !!}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="swiper-pagination"></div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                            <div class="gallery__btns testimonials-gallery__btns">
                                <button class="btn btn-icon-only btn--link btn--prev testimonials-gallery__btn-prev"><span></span>
                                    <svg class="icon stroke-current" width="24" height="24">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                    </svg>
                                </button>
                                <button class="btn btn-icon-only btn--link btn--next testimonials-gallery__btn-next"><span></span>
                                    <svg class="icon stroke-current" width="24" height="24">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                                    </svg>
                                </button>
                            </div>
                            <div class="modal">
                                <div class="modal__wrapper">
                                    <div class="modal__close">
                                        <svg class="icon stroke-black" width="20" height="20">
                                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                                        </svg>
                                    </div>
                                    <div class="modal__content">
                                        <div class="swiper-container gallery-swiper">
                                            <div class="swiper-wrapper">
                                                @foreach($reviews as $review)
                                                    <div class="swiper-slide">
                                                    <div class="testimonial">
                                                        <div class="testimonial__wrapper">
                                                            <div class="static">
                                                                <div class="clearfix">
                                                                    {!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->description) !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="testimonial__label">
                                                            <div class="avatar avatar--complex">
                                                                <div class="avatar__img-container">
                                                                    <img class="avatar__img" src="{{ asset($review->getImage()) }}" alt="alt"/></div>
                                                                <div class="avatar__text">
                                                                    <div class="avatar__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->person_name) !!}</div>
                                                                    <div class="avatar__subtitle">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $review->role) !!}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                            <div class="swiper-pagination"></div>
                                            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="align--center">
                            <button class="btn btn--primary btn--custom testimonials__link" data-modal="#testimonial-modal">
                                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->add_review) !!}</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif