@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<button class="btn btn-icon-only btn--link btn__round-icon btn--bottom-fixed js-scroll-to-top hidden-sm is-hidden"><span></span>
    <svg class="icon" width="18" height="18">
        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow-up') }}"></use>
    </svg>
</button>
<!-- Start: Modals-->
<div class="modal video-modal" id="product-video-modal">
    <div class="modal__wrapper">
        <div class="modal__close">
            <svg class="icon stroke-black" width="20" height="20">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
            </svg>
        </div>
        <div class="modal__content">
            <div class="iframe iframe--video">
                <iframe data-src="{{ asset('https://www.youtube.com/embed/sOkdDziRwB8') }}"
                        allow="autoplay; encrypted-media" allowfullscreen="">
                </iframe>
            </div>
        </div>
    </div>
</div>
<div class="modal modal--scrollable" id="testimonial-modal">
    <div class="modal__wrapper modal__wrapper--scrollable">
        <div class="modal__close">
            <svg class="icon stroke-black" width="20" height="20">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
            </svg>
        </div>
        <div class="modal__content modal__content--scrollable">
            <form class="form review-form" novalidate="novalidate" data-scrolling-container=".modal__content--scrollable">
                <div class="form__title ff-secondary">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->add_review) !!}</div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <input type="text" name="person_name" required="" placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->name) !!}"/>
                    </label><span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                </div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <input type="email" name="email" required="" placeholder="Email"/>
                    </label><span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                </div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <textarea name="message" placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->message) !!}" data-min="2"></textarea>
                    </label>
                </div>
                <div class="align--center">
                    <button class="btn btn--primary btn--custom form__btn" id="send-review">
                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->send) !!}</span>
                    </button>
                </div>
                <div class="form__info">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->confirm_with) !!} <a href="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $variables->privacy_policy_link) !!}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->privacy_policy) !!} </a>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->company) !!} </div>
                <div class="form__success">
                    <img class="form__success-img" src="{{ asset('assets/img/icon-success.svg') }}"/>
                    <span class="form__success-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->form_sent) !!} </span>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal modal--scrollable" id="request-modal">
    <div class="modal__wrapper modal__wrapper--scrollable">
        <div class="modal__close">
            <svg class="icon stroke-black" width="20" height="20">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
            </svg>
        </div>
        <div class="modal__content modal__content--scrollable">
            <form class="form jrequest-form" novalidate="novalidate" data-scrolling-container=".modal__content--scrollable">
                <div class="form__title ff-secondary">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->request_master) !!}</div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <input type="text" required="" name="person_name" placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->name) !!}"/>
                    </label>
                    <span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                </div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <input type="tel" required="" data-digits="data-digits" name="phone" placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->phone) !!}"/>
                    </label>
                    <span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                </div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <textarea placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->additional_info) !!}" name="message" data-min="2"></textarea>
                    </label>
                </div>
                <div class="align--center">
                    <button class="btn btn--primary btn--custom form__btn">
                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->send) !!}</span>
                    </button>
                </div>
                <div class="form__info">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->confirm_with) !!} <a href="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $variables->privacy_policy_link) !!}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->privacy_policy) !!} </a>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->company) !!}</div>
                <div class="form__success">
                    <img class="form__success-img" src="{{ asset('assets/img/icon-success.svg') }}"/>
                    <span class="form__success-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->form_sent) !!}</span>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal modal--scrollable" id="contact-modal">
    <div class="modal__wrapper modal__wrapper--scrollable">
        <div class="modal__close">
            <svg class="icon stroke-black" width="20" height="20">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
            </svg>
        </div>
        <div class="modal__content modal__content--scrollable">
            <form class="form" novalidate="novalidate" data-scrolling-container=".modal__content--scrollable">
                <div class="form__title ff-secondary">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->contact_us) !!}</div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <input type="text" required="" placeholder="ФИО"/>
                    </label><span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                </div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <input type="email" required="" placeholder="Email"/>
                    </label><span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                </div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <textarea placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->additional_info) !!}" data-min="2"></textarea>
                    </label>
                </div>
                <div class="align--center">
                    <button class="btn btn--primary btn--custom form__btn">
                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->send) !!}</span>
                    </button>
                </div>
                <div class="form__info">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->confirm_with) !!} <a href="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $variables->privacy_policy_link) !!}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->privacy_policy) !!} </a>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->company) !!}</div>
                <div class="form__success">
                    <img class="form__success-img" src="{{ asset('assets/img/icon-success.svg') }}"/><span class="form__success-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->form_sent) !!}</span>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal modal--scrollable" id="call-modal">
    <div class="modal__wrapper modal__wrapper--scrollable">
        <div class="modal__close">
            <svg class="icon stroke-black" width="20" height="20">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
            </svg>
        </div>
        <div class="modal__content modal__content--scrollable">
            <form class="form" id="call-modal" novalidate="novalidate" data-scrolling-container=".modal__content--scrollable">
                <div class="form__title ff-secondary">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->call_back) !!}</div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <input type="text" required="" name="person_name" placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->name) !!}"/>
                    </label>
                    <span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                </div>
                <div class="form__group">
                    <label class="form__wrapper">
                        <input type="text" required="" name="phone" placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->contact_phone) !!}" data-digits="data-digits"/>
                    </label>
                    <span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                </div>
                <input type="hidden" name="product_id">
                <div class="align--center">
                    <button class="btn btn--primary btn--custom form__btn"><span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->send) !!}</span>
                    </button>
                </div>
                <div class="form__info">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->confirm_with) !!} <a href="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $variables->privacy_policy_link) !!}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->privacy_policy) !!} </a>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->company) !!}</div>
                <div class="form__success">
                    <img class="form__success-img" src="{{ asset('assets/img/icon-success.svg') }}"/>
                    <span class="form__success-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->form_sent) !!}</span>
                </div>
            </form>
        </div>
    </div>
</div>
{{--<div class="modal video-modal" id="product-video-modal">--}}
    {{--<div class="modal__wrapper">--}}
        {{--<div class="modal__close">--}}
            {{--<svg class="icon stroke-black" width="20" height="20">--}}
                {{--<use xlink:href="assets/img/sprite.min.svg#ic-close"></use>--}}
            {{--</svg>--}}
        {{--</div>--}}
        {{--<div class="modal__content">--}}
            {{--<div class="iframe iframe--video">--}}
                {{--<iframe data-src="https://www.youtube.com/embed/sOkdDziRwB8" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<!-- End: Modals-->