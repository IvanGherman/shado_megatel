<a class="link--btn" style="display: none" href="{{ url($url) }}">
    <div class="btn__dot" style="display: none"></div>
    <span>{!! $title !!}</span>
</a>

<a class="link--btn" href="{{ url($url) }}">
    <div class="btn__dot"></div>
    <span>{!! $title !!}</span>
</a>