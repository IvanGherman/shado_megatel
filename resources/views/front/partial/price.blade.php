@if (!empty($item->getPrice()) || !empty($item->promo_price))
    <div class="product-item__details">
        <span class="product-item__detail">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->from) !!}</span>
        @if ($item->promo_price)
            @if ($item->price)
                <span class="product-item__detail product-item__detail--old">{{ $item->price }} MDL</span>
            @endif
            <span class="product-item__detail product-item__detail--new">{{ $item->promo_price }} MDL<span class="product-item__detail-label">м<sup>2</sup></span></span>
        @else
            <span class="product-item__detail product-item__detail--new">{{ $item->price }} MDL<span class="product-item__detail-label">м<sup>2</sup></span></span>
        @endif
    </div>
@elseif (!empty($no_price))
    <div class="no-price" data-id="{{ $item->id }}" data-modal="#call-modal">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->ask_for_price) !!}</div>
@endif