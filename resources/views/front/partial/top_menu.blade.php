@if($items)
    @php
        $locale = Config::get('app.locale');
    @endphp
    @foreach($items->sortBy('item_order') as $item)
        @if (!empty($item['children']) && count($item['children']) > 0)
            <li class="nav__item has-dropdown">
                <div class="nav__link-wrapper">
                    <div class="nav__link text--lg">{{ \App\Http\Helpers\webus_help::translate_manual($locale, $item['name']) }}</div>
                    <svg class="icon stroke-current nav__icon dropdown__icon" width="32" height="32">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-open') }}"></use>
                    </svg>
                </div>
                <div class="dropdown">
                    <ul class="dropdown__list">
                        @foreach($item['children'] as $subItem)
                            <li class="dropdown__item">
                                <a class="dropdown__link"
                                   href="@if(!empty($subItem['url'])){{ LaravelLocalization::localizeUrl($subItem['url']) }}@endif">
                                    <span>{{ \App\Http\Helpers\webus_help::translate_manual($locale, $subItem['name']) }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </li>
        @else
            <li class="nav__item {{ Request::is('*'.$item['url']) ? 'active' : '' }}">
                <div class="nav__link-wrapper">
                    <a class="nav__link text--lg"
                       href="@if(!empty($item['url'])){{ LaravelLocalization::localizeUrl($item['url']) }}@endif">{{ \App\Http\Helpers\webus_help::translate_manual($locale, $item['name']) }}</a>
                </div>
            </li>
        @endif
    @endforeach
@endif
