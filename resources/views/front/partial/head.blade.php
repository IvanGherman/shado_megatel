<meta charset="UTF-8"/>
<meta name="keywords" content="{!! $meta['keywords'] !!}" />
<meta name="title" content="{!! $meta['title'] !!}" />
<meta name="description" content="{!! $meta['description'] !!}" />
<title>{!! $meta['page_name'] !!}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0"/>
<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicon/apple-touch-icon.png') }}"/>
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon/favicon-32x32.png') }}"/>
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/favicon/favicon-16x16.png') }}"/>
<link rel="manifest" href="{{ asset('assets/favicon/manifest.json') }}"/>
<link rel="mask-icon" href="{{ asset('assets/favicon/safari-pinned-tab.svg') }}" color="#5bbad5"/>
<meta name="theme-color" content="#000"/>
<link rel="manifest" href="{{ asset('favicon/manifest.json') }}"/>
<link rel="stylesheet" href="{{ asset('css/vendors.bundle.css') }}"/>
<link rel="stylesheet" href="{{ asset('css/main.bundle.css') }}"/>
<script src="{{ asset('js/vendors/jquery.js') }}" type="text/javascript"></script>
<style>
    .preloader img {
        animation: rotate 1.3s linear infinite;
        transform-origin: 50% 50%;
    }
    @keyframes rotate{
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-46739405-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-46739405-1');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(62185036, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/62185036" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->