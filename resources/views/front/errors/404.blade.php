<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8"/>
    <meta name="keywords" content="" />
    <meta name="title" content="SHADO" />
    <meta name="description" content="" />
    <title>SHADO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicon/apple-touch-icon.png') }}"/>
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon/favicon-32x32.png') }}"/>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/favicon/favicon-16x16.png') }}"/>
    <link rel="manifest" href="{{ asset('assets/favicon/manifest.json') }}"/>
    <link rel="mask-icon" href="{{ asset('assets/favicon/safari-pinned-tab.svg') }}" color="#5bbad5"/>
    <meta name="theme-color" content="#000"/>
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}"/>
    <link rel="stylesheet" href="{{ asset('css/vendors.bundle.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/main.bundle.css') }}"/>
    <script src="{{ asset('js/vendors/jquery.js') }}" type="text/javascript"></script>
</head>
<body>
@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<header class="header header--tiny" id="header">
    <div class="header__wrapper">
        <div class="header__section">
            <a class="header__logo" href="{{ route('home') }}">
                <img src="{{ asset('assets/img/shado-logo.svg') }}" alt=""/>
            </a>
            <div class="header__contacts">
                <div class="contacts hidden-lg">
                    <svg class="icon" width="32" height="32">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-phone-chat') }}"></use>
                    </svg>
                    <a class="contacts__link" href="tel:{{ \App\Variables::changePhone($variables, 1) }}">{{ \App\Variables::changePhone($variables, 1) }}</a>
                    <a class="contacts__link" href="tel:{{ \App\Variables::changePhone($variables, 2) }}">{{ \App\Variables::changePhone($variables, 2) }}</a>
                </div>
            </div>
        </div>
        <div class="header__section">
            <div class="header__menu hidden-xl">
                <nav class="nav">
                    <div class="nav__list">
                        <ul class="nav__items">
                            <li class="nav__item active header__mega-menu-open">
                                <div class="nav__link-wrapper">
                                    <div class="nav__link text--lg">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->catalog) !!}</div>
                                    <svg class="icon stroke-current ml-7" width="10" height="10">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-open') }}"></use>
                                    </svg>
                                </div>
                            </li>
                            {!! \App\Http\Helpers\webus_help::custom_menu('top-menu') !!}
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="header__lang has-dropdown">
                @foreach(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                    @if ($localeCode === $locale)
                        <button class="btn btn--link">
                            @if ($localeCode === 'ru')
                                <span>Ру</span>
                            @elseif($localeCode === 'ro')
                                <span>Ro</span>
                            @endif
                            <svg class="icon stroke-current" width="32" height="32">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-open') }}"></use>
                            </svg>
                        </button>
                    @endif
                @endforeach
                <div class="dropdown" id="header__lang-dropdown">
                    <ul class="dropdown__list">
                        @foreach(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                            <li class="dropdown__item">
                                <a class="dropdown__link" href="{{ route('change_lang', ['lang' => $localeCode]) }}">
                                    @if ($localeCode === 'ru')
                                        <span>Ру</span>
                                    @elseif($localeCode === 'ro')
                                        <span>Ro</span>
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="header__contacts hidden-sm">
                <div class="header__btn">
                    <button class="btn btn-ghost-primary-dark" data-modal="#request-modal">
                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->request_master) !!}</span>
                    </button>
                </div>
            </div>
            <button class="btn btn-icon-only btn--nopadding btn--link header__open-menu nav__toggle" id="nav__toggle"><span></span>
                <svg class="icon" width="29" height="29">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-hamburger') }}"></use>
                </svg>
            </button>
        </div>
    </div>
    {!! \App\Http\Helpers\webus_help::get_categories() !!}
</header>

<nav class="nav nav--2" id="nav">
    <div class="nav__list">
        <button class="nav__toggle nav__ic-close">
            <svg class="nav__toggle-ic" viewbox="0 0 22 22" xmlns="http://www.w3.org/2000/svg">
                <g stroke-width="2" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M1 1l20 20M21 1L1 21"></path>
                </g>
            </svg>
        </button>
        <ul class="nav__items">
            {!! \App\Http\Helpers\webus_help::custom_menu('burger-menu') !!}
        </ul>
    </div>
    <div class="nav__bg"></div>
</nav>
@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<main id="main">
    <div class="redirect">
        <div class="redirect__bg-img"><img src="{{ asset('assets/img/404-bg.png') }}" alt=""/></div>
        <div class="container">
            <div class="redirect__content">
                <div class="redirect__content-container">
                    <h1 class="redirect__main-title">404</h1>
                    <h2 class="title--xl redirect__title">Страница не найдена</h2>
                    <p class="redirect__text">Видимо вы что-то ввели неправильно в адресной строке, чтобы вернуться на главную страницу нажмите на ссылку снизу</p>
                    <div class="redirect__btns">
                        <div class="align--center">
                            <a class="btn btn--primary btn--custom" href="{{ route('home') }}"><span>На главную</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@yield('modal_content')

@include('front.partial.modals')
@include('front.partial.footer')
@include('front.partial.scripts')
</body>
</html>
