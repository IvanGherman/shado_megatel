@if($totals)
    <ul>
        @foreach($totals as $item_total)
            <li>{{ $item_total['text'] }}: <span class="{{ $item_total['code'] }}">{{ $item_total['value'] }}</span></li>
        @endforeach
    </ul>
@endif