@extends('layouts.front')
@section('content')
    <div class="page-block detailed-news-page-block">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 detailed-news-col">
                <div class="detailed-news-box">
                    <h2 style="text-align: center;">{!! $text !!}</h2>
                </div>
            </div>
        </div>
    </div>
@endsection