@extends('layouts.front')
@section('head')
<script>
var text_error_remove_product = '{{ trans('cart.text_error_remove_product') }}';
</script>
@endsection
@section('content')
    <div class="page-block start-block product-page-start">
        <div class="container animateMe">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="start-block__text">
                        <div class="start-block__text-title">{{ $title }}</div>
                        <div class="start-block__text-content">{{ trans('cart.text_count') }} <span class="card-items-volume">{{ Cart::count() }}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-block card-block">
        @include('front.includes.preloader')
        <div class="container">
            @if($products != null)
                @foreach($products as $product)
                    <div class="row item-row animateMe">
                        <div class="col-lg-6 col-md- col-sm-6 col-xs-12 product-col">
                            <div class="product-pic">
                                @if(!empty($product['image']))
                                    <img src="{{ $product['image'] }}" alt="">
                                @endif
                            </div>
                            <div class="product-name">{{ $product['name'] }}</div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 product-data-col">
                            <div class="product-amount-block">
                          <span class="product-amount-data reduce-amount">
                            <svg class="user-soc">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#minus"></use>
                            </svg>
                          </span>
                                <input type="text" class="product-amount__value" value="{{ $product['quantity'] }}" data-key="{{ $product['key'] }}">
                          <span class="product-amount-data increase-amount">
                            <svg class="user-soc">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#plus"></use>
                            </svg>
                          </span>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 product-data-col">
                            <span class="currency-data currency-amount">{{ $product['price'] }}</span>
                            <span class="currency-data currency-data-name">{{ $curren_currency }}</span>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 product-data-col summary-col">
                            <span class="currency-data currency-amount product-price-total">{{ $product['price_total'] }}</span>
                            <span class="currency-data currency-data-name">{{ $curren_currency }}</span>
                        </div>
                        <div class="clear-row-data remove-product-cart">
                            <svg class="user-soc">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use>
                            </svg>
                        </div>
                    </div>
                @endforeach

                <div class="row item-row shipping-row animateMe">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 shipping-col">
                        <div class="shipping-col__content shipping-destination">
                            <div class="destination__pic">
                                <svg class="user-soc">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shipping"></use>
                                </svg>
                            </div>
                            <div class="destination__content">
                                <div class="shipping__title">{{ trans('delivery/free.title') }}</div>
                                <div class="shipping__text">{{ trans('delivery/free.description') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 shipping-col overall-summary-col">
                        <div class="shipping-col__content">
                            <div class="shipping__title">{{ trans('cart.text_total') }}</div>
                            <div class="shipping-price">
                                <span class="currency-data currency-amount subtotal-price">{{ $total }}</span>
                                <span class="currency-data currency-data-name">{{ $curren_currency }}</span>
                            </div>
                            <a href="{{ route('checkout') }}" class="page-button page-button__green">{{ trans('cart.text_checkout') }}</a>
                        </div>
                    </div>
                </div>

            @else
                <h2>{{ trans('cart.text_empty_cart') }}</h2>
            @endif
        </div>
    </div>

@endsection
@push('cart')
<script src="/js/cart.js"></script>
@endpush