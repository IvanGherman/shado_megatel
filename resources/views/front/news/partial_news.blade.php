@php
    $locale = Config::get('app.locale');
@endphp
@foreach($data as $item)
    <div class="col-4 col-md-6 col-sm-12 js-news-item">
        <a class="news-card" href="{{ route('news_view', ['slug' => $item->slug]) }}">
            <div class="news-card__img">
                <img src="{{ asset($item->image) }}" alt="{{ $item->getTitle() }}"/>
            </div>
            <div class="news-card__text">
                <h3 class="title--sm news-card__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->title) !!}</h3>
                @php $desc = \App\Http\Helpers\webus_help::translate_manual($locale, $item->short_description); @endphp
                @if (mb_strlen($desc) < 151)
                    <p class="news-card__text-content">{!! $desc !!}</p>
                @else
                    <p class="news-card__text-content">{!! mb_substr($desc, 0, 150) !!}...</p>
                @endif
                <div class="news-card__date">{{ $item->getDate($locale) }}</div>
            </div>
        </a>
    </div>
@endforeach