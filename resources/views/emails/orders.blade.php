@component('mail::message')

## {{ $text_for }}

@component('mail::panel')
+ **Имя**: {{ $order_info['address_name'] }}
+ **E-mail**: {{ $customer_email }}
@endcomponent

@component('mail::panel')
+ **Способ оплаты**: {{ $payment_method }}
+ **Общая цена**: {{ $order_info['total'] }}
+ **Валюта**: {{ $order_info['currency'] }}
@endcomponent

@component('mail::panel')
### Totals
@foreach(json_decode($order_info['totals']) as $item_total)
    + **{{ $item_total->text }}**: {{ $item_total->value }}
@endforeach
@endcomponent

@component('mail::table')

| Название продукта | Количество | Цена | Общая цена  |
| :------------- |:-------------:| --------:| --------:|
@if($products_cart != null)
    @foreach($products_cart as $product)
        | {{ $product['name'] }} | {{ $product['quantity'] }} | {{ $product['price'] }} | {{ $product['price_total'] }} |
    @endforeach
@endif

@endcomponent

@endcomponent