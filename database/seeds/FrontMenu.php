<?php

use Illuminate\Database\Seeder;

class FrontMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = DB::table('menu')->insertGetId([
            'name' => 'Top menu',
            'slug' => 'top-menu',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
