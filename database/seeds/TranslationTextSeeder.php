<?php

use Illuminate\Database\Seeder;

class TranslationTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translations = [
            [
                'name' => 'all',
                'key' => 'all',
                'value' => '[:ru]Все',
            ],
            [
                'name' => 'search_by_code',
                'key' => 'search_by_code',
                'value' => '[:ru]Поиск по коду товара',
            ],
            [
                'name' => 'departments_addresses',
                'key' => 'departments_addresses',
                'value' => '[:ru]Адреса филиалов',
            ],
            [
                'name' => 'phone',
                'key' => 'phone',
                'value' => '[:ru]Телефон',
            ],
            [
                'name' => 'schedule',
                'key' => 'schedule',
                'value' => '[:ru]График работы',
            ],
            [
                'name' => 'free_day',
                'key' => 'free_day',
                'value' => '[:ru]Выходной',
            ],
            [
                'name' => 'from',
                'key' => 'from',
                'value' => '[:ru]От',
            ],
            [
                'name' => 'free_days',
                'key' => 'free_days',
                'value' => '[:ru]Выходные',
            ],
            [
                'name' => 'contact_us',
                'key' => 'contact_us',
                'value' => '[:ru]Напишите нам',
            ],
            [
                'name' => 'not_valid',
                'key' => 'not_valid',
                'value' => '[:ru]Это поле заполнено не верно.',
            ],
            [
                'name' => 'send',
                'key' => 'send',
                'value' => '[:ru]Отправить',
            ],
            [
                'name' => 'form_sent',
                'key' => 'form_sent',
                'value' => '[:ru]Форма успешно отправлена',
            ],
            [
                'name' => 'confirm_with',
                'key' => 'confirm_with',
                'value' => '[:ru]Нажимая кнопку отправить вы соглашаетесь с ',
            ],
            [
                'name' => 'privacy_policy',
                'key' => 'privacy_policy',
                'value' => '[:ru]политикой конфиденциальности ',
            ],
            [
                'name' => 'company',
                'key' => 'company',
                'value' => '[:ru]компании',
            ],
            [
                'name' => 'additional_info',
                'key' => 'additional_info',
                'value' => '[:ru]Дополнительная информация',
            ],
            [
                'name' => 'go_catalog',
                'key' => 'go_catalog',
                'value' => '[:ru]Перейти в каталог',
            ],
            [
                'name' => 'name',
                'key' => 'name',
                'value' => '[:ru]ФИО',
            ],
            [
                'name' => 'name',
                'key' => 'name',
                'value' => '[:ru]ФИО',
            ],
            [
                'name' => 'our_clients',
                'key' => 'our_clients',
                'value' => '[:ru]Наши клиенты',
            ],
            [
                'name' => 'our_clients',
                'key' => 'our_clients',
                'value' => '[:ru]Наши клиенты',
            ],
            [
                'name' => 'show_more',
                'key' => 'show_more',
                'value' => '[:ru]Показать ещё',
            ],
            [
                'name' => 'show_more',
                'key' => 'show_more',
                'value' => '[:ru]Показать ещё',
            ],
            [
                'name' => 'hide',
                'key' => 'hide',
                'value' => '[:ru]Скрыть',
            ],
            [
                'name' => 'news',
                'key' => 'news',
                'value' => '[:ru]Новости',
            ],
            [
                'name' => 'reviews_about_us',
                'key' => 'reviews_about_us',
                'value' => '[:ru]Отзывы о нас',
            ],
            [
                'name' => 'all_news',
                'key' => 'all_news',
                'value' => '[:ru]Все новости',
            ],
            [
                'name' => 'add_review',
                'key' => 'add_review',
                'value' => '[:ru]Оставить отзыв',
            ],
            [
                'name' => 'product_code',
                'key' => 'product_code',
                'value' => '[:ru]Код товара',
            ],
            [
                'name' => 'detailed',
                'key' => 'detailed',
                'value' => '[:ru]Подробнее',
            ],
            [
                'name' => 'made_in_moldova',
                'key' => 'made_in_moldova',
                'value' => '[:ru]Производство жалюзи в Молдове',
            ],
            [
                'name' => 'our_news',
                'key' => 'our_news',
                'value' => '[:ru]Наши новости',
            ],
            [
                'name' => 'previous_news',
                'key' => 'previous_news',
                'value' => '[:ru]Предыдущие новости',
            ],
            [
                'name' => 'questions_answers',
                'key' => 'questions_answers',
                'value' => '[:ru]Вопросы и ответы',
            ],
            [
                'name' => 'zoom',
                'key' => 'zoom',
                'value' => '[:ru]Увеличить',
            ],
            [
                'name' => 'request_check',
                'key' => 'request_check',
                'value' => '[:ru]Заявка на замер',
            ],
            [
                'name' => 'request_check',
                'key' => 'request_check',
                'value' => '[:ru]Заявка на замер',
            ],
            [
                'name' => 'choose_color',
                'key' => 'choose_color',
                'value' => '[:ru]Выберите цвет',
            ],
            [
                'name' => 'attributes',
                'key' => 'attributes',
                'value' => '[:ru]Характеристики',
            ],
            [
                'name' => 'about',
                'key' => 'about',
                'value' => '[:ru]О товаре',
            ],
            [
                'name' => 'check_install',
                'key' => 'check_install',
                'value' => '[:ru]Замер и установка',
            ],
            [
                'name' => 'description',
                'key' => 'description',
                'value' => '[:ru]Описание продукта',
            ],
            [
                'name' => 'how_to_check_jaluzi',
                'key' => 'how_to_check_jaluzi',
                'value' => '[:ru]Как замерять окно для жалюзи?',
            ],
            [
                'name' => 'if_you_plan_to_install',
                'key' => 'if_you_plan_to_install',
                'value' => '[:ru]Если вы планируете произвести монтаж (установку) жалюзи самостоятельно, вам необходимо знать, как правильно снять размеры для предполагаемых жалюзи. Вначале стоит определиться с видом жалюзи и методом их установки.',
            ],
            [
                'name' => 'product_duration',
                'key' => 'product_duration',
                'value' => '[:ru]Срок изготовления',
            ],
            [
                'name' => 'type',
                'key' => 'type',
                'value' => '[:ru]Тип',
            ],
            [
                'name' => 'transparency',
                'key' => 'transparency',
                'value' => '[:ru]Светопроницаемость',
            ],
            [
                'name' => 'country_producer',
                'key' => 'country_producer',
                'value' => '[:ru]Страна производитель',
            ],
            [
                'name' => 'discount',
                'key' => 'discount',
                'value' => '[:ru]Скидка',
            ],
            [
                'name' => 'preview',
                'key' => 'preview',
                'value' => '[:ru]Быстрый просмотр',
            ],
            [
                'name' => 'show_by',
                'key' => 'show_by',
                'value' => '[:ru]Показывать по',
            ],
            [
                'name' => 'promo_text',
                'key' => 'promo_text',
                'value' => '[:ru]Вот вам яркий пример современных тенденций - курс на социально-ориентированный национальный проект позволяет выполнить важные задания по разработке укрепления моральных ценностей. Учитывая ключевые сценарии поведения, экономическая повестка сегодняшнего дня, в своем классическом представлении, допускает внедрение модели развития. Современные технологии достигли такого уровня, что сложившаяся структура организации в значительной степени обусловливает важность стандартных подходов курс на социально-ориентированный национальный проект позволяет.',
            ],
            [
                'name' => 'categories',
                'key' => 'categories',
                'value' => '[:ru]Категории',
            ],
            [
                'name' => 'promotion',
                'key' => 'promotion',
                'value' => '[:ru]Акция',
            ],
            [
                'name' => 'color',
                'key' => 'color',
                'value' => '[:ru]Цвет',
            ],
            [
                'name' => 'texture_type',
                'key' => 'texture_type',
                'value' => '[:ru]Тип текстуры',
            ],
            [
                'name' => 'texture_type',
                'key' => 'texture_type',
                'value' => '[:ru]Тип текстуры',
            ],
            [
                'name' => 'countries_producer',
                'key' => 'countries_producer',
                'value' => '[:ru]Страны производители',
            ],
            [
                'name' => 'tree_type',
                'key' => 'tree_type',
                'value' => '[:ru]Вид дерева',
            ],
            [
                'name' => 'price',
                'key' => 'price',
                'value' => '[:ru]Стоимость',
            ],
            [
                'name' => 'apply',
                'key' => 'apply',
                'value' => '[:ru]Применить',
            ],
            [
                'name' => 'reset',
                'key' => 'reset',
                'value' => '[:ru]Сбросить',
            ],
            [
                'name' => 'filters',
                'key' => 'filters',
                'value' => '[:ru]Фильтры',
            ],
            [
                'name' => 'reset_filters',
                'key' => 'reset_filters',
                'value' => '[:ru]Сбросить фильтры',
            ],
            [
                'name' => 'we_in_social',
                'key' => 'we_in_social',
                'value' => '[:ru]Мы в соцсетях',
            ],
            [
                'name' => 'request_master',
                'key' => 'request_master',
                'value' => '[:ru]Вызвать замерщика',
            ],
            [
                'name' => 'catalog',
                'key' => 'catalog',
                'value' => '[:ru]Каталог',
            ],
            [
                'name' => 'promotions',
                'key' => 'promotions',
                'value' => '[:ru]Акции',
            ],
            [
                'name' => 'contacts',
                'key' => 'contacts',
                'value' => '[:ru]Контакты',
            ],
            [
                'name' => 'message',
                'key' => 'message',
                'value' => '[:ru]Сообщение',
            ],
            [
                'name' => 'call_back',
                'key' => 'call_back',
                'value' => '[:ru]Обратный звонок',
            ],
            [
                'name' => 'contact_phone',
                'key' => 'contact_phone',
                'value' => '[:ru]Контактный телефон',
            ],
            [
                'name' => 'transparencies',
                'key' => 'transparencies',
                'value' => '[:ru]Светопроницаемость',
            ],
            [
                'name' => 'breadcrumb_home',
                'key' => 'breadcrumb_home',
                'value' => '[:ru]Главная',
            ],
            [
                'name' => 'breadcrumb_catalog',
                'key' => 'breadcrumb_catalog',
                'value' => '[:ru]Каталог',
            ],
        ];

        foreach ($translations as $translation) {
            DB::table('translations')->insert([
                'key' => $translation['key'],
                'name' => $translation['name'],
                'value' => $translation['value'],
            ]);
        }
    }
}
