<?php

use Illuminate\Database\Seeder;

class CustomMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = App\WebusModels\WebusMenu::where('slug', 'backend')->first();
        // categories
        $parent = DB::table('menu_items')->insertGetId([
            'menu_id' => $menu->id,
            'parent_id' => null,
            'name' => 'Custom menu',
            'url' => '/admin/custom-menu',
            'icon' => 'voyager-helm',
            'target' => '_self',
            'item_order' => '24',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('menu_items')->insert([
            'menu_id' => $menu->id,
            'parent_id' => $parent,
            'name' => 'All custom menu items',
            'url' => '/admin/custom-menu',
            'icon' => 'voyager-list',
            'target' => '_self',
            'item_order' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('menu_items')->insert([
            'menu_id' => $menu->id,
            'parent_id' => $parent,
            'name' => 'New custom menu item',
            'url' => '/admin/custom-menu/create',
            'icon' => 'voyager-plus',
            'target' => '_self',
            'item_order' => '2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
