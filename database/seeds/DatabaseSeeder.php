<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('PermissionsTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('ConnectRelationshipsSeeder');
        $this->call('UsersTableSeeder');
        $this->call('WebusAdminTableSeeder');
        $this->call('FrontMenu');
        $this->call('HomePage');
        $this->call('TranslationsSeeder');
        $this->call('VariablesTestsSeeder');
        $this->call('GalleriesSeeder');
        $this->call('ClientSeeder');
        $this->call('ProductsSeeder');
        $this->call('ComfortsSeeder');
        $this->call('ContactsSeeder');
        $this->call('FAQ');
        $this->call('ReviewsSeeder');
        $this->call('SelectedProducts');
        $this->call('TranslationTextSeeder');
        $this->call('CustomMenuSeeder');
        $this->call('BurgerMenu');
        $this->call('OrderSteps');
        $this->call('StaticPages');
        $this->call('AdditionalTranslations');

        Model::reguard();
    }
}
