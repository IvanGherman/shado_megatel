<?php

use Illuminate\Database\Seeder;

class StaticPages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'slug' => 'contacts',
            'name' => 'Contacts',
            'template' => 'default',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'post_status' => 1,
        ]);

        DB::table('pages')->insert([
            'slug' => 'news',
            'name' => 'News',
            'template' => 'default',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'post_status' => 1,
        ]);

        DB::table('pages')->insert([
            'slug' => 'faq',
            'name' => 'FAQ',
            'template' => 'default',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'post_status' => 1,
        ]);
    }
}
