<?php

use Illuminate\Database\Seeder;

class VariablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = App\WebusModels\WebusMenu::where('slug', 'backend')->first();

        $parent = DB::table('menu_items')->insertGetId([
            'menu_id' => $menu->id,
            'parent_id' => null,
            'name' => 'Variables',
            'url' => '/admin/variables',
            'icon' => 'voyager-terminal',
            'target' => '_self',
            'item_order' => '11',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
