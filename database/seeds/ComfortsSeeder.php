<?php

use Illuminate\Database\Seeder;

class ComfortsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = App\WebusModels\WebusMenu::where('slug', 'backend')->first();
        // categories
        $parent = DB::table('menu_items')->insertGetId([
            'menu_id' => $menu->id,
            'parent_id' => null,
            'name' => 'Comforts',
            'url' => '/admin/comforts',
            'icon' => 'voyager-paperclip',
            'target' => '_self',
            'item_order' => '21',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('menu_items')->insert([
            'menu_id' => $menu->id,
            'parent_id' => $parent,
            'name' => 'All comforts',
            'url' => '/admin/comforts',
            'icon' => 'voyager-list',
            'target' => '_self',
            'item_order' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('menu_items')->insert([
            'menu_id' => $menu->id,
            'parent_id' => $parent,
            'name' => 'New comfort',
            'url' => '/admin/comfort/create',
            'icon' => 'voyager-plus',
            'target' => '_self',
            'item_order' => '2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
