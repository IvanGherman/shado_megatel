<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TextureTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('texture_types', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name')->nullable(false);
            $table->string('title')->nullable(false);
            $table->string('icon')->nullable(true);
            $table->integer('post_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
