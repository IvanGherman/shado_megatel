<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MetaBoxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_boxes', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->integer('post_id');
            $table->string('post_type')->nullable();
            $table->index('post_id', 'post_id_index');
            $table->string('meta_key')->nullable();
            $table->longtext('meta_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_boxes');
    }
}
